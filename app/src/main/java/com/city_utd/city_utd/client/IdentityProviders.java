package com.city_utd.city_utd.client;

/**
 * Created by Admin on 05.07.2017.
 */

public final class IdentityProviders {

    public static final String VK = "Vk";
    public static final String FACEBOOK = "Facebook";
    public static final String OK = "Odnoklassniki";
    public static final String TWITTER = "Twitter";

    private IdentityProviders(){}
}
