package com.city_utd.city_utd.client;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Admin on 05.07.2017.
 */

public class NearestQueryParamsHolder extends PagingQueryParamsHolder {

    private static final String PARAM_NAME_LATITUDE = "latitude";
    private static final String PARAM_NAME_LONGITUDE = "longitude";
    private static final String PARAM_NAME_RADIUS = "radius";

    private static final int DEFAULT_PAGE_SIZE = 1000;

    public NearestQueryParamsHolder(int pageSize, double latitude, double longitude){
        super(pageSize);
        setLatitude(latitude);
        setLongitude(longitude);
    }

    public NearestQueryParamsHolder(int pageSize, LatLng latLng){
        this(pageSize, latLng.latitude, latLng.longitude);
    }

    public NearestQueryParamsHolder(double latitude, double longitude){
        this(DEFAULT_PAGE_SIZE, latitude, longitude);
    }

    public NearestQueryParamsHolder(LatLng latLng){
        this(DEFAULT_PAGE_SIZE, latLng);
    }

    public void setLatitude(double latitude){
        putParam(PARAM_NAME_LATITUDE, latitude);
    }

    public double getLatitude(){
        return getParam(PARAM_NAME_LATITUDE, -1.);
    }

    public void setLongitude(double longitude){
        putParam(PARAM_NAME_LONGITUDE, longitude);
    }

    public double getLongitude(){
        return getParam(PARAM_NAME_LONGITUDE, -1.);
    }

    public void setCoordinates(LatLng latLng){
        setLatitude(latLng.latitude);
        setLongitude(latLng.longitude);
    }

    public void setRadius(int radius){
        putParam(PARAM_NAME_RADIUS, radius);
    }

    public int getRadius(){
        return getParam(PARAM_NAME_RADIUS, 0);
    }
}
