package com.city_utd.city_utd.client;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by Admin on 06.07.2017.
 */

class LatLngTypeAdapter extends TypeAdapter<LatLng> {

    private static final String LATITUDE = "latitude";
    private static final String LONGITUDE = "longitude";

    @Override
    public void write(JsonWriter out, LatLng value) throws IOException {
        if(value == null){
            out.nullValue();
            return;
        }

        final double latitude = value.latitude;
        final double longitude = value.longitude;

        out.beginObject();
        out.name(LATITUDE).value(latitude).name(LONGITUDE).value(longitude);
        out.endObject();
    }

    @Override
    public LatLng read(JsonReader in) throws IOException {
        if(in.peek() == JsonToken.NULL){
            in.nextNull();
            return null;
        }

        double latitude = 0, longitude = 0;
        in.beginObject();
        while (in.hasNext()){
            String name = in.nextName();
            switch (name){
                case LATITUDE:
                    latitude = in.nextDouble();
                    break;
                case LONGITUDE:
                    longitude = in.nextDouble();
                    break;
            }
        }
        in.endObject();
        return new LatLng(latitude, longitude);
    }
}
