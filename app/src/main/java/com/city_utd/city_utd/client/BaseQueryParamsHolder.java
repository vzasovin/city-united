package com.city_utd.city_utd.client;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 05.07.2017.
 */

class BaseQueryParamsHolder implements QueryParamsHolder {

    private final Map<String, Object> queryParams;

    BaseQueryParamsHolder(){
        queryParams = new HashMap<>();
    }

    final void putParam(String key, Object value){
        queryParams.put(key, value);
    }

    @SuppressWarnings("unchecked")
    final <T> T getParam(String key, T defaultValue){
        Object value = queryParams.get(key);
        if(value != null){
            return (T) value;
        }else {
            return defaultValue;
        }
    }

    @Override
    public final Map<String, Object> getQueryParams() {
        return queryParams;
    }
}
