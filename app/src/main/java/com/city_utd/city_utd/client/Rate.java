package com.city_utd.city_utd.client;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

/**
 * Created by Admin on 06.07.2017.
 */

public class Rate implements Parcelable{

    public static final String FIELD_SPEED = "speed";
    public static final String FIELD_STAMINA = "stamina";
    public static final String FIELD_TECHNIQUE = "technique";
    public static final String FIELD_IMPACT_FORCE = "impact_force";
    public static final String FIELD_PASS = "pass";
    public static final String FIELD_DEFENSE = "defense";
    public static final String FIELD_FRIENDLINESS = "friendliness";
    public static final String FIELD_REACTION = "reaction";
    public static final String FIELD_RELIABILITY = "reliability";
    public static final String FIELD_HANDS = "hands";
    public static final String FIELD_FEET = "feet";
    public static final String FIELD_JUMP = "jump";
    public static final String FIELD_COURAGE = "courage";

    @SerializedName("speed")
    @DatabaseField(columnName = FIELD_SPEED, dataType = DataType.INTEGER_OBJ)
    private Integer speed;

    @SerializedName("stamina")
    @DatabaseField(columnName = FIELD_STAMINA, dataType = DataType.INTEGER_OBJ)
    private Integer stamina;

    @SerializedName("technique")
    @DatabaseField(columnName = FIELD_TECHNIQUE, dataType = DataType.INTEGER_OBJ)
    private Integer technique;

    @SerializedName("impactForce")
    @DatabaseField(columnName = FIELD_IMPACT_FORCE, dataType = DataType.INTEGER_OBJ)
    private Integer impactForce;

    @SerializedName("pass")
    @DatabaseField(columnName = FIELD_PASS, dataType = DataType.INTEGER_OBJ)
    private Integer pass;

    @SerializedName("defense")
    @DatabaseField(columnName = FIELD_DEFENSE, dataType = DataType.INTEGER_OBJ)
    private Integer defense;

    @SerializedName("friendliness")
    @DatabaseField(columnName = FIELD_FRIENDLINESS, dataType = DataType.INTEGER_OBJ)
    private Integer friendliness;

    @SerializedName("reaction")
    @DatabaseField(columnName = FIELD_REACTION, dataType = DataType.INTEGER_OBJ)
    private Integer reaction;

    @SerializedName("reliability")
    @DatabaseField(columnName = FIELD_RELIABILITY, dataType = DataType.INTEGER_OBJ)
    private Integer reliability;

    @SerializedName("hands")
    @DatabaseField(columnName = FIELD_HANDS, dataType = DataType.INTEGER_OBJ)
    private Integer hands;

    @SerializedName("feet")
    @DatabaseField(columnName = FIELD_FEET, dataType = DataType.INTEGER_OBJ)
    private Integer feet;

    @SerializedName("jump")
    @DatabaseField(columnName = FIELD_JUMP, dataType = DataType.INTEGER_OBJ)
    private Integer jump;

    @SerializedName("courage")
    @DatabaseField(columnName = FIELD_COURAGE, dataType = DataType.INTEGER_OBJ)
    private Integer courage;

    protected Rate(Parcel in) {
        speed = readInteger(in);
        stamina = readInteger(in);
        technique = readInteger(in);
        impactForce = readInteger(in);
        pass = readInteger(in);
        defense = readInteger(in);
        friendliness = readInteger(in);
        reaction = readInteger(in);
        reliability = readInteger(in);
        hands = readInteger(in);
        feet = readInteger(in);
        jump = readInteger(in);
        courage = readInteger(in);
    }

    private Integer readInteger(Parcel in){
        return (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public Rate(){}

    public int getSpeed() {
        return speed != null ? speed : 0;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public int getStamina() {
        return stamina != null ? stamina : 0;
    }

    public void setStamina(int stamina) {
        this.stamina = stamina;
    }

    public int getTechnique() {
        return technique != null ? technique : 0;
    }

    public void setTechnique(int technique) {
        this.technique = technique;
    }

    public int getImpactForce() {
        return impactForce != null ? impactForce : 0;
    }

    public void setImpactForce(int impactForce) {
        this.impactForce = impactForce;
    }

    public int getPass() {
        return pass != null ? pass : 0;
    }

    public void setPass(int pass) {
        this.pass = pass;
    }

    public int getDefense() {
        return defense != null ? defense : 0;
    }

    public void setDefense(int defense) {
        this.defense = defense;
    }

    public int getFriendliness() {
        return friendliness != null ? friendliness : 0;
    }

    public void setFriendliness(int friendliness) {
        this.friendliness = friendliness;
    }

    public int getReaction() {
        return reaction != null ? reaction : 0;
    }

    public void setReaction(int reaction) {
        this.reaction = reaction;
    }

    public int getReliability() {
        return reliability != null ? reliability : 0;
    }

    public void setReliability(int reliability) {
        this.reliability = reliability;
    }

    public int getHands() {
        return hands != null ? hands : 0;
    }

    public void setHands(int hands) {
        this.hands = hands;
    }

    public int getFeet() {
        return feet != null ? feet : 0;
    }

    public void setFeet(int feet) {
        this.feet = feet;
    }

    public int getJump() {
        return jump != null ? jump : 0;
    }

    public void setJump(int jump) {
        this.jump = jump;
    }

    public int getCourage() {
        return courage != null ? courage : 0;
    }

    public void setCourage(int courage) {
        this.courage = courage;
    }

    public static final Creator<Rate> CREATOR = new Creator<Rate>() {
        @Override
        public Rate createFromParcel(Parcel in) {
            return new Rate(in);
        }

        @Override
        public Rate[] newArray(int size) {
            return new Rate[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(speed);
        dest.writeValue(stamina);
        dest.writeValue(technique);
        dest.writeValue(impactForce);
        dest.writeValue(pass);
        dest.writeValue(defense);
        dest.writeValue(friendliness);
        dest.writeValue(reaction);
        dest.writeValue(reliability);
        dest.writeValue(hands);
        dest.writeValue(feet);
        dest.writeValue(jump);
        dest.writeValue(courage);
    }
}
