package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 23.07.2017.
 */

class UpdateClientTokenRequestBody {

    @SerializedName("oldToken")
    private final String oldToken;

    @SerializedName("newToken")
    private final String newToken;

    UpdateClientTokenRequestBody(String oldToken, String newToken){
        this.oldToken = oldToken;
        this.newToken = newToken;
    }
}
