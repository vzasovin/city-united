package com.city_utd.city_utd.client;

import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;

import java.util.Date;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Created by Admin on 05.07.2017.
 */

interface PlayingFields {

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY)
    @GET("/fields")
    Call<List<PlayingField>> all(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_OPTIONAL)
    @POST("/fields")
    Call<PlayingField> create(@Body MultipartBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY)
    @GET("/fields/nearest")
    Call<List<PlayingField>> nearest(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_OPTIONAL)
    @GET("/fields/{id}")
    Call<PlayingFieldDetailed> id(@Path("id") int id, @Query("schedule_max_date") String maxDateString);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/fields/{id}")
    Call<PlayingField> update(@Path("id") int id, @Body MultipartBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/fields/{id}/subscribe")
    Call<PlayingFieldDetailed> subscribe(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/fields/{id}/unsubscribe")
    Call<PlayingFieldDetailed> unsubscribe(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/fields/{id}/schedule")
    Call<String> addRecord(@Path("id") int id, @Body AddRecordRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @PATCH("/fields/{id}/schedule")
    Call<String> updateRecord(@Path("id") int id, @Body UpdateRecordRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @DELETE("/fields/{id}/schedule")
    Call<String> deleteRecord(@Path("id") int id, @Query("date") Date date);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_OPTIONAL)
    @GET("/fields/{id}/schedule")
    Call<List<ScheduleItem>> schedule(@Path("id") int id, @Query("min_date") String minDateString,
                                      @Query("max_date") String maxDateString,
                                      @Query("exclude_self") boolean excludeSelf);
}
