package com.city_utd.city_utd.client;

import com.city_utd.city_utd.model.PlayerLines;
import com.city_utd.city_utd.util.Util;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 06.07.2017.
 */

public final class ProfileData implements QueryParamsHolder {

    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_LINE = "line";
    private static final String PARAM_NAME_DATE_OF_BIRTH = "dateOfBirth";
    private static final String PARAM_NAME_HEIGHT = "height";
    private static final String PARAM_NAME_WEIGHT = "weight";
    private static final String PARAM_NAME_PHONE = "phone";
    private static final String PARAM_NAME_EMAIL = "email";

    private String name;
    private String line;
    private Date dateOfBirth;
    private Integer height;
    private Integer weight;
    private String phone;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        if(line != null && !PlayerLines.validate(line)){
            throw new IllegalArgumentException("Unsupported line: " + line);
        }
        this.line = line;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Map<String, Object> getQueryParams() {
        Map<String, Object> map = new HashMap<>();

        if(name != null){
            map.put(PARAM_NAME_NAME, name);
        }

        if(line != null){
            map.put(PARAM_NAME_LINE, line);
        }

        if(dateOfBirth != null){
            String date = Util.Dates.dateToServerString(dateOfBirth);
            map.put(PARAM_NAME_DATE_OF_BIRTH, date);
        }

        int height = this.height != null ? this.height : 0;
        int weight = this.weight != null ? this.weight : 0;
        map.put(PARAM_NAME_HEIGHT, height);
        map.put(PARAM_NAME_WEIGHT, weight);

        if(phone != null){
            map.put(PARAM_NAME_PHONE, phone);
        }

        if(email != null){
            map.put(PARAM_NAME_EMAIL, email);
        }

        return map;
    }
}
