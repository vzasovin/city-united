package com.city_utd.city_utd.client;

import com.city_utd.city_utd.model.Invitation;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.model.UserDetailed;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.QueryMap;

/**
 * Created by Admin on 05.07.2017.
 */

interface Users {

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY)
    @GET("/users")
    Call<List<User>> all(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_OPTIONAL)
    @GET("/users/nearest")
    Call<List<User>> nearest(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY)
    @GET("/users/best_player")
    Call<User> bestPlayer();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me")
    Call<UserDetailed> me();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/subscriptions")
    Call<List<User>> subscriptions(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/playing_fields")
    Call<List<PlayingField>> playingFields(@QueryMap Map<String, Object> map);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/me")
    Call<UserDetailed> update(@Body MultipartBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @PATCH("/users/me/position")
    Call<UserDetailed> updatePosition(@Body LatLng coordinates);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/stated_rates")
    Call<List<RateInfo>> statedRates();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/obtained_rates")
    Call<List<RateInfo>> obtainedRates();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/me/gallery")
    Call<String> addToGallery(@Body MultipartBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/me/gallery/delete")
    Call<String> removeFromGallery(@Body RemoveFromGalleryRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/gallery")
    Call<List<String>> myGallery();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/incoming_invitations")
    Call<List<Invitation>> incomingInvitations();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/outgoing_invitations")
    Call<List<Invitation>> outgoingInvitations();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/me/notifications_settings")
    Call<String> updateNotificationsSettings(@Body List<NotificationSetting> settings);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/me/notifications_settings")
    Call<List<NotificationSetting>> notificationsSettings();

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_OPTIONAL)
    @GET("/users/{id}")
    Call<UserDetailed> id(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/{id}/subscribe")
    Call<UserDetailed> subscribe(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/{id}/unsubscribe")
    Call<UserDetailed> unsubscribe(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/{id}/rate")
    Call<String> rate(@Path("id") int id, @Body Rate rate);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @DELETE("/users/{id}/rate")
    Call<String> deleteRate(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @GET("/users/{id}/rate")
    Call<RateInfo> statedRate(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY)
    @GET("/users/{id}/gallery")
    Call<List<String>> gallery(@Path("id") int id);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/{id}/invite")
    Call<String> invite(@Path("id") int id, @Body InvitationRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/reject_invitation")
    Call<String> rejectInvitation(@Body RejectInvitationRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/users/invite")
    Call<String> invite(@Body MultipleInvitationRequestBody body);
}
