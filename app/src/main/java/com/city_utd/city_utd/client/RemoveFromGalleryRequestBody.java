package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Admin on 06.07.2017.
 */

class RemoveFromGalleryRequestBody {

    @SerializedName("names")
    private final List<String> names;

    RemoveFromGalleryRequestBody(List<String> names){
        this.names = names;
    }
}
