package com.city_utd.city_utd.client;

import android.text.TextUtils;

/**
 * Created by Admin on 14.07.2017.
 */

public class ImageUrlBuilder {

    private final StringBuilder stringBuilder;
    private final int baseUrlLength;

    ImageUrlBuilder(String baseUrl){
        stringBuilder = new StringBuilder(baseUrl);
        baseUrlLength = baseUrl.length();
    }

    public String build(String path){
        if(TextUtils.isEmpty(path)){
            return null;
        }

        if(path.startsWith("http")){
            return path;
        }

        int length = stringBuilder.length();
        stringBuilder.delete(baseUrlLength, length);
        return stringBuilder.append(path).toString();
    }
}
