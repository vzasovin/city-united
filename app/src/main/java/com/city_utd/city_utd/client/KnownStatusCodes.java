package com.city_utd.city_utd.client;

/**
 * Created by Admin on 10.07.2017.
 */

public final class KnownStatusCodes {

    public static final int OK = 200;
    public static final int BAD_REQUEST = 400;
    public static final int UNAUTHORIZED = 401;
    public static final int NOT_FOUND = 404;
    public static final int INTERNAL_SERVER_ERROR = 500;
    public static final int ALREADY_IN_SCHEDULE = 417;


    private KnownStatusCodes(){}
}
