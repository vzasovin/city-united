package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 07.07.2017.
 */

class MultipleInvitationRequestBody extends InvitationRequestBody {

    @SerializedName("usersIds")
    private final int[] usersIds;

    MultipleInvitationRequestBody(int[] usersIds, int playingFieldId, Date date, String message) {
        super(playingFieldId, date, message);
        this.usersIds = usersIds;
    }
}
