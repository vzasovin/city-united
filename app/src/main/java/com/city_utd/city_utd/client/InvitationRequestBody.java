package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 07.07.2017.
 */

class InvitationRequestBody {

    @SerializedName("playingFieldId")
    private final int playingFieldId;

    @SerializedName("invitationDate")
    private final Date date;

    @SerializedName("message")
    private final String message;

    InvitationRequestBody(int playingFieldId, Date date, String message) {
        this.playingFieldId = playingFieldId;
        this.date = date;
        this.message = message;
    }
}
