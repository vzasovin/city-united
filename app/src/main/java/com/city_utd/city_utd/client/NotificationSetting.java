package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 06.07.2017.
 */

public final class NotificationSetting {

    @SerializedName("type")
    private String type;

    @SerializedName("enabled")
    private boolean enabled;

    public NotificationSetting(String type, boolean enabled){
        this.type = type;
        this.enabled = enabled;
    }

    public String getType() {
        return type;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
