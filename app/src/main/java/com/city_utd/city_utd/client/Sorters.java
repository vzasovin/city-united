package com.city_utd.city_utd.client;

/**
 * Created by Admin on 05.07.2017.
 */

public final class Sorters {

    public static final String ASC = "asc";
    public static final String DESC = "desc";

    private Sorters(){}

    public static boolean validate(String sorter){
        return ASC.equalsIgnoreCase(sorter) || DESC.equalsIgnoreCase(sorter);
    }
}
