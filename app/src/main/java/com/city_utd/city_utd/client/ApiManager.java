package com.city_utd.city_utd.client;

import android.content.Context;
import android.support.annotation.NonNull;
import android.text.TextUtils;

import com.bumptech.glide.Glide;
import com.bumptech.glide.integration.okhttp3.OkHttpUrlLoader;
import com.bumptech.glide.load.model.GlideUrl;
import com.city_utd.city_utd.BuildConfig;
import com.city_utd.city_utd.model.Invitation;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.util.Util;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.imogene.android.carcase.client.ApiCore;
import com.imogene.android.carcase.client.BaseApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.io.File;
import java.io.InputStream;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import retrofit2.Call;

/**
 * Created by Admin on 05.07.2017.
 */

public class ApiManager extends BaseApiManager {

    private static final String BASE_URL_TEST = "http://192.168.1.36:3000";
    private static final String BASE_URL_PRODUCTION = "http://207.154.234.130:8080";

    private static final String BASE_URL;

    static {
        if(!BuildConfig.DEBUG){
            BASE_URL = BASE_URL_TEST;
        }else {
            BASE_URL = BASE_URL_PRODUCTION;
        }
    }

    private static final long READ_TIMEOUT_SECONDS = 10;
    private static final long WRITE_TIMEOUT_SECONDS = 15;

    private volatile Auth auth;
    private volatile Users users;
    private volatile PlayingFields fields;

    public static ApiManager getInstance(){
        return ApiManagerInstanceHolder.INSTANCE;
    }

    private static final class ApiManagerInstanceHolder{

        private static final ApiManager INSTANCE = new ApiManager();
    }

    public void setupGlide(Context context){
        OkHttpClient client = getClient();
        OkHttpUrlLoader.Factory factory = new OkHttpUrlLoader.Factory(client);
        Glide glide = Glide.get(context);
        glide.register(GlideUrl.class, InputStream.class, factory);
    }

    public static ImageUrlBuilder newImageUrlBuilder(){
        return new ImageUrlBuilder(BASE_URL);
    }

    public static Gson getGsonInstance(){
        return GsonInstanceHolder.INSTANCE;
    }

    private static final class GsonInstanceHolder{

        private static final Gson INSTANCE;

        static {
            INSTANCE = new GsonBuilder()
                    .registerTypeAdapter(Date.class, new DateTypeAdapter())
                    .registerTypeAdapter(LatLng.class, new LatLngTypeAdapter())
                    .registerTypeAdapter(UserDetailed.class, new UserDetailed.Adapter())
                    .registerTypeAdapter(PlayingFieldDetailed.class, new PlayingFieldDetailed.Adapter())
                    .create();
        }
    }

    @NonNull
    @Override
    protected ApiCore createApiCore() {
        ApiCore.Builder builder = new ApiCore.Builder()
                .baseUrl(BASE_URL)
                .readTimeout(READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT_SECONDS, TimeUnit.SECONDS)
                .addScalarsConverterFactory()
                .addGsonConverterFactory(getGsonInstance())
                .addInterceptor(new AuthorizationHeaderSetter());

        if(BuildConfig.DEBUG){
            builder.enableLogging();
        }

        return builder.build();
    }

    private Auth auth(){
        Auth localRef = auth;
        if(localRef == null){
            synchronized (Auth.class){
                localRef = auth;
                if(localRef == null){
                    localRef = auth = createApi(Auth.class);
                }
            }
        }
        return localRef;
    }

    private Users users(){
        Users localRef = users;
        if(localRef == null){
            synchronized (Users.class){
                localRef = users;
                if(localRef == null){
                    localRef = users = createApi(Users.class);
                }
            }
        }
        return localRef;
    }

    private PlayingFields fields(){
        PlayingFields localRef = fields;
        if(localRef == null){
            synchronized (PlayingFields.class){
                localRef = fields;
                if(localRef == null){
                    localRef = fields = createApi(PlayingFields.class);
                }
            }
        }
        return localRef;
    }

    public AuthorizationResponse authorize(String identityProvider, String accessToken) throws RequestException{
        if(TextUtils.isEmpty(identityProvider)){
            throw new IllegalArgumentException("The specified identity provider is an empty string");
        }
        if(TextUtils.isEmpty(accessToken)){
            throw new IllegalArgumentException("The specified access token is an empty string");
        }

        Call<AuthorizationResponse> call;
        switch (identityProvider){
            case IdentityProviders.VK:
                call = auth().vk(accessToken);
                break;
            case IdentityProviders.FACEBOOK:
                call = auth().facebook(accessToken);
                break;
            case IdentityProviders.OK:
                call = auth().ok(accessToken);
                break;
            case IdentityProviders.TWITTER:
                throw new UnsupportedOperationException("There is special method for authorization via Twitter");
            default:
                throw new IllegalArgumentException("Unsupported identity provider: " + identityProvider);
        }

        return executeCall(call);
    }

    public AuthorizationResponse authorizeViaTwitter(long userId, String userName) throws RequestException{
        Call<AuthorizationResponse> call = auth().twitter(userId, userName);
        return executeCall(call);
    }

    public AuthorizationResponse authorizeViaPhoneNumber(String phoneNumber) throws RequestException{
        Call<AuthorizationResponse> call = auth().phoneNumber(phoneNumber);
        return executeCall(call);
    }

    public boolean logout(String clientToken) throws RequestException{
        LogoutRequestBody body = new LogoutRequestBody(clientToken);
        Call<String> call = auth().logout(body);
        return executeCall(call) != null;
    }

    public boolean enableClient(String token) throws RequestException{
        ClientTokenRequestBody body = new ClientTokenRequestBody(token);
        Call<String> call = auth().enableClient(body);
        return executeCall(call) != null;
    }

    public boolean disableClient(String token) throws RequestException{
        ClientTokenRequestBody body = new ClientTokenRequestBody(token);
        Call<String> call = auth().disableClient(body);
        return executeCall(call) != null;
    }

    public boolean updateClientToken(String oldToken, String newToken) throws RequestException{
        UpdateClientTokenRequestBody body = new UpdateClientTokenRequestBody(oldToken, newToken);
        Call<String> call = auth().updateClientToken(body);
        return executeCall(call) != null;
    }

    public List<User> getAllUsers(UsersQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<User>> call = users().all(params);
        return executeCall(call);
    }

    public List<User> getNearestUsers(double latitude, double longitude, int maxCount, int radius) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(maxCount, latitude, longitude);
        paramsHolder.setPage(1);
        paramsHolder.setRadius(radius);
        return getNearestUsers(paramsHolder);
    }

    public List<User> getNearestUsers(double latitude, double longitude) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(latitude, longitude);
        paramsHolder.setPage(1);
        return getNearestUsers(paramsHolder);
    }

    public List<User> getNearestUsers(LatLng latLng, int maxCount, int radius) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(maxCount, latLng);
        paramsHolder.setPage(1);
        paramsHolder.setRadius(radius);
        return getNearestUsers(paramsHolder);
    }

    public List<User> getNearestUsers(LatLng latLng) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(latLng);
        return getNearestUsers(paramsHolder);
    }

    public List<User> getNearestUsers(NearestQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<User>> call = users().nearest(params);
        return executeCall(call);
    }

    public User getBestPlayer() throws RequestException{
        Call<User> call = users().bestPlayer();
        return executeCall(call);
    }

    public UserDetailed getMyProfile() throws RequestException{
        Call<UserDetailed> call = users().me();
        return executeCall(call);
    }

    public List<User> getSubscriptions(PagingQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<User>> call = users().subscriptions(params);
        return executeCall(call);
    }

    public List<PlayingField> getPlayingFieldsSubscriptions(PagingQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<PlayingField>> call = users().playingFields(params);
        return executeCall(call);
    }

    public UserDetailed updateProfile(ProfileData data) throws RequestException{
        return updateProfile(data, null, false);
    }

    public UserDetailed updateProfile(ProfileData data, File avatar) throws RequestException{
        return updateProfile(data, avatar, false);
    }

    public UserDetailed updateProfile(ProfileData data, String avatarPath) throws RequestException{
        File file = null;
        if(!TextUtils.isEmpty(avatarPath)){
            file = new File(avatarPath);
        }
        return updateProfile(data, file);
    }

    public UserDetailed updateProfile(ProfileData data, boolean clearAvatar) throws RequestException{
        return updateProfile(data, null, clearAvatar);
    }

    public UserDetailed updateAvatar(File avatar) throws RequestException{
        return updateProfile(null, avatar);
    }

    public UserDetailed updateAvatar(String avatarPath) throws RequestException{
        return updateAvatar(new File(avatarPath));
    }

    public UserDetailed clearAvatar() throws RequestException{
        return updateProfile(null, null, true);
    }

    private UserDetailed updateProfile(ProfileData data, File avatar, boolean clearAvatar) throws RequestException{
        MultipartBody body = createMultipartBody(data, avatar, "avatar", clearAvatar, "clearAvatar");
        Call<UserDetailed> call = users().update(body);
        return executeCall(call);
    }

    private MultipartBody createMultipartBody(QueryParamsHolder paramsHolder,
                                              File image, String imageKey,
                                              boolean clearImage, String clearImageKey){
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        if(paramsHolder != null){
            Map<String, Object> params = paramsHolder.getQueryParams();
            if(params != null && !params.isEmpty()){
                for(Map.Entry<String, Object> entry : params.entrySet()){
                    String name = entry.getKey();
                    String value = entry.getValue().toString();
                    builder.addFormDataPart(name, value);
                }
            }
        }

        clearImage = clearImage && image == null;
        if(!clearImage && image != null && !TextUtils.isEmpty(imageKey)){
            MultipartBody.Part part = createPart(image, imageKey);
            builder.addPart(part);
        }else if(clearImage && !TextUtils.isEmpty(clearImageKey)){
            builder.addFormDataPart(clearImageKey, Boolean.TRUE.toString());
        }

        return builder.build();
    }

    private MultipartBody.Part createPart(File file, String key){
        String mimeType = Util.Commons.extractMimeType(file);
        MediaType mediaType = MediaType.parse(mimeType);
        RequestBody requestBody = RequestBody.create(mediaType, file);
        String fileName = file.getName();
        return MultipartBody.Part.createFormData(key, fileName, requestBody);
    }

    public UserDetailed updatePosition(LatLng coordinates) throws RequestException{
        Call<UserDetailed> call = users().updatePosition(coordinates);
        return executeCall(call);
    }

    public List<RateInfo> getStatedRates() throws RequestException{
        Call<List<RateInfo>> call = users().statedRates();
        return executeCall(call);
    }

    public List<RateInfo> getObtainedRates() throws RequestException{
        Call<List<RateInfo>> call = users().obtainedRates();
        return executeCall(call);
    }

    public boolean addToGallery(List<File> files) throws RequestException{
        MultipartBody body = createGalleryMultipartBody(files);
        Call<String> call = users().addToGallery(body);
        return executeCall(call) != null;
    }

    public boolean addToGallery(File file) throws RequestException{
        return addToGallery(Collections.singletonList(file));
    }

    private MultipartBody createGalleryMultipartBody(List<File> files){
        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        for(File file : files){
            MultipartBody.Part part = createPart(file, "gallery");
            builder.addPart(part);
        }

        return builder.build();
    }

    public boolean removeFromGallery(List<String> names) throws RequestException{
        RemoveFromGalleryRequestBody body = new RemoveFromGalleryRequestBody(names);
        Call<String> call = users().removeFromGallery(body);
        return executeCall(call) != null;
    }

    public List<String> getMyGallery() throws RequestException{
        Call<List<String>> call = users().myGallery();
        return executeCall(call);
    }

    public List<Invitation> getIncomingInvitations() throws RequestException{
        Call<List<Invitation>> call = users().incomingInvitations();
        return executeCall(call);
    }

    public List<Invitation> getOutgoingInvitations() throws RequestException{
        Call<List<Invitation>> call = users().outgoingInvitations();
        return executeCall(call);
    }

    public boolean updateNotificationsSettings(List<NotificationSetting> settings) throws RequestException{
        Call<String> call = users().updateNotificationsSettings(settings);
        return executeCall(call) != null;
    }

    public List<NotificationSetting> getNotificationSettings() throws RequestException {
        Call<List<NotificationSetting>> call = users().notificationsSettings();
        return executeCall(call);
    }

    public UserDetailed getUser(int id) throws RequestException{
        Call<UserDetailed> call = users().id(id);
        return executeCall(call);
    }

    public UserDetailed subscribeOnUser(int id) throws RequestException{
        Call<UserDetailed> call = users().subscribe(id);
        return executeCall(call);
    }

    public UserDetailed unsubscribeFromUser(int id) throws RequestException{
        Call<UserDetailed> call = users().unsubscribe(id);
        return executeCall(call);
    }

    public boolean rateUser(int id, Rate rate) throws RequestException{
        Call<String> call = users().rate(id, rate);
        return executeCall(call) != null;
    }

    public boolean deleteUserRate(int id) throws RequestException{
        Call<String> call = users().deleteRate(id);
        return executeCall(call) != null;
    }

    public RateInfo getStatedRate(int id) throws RequestException{
        Call<RateInfo> call = users().statedRate(id);
        return executeCall(call);
    }

    public List<String> getGallery(int id) throws RequestException{
        Call<List<String>> call = users().gallery(id);
        return executeCall(call);
    }

    public boolean invite(int userId, int playingFieldId, Date date, String message) throws RequestException{
        InvitationRequestBody body = new InvitationRequestBody(playingFieldId, date, message);
        Call<String> call = users().invite(userId, body);
        return executeCall(call) != null;
    }

    public boolean rejectInvitation(int userId, int playingFieldId, Date invitationDate) throws RequestException{
        RejectInvitationRequestBody body = new RejectInvitationRequestBody(userId, playingFieldId, invitationDate);
        Call<String> call = users().rejectInvitation(body);
        return executeCall(call) != null;
    }

    public boolean invite(int[] usersIds, int playingFieldId, Date date, String message) throws RequestException{
        MultipleInvitationRequestBody body = new MultipleInvitationRequestBody(usersIds, playingFieldId, date, message);
        Call<String> call = users().invite(body);
        return executeCall(call) != null;
    }

    public List<PlayingField> getAllPlayingFields(PagingQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<PlayingField>> call = fields().all(params);
        return executeCall(call);
    }

    public PlayingField createPlayingField(PlayingFieldData data, File image) throws RequestException{
        MultipartBody body = createMultipartBody(data, image, "image", false, null);
        Call<PlayingField> call = fields().create(body);
        return executeCall(call);
    }

    public PlayingField createPlayingField(PlayingFieldData data, String imagePath) throws RequestException{
        File file = new File(imagePath);
        return createPlayingField(data, file);
    }

    public PlayingField createPlayingField(PlayingFieldData data) throws RequestException{
        return createPlayingField(data, (File) null);
    }

    public List<PlayingField> getNearestFields(double latitude, double longitude, int maxCount, int radius) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(maxCount, latitude, longitude);
        paramsHolder.setPage(1);
        paramsHolder.setRadius(radius);
        return getNearestFields(paramsHolder);
    }

    public List<PlayingField> getNearestFields(double latitude, double longitude) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(latitude, longitude);
        paramsHolder.setPage(1);
        return getNearestFields(paramsHolder);
    }

    public List<PlayingField> getNearestFields(LatLng latLng, int maxCount, int radius) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(maxCount, latLng);
        paramsHolder.setPage(1);
        paramsHolder.setRadius(radius);
        return getNearestFields(paramsHolder);
    }

    public List<PlayingField> getNearestFields(LatLng latLng) throws RequestException{
        NearestQueryParamsHolder paramsHolder = new NearestQueryParamsHolder(latLng);
        return getNearestFields(paramsHolder);
    }

    public List<PlayingField> getNearestFields(NearestQueryParamsHolder paramsHolder) throws RequestException{
        Map<String, Object> params = paramsHolder.getQueryParams();
        Call<List<PlayingField>> call = fields().nearest(params);
        return executeCall(call);
    }

    public PlayingFieldDetailed getPlayingField(int id, Date maxDate) throws RequestException{
        String maxDateString = null;
        if(maxDate != null){
            maxDateString = Util.Dates.dateToServerString(maxDate);
        }
        Call<PlayingFieldDetailed> call = fields().id(id, maxDateString);
        return executeCall(call);
    }

    public PlayingField updatePlayingField(int id, PlayingFieldData data, File image) throws RequestException{
        return updatePlayingField(id, data, image, false);
    }

    public PlayingField updatePlayingField(int id, PlayingFieldData data, String imagePath) throws RequestException{
        File file = null;
        if(!TextUtils.isEmpty(imagePath)){
            file = new File(imagePath);
        }
        return updatePlayingField(id, data, file);
    }

    public PlayingField updatePlayingField(int id, PlayingFieldData data) throws RequestException{
        return updatePlayingField(id, data, (File) null);
    }

    public PlayingField updatePlayingFieldImage(int id, File image) throws RequestException{
        return updatePlayingField(id, null, image);
    }

    public PlayingField updatePlayingFieldImage(int id, String imagePath) throws RequestException{
        File file = null;
        if(!TextUtils.isEmpty(imagePath)){
            file = new File(imagePath);
        }
        return updatePlayingField(id, null, file);
    }

    public PlayingField clearPlayingFieldImage(int id) throws RequestException{
        return updatePlayingField(id, null, null, true);
    }

    public PlayingField updatePlayingField(int id, PlayingFieldData data, File image, boolean clearImage) throws RequestException{
        MultipartBody body = createMultipartBody(data, image, "image", clearImage, "clearImage");
        Call<PlayingField> call = fields().update(id, body);
        return executeCall(call);
    }

    public PlayingFieldDetailed subscribeOnField(int id) throws RequestException{
        Call<PlayingFieldDetailed> call = fields().subscribe(id);
        return executeCall(call);
    }

    public PlayingFieldDetailed unsubscribeFromField(int id) throws RequestException{
        Call<PlayingFieldDetailed> call = fields().unsubscribe(id);
        return executeCall(call);
    }

    public boolean addRecord(int playingFieldId, Date date) throws RequestException{
        return addRecord(playingFieldId, date, null);
    }

    private boolean addRecord(int playingFieldId, Date date, Integer sourceId) throws RequestException{
        AddRecordRequestBody body = new AddRecordRequestBody(date, sourceId);
        Call<String> call = fields().addRecord(playingFieldId, body);
        return executeCall(call) != null;
    }

    public boolean acceptInvitation(int playingFieldId, Date date, int sourceId) throws RequestException{
        return addRecord(playingFieldId, date, sourceId);
    }

    public boolean updateRecord(int playingFieldId, Date oldDate, Date newDate) throws RequestException{
        UpdateRecordRequestBody body = new UpdateRecordRequestBody(oldDate, newDate);
        Call<String> call = fields().updateRecord(playingFieldId, body);
        return executeCall(call) != null;
    }

    public boolean deleteRecord(int playingFieldId, Date date) throws RequestException{
        Call<String> call = fields().deleteRecord(playingFieldId, date);
        return executeCall(call) != null;
    }

    public List<ScheduleItem> getSchedule(int playingFieldId, Date minDate, Date maxDate, boolean excludeSelf) throws RequestException{
        String minDateString = null;
        String maxDateString = null;

        if(minDate != null){
            minDateString = Util.Dates.dateToServerString(minDate);
        }
        if(maxDate != null){
            maxDateString = Util.Dates.dateToServerString(maxDate);
        }

        Call<List<ScheduleItem>> call = fields().schedule(playingFieldId, minDateString, maxDateString, excludeSelf);
        return executeCall(call);
    }
}
