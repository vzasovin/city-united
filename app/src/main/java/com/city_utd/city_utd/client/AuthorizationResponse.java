package com.city_utd.city_utd.client;

import com.city_utd.city_utd.model.UserDetailed;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 05.07.2017.
 */

public class AuthorizationResponse {

    @SerializedName("user")
    private UserDetailed user;

    @SerializedName("created")
    private boolean created;

    @SerializedName("jwtToken")
    private String token;

    public UserDetailed getUser() {
        return user;
    }

    public boolean isCreated() {
        return created;
    }

    public String getToken() {
        return token;
    }
}
