package com.city_utd.city_utd.client;

/**
 * Created by Admin on 05.07.2017.
 */

public class PagingQueryParamsHolder extends BaseQueryParamsHolder {

    private static final String PARAM_NAME_PAGE = "page";
    private static final String PARAM_NAME_PAGE_SIZE = "page_size";
    private static final String PARAM_NAME_QUERY = "query";

    private static final int DEFAULT_PAGE_SIZE = 100;

    public PagingQueryParamsHolder(int pageSize){
        putParam(PARAM_NAME_PAGE_SIZE, pageSize);
    }

    public PagingQueryParamsHolder(){
        this(DEFAULT_PAGE_SIZE);
    }

    public int getPageSize(){
        return getParam(PARAM_NAME_PAGE_SIZE, -1);
    }

    public void setPage(int page){
        putParam(PARAM_NAME_PAGE, page);
    }

    public int getPage(){
        return getParam(PARAM_NAME_PAGE, -1);
    }

    public void setQuery(String query){
        putParam(PARAM_NAME_QUERY, query);
    }

    public String getQuery(){
        return getParam(PARAM_NAME_QUERY, "");
    }
}
