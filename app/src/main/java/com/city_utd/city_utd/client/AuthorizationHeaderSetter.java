package com.city_utd.city_utd.client;

import android.content.Context;
import android.text.TextUtils;

import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.util.LocalDataManager;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by Admin on 05.07.2017.
 */

class AuthorizationHeaderSetter implements Interceptor {

    static final String AUTHORIZATION_NECESSITY_HEADER_REQUIRED = "Authorization-Necessity: required";
    static final String AUTHORIZATION_NECESSITY_HEADER_OPTIONAL = "Authorization-Necessity: optional";
    static final String AUTHORIZATION_NECESSITY_HEADER_NEEDLESSLY = "Authorization-Necessity: needlessly";

    private static final String REQUIRED = "required";
    private static final String OPTIONAL = "optional";
    private static final String NEEDLESSLY = "needlessly";

    private static final String AUTHORIZATION_NECESSITY_HEADER_NAME = "Authorization-Necessity";
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request original = chain.request();
        String value = original.header(AUTHORIZATION_NECESSITY_HEADER_NAME);

        if(!TextUtils.isEmpty(value)){
            String token;
            boolean addHeader = true;
            switch (value){
                case REQUIRED:
                    token = getJwtTokenOrThrow();
                    break;
                case OPTIONAL:
                    token = getJwtToken();
                    break;
                case NEEDLESSLY:
                default:
                    token = null;
                    addHeader = false;
                    break;
            }

            if(addHeader && !TextUtils.isEmpty(token)){
                String headerValue = "JWT " + token;
                Request intercepted = original.newBuilder()
                        .addHeader(AUTHORIZATION_HEADER_NAME, headerValue)
                        .build();
                return chain.proceed(intercepted);
            }
        }

        return chain.proceed(original);
    }

    private String getJwtToken(){
        Context context = CityUtdApplication.getAppContext();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        return manager.getJwtToken();
    }

    private String getJwtTokenOrThrow(){
        String token = getJwtToken();
        if(TextUtils.isEmpty(token)){
            throw new JwtTokenNotFoundException();
        }
        return token;
    }
}
