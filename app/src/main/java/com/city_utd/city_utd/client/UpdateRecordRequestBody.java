package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 10.07.2017.
 */

class UpdateRecordRequestBody {

    @SerializedName("oldDate")
    private final Date oldDate;

    @SerializedName("newDate")
    private final Date newDate;

    UpdateRecordRequestBody(Date oldDate, Date newDate){
        this.oldDate = oldDate;
        this.newDate = newDate;
    }
}
