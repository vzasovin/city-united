package com.city_utd.city_utd.client;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Admin on 07.07.2017.
 */

public final class PlayingFieldData implements QueryParamsHolder, Parcelable {

    private static final String PARAM_NAME_NAME = "name";
    private static final String PARAM_NAME_ADDRESS = "address";
    private static final String PARAM_NAME_LATITUDE = "latitude";
    private static final String PARAM_NAME_LONGITUDE = "longitude";

    private String name;
    private String address;
    private double latitude;
    private double longitude;

    public PlayingFieldData(){}

    private PlayingFieldData(Parcel in) {
        name = in.readString();
        address = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
    }

    public static final Creator<PlayingFieldData> CREATOR = new Creator<PlayingFieldData>() {
        @Override
        public PlayingFieldData createFromParcel(Parcel in) {
            return new PlayingFieldData(in);
        }

        @Override
        public PlayingFieldData[] newArray(int size) {
            return new PlayingFieldData[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public Map<String, Object> getQueryParams() {
        Map<String, Object> map = new HashMap<>();

        if(name != null){
            map.put(PARAM_NAME_NAME, name);
        }

        if(address != null){
            map.put(PARAM_NAME_ADDRESS, address);
        }

        if(latitude != 0){
            map.put(PARAM_NAME_LATITUDE, latitude);
        }

        if(longitude != 0){
            map.put(PARAM_NAME_LONGITUDE, longitude);
        }

        return map;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(address);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
    }
}
