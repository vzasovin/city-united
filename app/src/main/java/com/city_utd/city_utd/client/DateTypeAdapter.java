package com.city_utd.city_utd.client;

import com.city_utd.city_utd.exception.RuntimeParseException;
import com.city_utd.city_utd.util.Util;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Date;

/**
 * Created by Admin on 06.07.2017.
 */

class DateTypeAdapter extends TypeAdapter<Date> {

    @Override
    public void write(JsonWriter out, Date value) throws IOException {
        if(value == null){
            out.nullValue();
            return;
        }
        out.value(Util.Dates.dateToServerString(value));
    }

    @Override
    public Date read(JsonReader in) throws IOException {
        if(in.peek() == JsonToken.NULL){
            in.nextNull();
            return null;
        }

        String s = in.nextString();
        try {
            return Util.Dates.serverStringToDate(s);
        }catch (RuntimeParseException e){
            return null;
        }
    }
}
