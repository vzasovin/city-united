package com.city_utd.city_utd.client;

import android.os.Parcel;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 06.07.2017.
 */

public class RateInfo extends Rate {

    @SerializedName("id")
    int userId;

    @SerializedName("name")
    String userName;

    @SerializedName("imageUrl")
    String userImageUrl;

    @SerializedName("line")
    String userLine;

    @SerializedName("updatedAt")
    Date lastRateDate;

    RateInfo(){
        super();
    }

    private RateInfo(Parcel in){
        super(in);
        userId = in.readInt();
        userName = in.readString();
        userImageUrl = in.readString();
        userLine = in.readString();
        lastRateDate = (Date) in.readSerializable();
    }

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public String getUserLine() {
        return userLine;
    }

    public Date getLastRateDate(){
        return lastRateDate;
    }

    public static final Creator<RateInfo> CREATOR = new Creator<RateInfo>() {
        @Override
        public RateInfo createFromParcel(Parcel in) {
            return new RateInfo(in);
        }

        @Override
        public RateInfo[] newArray(int size) {
            return new RateInfo[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(userId);
        dest.writeString(userName);
        dest.writeString(userImageUrl);
        dest.writeString(userLine);
        dest.writeSerializable(lastRateDate);
    }
}
