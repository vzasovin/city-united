package com.city_utd.city_utd.client;

/**
 * Created by Admin on 05.07.2017.
 */

class JwtTokenNotFoundException extends RuntimeException{

    JwtTokenNotFoundException(){
        super("An JWT token is not found in the app preferences");
    }
}
