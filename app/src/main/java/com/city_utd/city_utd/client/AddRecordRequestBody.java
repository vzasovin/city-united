package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 10.07.2017.
 */

class AddRecordRequestBody {

    @SerializedName("date")
    private final Date date;

    @SerializedName("sourceId")
    private final Integer sourceId;

    AddRecordRequestBody(Date date, Integer sourceId){
        this.date = date;
        this.sourceId = sourceId;
    }

    AddRecordRequestBody(Date date){
        this(date, null);
    }
}
