package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 23.07.2017.
 */

class ClientTokenRequestBody {

    @SerializedName("token")
    private final String token;

    ClientTokenRequestBody(String token){
        this.token = token;
    }
}
