package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 23.07.2017.
 */

class LogoutRequestBody {

    @SerializedName("clientToken")
    private final String clientToken;

    LogoutRequestBody(String clientToken){
        this.clientToken = clientToken;
    }
}
