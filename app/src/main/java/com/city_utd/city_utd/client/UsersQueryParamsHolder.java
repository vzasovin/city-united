package com.city_utd.city_utd.client;

import android.text.TextUtils;

import com.city_utd.city_utd.model.PlayerLines;

/**
 * Created by Admin on 05.07.2017.
 */

public class UsersQueryParamsHolder extends PagingQueryParamsHolder {

    private static final String PARAM_NAME_LINE = "line";
    private static final String PARAM_NAME_SORTER_RATING = "sorter_rating";

    public UsersQueryParamsHolder(int pageSize, String line, String sorterRating){
        super(pageSize);
        if(!TextUtils.isEmpty(line)){
            setLine(line);
        }
        if(!TextUtils.isEmpty(sorterRating)){
            setSorterRating(sorterRating);
        }
    }

    public void setLine(String line){
        if(!PlayerLines.validate(line)){
            throw new IllegalArgumentException("Unsupported line: " + line);
        }
        putParam(PARAM_NAME_LINE, line);
    }

    public String getLine(){
        return getParam(PARAM_NAME_LINE, null);
    }

    public void setSorterRating(String sorter){
        if(!Sorters.validate(sorter)){
            throw new IllegalArgumentException("Unsupported sorter: " + sorter);
        }
        putParam(PARAM_NAME_SORTER_RATING, sorter);
    }

    public String getSorterRating(){
        return getParam(PARAM_NAME_SORTER_RATING, "");
    }
}
