package com.city_utd.city_utd.client;

import java.util.Map;

/**
 * Created by Admin on 05.07.2017.
 */

interface QueryParamsHolder {

    Map<String, Object> getQueryParams();
}
