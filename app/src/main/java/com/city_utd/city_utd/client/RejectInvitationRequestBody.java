package com.city_utd.city_utd.client;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Admin on 07.07.2017.
 */

class RejectInvitationRequestBody {

    @SerializedName("userId")
    private final int userId;

    @SerializedName("playingFieldId")
    private final int playingFieldId;

    @SerializedName("invitationDate")
    private final Date invitationDate;

    RejectInvitationRequestBody(int userId, int playingFieldId, Date invitationDate) {
        this.userId = userId;
        this.playingFieldId = playingFieldId;
        this.invitationDate = invitationDate;
    }
}
