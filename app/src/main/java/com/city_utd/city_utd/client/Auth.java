package com.city_utd.city_utd.client;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Admin on 05.07.2017.
 */

interface Auth {

    @GET("/auth/vk")
    Call<AuthorizationResponse> vk(@Query("access_token") String accessToken);

    @GET("/auth/facebook")
    Call<AuthorizationResponse> facebook(@Query("access_token") String accessToken);

    @GET("/auth/odnoklassniki")
    Call<AuthorizationResponse> ok(@Query("access_token") String accessToken);

    @GET("/auth/twitter")
    Call<AuthorizationResponse> twitter(@Query("user_id") long userId, @Query("user_name") String userName);

    @GET("/auth/phone_number")
    Call<AuthorizationResponse> phoneNumber(@Query("phone_number") String phoneNumber);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/auth/logout")
    Call<String> logout(@Body LogoutRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/auth/fcm/enable_client")
    Call<String> enableClient(@Body ClientTokenRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/auth/fcm/disable_client")
    Call<String> disableClient(@Body ClientTokenRequestBody body);

    @Headers(AuthorizationHeaderSetter.AUTHORIZATION_NECESSITY_HEADER_REQUIRED)
    @POST("/auth/fcm/update_client_token")
    Call<String> updateClientToken(@Body UpdateClientTokenRequestBody body);
}
