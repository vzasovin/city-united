package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.GallerySlideShowFragment;

import java.util.ArrayList;

/**
 * Created by Admin on 31.07.2017.
 */

public class GallerySlideShowActivity extends AbstractActivity {

    public static final String EXTRA_GALLERY = "com.city_utd.city_utd.GallerySlideShowActivity.EXTRA_GALLERY";
    public static final String EXTRA_SELECTED_IMAGE_URL = "com.city_utd.city_utd.GallerySlideShowActivity.EXTRA_SELECTED_IMAGE_URL";

    @Override
    protected Fragment createFragment(Intent intent) {
        ArrayList<String> gallery = intent.getStringArrayListExtra(EXTRA_GALLERY);
        String selectedImageUrl = intent.getStringExtra(EXTRA_SELECTED_IMAGE_URL);
        return GallerySlideShowFragment.newInstance(gallery, selectedImageUrl);
    }
}
