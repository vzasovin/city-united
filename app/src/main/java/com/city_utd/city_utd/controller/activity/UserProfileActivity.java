package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.MyProfileFragment;
import com.city_utd.city_utd.controller.fragment.UserProfileFragment;
import com.city_utd.city_utd.util.LocalDataManager;

/**
 * Created by Admin on 19.07.2017.
 */

public class UserProfileActivity extends AbstractActivity {

    public static final String EXTRA_USER_ID = "com.city_utd.city_utd.UserProfileActivity.EXTRA_USER_ID";

    @Override
    protected Fragment createFragment(Intent intent) {
        int userId = intent.getIntExtra(EXTRA_USER_ID, 0);
        LocalDataManager manager = LocalDataManager.getInstance(this);
        int myId = manager.getUserId();
        if(userId == myId){
            return MyProfileFragment.newInstance();
        }
        return UserProfileFragment.newInstance(userId);
    }
}
