package com.city_utd.city_utd.controller.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.activity.MainNavigationActivity;
import com.city_utd.city_utd.service.RequestLocationUpdatesJobService;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.AppUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 14.07.2017.
 */

public class FineLocationPermissionExplanationFragment extends AbstractFragment
        implements View.OnClickListener {

    private static final int RC_REQUEST_FINE_LOCATION_PERMISSION = 1;

    @Bind(R.id.allow_button)
    TextView allowButton;

    @Bind(R.id.skip_button)
    TextView skipButton;

    public static FineLocationPermissionExplanationFragment newInstance() {
        return new FineLocationPermissionExplanationFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_fine_location_permission_explanation;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        allowButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.allow_button:
                requestPermission();
                break;
            case R.id.skip_button:
                openMainNavigationActivity();
                clearShowFineLocationPermissionExplanationOnLaunchFlag();
                break;
        }
    }

    private void requestPermission(){
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        int result = requestPermission(permission, false, RC_REQUEST_FINE_LOCATION_PERMISSION);
        if(result == AppUtils.Permissions.RESULT_HAS_PERMISSION){
            openMainNavigationActivity();
        }
    }

    private void openMainNavigationActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, MainNavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void clearShowFineLocationPermissionExplanationOnLaunchFlag(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setShowFineLocationPermissionExplanationOnLaunchFlag(false);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case RC_REQUEST_FINE_LOCATION_PERMISSION:
                openMainNavigationActivity();

                clearShowFineLocationPermissionExplanationOnLaunchFlag();
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    RequestLocationUpdatesJobService.requestLocationUpdates();
                }
                break;
        }
    }
}
