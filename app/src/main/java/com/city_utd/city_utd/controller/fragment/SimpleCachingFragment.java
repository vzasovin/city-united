package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;

import com.city_utd.city_utd.R;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import butterknife.ButterKnife;

/**
 * Created by vadim on 7/17/17.
 */

public abstract class SimpleCachingFragment<D> extends CachingFragment<D>{

    private View container;
    private View progressView;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        container = ButterKnife.findById(view, R.id.container);
        progressView = ButterKnife.findById(view, R.id.progress_view);

        container.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
    }

    @Override
    protected void onLoadFinished(BaseLoader<D> loader, D data) {
        super.onLoadFinished(loader, data);

        final int source = loader.getSource();
        final boolean fromCache = source == BaseLoader.SOURCE_DATABASE;
        final boolean hasData = data != null;

        if(fromCache){
            if(hasData){
                showContentIfNeeded();
            }else if(checkNetworkAvailability()){
                showProgressViewIfNeeded();
            }
        }else {
            hideProgressViewIfNeeded(hasData);
        }
    }

    protected final void showContentIfNeeded(){
        if(!isVisible(container)){
            showContent();
        }
    }

    private void showContent(){
        animateFadeIn(container, 0, null, null);
    }

    protected final void showProgressViewIfNeeded(){
        if(!isVisible(progressView)){
            showProgressView();
        }
    }

    private boolean isVisible(View view){
        return view.getVisibility() == View.VISIBLE;
    }

    private void showProgressView(){
        animateFadeIn(progressView, 0, new Runnable() {
            @Override
            public void run() {
                startProgressAnimation();
            }
        }, null);
    }

    private void animateFadeIn(View view, long startDelay, Runnable startAction, Runnable endAction){
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        ViewPropertyAnimator animator = view.animate();
        animator.alpha(1).setDuration(200).setStartDelay(startDelay);
        if(startAction != null){
            animator.withStartAction(startAction);
        }
        if(endAction != null){
            animator.withEndAction(endAction);
        }
    }

    private void startProgressAnimation(){
        Context context = getActivity();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.ball_rotation);
        animation.setRepeatMode(Animation.RESTART);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        progressView.startAnimation(animation);
    }

    private void hideProgressViewIfNeeded(boolean showContent){
        if(isVisible(progressView)){
            hideProgressView(showContent);
        }else if(showContent){
            showContentIfNeeded();
        }
    }

    private void hideProgressView(final boolean showContent){
        animateFadeOut(progressView, 0, null, new Runnable() {
            @Override
            public void run() {
                stopProgressAnimation();
                progressView.setVisibility(View.GONE);
                if(showContent){
                    showContentIfNeeded();
                }
            }
        });
    }

    private void animateFadeOut(View view, long startDelay, Runnable startAction, Runnable endAction){
        view.setAlpha(1);
        ViewPropertyAnimator animator = view.animate();
        animator.alpha(0).setDuration(200).setStartDelay(startDelay);
        if(startAction != null){
            animator.withStartAction(startAction);
        }
        if(endAction != null){
            animator.withEndAction(endAction);
        }
    }

    private void stopProgressAnimation(){
        progressView.clearAnimation();
    }
}
