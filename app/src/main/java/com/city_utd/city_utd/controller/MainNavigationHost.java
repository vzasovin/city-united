package com.city_utd.city_utd.controller;

/**
 * Created by Admin on 11.07.2017.
 */

public interface MainNavigationHost {

    void hideBottomNavigationView();

    void showBottomNavigationView();

    void hideFloatingActionButton();

    void showFloatingActionButton();
}
