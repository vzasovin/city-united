package com.city_utd.city_utd.controller.adapter;

import android.support.annotation.LayoutRes;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.city_utd.city_utd.R;

import java.util.List;

/**
 * Created by Admin on 26.07.2017.
 */

public abstract class FakeItemAdapter<T, VH extends RecyclerView.ViewHolder> extends PagingAdapter<T, RecyclerView.ViewHolder> {

    private static final int VIEW_TYPE_FAKE = 34500659;

    private boolean hasFakeItem;

    public FakeItemAdapter(List<T> data) {
        super(data);
    }

    @Override
    public int getItemCount() {
        int itemCount = super.getItemCount();
        if(hasFakeItem){
            itemCount++;
        }
        return itemCount;
    }

    @Override
    public int getItemViewType(int position) {
        if(hasFakeItem && position == 0){
            return VIEW_TYPE_FAKE;
        }
        return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == VIEW_TYPE_FAKE){
            @LayoutRes int layoutRes = R.layout.list_item_fake;
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(layoutRes, parent, false);
            return new FakeViewHolder(itemView);
        }
        return onCreateNormalViewHolder(parent, viewType);
    }

    private final class FakeViewHolder extends RecyclerView.ViewHolder{

        private FakeViewHolder(View itemView) {
            super(itemView);
        }
    }

    protected abstract VH onCreateNormalViewHolder(ViewGroup parent, int viewType);

    @Override
    public T getItem(int position) {
        if(hasFakeItem){
            position--;
        }
        return super.getItem(position);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        if(viewType == VIEW_TYPE_FAKE){
            return;
        }

        T item = getItem(position);
        onBindViewHolder(holder, item);
    }

    public final void addFakeItem(){
        hasFakeItem = true;
        notifyItemInserted(0);
    }

    public final void removeFakeItem(){
        hasFakeItem = false;
        notifyItemRemoved(0);
    }
}
