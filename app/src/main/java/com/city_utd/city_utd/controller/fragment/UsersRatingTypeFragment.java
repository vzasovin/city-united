package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.Sorters;
import com.city_utd.city_utd.client.UsersQueryParamsHolder;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.PagingAdapter;
import com.city_utd.city_utd.loader.UsersLoader;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.util.Util;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.controller.paging.OnThresholdReachedListener;
import com.imogene.android.carcase.controller.paging.RecyclerViewLoadMoreListener;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 22.07.2017.
 */

public class UsersRatingTypeFragment extends SimpleCachingFragment<List<User>>
        implements OnThresholdReachedListener {

    public static final int IN_THE_CITY = 0;
    public static final int IN_THE_COUNTRY = 1;
    public static final int IN_THE_WORLD = 2;

    private static final String EXTRA_TYPE = "com.city_utd.city_utd.UsersRatingTypeFragment.EXTRA_TYPE";

    @Bind(R.id.container)
    RecyclerView recyclerView;

    private int type;
    private UsersAdapter adapter;
    private RecyclerViewLoadMoreListener loadMoreListener;
    private UsersQueryParamsHolder paramsHolder;

    public static UsersRatingTypeFragment newInstance(int type) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_TYPE, type);
        UsersRatingTypeFragment fragment = new UsersRatingTypeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        type = args.getInt(EXTRA_TYPE);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.layout_recycler_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UsersAdapter(null);
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        recyclerView.addItemDecoration(decoration);

        loadMoreListener = new RecyclerViewLoadMoreListener(.7f, this);
        recyclerView.addOnScrollListener(loadMoreListener);
    }

    class UsersAdapter extends PagingAdapter<User, UsersAdapter.UserViewHolder>{

        private ImageUrlBuilder imageUrlBuilder;

        UsersAdapter(List<User> data) {
            super(data);
        }

        @Override
        public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_user_rating, parent, false);
            return new UserViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(UserViewHolder holder, User item) {
            int position = holder.getAdapterPosition();
            String indexLabel = "#" + (position + 1);
            holder.indexTextView.setText(indexLabel);

            ensureImageUrlBuilder();
            String imageUrl = item.getImageUrl();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(UsersRatingTypeFragment.this).load(imageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(holder.imageView);

            String name = item.getName();
            if(!TextUtils.isEmpty(name)){
                holder.nameTextView.setVisibility(View.VISIBLE);
                holder.nameTextView.setText(name);
            }else {
                holder.nameTextView.setVisibility(View.GONE);
            }

            String line = item.getLine();
            String lineString = Util.Commons.getLineString(getActivity(), line);
            holder.lineTextView.setText(lineString);

            float rating = item.getActualRating();
            holder.ratingBar.setRating(rating);
        }

        private void ensureImageUrlBuilder(){
            if(imageUrlBuilder == null){
                imageUrlBuilder = ApiManager.newImageUrlBuilder();
            }
        }

        class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.index_text_view)
            TextView indexTextView;

            @Bind(R.id.image_view)
            ImageView imageView;

            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Bind(R.id.line_text_view)
            TextView lineTextView;

            @Bind(R.id.rating_bar)
            RatingBar ratingBar;

            UserViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                User user = getItem(adapterPosition);
                int userId = user.getId();
                openUserProfileActivity(userId);
            }
        }
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    public void onThresholdReached(int position) {
        ensureParamsHolder();
        int page = paramsHolder.getPage();
        paramsHolder.setPage(page + 1);
        onLoadFromServer();
    }

    @Override
    protected boolean isCachingEnabled() {
        return false;
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {}

    @Override
    protected void onLoadFromServer() {
        reloadData(0, null);
    }

    @Override
    protected BaseLoader<List<User>> onCreateLoader(Context context, int id, Bundle args) {
        ensureParamsHolder();
        return new UsersLoader(context, paramsHolder);
    }

    private void ensureParamsHolder(){
        if(paramsHolder == null){
            paramsHolder = new UsersQueryParamsHolder(100, null, Sorters.DESC);
            paramsHolder.setPage(1);
        }
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<User>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<User>> loader, List<User> data) {
        super.onLoadFinished(loader, data);

        final boolean hasData = data != null;
        final String errorMessage = loader.getErrorMessage();

        if(hasData){
            adapter.addData(data);
            boolean emptyData = data.isEmpty();
            if(!emptyData){
                loadMoreListener.notifyContinueListen();
            }
        }else if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(checkNetworkAvailability()){
            showProgressViewIfNeeded();
        }
    }
}
