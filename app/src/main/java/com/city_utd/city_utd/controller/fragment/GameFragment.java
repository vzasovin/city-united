package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.GameActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.BaseAdapter;
import com.city_utd.city_utd.controller.dialog.RateUserDialogFragment;
import com.city_utd.city_utd.loader.ScheduleLoader;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.task.DeleteRateAsyncTask;
import com.city_utd.city_utd.task.RateUserAsyncTask;
import com.city_utd.city_utd.util.Util;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.city_utd.city_utd.util.Util.Dates.DATE_FORMAT_1;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TODAY;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_YESTERDAY;
import static com.city_utd.city_utd.util.Util.Dates.dateToString;
import static com.city_utd.city_utd.util.Util.Dates.dateToTimeString;
import static com.city_utd.city_utd.util.Util.Dates.getDayLabel;

/**
 * Created by Admin on 13.08.2017.
 */

public class GameFragment extends SimpleCachingFragment<List<ScheduleItem>>
        implements View.OnClickListener, BaseAsyncTask.Callbacks{

    private static final String STATE_TEMP_USER_ID = "com.city_utd.city_utd.GameFragment.STATE_TEMP_USER_ID";

    private static final int RC_VIEW_USER_PROFILE = 1;
    private static final int RC_RATE_USER = 2;

    private static final int TASK_ID_DELETE_RATE = 1;
    private static final int TASK_ID_RATE_USER = 2;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.name_text_view)
    TextView nameTextView;

    @Bind(R.id.address_text_view)
    TextView addressTextView;

    @Bind(R.id.date_text_view)
    TextView dateTextView;

    @Bind(R.id.schedule_recycler_view)
    RecyclerView scheduleRecyclerView;

    private int playingFieldId;
    private String playingFieldName;
    private String playingFieldAddress;
    private String imageUrl;
    private Date minDate;
    private Date maxDate;
    private Date date;

    private ScheduleItemAdapter adapter;
    private ImageUrlBuilder imageUrlBuilder;
    private boolean loadWithSyntheticDelay;
    private int tempUserId;

    public static GameFragment newInstance(int playingFieldId, String playingFieldName,
                                           String playingFieldAddress, String imageUrl,
                                           Date minDate, Date maxDate, Date date) {
        Bundle args = new Bundle();
        args.putInt(GameActivity.EXTRA_PLAYING_FIELD_ID, playingFieldId);
        args.putString(GameActivity.EXTRA_PLAYING_FIELD_NAME, playingFieldName);
        args.putString(GameActivity.EXTRA_PLAYING_FIELD_ADDRESS, playingFieldAddress);
        args.putString(GameActivity.EXTRA_IMAGE_URL, imageUrl);
        args.putSerializable(GameActivity.EXTRA_MIN_DATE, minDate);
        args.putSerializable(GameActivity.EXTRA_MAX_DATE, maxDate);
        args.putSerializable(GameActivity.EXTRA_DATE, date);
        GameFragment fragment = new GameFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        playingFieldId = args.getInt(GameActivity.EXTRA_PLAYING_FIELD_ID);
        playingFieldName = args.getString(GameActivity.EXTRA_PLAYING_FIELD_NAME);
        playingFieldAddress = args.getString(GameActivity.EXTRA_PLAYING_FIELD_ADDRESS);
        imageUrl = args.getString(GameActivity.EXTRA_IMAGE_URL);
        minDate = (Date) args.getSerializable(GameActivity.EXTRA_MIN_DATE);
        maxDate = (Date) args.getSerializable(GameActivity.EXTRA_MAX_DATE);
        date = (Date) args.getSerializable(GameActivity.EXTRA_DATE);

        if(savedInstanceState != null){
            tempUserId = savedInstanceState.getInt(STATE_TEMP_USER_ID);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_game;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // setup on click listeners
        imageView.setOnClickListener(this);
        nameTextView.setOnClickListener(this);
        addressTextView.setOnClickListener(this);

        // setup recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        scheduleRecyclerView.setLayoutManager(layoutManager);
        adapter = new ScheduleItemAdapter(null);
        scheduleRecyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        scheduleRecyclerView.addItemDecoration(decoration);
        scheduleRecyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void onClick(View v) {
        openFieldDetailedActivity();
    }

    private void openFieldDetailedActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, playingFieldId);
        startActivity(intent);
    }

    class ScheduleItemAdapter extends BaseAdapter<ScheduleItem, ScheduleItemAdapter.ScheduleItemViewHolder>{

        ScheduleItemAdapter(List<ScheduleItem> data) {
            super(data);
        }

        @Override
        public ScheduleItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_user_action_rate, parent, false);
            return new ScheduleItemViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(ScheduleItemViewHolder holder, ScheduleItem item) {
            ensureImageUrlBuilder();
            String imageUrl = item.getImageUrl();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(GameFragment.this).load(imageUrl)
                    .placeholder(placeholderRes)
                    .error(placeholderRes)
                    .into(holder.imageView);

            String name = item.getUserName();
            if(!TextUtils.isEmpty(name)){
                holder.nameTextView.setVisibility(View.VISIBLE);
                holder.nameTextView.setText(name);
            }else {
                holder.nameTextView.setVisibility(View.GONE);
            }

            String line = item.getLine();
            String lineString = Util.Commons.getLineString(getActivity(), line);
            holder.lineTextView.setText(lineString);

            float rating = item.getActualRating();
            holder.ratingBar.setRating(rating);
        }

        class ScheduleItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.image_view)
            ImageView imageView;

            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Bind(R.id.line_text_view)
            TextView lineTextView;

            @Bind(R.id.rating_bar)
            RatingBar ratingBar;

            @Bind(R.id.rate_button)
            Button rateButton;

            ScheduleItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
                rateButton.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                ScheduleItem item = getItem(adapterPosition);
                int userId = item.getUserId();
                int id = v.getId();
                switch (id){
                    case R.id.rate_button:
                        String line = item.getLine();
                        rateUser(userId, line);
                        break;
                    default:
                        openUserProfileActivity(userId);
                        break;
                }
            }
        }
    }

    private void rateUser(int userId, String line){
        tempUserId = userId;
        showRateUserDialog(userId, line);
    }

    private void showRateUserDialog(int userId, String line){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = RateUserDialogFragment.newInstance(line, userId);
        dialog.setTargetFragment(this, RC_RATE_USER);
        dialog.show(fragmentManager, "RATE_USER_DIALOG");
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivityForResult(intent, RC_VIEW_USER_PROFILE);
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_VIEW_USER_PROFILE:
                if(resultCode == Activity.RESULT_OK){
                    loadWithSyntheticDelay = false;
                    reloadData(0, null);
                }
                break;
            case RC_RATE_USER:
                if(resultCode == Activity.RESULT_OK && data != null){
                    boolean deleteRate = data.getBooleanExtra(RateUserDialogFragment.EXTRA_DELETE_RATE, false);
                    if(deleteRate){
                        deleteRate();
                    }else {
                        Rate rate = data.getParcelableExtra(RateUserDialogFragment.EXTRA_RATE);
                        rateUser(rate);
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void deleteRate(){
        DeleteRateAsyncTask asyncTask = new DeleteRateAsyncTask(TASK_ID_DELETE_RATE, this, tempUserId);
        asyncTask.execute();
    }

    private void rateUser(Rate rate){
        RateUserAsyncTask asyncTask = new RateUserAsyncTask(TASK_ID_RATE_USER, this, tempUserId, rate);
        asyncTask.execute();
    }

    @Override
    public void onTaskStarted(int taskId) {}

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_DELETE_RATE:
            case TASK_ID_RATE_USER:
                if((boolean) result){
                    loadWithSyntheticDelay = false;
                    reloadData(0, null);
                }
                break;
        }
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    protected boolean isCachingEnabled() {
        return false;
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {}

    @Override
    protected void onLoadFromServer() {
        loadWithSyntheticDelay = true;
        reloadData(0, null);
    }

    @Override
    protected BaseLoader<List<ScheduleItem>> onCreateLoader(Context context, int id, Bundle args) {
        Date minDate;
        Date maxDate;
        if(date == null){
            minDate = this.minDate;
            maxDate = this.maxDate;
        }else {
            minDate = date;
            maxDate = date;
        }
        return new ScheduleLoader(context, playingFieldId, minDate, maxDate, true, loadWithSyntheticDelay);
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<ScheduleItem>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<ScheduleItem>> loader, List<ScheduleItem> data) {
        super.onLoadFinished(loader, data);
        if(data != null){
            adapter.setData(data);
            bindPlayingFieldInfo();
        }
    }

    private void bindPlayingFieldInfo(){
        ensureImageUrlBuilder();
        imageUrl = imageUrlBuilder.build(imageUrl);
        @DrawableRes int placeholderRes = R.drawable.ic_field_placeholder;
        Glide.with(this).load(imageUrl)
                .placeholder(placeholderRes)
                .error(placeholderRes)
                .into(imageView);

        if(!TextUtils.isEmpty(playingFieldName)){
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(playingFieldName);
        }else {
            nameTextView.setVisibility(View.GONE);
        }

        if(!TextUtils.isEmpty(playingFieldAddress)){
            addressTextView.setVisibility(View.VISIBLE);
            addressTextView.setText(playingFieldAddress);
        }else {
            addressTextView.setVisibility(View.GONE);
        }

        String dateString;
        String startTimeString;
        String endTimeString;
        String timeString;

        boolean range = date == null;
        Date dayLabelDate = !range ? date : minDate;
        int dayLabel = getDayLabel(dayLabelDate);
        if(dayLabel == DAY_LABEL_TODAY){
            if(range){
                startTimeString = dateToTimeString(minDate);
                endTimeString = dateToTimeString(maxDate);
                dateString = getString(R.string.today_from_to, startTimeString, endTimeString);
            }else {
                timeString = dateToTimeString(date);
                dateString = getString(R.string.today_in, timeString);
            }
        }else if(dayLabel == DAY_LABEL_YESTERDAY){
            if(range){
                startTimeString = dateToTimeString(minDate);
                endTimeString = dateToTimeString(maxDate);
                dateString = getString(R.string.yesterday_from_to, startTimeString, endTimeString);
            }else {
                timeString = dateToTimeString(date);
                dateString = getString(R.string.yesterday_in, timeString);
            }
        }else {
            dateString = dateToString(minDate, DATE_FORMAT_1);
            if(range){
                startTimeString = dateToTimeString(minDate);
                endTimeString = dateToTimeString(maxDate);
                dateString = getString(R.string.earlier_from_to, dateString, startTimeString, endTimeString);
            }else {
                timeString = dateToTimeString(date);
                dateString = getString(R.string.earlier_in, dateString, timeString);
            }
        }

        dateTextView.setText(dateString);
    }

    @Override
    public boolean shouldDisplayConnectivityState() {
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_TEMP_USER_ID, tempUserId);
    }
}
