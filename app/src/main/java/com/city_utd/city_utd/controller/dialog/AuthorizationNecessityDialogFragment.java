package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.city_utd.city_utd.R;

/**
 * Created by Admin on 19.07.2017.
 */

public class AuthorizationNecessityDialogFragment extends DialogFragment
        implements MaterialDialog.SingleButtonCallback {

    public static final int REASON_USER_SUBSCRIPTION = 1;
    public static final int REASON_FIELD_SUBSCRIPTION = 2;
    public static final int REASON_USER_RATE = 3;
    public static final int REASON_ADDING_RECORD = 4;

    private static final String EXTRA_REASON = "com.city_utd.city_utd.AuthorizationNecessityDialogFragment.EXTRA_REASON";

    private int reason;

    public static AuthorizationNecessityDialogFragment newInstance(int reason) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_REASON, reason);
        AuthorizationNecessityDialogFragment fragment = new AuthorizationNecessityDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        reason = args.getInt(EXTRA_REASON);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        return new MaterialDialog.Builder(context)
                .backgroundColorRes(R.color.colorPrimary)
                .content(getContent())
                .positiveText(R.string.yes)
                .negativeText(R.string.cancel)
                .onAny(this)
                .build();
    }

    private String getContent(){
        switch (reason){
            case REASON_USER_SUBSCRIPTION:
                return getString(R.string.authorization_necessity_user_subscription);
            case REASON_FIELD_SUBSCRIPTION:
                return getString(R.string.authorization_necessity_field_subscription);
            case REASON_USER_RATE:
                return getString(R.string.authorization_necessity_user_rate);
            case REASON_ADDING_RECORD:
                return getString(R.string.authorization_necessity_adding_record);
            default:
                throw new IllegalStateException("Unsupported reason: " + reason);
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = which ==DialogAction.POSITIVE ? Activity.RESULT_OK : Activity.RESULT_CANCELED;
            targetFragment.onActivityResult(requestCode, resultCode, null);
        }
        dismiss();
    }
}
