package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.ProfileEditingFragment;

/**
 * Created by Admin on 01.08.2017.
 */

public class ProfileEditingActivity extends AbstractActivity {

    @Override
    protected Fragment createFragment(Intent intent) {
        return ProfileEditingFragment.newInstance();
    }
}
