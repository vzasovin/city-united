package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.PagingQueryParamsHolder;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.FakeItemAdapter;
import com.city_utd.city_utd.loader.SubscriptionsLoader;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.util.Util;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.controller.paging.OnThresholdReachedListener;
import com.imogene.android.carcase.controller.paging.RecyclerViewLoadMoreListener;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vadim on 7/17/17.
 */

public class SubscriptionsFragment extends SimpleCachingFragment<List<User>>
        implements MyTeamPart, OnThresholdReachedListener{

    private static final int RC_VIEW_USER_PROFILE = 1;

    @Bind(R.id.container)
    RecyclerView recyclerView;

    private PagingQueryParamsHolder paramsHolder;
    private UsersAdapter adapter;
    private RecyclerViewLoadMoreListener loadMoreListener;

    public static SubscriptionsFragment newInstance() {
        return new SubscriptionsFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.layout_recycler_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UsersAdapter(null);
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        recyclerView.addItemDecoration(decoration);

        loadMoreListener = new RecyclerViewLoadMoreListener(.7f, this);
        recyclerView.addOnScrollListener(loadMoreListener);
    }

    class UsersAdapter extends FakeItemAdapter<User, UsersAdapter.UserViewHolder> {

        private ImageUrlBuilder imageUrlBuilder;

        UsersAdapter(List<User> data) {
            super(data);
        }

        @Override
        public UserViewHolder onCreateNormalViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_user, parent, false);
            return new UserViewHolder(itemView);
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        protected void onBindViewHolder(RecyclerView.ViewHolder holder, User item) {
            UserViewHolder viewHolder = (UserViewHolder) holder;

            ensureImageUrlBuilder();
            String imageUrl = item.getImageUrl();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(SubscriptionsFragment.this).load(imageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(viewHolder.imageView);

            String name = item.getName();
            if(!TextUtils.isEmpty(name)){
                viewHolder.nameTextView.setVisibility(View.VISIBLE);
                viewHolder.nameTextView.setText(name);
            }else {
                viewHolder.nameTextView.setVisibility(View.INVISIBLE);
            }

            String line = item.getLine();
            String lineString = Util.Commons.getLineString(getActivity(), line);
            viewHolder.lineTextView.setText(lineString);

            float rating = item.getActualRating();
            viewHolder.ratingBar.setRating(rating);
        }

        private void ensureImageUrlBuilder(){
            if(imageUrlBuilder == null){
                imageUrlBuilder = ApiManager.newImageUrlBuilder();
            }
        }

        class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Nullable
            @Bind(R.id.image_view)
            ImageView imageView;

            @Nullable
            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Nullable
            @Bind(R.id.line_text_view)
            TextView lineTextView;

            @Nullable
            @Bind(R.id.rating_bar)
            RatingBar ratingBar;

            UserViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                User user = getItem(adapterPosition);
                int userId = user.getId();
                openUserProfileActivity(userId);
            }
        }
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivityForResult(intent, RC_VIEW_USER_PROFILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_VIEW_USER_PROFILE:
                if(resultCode == Activity.RESULT_OK){
                    reload();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    public void onThresholdReached(int position) {
        ensureParamsHolder();
        int page = paramsHolder.getPage();
        paramsHolder.setPage(page + 1);
        onLoadFromServer();
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        int source;
        if(paramsHolder != null){
            int page = paramsHolder.getPage();
            source = page > 1 ? BaseLoader.SOURCE_SERVER : BaseLoader.SOURCE_SERVER_DATABASE;
        }else {
            source = BaseLoader.SOURCE_SERVER_DATABASE;
        }

        Bundle args = updateLoaderArgs(source);
        reloadData(0, args);
    }

    @Override
    protected BaseLoader<List<User>> onCreateLoader(Context context, int id, Bundle args) {
        final int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        final boolean fromCache = source == BaseLoader.SOURCE_DATABASE;
        if(fromCache){
            return new SubscriptionsLoader(context);
        }else {
            ensureParamsHolder();
            return new SubscriptionsLoader(context, source, paramsHolder);
        }
    }

    private void ensureParamsHolder(){
        if(paramsHolder == null){
            paramsHolder = new PagingQueryParamsHolder();
            paramsHolder.setPage(1);
        }
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<User>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<User>> loader, List<User> data) {
        super.onLoadFinished(loader, data);

        final boolean hasData = data != null;
        final String errorMessage = loader.getErrorMessage();

        if(hasData){
            SubscriptionsLoader subscriptionsLoader = (SubscriptionsLoader) loader;
            final int page = subscriptionsLoader.getPage();

            if(page > 1){
                adapter.addData(data);
            }else {
                adapter.setData(data);
            }

            boolean emptyData = data.isEmpty();
            if(!emptyData){
                loadMoreListener.notifyContinueListen();
            }
        }else if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    public void reload() {
        ensureParamsHolder();
        paramsHolder.setPage(1);
        onLoadFromServer();
    }

    @Override
    protected ConnectivityStateHost getConnectivityStateHost() {
        return (ConnectivityStateHost) getParentFragment();
    }

    @Override
    public void addFakeItem() {
        adapter.addFakeItem();
    }

    @Override
    public void removeFakeItem() {
        adapter.removeFakeItem();
    }
}
