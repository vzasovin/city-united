package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.FakeItemAdapter;
import com.city_utd.city_utd.loader.IncomingInvitationsLoader;
import com.city_utd.city_utd.model.Invitation;
import com.city_utd.city_utd.task.RespondToInvitationAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.city_utd.city_utd.util.Util.Dates.DATE_TIME_FORMAT_1;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TODAY;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TOMORROW;
import static com.city_utd.city_utd.util.Util.Dates.dateToString;
import static com.city_utd.city_utd.util.Util.Dates.dateToTimeString;
import static com.city_utd.city_utd.util.Util.Dates.getDayLabel;

/**
 * Created by Admin on 26.07.2017.
 */

public class IncomingInvitationsFragment extends SimpleCachingFragment<List<Invitation>>
        implements View.OnClickListener, BaseAsyncTask.Callbacks, SwipeRefreshLayout.OnRefreshListener{

    private static final int TASK_ID_ACCEPT_INVITATION = 1;
    private static final int TASK_ID_REJECT_INVITATION = 2;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    @Bind(R.id.container)
    RecyclerView recyclerView;

    private InvitationsAdapter adapter;
    private int tempAdapterPosition = -1;

    public static IncomingInvitationsFragment newInstance() {
        return new IncomingInvitationsFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_incoming_invitations;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // setup toolbar
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);

        // setup swipe refresh layout
        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary, R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(this);

        // setup recycler view
        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new InvitationsAdapter(null);
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        recyclerView.addItemDecoration(decoration);
    }

    @Override
    public void onClick(View v) {
        getActivity().finish();
    }

    @Override
    public void onRefresh() {
        if(checkNetworkAvailability()){
            onLoadFromServer();
        }else {
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    class InvitationsAdapter extends FakeItemAdapter<Invitation, InvitationsAdapter.InvitationViewHolder> {

        private ImageUrlBuilder imageUrlBuilder;

        InvitationsAdapter(List<Invitation> data) {
            super(data);
        }

        @Override
        public InvitationViewHolder onCreateNormalViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_incoming_invitation, parent, false);
            return new InvitationViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(RecyclerView.ViewHolder holder, Invitation item) {
            InvitationViewHolder viewHolder = (InvitationViewHolder) holder;

            ensureImageUrlBuilder();

            String userImageUrl = item.getUserImageUrl();
            userImageUrl = imageUrlBuilder.build(userImageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(IncomingInvitationsFragment.this).load(userImageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(viewHolder.userImageView);

            String userName = item.getUserName();
            if(!TextUtils.isEmpty(userName)){
                viewHolder.userNameTextView.setText(userName);
                viewHolder.userNameTextView.setVisibility(View.VISIBLE);
            }else {
                viewHolder.userNameTextView.setVisibility(View.GONE);
            }

            String fieldImageUrl = item.getFieldImageUrl();
            fieldImageUrl = imageUrlBuilder.build(fieldImageUrl);
            placeholderRes = R.drawable.ic_field_placeholder;
            Glide.with(IncomingInvitationsFragment.this).load(fieldImageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(viewHolder.fieldImageView);

            String fieldName = item.getFieldName();
            if(!TextUtils.isEmpty(fieldName)){
                viewHolder.fieldNameTextView.setText(fieldName);
            }else {
                viewHolder.fieldNameTextView.setText(R.string.playing_field);
            }

            String fieldAddress = item.getFieldAddress();
            if(!TextUtils.isEmpty(fieldAddress)){
                viewHolder.fieldAddressTextView.setText(fieldAddress);
                viewHolder.fieldAddressTextView.setVisibility(View.VISIBLE);
            }else {
                viewHolder.fieldAddressTextView.setVisibility(View.GONE);
            }

            Date date = item.getDate();
            int dayLabel = getDayLabel(date);
            String dateString;
            switch (dayLabel){
                case DAY_LABEL_TODAY:
                    dateString = dateToTimeString(date);
                    dateString = getString(R.string.today_in, dateString);
                    break;
                case DAY_LABEL_TOMORROW:
                    dateString = dateToTimeString(date);
                    dateString = getString(R.string.tomorrow_in, dateString);
                    break;
                default:
                    dateString = dateToString(date, DATE_TIME_FORMAT_1);
                    break;
            }
            viewHolder.dateTextView.setText(dateString);

            String status = item.getStatus();
            @StringRes int statusRes;
            @ColorRes int colorRes;
            switch (status){
                case Invitation.STATUS_ACCEPTED:
                    statusRes = R.string.accepted;
                    colorRes = R.color.colorGreen;
                    break;
                case Invitation.STATUS_REJECTED:
                    statusRes = R.string.rejected;
                    colorRes = R.color.colorRed;
                    break;
                default:
                    statusRes = R.string.active;
                    colorRes = android.R.color.white;
                    break;
            }
            Resources resources = getResources();
            @SuppressWarnings("deprecation")
            int color = resources.getColor(colorRes);
            viewHolder.statusTextView.setText(statusRes);
            viewHolder.statusTextView.setTextColor(color);
        }

        private void ensureImageUrlBuilder(){
            if(imageUrlBuilder == null){
                imageUrlBuilder = ApiManager.newImageUrlBuilder();
            }
        }

        class InvitationViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.user_image_view)
            ImageView userImageView;

            @Bind(R.id.user_name_text_view)
            TextView userNameTextView;

            @Bind(R.id.field_image_view)
            ImageView fieldImageView;

            @Bind(R.id.field_name_text_view)
            TextView fieldNameTextView;

            @Bind(R.id.field_address_text_view)
            TextView fieldAddressTextView;

            @Bind(R.id.date_text_view)
            TextView dateTextView;

            @Bind(R.id.status_text_view)
            TextView statusTextView;

            @Bind(R.id.accept_button)
            Button acceptButton;

            @Bind(R.id.reject_button)
            Button rejectButton;

            InvitationViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                userImageView.setOnClickListener(this);
                userNameTextView.setOnClickListener(this);
                fieldImageView.setOnClickListener(this);
                fieldNameTextView.setOnClickListener(this);
                fieldAddressTextView.setOnClickListener(this);
                acceptButton.setOnClickListener(this);
                rejectButton.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                Invitation invitation = getItem(adapterPosition);
                int id = v.getId();
                switch (id){
                    case R.id.user_image_view:
                    case R.id.user_name_text_view:
                        int userId = invitation.getUserId();
                        openUserProfileActivity(userId);
                        break;
                    case R.id.field_image_view:
                    case R.id.field_name_text_view:
                    case R.id.field_address_text_view:
                        int fieldId = invitation.getFieldId();
                        openFieldDetailedActivity(fieldId);
                        break;
                    case R.id.accept_button:
                        tempAdapterPosition = adapterPosition;
                        acceptInvitation(invitation);
                        break;
                    case R.id.reject_button:
                        tempAdapterPosition = adapterPosition;
                        rejectInvitation(invitation);
                        break;
                }
            }
        }
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    private void openFieldDetailedActivity(int playingFieldId){
        Context context = getActivity();
        Intent intent = new Intent(context, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, playingFieldId);
        startActivity(intent);
    }

    private void acceptInvitation(Invitation invitation){
        respondToInvitation(invitation, true);
    }

    private void rejectInvitation(Invitation invitation){
        respondToInvitation(invitation, false);
    }

    private void respondToInvitation(Invitation invitation, boolean accept){
        if(checkNetworkAvailability()){
            respondToInvitationInternal(invitation, accept);
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void respondToInvitationInternal(Invitation invitation, boolean accept){
        int taskId = accept ? TASK_ID_ACCEPT_INVITATION : TASK_ID_REJECT_INVITATION;
        int sourceId = invitation.getUserId();
        int playingFieldId = invitation.getFieldId();
        Date date = invitation.getDate();
        RespondToInvitationAsyncTask asyncTask = new RespondToInvitationAsyncTask(
                taskId, this, accept, sourceId, playingFieldId, date);
        asyncTask.execute();
    }

    @Override
    public void onTaskStarted(int taskId) {}

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_ACCEPT_INVITATION:
            case TASK_ID_REJECT_INVITATION:
                boolean accepted = taskId == TASK_ID_ACCEPT_INVITATION;
                handleResponseOnInvitationResult(accepted);
                break;
        }
    }

    private void handleResponseOnInvitationResult(boolean accepted){
        if(tempAdapterPosition != -1){
            String status = accepted ? Invitation.STATUS_ACCEPTED : Invitation.STATUS_REJECTED;
            Invitation invitation = adapter.getItem(tempAdapterPosition);
            invitation.setStatus(status);
            adapter.notifyItemChanged(tempAdapterPosition);
            tempAdapterPosition = -1;
        }
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_SERVER_DATABASE);
        reloadData(0, args);
    }

    @Override
    protected BaseLoader<List<Invitation>> onCreateLoader(Context context, int id, Bundle args) {
        int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        return new IncomingInvitationsLoader(context, source);
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<Invitation>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<Invitation>> loader, List<Invitation> data) {
        super.onLoadFinished(loader, data);

        final int source = loader.getSource();
        final boolean fromServer = source != BaseLoader.SOURCE_DATABASE;
        final boolean hasData = data != null;
        final String errorMessage = loader.getErrorMessage();

        if(hasData){
            adapter.setData(data);
            if(fromServer){
                resetUnreadIncomingInvitationsCount();
                getActivity().setResult(Activity.RESULT_OK);
            }
        }else if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }

        if(swipeRefreshLayout.isRefreshing()){
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    private void resetUnreadIncomingInvitationsCount(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.resetUnreadIncomingInvitationsCount();
    }

    @Override
    public boolean showConnectivityState(int appearance) {
        boolean result = super.showConnectivityState(appearance);
        if(result){
            adapter.addFakeItem();
        }
        return result;
    }

    @Override
    public boolean hideConnectivityState() {
        boolean result = super.hideConnectivityState();
        if(result){
            adapter.removeFakeItem();
        }
        return result;
    }
}
