package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.MainNavigationHost;
import com.city_utd.city_utd.controller.MainNavigationPart;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vadim on 7/17/17.
 */

public class MyTeamFragment extends AbstractFragment
        implements MainNavigationPart, ConnectivityStateHost {

    @Bind(R.id.tab_layout)
    TabLayout tabLayout;

    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private MyTeamAdapter adapter;

    public static MyTeamFragment newInstance() {
        return new MyTeamFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_my_team;
    }

    @Override
    public MainNavigationHost getNavigationHost() {
        Activity activity = getActivity();
        if(activity instanceof MainNavigationHost){
            return (MainNavigationHost) activity;
        }
        return null;
    }

    @Override
    public void reloadContent() {
        SparseArray<MyTeamPart> teamParts = adapter.teamParts;
        teamParts.get(0).reload();
        teamParts.get(1).reload();
    }

    @Override
    public boolean setupFloatingActionButton(FloatingActionButton floatingActionButton) {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        FragmentManager fragmentManager = getChildFragmentManager();
        adapter = new MyTeamAdapter(fragmentManager);
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class MyTeamAdapter extends FragmentPagerAdapter{

        private static final int ITEMS_COUNT = 2;

        private final SparseArray<MyTeamPart> teamParts;

        private MyTeamAdapter(FragmentManager fm) {
            super(fm);
            teamParts = new SparseArray<>(ITEMS_COUNT);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                return SubscriptionsFragment.newInstance();
            }else {
                return FieldsSubscriptionsFragment.newInstance();
            }
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            Object item = super.instantiateItem(container, position);
            MyTeamPart teamPart = (MyTeamPart) item;
            teamParts.put(position, teamPart);
            return item;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            super.destroyItem(container, position, object);
            teamParts.delete(position);
        }

        @Override
        public int getCount() {
            return ITEMS_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return getString(R.string.players);
            }else {
                return getString(R.string.fields);
            }
        }
    }

    @Override
    public boolean shouldDisplayConnectivityState() {
        return true;
    }

    @Override
    public int getConnectivityStateFragmentAppearance() {
        return ConnectivityStateFragment.APPEARANCE_LIGHT;
    }

    @Override
    public boolean showConnectivityState(int appearance) {
        FragmentManager fragmentManager = getChildFragmentManager();
        ConnectivityStateFragment fragment = (ConnectivityStateFragment) fragmentManager
                .findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment == null){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment = ConnectivityStateFragment.newInstance(appearance);
            fragment.setOnReconnectListener(this);
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.add(R.id.connectivity_state_fragment_container, fragment);
            transaction.commit();

            addFakeItemToLists();
            return true;
        }
        return false;
    }

    private void addFakeItemToLists(){
        SparseArray<MyTeamPart> teamParts = adapter.teamParts;
        teamParts.get(0).addFakeItem();
        teamParts.get(1).addFakeItem();
    }

    @Override
    public boolean hideConnectivityState() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.remove(fragment);
            transaction.commit();

            removeFakeItems();
            return true;
        }
        return false;
    }

    private void removeFakeItems(){
        SparseArray<MyTeamPart> teamParts = adapter.teamParts;
        teamParts.get(0).removeFakeItem();
        teamParts.get(1).removeFakeItem();
    }

    @Override
    public boolean notifyReconnecting() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            ((ConnectivityStateFragment) fragment).notifyReconnecting();
            return true;
        }
        return false;
    }

    @Override
    public boolean notifyReconnectionFailed() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            ((ConnectivityStateFragment) fragment).notifyReconnectionFailed();
            return true;
        }
        return false;
    }

    @Override
    public boolean onReconnect() {
        if(checkNetworkAvailability()){
            reloadContent();
            return true;
        }
        return false;
    }
}
