package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.dialog.AuthorizationNecessityDialogFragment;
import com.city_utd.city_utd.loader.AbstractLoader;
import com.city_utd.city_utd.loader.UserLoader;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.task.SubscribeOnUserAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;

/**
 * Created by Admin on 19.07.2017.
 */

public class UserProfileFragment extends ProfileFragment implements BaseAsyncTask.Callbacks{

    private static final int ACTION_BUTTON_WIDTH_DP = 130;

    private static final int TASK_ID_SUBSCRIBE_ON_USER = 1;

    private int userId;
    private UserDetailed user;

    public static UserProfileFragment newInstance(int userId) {
        Bundle args = new Bundle();
        args.putInt(UserProfileActivity.EXTRA_USER_ID, userId);
        UserProfileFragment fragment = new UserProfileFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        userId = args.getInt(UserProfileActivity.EXTRA_USER_ID);
    }

    @Override
    protected boolean isMyProfile() {
        return false;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // setup toolbar
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        // setup action button
        ViewGroup.LayoutParams layoutParams = actionButton.getLayoutParams();
        layoutParams.width = convertDpsInPixels(ACTION_BUTTON_WIDTH_DP);
        actionButton.setLayoutParams(layoutParams);
    }

    @Override
    protected String getActionButtonText() {
        return getString(R.string.subscribe);
    }

    @Override
    protected void onActionButtonClick() {
        subscribeOrUnsubscribe();
    }

    private void subscribeOrUnsubscribe(){
        if(checkNetworkAvailability()){
            Context context = getActivity();
            LocalDataManager manager = LocalDataManager.getInstance(context);
            if(manager.isAuthorized()){
                subscribeOrUnsubscribeInternal();
            }else {
                int reason = AuthorizationNecessityDialogFragment.REASON_USER_SUBSCRIPTION;
                showAuthorizationNecessityDialog(reason);
            }
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void subscribeOrUnsubscribeInternal(){
        boolean subscribe = !user.isSubscription();
        SubscribeOnUserAsyncTask asyncTask = new SubscribeOnUserAsyncTask(TASK_ID_SUBSCRIBE_ON_USER, this, userId, subscribe);
        asyncTask.execute();
    }

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_SUBSCRIBE_ON_USER:
                UserDetailed user = (UserDetailed) result;
                this.user.setSubscription(user.isSubscription());
                updateActionButtonText();
                getActivity().setResult(Activity.RESULT_OK);
                break;
            default:
                super.onTaskFinished(taskId, result);
                break;
        }
    }

    private void updateActionButtonText(){
        boolean subscription = user.isSubscription();
        @StringRes int actionTextRes = subscription ? R.string.unsubscribe : R.string.subscribe;
        actionButton.setText(actionTextRes);
    }

    @Override
    protected UserLoader onCreateUserLoader(Context context, int source) {
        long minDuration = AbstractLoader.SUGGESTED_MIN_DURATION;
        return new UserLoader(context, source, minDuration, userId);
    }

    @Override
    protected void onLoadFinished(BaseLoader<UserDetailed> loader, UserDetailed data) {
        super.onLoadFinished(loader, data);
        if(data != null){
            user = data;
            updateActionButtonText();
        }
    }
}
