package com.city_utd.city_utd.controller.fragment;

import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.client.RateInfo;
import com.city_utd.city_utd.controller.activity.MainNavigationActivity;
import com.city_utd.city_utd.controller.activity.RateInfoActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.loader.AbstractLoader;
import com.city_utd.city_utd.loader.UserLoader;
import com.city_utd.city_utd.model.PlayerLines;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 27.07.2017.
 */

public class RateInfoFragment extends AbstractFragment
        implements LoaderManager.LoaderCallbacks<UserDetailed>,
        View.OnClickListener {

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.name_text_view)
    TextView nameTextView;

    @Bind(R.id.line_text_view)
    TextView lineTextView;

    @Bind(R.id.open_profile_button)
    Button openProfileButton;

    @Bind(R.id.date_text_view)
    TextView dateTextView;

    @Bind(R.id.char_title_1)
    TextView charTitle1;

    @Bind(R.id.rating_bar_1)
    RatingBar ratingBar1;

    @Bind(R.id.char_title_2)
    TextView charTitle2;

    @Bind(R.id.rating_bar_2)
    RatingBar ratingBar2;

    @Bind(R.id.char_title_3)
    TextView charTitle3;

    @Bind(R.id.rating_bar_3)
    RatingBar ratingBar3;

    @Bind(R.id.char_title_4)
    TextView charTitle4;

    @Bind(R.id.rating_bar_4)
    RatingBar ratingBar4;

    @Bind(R.id.char_title_5)
    TextView charTitle5;

    @Bind(R.id.rating_bar_5)
    RatingBar ratingBar5;

    @Bind(R.id.char_title_6)
    TextView charTitle6;

    @Bind(R.id.rating_bar_6)
    RatingBar ratingBar6;

    @Bind(R.id.char_title_7)
    TextView charTitle7;

    @Bind(R.id.rating_bar_7)
    RatingBar ratingBar7;

    private Rate rate;
    private int sourceId;
    private String sourceName;
    private String sourceImageUrl;
    private String sourceLine;
    private Date date;
    private String userLine;

    private boolean needsDetermineUserLine = true;

    public static RateInfoFragment newInstance(RateInfo rateInfo, String userLine) {
        Bundle args = new Bundle();
        args.putParcelable(RateInfoActivity.EXTRA_RATE_INFO, rateInfo);
        args.putString(RateInfoActivity.EXTRA_USER_LINE, userLine);
        RateInfoFragment fragment = new RateInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static RateInfoFragment newInstance(Rate rate, int sourceId, String sourceName,
                                               String sourceImageUrl, String sourceLine, Date date) {

        Bundle args = new Bundle();
        args.putParcelable(RateInfoActivity.EXTRA_RATE_INFO, rate);
        args.putInt(RateInfoActivity.EXTRA_SOURCE_ID, sourceId);
        args.putString(RateInfoActivity.EXTRA_SOURCE_NAME, sourceName);
        args.putString(RateInfoActivity.EXTRA_SOURCE_IMAGE_URL, sourceImageUrl);
        args.putString(RateInfoActivity.EXTRA_SOURCE_LINE, sourceLine);
        args.putSerializable(RateInfoActivity.EXTRA_DATE, date);
        RateInfoFragment fragment = new RateInfoFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        Rate rate = args.getParcelable(RateInfoActivity.EXTRA_RATE_INFO);
        if(rate instanceof RateInfo){
            RateInfo rateInfo = (RateInfo) rate;
            sourceId = rateInfo.getUserId();
            sourceName = rateInfo.getUserName();
            sourceImageUrl = rateInfo.getUserImageUrl();
            sourceLine = rateInfo.getUserLine();
            date = rateInfo.getLastRateDate();
            userLine = args.getString(RateInfoActivity.EXTRA_USER_LINE);
            needsDetermineUserLine = false;
        }else {
            sourceId = args.getInt(RateInfoActivity.EXTRA_SOURCE_ID);
            sourceName = args.getString(RateInfoActivity.EXTRA_SOURCE_NAME);
            sourceImageUrl = args.getString(RateInfoActivity.EXTRA_SOURCE_IMAGE_URL);
            sourceLine = args.getString(RateInfoActivity.EXTRA_SOURCE_LINE);
            date = (Date) args.getSerializable(RateInfoActivity.EXTRA_DATE);
        }
        this.rate = rate;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_rate_info;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        ImageUrlBuilder imageUrlBuilder = ApiManager.newImageUrlBuilder();
        String imageUrl = imageUrlBuilder.build(sourceImageUrl);
        @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
        Glide.with(this).load(imageUrl).placeholder(placeholderRes).error(placeholderRes).into(imageView);

        if(!TextUtils.isEmpty(sourceName)){
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(sourceName);
        }else {
            nameTextView.setVisibility(View.GONE);
        }

        Context context = getActivity();
        String lineString = Util.Commons.getLineString(context, sourceLine);
        lineTextView.setText(lineString);

        openProfileButton.setOnClickListener(this);

        String dateString = Util.Dates.dateToString(date, Util.Dates.DATE_FORMAT_1);
        dateTextView.setText(dateString);

        if(!needsDetermineUserLine){
            bindRateInfo();
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.open_profile_button:
                openUserProfile();
                break;
        }
    }

    private void openUserProfile(){
        Context context = getActivity();
        Intent firstIntent = new Intent(context, MainNavigationActivity.class);
        int destination = MainNavigationActivity.DESTINATION_MY_TEAM;
        firstIntent.putExtra(MainNavigationActivity.EXTRA_DESTINATION, destination);
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, sourceId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addNextIntent(firstIntent).addNextIntent(intent);
        stackBuilder.startActivities();
    }

    private void bindRateInfo(){
        boolean goalkeeper = PlayerLines.GOALKEEPER.equals(userLine);
        if(goalkeeper){
            bindGoalkeeperRateInfo();
        }else {
            bindFieldPlayerRateInfo();
        }
    }

    private void bindGoalkeeperRateInfo(){
        charTitle1.setText(R.string.reaction);
        ratingBar1.setRating(rate.getReaction());

        charTitle2.setText(R.string.reliability);
        ratingBar2.setRating(rate.getReliability());

        charTitle3.setText(R.string.hands);
        ratingBar3.setRating(rate.getHands());

        charTitle4.setText(R.string.feet);
        ratingBar4.setRating(rate.getFeet());

        charTitle5.setText(R.string.jump);
        ratingBar5.setRating(rate.getJump());

        charTitle6.setText(R.string.courage);
        ratingBar6.setRating(rate.getCourage());

        charTitle7.setText(R.string.friendliness);
        ratingBar7.setRating(rate.getFriendliness());
    }

    private void bindFieldPlayerRateInfo(){
        charTitle1.setText(R.string.speed);
        ratingBar1.setRating(rate.getSpeed());

        charTitle2.setText(R.string.stamina);
        ratingBar2.setRating(rate.getStamina());

        charTitle3.setText(R.string.technique);
        ratingBar3.setRating(rate.getTechnique());

        charTitle4.setText(R.string.impact_force);
        ratingBar4.setRating(rate.getImpactForce());

        charTitle5.setText(R.string.pass);
        ratingBar5.setRating(rate.getPass());

        charTitle6.setText(R.string.defense);
        ratingBar6.setRating(rate.getDefense());

        charTitle7.setText(R.string.friendliness);
        ratingBar7.setRating(rate.getFriendliness());
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(needsDetermineUserLine){
            loadUserDetailed();
        }
    }

    private void loadUserDetailed(){
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(0, null, this);
    }

    @Override
    public Loader<UserDetailed> onCreateLoader(int id, Bundle args) {
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        int userId = manager.getUserId();
        int source = BaseLoader.SOURCE_DATABASE;
        long minDuration = AbstractLoader.SUGGESTED_MIN_DURATION;
        return new UserLoader(context, source, minDuration, userId);
    }

    @Override
    public void onLoaderReset(Loader<UserDetailed> loader) {}

    @Override
    public void onLoadFinished(Loader<UserDetailed> loader, UserDetailed data) {
        if(data != null){
            userLine = data.getLine();
            bindRateInfo();
        }
    }
}
