package com.city_utd.city_utd.controller.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;

import com.city_utd.city_utd.R;
import com.imogene.android.carcase.worker.loader.BaseLoader;

/**
 * Created by Admin on 11.07.2017.
 */

public abstract class CachingFragment<D> extends LoaderFragment<D>
        implements ConnectivityStateHost{

    private static final String STATE_STATE = "com.city_utd.city_utd.CachingFragment.STATE_STATE";

    public static final int STATE_NONE = 0;
    public static final int STATE_CACHE = 1;
    public static final int STATE_SYNC = 2;
    public static final int STATE_OFFLINE = 3;
    public static final int STATE_ERROR = 4;
    public static final int STATE_NORMAL = 5;

    private int state;
    private BroadcastReceiver connectivityReceiver;
    private boolean registerConnectivityReceiverOnStart;
    private boolean restoringLoadFromCache;

    private void dispatchStateChanged(int newState){
        int oldState = state;
        state = newState;
        onStateChanged(oldState, newState);
    }

    protected void onStateChanged(int oldState, int newState){}

    protected final int getState(){
        return state;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState != null){
            state = savedInstanceState.getInt(STATE_STATE);
        }else {
            state = STATE_NONE;
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        int newState;
        if(isCachingEnabled()){
            onLoadFromCache(false);
            newState = STATE_CACHE;
        }else {
            onLoadFromServer();
            newState = STATE_SYNC;
        }
        dispatchStateChanged(newState);
    }

    protected boolean isCachingEnabled(){
        return true;
    }

    protected abstract void onLoadFromCache(boolean freshLoad);

    protected abstract void onLoadFromServer();

    @Override
    protected void onLoadFinished(BaseLoader<D> loader, D data) {
        final int source = loader.getSource();
        final boolean fromCache = source == BaseLoader.SOURCE_DATABASE;

        if(fromCache && !restoringLoadFromCache){
            if(checkNetworkAvailability()){
                onLoadFromServer();
                dispatchStateChanged(STATE_SYNC);
            }else {
                dispatchStateChanged(STATE_OFFLINE);
                registerConnectivityReceiver();
                showConnectivityStateIfNeeded();
            }
        }else if(!fromCache){
            final String errorMessage = loader.getErrorMessage();
            if(TextUtils.isEmpty(errorMessage)){
                dispatchStateChanged(STATE_NORMAL);
                hideConnectivityStateIfNeeded();
            }else {
                dispatchStateChanged(STATE_ERROR);
                notifyReconnectionFailedIfNeeded();

                if(isCachingEnabled()){
                    onLoadFromCache(true);
                    dispatchStateChanged(STATE_CACHE);
                    restoringLoadFromCache = true;
                }
            }
        }else {
            restoringLoadFromCache = false;
        }
    }

    private void registerConnectivityReceiver(){
        if(connectivityReceiver != null){
            return;
        }
        connectivityReceiver = new ConnectivityReceiver();
        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        Context context = getActivity();
        context.registerReceiver(connectivityReceiver, filter);
    }

    private class ConnectivityReceiver extends BroadcastReceiver{

        @Override
        public void onReceive(Context context, Intent intent) {
            if(checkNetworkAvailability()){
                if(state == STATE_OFFLINE || state == STATE_ERROR){
                    onLoadFromServer();
                    dispatchStateChanged(STATE_SYNC);
                    notifyReconnectingIfNeeded();
                }
                unregisterConnectivityReceiver();
            }
        }
    }

    private void unregisterConnectivityReceiver(){
        Context context = getActivity();
        context.unregisterReceiver(connectivityReceiver);
        connectivityReceiver = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_STATE, state);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(connectivityReceiver != null){
            unregisterConnectivityReceiver();
            registerConnectivityReceiverOnStart = true;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(registerConnectivityReceiverOnStart){
            registerConnectivityReceiver();
            registerConnectivityReceiverOnStart = false;
        }
    }

    protected ConnectivityStateHost getConnectivityStateHost(){
        return this;
    }

    @Override
    public boolean shouldDisplayConnectivityState(){
        return true;
    }

    private boolean checkShouldDisplayConnectivityState(){
        return getConnectivityStateHost().shouldDisplayConnectivityState();
    }

    @Override
    public int getConnectivityStateFragmentAppearance(){
        return ConnectivityStateFragment.APPEARANCE_LIGHT;
    }

    @Override
    public boolean showConnectivityState(int appearance) {
        FragmentManager fragmentManager = getChildFragmentManager();
        ConnectivityStateFragment fragment = (ConnectivityStateFragment) fragmentManager
                .findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment == null){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            fragment = ConnectivityStateFragment.newInstance(appearance);
            fragment.setOnReconnectListener(this);
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.add(R.id.connectivity_state_fragment_container, fragment);
            transaction.commit();
            return true;
        }
        return false;
    }

    private void showConnectivityStateIfNeeded(){
        if(checkShouldDisplayConnectivityState()){
            int appearance = getConnectivityStateAppearance();
            getConnectivityStateHost().showConnectivityState(appearance);
        }
    }

    private int getConnectivityStateAppearance(){
        return getConnectivityStateHost().getConnectivityStateFragmentAppearance();
    }

    @Override
    public boolean onReconnect() {
        if(checkNetworkAvailability()){
            onLoadFromServer();
            dispatchStateChanged(STATE_SYNC);
            return true;
        }
        return false;
    }

    @Override
    public boolean hideConnectivityState() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            transaction.remove(fragment);
            transaction.commit();
            return true;
        }
        return false;
    }

    private void hideConnectivityStateIfNeeded(){
        if(checkShouldDisplayConnectivityState()){
            getConnectivityStateHost().hideConnectivityState();
        }
    }

    @Override
    public boolean notifyReconnecting() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            ((ConnectivityStateFragment) fragment).notifyReconnecting();
            return true;
        }
        return false;
    }

    private void notifyReconnectingIfNeeded(){
        if(checkShouldDisplayConnectivityState()){
            getConnectivityStateHost().notifyReconnecting();
        }
    }

    @Override
    public boolean notifyReconnectionFailed() {
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.connectivity_state_fragment_container);
        if(fragment != null){
            ((ConnectivityStateFragment) fragment).notifyReconnectionFailed();
            return true;
        }
        return false;
    }

    private void notifyReconnectionFailedIfNeeded(){
        if(checkShouldDisplayConnectivityState()){
            getConnectivityStateHost().notifyReconnectionFailed();
        }
    }
}
