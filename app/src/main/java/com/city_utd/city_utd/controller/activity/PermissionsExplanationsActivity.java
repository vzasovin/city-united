package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.FineLocationPermissionExplanationFragment;

/**
 * Created by Admin on 14.07.2017.
 */

public class PermissionsExplanationsActivity extends AbstractActivity {

    @Override
    protected Fragment createFragment(Intent intent) {
        return FineLocationPermissionExplanationFragment.newInstance();
    }
}
