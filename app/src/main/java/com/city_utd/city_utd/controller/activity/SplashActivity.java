package com.city_utd.city_utd.controller.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageButton;
import android.widget.TextView;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.IdentityProviders;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.service.EnableClientJobService;
import com.city_utd.city_utd.service.RequestLocationUpdatesJobService;
import com.city_utd.city_utd.task.IdentityProviderAuthorizationAsyncTask;
import com.city_utd.city_utd.task.PhoneNumberAuthorizationAsyncTask;
import com.city_utd.city_utd.task.TwitterAuthorizationAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.view.AnimatedLogoView;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.model.VKScopes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collections;

import butterknife.Bind;
import ru.ok.android.sdk.Odnoklassniki;
import ru.ok.android.sdk.OkListener;
import ru.ok.android.sdk.Shared;
import ru.ok.android.sdk.util.OkAuthType;
import ru.ok.android.sdk.util.OkScope;

/**
 * Created by Admin on 13.07.2017.
 */

public class SplashActivity extends AbstractActivity implements View.OnClickListener,
        BaseAsyncTask.GenericCallbacks<Void, UserDetailed>{

    private static final int RC_VK_AUTHORIZATION = 10485;
    private static final int RC_PHONE_NUMBER_AUTHORIZATION = 1;

    private static final int TASK_ID_AUTHORIZE_ON_APP_SERVER = 1;

    @Bind(R.id.container)
    ConstraintLayout container;

    @Bind(R.id.logo_view)
    AnimatedLogoView logoView;

    @Bind(R.id.title)
    TextView titleTextView;

    @Bind(R.id.ok_button)
    ImageButton okButton;

    @Bind(R.id.vk_button)
    ImageButton vkButton;

    @Bind(R.id.twitter_button)
    ImageButton twitterButton;

    @Bind(R.id.facebook_button)
    ImageButton facebookButton;

    @Bind(R.id.authorize_through_phone_button)
    TextView phoneButton;

    @Bind(R.id.skip_button)
    TextView skipButton;

    @Bind(R.id.authorization_succeeded_label)
    TextView authSucceededLabel;

    @Bind(R.id.next_button)
    TextView nextButton;

    private boolean logoAnimated;
    private Handler handler;
    private long ballProgressAnimationStartTime;
    private boolean authorizationOnAppServerRunning;
    private float ballFinalRotation;
    private CallbackManager facebookCallbackManager;
    private TwitterAuthClient twitterAuthClient;

    @Override
    protected Fragment createFragment(Intent intent) {
        return null;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_splash;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            finish();
            return;
        }

        okButton.setOnClickListener(this);
        vkButton.setOnClickListener(this);
        twitterButton.setOnClickListener(this);
        facebookButton.setOnClickListener(this);
        phoneButton.setOnClickListener(this);
        skipButton.setOnClickListener(this);
        nextButton.setOnClickListener(this);
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        if(hasFocus && !logoAnimated){
            logoAnimated = true;
            logoView.animateLogo(new AnimatorListenerAdapter() {

                @Override
                public void onAnimationEnd(Animator animation) {
                    animateTitle();
                }
            });
        }
    }

    private void animateTitle(){
        titleTextView.animate().alpha(1).setDuration(500).withEndAction(new Runnable() {
            @Override
            public void run() {
                performNextStepDelayed();
            }
        });
    }

    private void performNextStepDelayed(){
        ensureHandler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                performNextStep();
            }
        }, 250);
    }

    private void ensureHandler(){
        if(handler == null){
            handler = new Handler();
        }
    }

    private void performNextStep(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(manager.getFistLaunchFlag()){
            showAuthorizationControls();
        }else if(manager.needsShowFineLocationPermissionExplanationOnLaunch()){
            openPermissionsExplanationsActivity();
        }else {
            openMainNavigationActivity();
        }
    }

    private void showAuthorizationControls(){
        setupAuthorizationControlsInitialProperties();
        showIdentityProvidersButtons();
        showPhoneAndSkipButtonsDelayed();
    }

    private void setupAuthorizationControlsInitialProperties(){
        phoneButton.setAlpha(0);
        skipButton.setAlpha(0);

        phoneButton.setVisibility(View.VISIBLE);
        skipButton.setVisibility(View.VISIBLE);

        int containerHeight = container.getHeight();
        int identityProviderButtonBottom = okButton.getBottom();
        int identityProviderButtonHeight = okButton.getHeight();
        float translation = containerHeight - identityProviderButtonBottom + identityProviderButtonHeight;

        okButton.setTranslationY(translation);
        vkButton.setTranslationY(translation);
        twitterButton.setTranslationY(translation);
        facebookButton.setTranslationY(translation);

        okButton.setVisibility(View.VISIBLE);
        vkButton.setVisibility(View.VISIBLE);
        twitterButton.setVisibility(View.VISIBLE);
        facebookButton.setVisibility(View.VISIBLE);
    }

    private void showIdentityProvidersButtons(){
        animateTranslationYProperty(okButton, vkButton, twitterButton, facebookButton);
    }

    private void animateTranslationYProperty(View... views){
        for(int i = 0; i < views.length; i++){
            long startDelay = i * 100;
            View view = views[i];
            view.animate()
                    .translationY(0)
                    .setStartDelay(startDelay)
                    .setInterpolator(new AccelerateDecelerateInterpolator());
        }
    }

    private void showPhoneAndSkipButtonsDelayed(){
        ensureHandler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showPhoneAndSkipButtons();
            }
        }, 600);
    }

    private void showPhoneAndSkipButtons(){
        animateFadeIn(phoneButton, null);
        animateFadeIn(skipButton, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animateSkipButtonInfinitely();
            }
        });
    }

    private void animateFadeIn(View view, Animator.AnimatorListener listener){
        ViewPropertyAnimator animator = view.animate().alpha(1);
        if(listener != null){
            animator.setListener(listener);
        }
    }

    private void animateSkipButtonInfinitely(){
        animateAlphaInfinitely(skipButton);
    }

    private void animateAlphaInfinitely(View view){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setDuration(800);
        view.startAnimation(animation);
    }

    private void openMainNavigationActivity(){
        Intent intent = new Intent(this, MainNavigationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    public void onClick(View view) {
        if(authorizationOnAppServerRunning){
            return;
        }

        int id = view.getId();
        switch (id){
            case R.id.ok_button:
                authorizeViaOk();
                break;
            case R.id.vk_button:
                authorizeViaVk();
                break;
            case R.id.twitter_button:
                authorizeViaTwitter();
                break;
            case R.id.facebook_button:
                authorizeViaFacebook();
                break;
            case R.id.authorize_through_phone_button:
                openAuthorizationViaPhoneNumberActivity();
                break;
            case R.id.skip_button:
                clearFirstLaunchFlag();
                openMainNavigationActivity();
                break;
            case R.id.next_button:
                openNextActivityAfterAuthorization();
                break;
        }
    }

    private void authorizeViaOk(){
        String appId = getString(R.string.ok_app_id);
        String appKey = getString(R.string.ok_app_public_key);
        String redirectUri = getString(R.string.ok_redirect_uri);

        Odnoklassniki instance = Odnoklassniki.createInstance(this, appId, appKey);
        instance.requestAuthorization(this, redirectUri, OkAuthType.ANY, OkScope.VALUABLE_ACCESS, OkScope.LONG_ACCESS_TOKEN);
    }

    private void authorizeViaVk(){
        VKSdk.login(this, VKScopes.EMAIL);
    }

    private void authorizeViaTwitter(){
        ensureTwitterAuthClient();
        twitterAuthClient.authorize(this, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                TwitterSession session = result.data;
                long userId = session.getUserId();
                String userName = session.getUserName();
                authorizeOnAppServer(userId, userName);
            }

            @Override
            public void failure(TwitterException exception) {}
        });
    }

    private void ensureTwitterAuthClient(){
        if(twitterAuthClient == null){
            twitterAuthClient = new TwitterAuthClient();
        }
    }

    private void authorizeViaFacebook(){
        ensureFacebookCallbackManager();
        LoginManager manager = LoginManager.getInstance();
        manager.registerCallback(facebookCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                AccessToken accessToken = loginResult.getAccessToken();
                String token = accessToken.getToken();
                authorizeOnAppServer(IdentityProviders.FACEBOOK, token);
            }

            @Override
            public void onCancel() {}

            @Override
            public void onError(FacebookException error) {}
        });
        manager.logInWithReadPermissions(this, Collections.singletonList("public_profile"));
    }

    private void ensureFacebookCallbackManager(){
        if(facebookCallbackManager == null){
            facebookCallbackManager = CallbackManager.Factory.create();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_VK_AUTHORIZATION:
                VKSdk.onActivityResult(requestCode, resultCode, data, new VKCallback<VKAccessToken>() {
                    @Override
                    public void onResult(VKAccessToken res) {
                        String accessToken = res.accessToken;
                        authorizeOnAppServer(IdentityProviders.VK, accessToken);
                    }

                    @Override
                    public void onError(VKError error) {}
                });
                break;
            case Shared.OK_AUTH_REQUEST_CODE:
                Odnoklassniki instance = Odnoklassniki.getInstance();
                instance.onAuthActivityResult(requestCode, resultCode, data, new OkListener() {
                    @Override
                    public void onSuccess(JSONObject json) {
                        try {
                            String accessToken = json.getString("access_token");
                            authorizeOnAppServer(IdentityProviders.OK, accessToken);
                        } catch (JSONException e) {
                            throw new RuntimeException(e);
                        }
                    }

                    @Override
                    public void onError(String error) {}
                });
                break;
            case RC_PHONE_NUMBER_AUTHORIZATION:
                if(resultCode == Activity.RESULT_OK && data != null){
                    String phoneNumber = data.getStringExtra(AuthorizationViaPhoneNumberActivity.EXTRA_PHONE_NUMBER);
                    authorizeOnAppServer(phoneNumber);
                }
                break;
            default:
                if(facebookCallbackManager == null || !facebookCallbackManager
                        .onActivityResult(requestCode, resultCode, data)){
                    if(twitterAuthClient != null){
                        twitterAuthClient.onActivityResult(requestCode, resultCode, data);
                    }
                }
                break;
        }
    }

    private void authorizeOnAppServer(String identityProvider, String accessToken){
        IdentityProviderAuthorizationAsyncTask asyncTask =
                new IdentityProviderAuthorizationAsyncTask(TASK_ID_AUTHORIZE_ON_APP_SERVER, this);
        asyncTask.execute(identityProvider, accessToken);
    }

    private void authorizeOnAppServer(long twitterId, String userName){
        TwitterAuthorizationAsyncTask asyncTask =
                new TwitterAuthorizationAsyncTask(TASK_ID_AUTHORIZE_ON_APP_SERVER, this, twitterId, userName);
        asyncTask.execute();
    }

    private void authorizeOnAppServer(String phoneNumber){
        PhoneNumberAuthorizationAsyncTask asyncTask =
                new PhoneNumberAuthorizationAsyncTask(TASK_ID_AUTHORIZE_ON_APP_SERVER, this);
        asyncTask.execute(phoneNumber);
    }

    @Override
    public void onTaskStarted(int taskId) {
        authorizationOnAppServerRunning = true;
        startBallProgressAnimation();
    }

    private void startBallProgressAnimation(){
        Animation animation = AnimationUtils.loadAnimation(this, R.anim.ball_rotation);
        animation.setRepeatMode(Animation.RESTART);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        logoView.startAnimation(animation);
        ballProgressAnimationStartTime = System.currentTimeMillis();
    }

    @Override
    public void onTaskProgress(int taskId, Void[] progress) {}

    @Override
    public void onTaskFinished(int taskId, UserDetailed userDetailed) {
        clearFirstLaunchFlag();
        requestLocationUpdatesIfNeeded();
        EnableClientJobService.enableClient();
        authorizationOnAppServerRunning = false;
        stopBallProgressAnimation();
        clearScreenAfterSuccessfulAuthorization();
        showSuccessViewsDelayed();
    }

    private void openAuthorizationViaPhoneNumberActivity(){
        Intent intent = new Intent(this, AuthorizationViaPhoneNumberActivity.class);
        startActivityForResult(intent, RC_PHONE_NUMBER_AUTHORIZATION);
    }

    private void clearFirstLaunchFlag(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        manager.setFirstLaunchFlag(false);
    }

    private void requestLocationUpdatesIfNeeded(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(!manager.needsShowFineLocationPermissionExplanationOnLaunch()){
            RequestLocationUpdatesJobService.requestLocationUpdates();
        }
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        authorizationOnAppServerRunning = false;
        stopBallProgressAnimation();
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    private void stopBallProgressAnimation(){
        long currentTime = System.currentTimeMillis();
        long duration = currentTime - ballProgressAnimationStartTime;
        ballFinalRotation += duration * 360 / 2000;

        logoView.clearAnimation();
        logoView.setRotation(ballFinalRotation);
    }

    private void clearScreenAfterSuccessfulAuthorization(){
        clearScreen(logoView, titleTextView, okButton, vkButton,
                twitterButton, facebookButton, phoneButton, skipButton);
    }

    private void clearScreen(View... views){
        final float containerCenterY = container.getHeight() / 2;
        for(View view : views){
            ViewPropertyAnimator animator = view.animate();
            animator.alpha(0).scaleX(0.8f).scaleY(0.8f);
            int viewBottom = view.getBottom();
            float translation = containerCenterY - viewBottom;
            animator.translationY(translation);
        }
    }

    private void showSuccessViewsDelayed(){
        ensureHandler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showSuccessViews();
            }
        }, 300);
    }

    private void showSuccessViews(){
        authSucceededLabel.setAlpha(0);
        nextButton.setAlpha(0);
        authSucceededLabel.setVisibility(View.VISIBLE);
        nextButton.setVisibility(View.VISIBLE);

        animateFadeIn(authSucceededLabel, null);
        animateFadeIn(nextButton, new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animateNextButtonInfinitely();
            }
        });
    }

    private void animateNextButtonInfinitely(){
        animateAlphaInfinitely(nextButton);
    }

    private void openNextActivityAfterAuthorization(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(manager.needsShowFineLocationPermissionExplanationOnLaunch()){
            openPermissionsExplanationsActivity();
        }else {
            openMainNavigationActivity();
        }
    }

    private void openPermissionsExplanationsActivity(){
        Intent intent = new Intent(this, PermissionsExplanationsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
