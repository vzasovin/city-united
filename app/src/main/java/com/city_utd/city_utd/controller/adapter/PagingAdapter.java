package com.city_utd.city_utd.controller.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by Admin on 17.07.2017.
 */

public abstract class PagingAdapter<T, VH extends RecyclerView.ViewHolder> extends BaseAdapter<T, VH> {

    public PagingAdapter(List<T> data) {
        super(data);
    }

    public final void addData(List<T> data){
        if(this.data == null){
            setData(data);
        }else {
            if(data != null){
                int size = data.size();
                if(size > 0){
                    int positionStart = getItemCount();
                    this.data.addAll(data);
                    notifyItemRangeInserted(positionStart, size);
                }
            }
        }
    }
}
