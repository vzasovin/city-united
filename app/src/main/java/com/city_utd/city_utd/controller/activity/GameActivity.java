package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.GameFragment;

import java.util.Date;

/**
 * Created by Admin on 13.08.2017.
 */

public class GameActivity extends AbstractActivity {

    public static final String EXTRA_PLAYING_FIELD_ID = "com.city_utd.city_utd.GameActivity.EXTRA_PLAYING_FIELD_ID";
    public static final String EXTRA_PLAYING_FIELD_NAME = "com.city_utd.city_utd.GameActivity.EXTRA_PLAYING_FIELD_NAME";
    public static final String EXTRA_PLAYING_FIELD_ADDRESS = "com.city_utd.city_utd.GameActivity.EXTRA_PLAYING_FIELD_ADDRESS";
    public static final String EXTRA_IMAGE_URL = "com.city_utd.city_utd.GameActivity.EXTRA_IMAGE_URL";
    public static final String EXTRA_MIN_DATE = "com.city_utd.city_utd.GameActivity.EXTRA_MIN_DATE";
    public static final String EXTRA_MAX_DATE = "com.city_utd.city_utd.GameActivity.EXTRA_MAX_DATE";
    public static final String EXTRA_DATE = "com.city_utd.city_utd.GameActivity.EXTRA_DATE";

    @Override
    protected Fragment createFragment(Intent intent) {
        int playingFieldId = intent.getIntExtra(EXTRA_PLAYING_FIELD_ID, 0);
        String playingFieldName = intent.getStringExtra(EXTRA_PLAYING_FIELD_NAME);
        String playingFieldAddress = intent.getStringExtra(EXTRA_PLAYING_FIELD_ADDRESS);
        String imageUrl = intent.getStringExtra(EXTRA_IMAGE_URL);
        Date minDate = (Date) intent.getSerializableExtra(EXTRA_MIN_DATE);
        Date maxDate = (Date) intent.getSerializableExtra(EXTRA_MAX_DATE);
        Date date = (Date) intent.getSerializableExtra(EXTRA_DATE);
        return GameFragment.newInstance(playingFieldId, playingFieldName, playingFieldAddress, imageUrl, minDate, maxDate, date);
    }
}
