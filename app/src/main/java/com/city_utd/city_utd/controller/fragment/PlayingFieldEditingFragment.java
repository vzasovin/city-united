package com.city_utd.city_utd.controller.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.target.SquaringDrawable;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.PlayingFieldData;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.MapsActivity;
import com.city_utd.city_utd.controller.activity.PlayingFieldEditingActivity;
import com.city_utd.city_utd.controller.dialog.ProgressDialogFragment;
import com.city_utd.city_utd.loader.AddressLoader;
import com.city_utd.city_utd.loader.PlayingFieldDataLoader;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.task.CreatePlayingFieldAsyncTask;
import com.city_utd.city_utd.task.UpdatePlayingFieldAsyncTask;
import com.city_utd.city_utd.util.Util;
import com.google.android.gms.maps.model.LatLng;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.loader.FilesLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;

import java.io.File;
import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 03.08.2017.
 */

public class PlayingFieldEditingFragment extends LoaderFragment<PlayingFieldData>
        implements View.OnClickListener, BaseAsyncTask.Callbacks {

    private static final String STATE_PLAYING_FIELD_DATA = "com.city_utd.city_utd.PlayingFieldEditingFragment.STATE_PLAYING_FIELD_DATA";
    private static final String STATE_TEMP_PHOTO_FILE_PATH = "com.city_utd.city_utd.PlayingFieldEditingFragment.STATE_TEMP_PHOTO_FILE_PATH";
    private static final String STATE_CLEAR_IMAGE = "com.city_utd.city_utd.PlayingFieldEditingFragment.STATE_CLEAR_IMAGE";

    private static final int RC_TAKE_PHOTO = 1;
    private static final int RC_CHOOSE_PHOTO = 2;
    private static final int RC_SPECIFY_LOCATION = 3;

    private static final int LOADER_ID_PHOTO_FILE = 1;
    private static final int LOADER_ID_ADDRESS = 2;

    private static final int TASK_ID_UPDATE = 1;
    private static final int TASK_ID_CREATE = 2;

    private static final String PROGRESS_DIALOG_TAG = "PROGRESS_DIALOG";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.take_photo)
    View takePhotoButton;

    @Bind(R.id.choose_photo)
    View choosePhotoButton;

    @Bind(R.id.delete_image)
    View deleteImageButton;

    @Bind(R.id.name_edit_text)
    EditText nameEditText;

    @Bind(R.id.address_edit_text)
    EditText addressEditText;

    @Bind(R.id.coordinates_text_view)
    TextView coordinatesTextView;

    @Bind(R.id.specify_location_button)
    Button specifyLocationButton;

    private int playingFieldId;
    private boolean editingMode;
    private PlayingFieldData playingFieldData;
    private String imageUrl;
    private String tempPhotoFilePath;
    private boolean clearImage;
    private Bundle photoFileLoaderArgs;
    private PhotoFileLoaderCallbacks photoFileLoaderCallbacks;
    private Bundle addressLoaderArgs;
    private AddressLoaderCallbacks addressLoaderCallbacks;

    public static PlayingFieldEditingFragment newInstance(int playingFieldId) {
        Bundle args = new Bundle();
        args.putInt(PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_ID, playingFieldId);
        PlayingFieldEditingFragment fragment = new PlayingFieldEditingFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        Bundle args = getArguments();
        playingFieldId = args.getInt(PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_ID, -1);
        editingMode = playingFieldId != -1;

        if(savedInstanceState != null){
            tempPhotoFilePath = savedInstanceState.getString(STATE_TEMP_PHOTO_FILE_PATH);
            clearImage = savedInstanceState.getBoolean(STATE_CLEAR_IMAGE);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_playing_field_editing;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        AbstractActivity activity = (AbstractActivity) getActivity();

        // setup mode dependent views
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);
        @StringRes int titleRes = editingMode ? R.string.field_editing : R.string.field_creation;
        toolbar.setTitle(titleRes);

        // setup on click listeners
        takePhotoButton.setOnClickListener(this);
        choosePhotoButton.setOnClickListener(this);
        deleteImageButton.setOnClickListener(this);
        specifyLocationButton.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(!handled){
            int itemId = item.getItemId();
            if(itemId == R.id.save){
                saveData();
                handled = true;
            }
        }
        return handled;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.take_photo:
                dispatchTakePhotoIntent();
                break;
            case R.id.choose_photo:
                dispatchChoosePhotoIntent();
                break;
            case R.id.delete_image:
                deleteImage();
                break;
            case R.id.specify_location_button:
                openMapsActivity();
                break;
            default:
                getActivity().finish();
                break;
        }
    }

    private void dispatchTakePhotoIntent(){
        Context context = getActivity();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = context.getPackageManager();
        if(intent.resolveActivity(packageManager) != null){
            String mimeType = "image/jpeg";
            File file;
            try {
                file = Util.Storage.createTempFile(context, mimeType);
            }catch (IOException e){
                showToast(R.string.could_not_create_file_to_store_photo, false);
                return;
            }

            String authority = "com.city_utd.city_utd.pictures";
            Uri uri = FileProvider.getUriForFile(context, authority, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, RC_TAKE_PHOTO);
            tempPhotoFilePath = file.getAbsolutePath();
        }else {
            showToast(R.string.could_not_find_suitable_app_to_make_photo, false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_TAKE_PHOTO:
                if(resultCode == Activity.RESULT_OK && !TextUtils.isEmpty(tempPhotoFilePath)){
                    bindImage(tempPhotoFilePath);
                    clearImage = false;
                }
                break;
            case RC_CHOOSE_PHOTO:
                if(resultCode == Activity.RESULT_OK && data != null){
                    Uri uri = data.getData();
                    if(uri != null){
                        copyPhotoFile(uri);
                    }
                }
                break;
            case RC_SPECIFY_LOCATION:
                if(resultCode == Activity.RESULT_OK && data != null){
                    LatLng location = data.getParcelableExtra(MapsActivity.EXTRA_LOCATION);
                    playingFieldData.setLatitude(location.latitude);
                    playingFieldData.setLongitude(location.longitude);
                    bindCoordinates();

                    if(AddressLoader.canBeUsed()){
                        loadAddress(location);
                    }
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void dispatchChoosePhotoIntent(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            Intent chooser = Intent.createChooser(intent, getString(R.string.choose_app));
            startActivityForResult(chooser, RC_CHOOSE_PHOTO);
        }else {
            showToast(R.string.could_not_find_suitable_app_to_choose_photo, false);
        }
    }

    private void copyPhotoFile(Uri uri){
        File dest;
        try {
            Context context = getActivity();
            String mimeType = "image/jpeg";
            dest = Util.Storage.createTempFile(context, mimeType);
        }catch (IOException e){
            showToast(R.string.could_not_create_file_to_store_photo, false);
            return;
        }
        updatePhotoFileLoaderCallbacks(dest);
        updatePhotoFileLoaderArgs(uri);
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.restartLoader(LOADER_ID_PHOTO_FILE, photoFileLoaderArgs, photoFileLoaderCallbacks);
    }

    private void updatePhotoFileLoaderCallbacks(File dest){
        if(photoFileLoaderCallbacks == null){
            photoFileLoaderCallbacks = new PhotoFileLoaderCallbacks();
        }
        photoFileLoaderCallbacks.dest = dest;
    }

    private void updatePhotoFileLoaderArgs(Uri uri){
        if(photoFileLoaderArgs == null){
            photoFileLoaderArgs = new Bundle();
        }
        photoFileLoaderArgs.putParcelable(FilesLoader.EXTRA_URI, uri);
    }

    private class PhotoFileLoaderCallbacks implements LoaderManager.LoaderCallbacks<FilesLoader.Result>{

        private File dest;

        @Override
        public Loader<FilesLoader.Result> onCreateLoader(int id, Bundle args) {
            Context context = getActivity();
            Uri uri = args.getParcelable(FilesLoader.EXTRA_URI);
            return new FilesLoader.Builder(context, uri).file(dest).build();
        }

        @Override
        public void onLoaderReset(Loader<FilesLoader.Result> loader) {}

        @Override
        public void onLoadFinished(Loader<FilesLoader.Result> loader, FilesLoader.Result data) {
            if(data != null && data.getItemsCount() == 1){
                FilesLoader.Item item = data.getItemAt(0);
                if(item.isSuccessful()){
                    File file = item.getFile();
                    tempPhotoFilePath = file.getAbsolutePath();
                    bindImage(tempPhotoFilePath);
                    clearImage = false;
                }else {
                    int errorCode = item.getErrorCode();
                    handleFileCopyingError(errorCode);
                }
            }
        }
    }

    private void handleFileCopyingError(int errorCode){
        @StringRes int messageRes;
        switch (errorCode){
            case FilesLoader.ERROR_TOO_LARGE_FILE:
                messageRes = R.string.too_large_file;
                break;
            case FilesLoader.ERROR_SOURCE_NOT_FOUND:
                messageRes = R.string.file_is_not_found;
                break;
            default:
                messageRes = R.string.file_reading_error;
                break;
        }
        showToast(messageRes, false);
    }

    private void loadAddress(LatLng location){
        updateAddressLoaderArgs(location);
        ensureAddressLoaderCallbacks();
        reloadData(LOADER_ID_ADDRESS, addressLoaderArgs, addressLoaderCallbacks);
    }

    private void updateAddressLoaderArgs(LatLng location){
        if(addressLoaderArgs == null){
            addressLoaderArgs = new Bundle();
        }
        addressLoaderArgs.putParcelable(AddressLoader.EXTRA_LOCATION, location);
    }

    private void ensureAddressLoaderCallbacks(){
        if(addressLoaderCallbacks == null){
            addressLoaderCallbacks = new AddressLoaderCallbacks();
        }
    }

    private class AddressLoaderCallbacks implements LoaderManager.LoaderCallbacks<Address>{

        @Override
        public Loader<Address> onCreateLoader(int id, Bundle args) {
            Context context = getActivity();
            LatLng location = args.getParcelable(AddressLoader.EXTRA_LOCATION);
            return new AddressLoader(context, location);
        }

        @Override
        public void onLoaderReset(Loader<Address> loader) {}

        @Override
        public void onLoadFinished(Loader<Address> loader, Address data) {
            if(data != null){
                bindAddress(data);
            }else {
                clearAddressInfo();
            }
        }
    }

    private void bindAddress(Address address){
        int linesCount = address.getMaxAddressLineIndex();
        if(linesCount > 0){
            StringBuilder sb = new StringBuilder();
            for(int i = 0; i < linesCount; i++){
                if(sb.length() > 0){
                    sb.append(", ");
                }
                String line = address.getAddressLine(i);
                sb.append(line);
            }
            String addressString = sb.toString();
            playingFieldData.setAddress(addressString);
            addressEditText.setText(addressString);
        }else {
            clearAddressInfo();
        }
    }

    private void clearAddressInfo(){
        playingFieldData.setAddress("");
        addressEditText.setText(null);
    }

    private void deleteImage(){
        tempPhotoFilePath = null;
        if(!TextUtils.isEmpty(imageUrl)){
            clearImage = true;
        }
        bindImage(null);
    }

    private void openMapsActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(MapsActivity.EXTRA_MODE, MapsFragment.MODE_LOCATE);
        startActivityForResult(intent, RC_SPECIFY_LOCATION);
    }

    private void saveData(){
        if(checkNetworkAvailability()){
            if(checkData()){
                prepareData();
                saveDataInternal();
            }else {
                showToast(R.string.please_specify_playing_field_location, false);
            }
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private boolean checkData(){
        return isLocationSpecified();
    }

    private boolean isLocationSpecified(){
        String address = playingFieldData.getAddress();
        if(TextUtils.isEmpty(address)){
            address = addressEditText.getText().toString();
        }
        double latitude = playingFieldData.getLatitude();
        double longitude = playingFieldData.getLongitude();
        return !TextUtils.isEmpty(address) || (latitude != 0 && longitude != 0);
    }

    private void prepareData(){
        String name = nameEditText.getText().toString();
        playingFieldData.setName(name);

        String address = addressEditText.getText().toString();
        playingFieldData.setAddress(address);
    }

    private void saveDataInternal(){
        if(editingMode){
            UpdatePlayingFieldAsyncTask asyncTask = new UpdatePlayingFieldAsyncTask(
                    TASK_ID_UPDATE, this, playingFieldId, playingFieldData, tempPhotoFilePath, clearImage);
            asyncTask.execute();
        }else {
            CreatePlayingFieldAsyncTask asyncTask = new CreatePlayingFieldAsyncTask(
                    TASK_ID_CREATE, this, playingFieldData, tempPhotoFilePath);
            asyncTask.execute();
        }
    }

    @Override
    public void onTaskStarted(int taskId) {
        @StringRes int messageRes = taskId == TASK_ID_UPDATE ?
                R.string.field_updating :
                R.string.field_creation;
        String message = getString(messageRes);
        showProgressDialog(message);
    }

    private void showProgressDialog(String message){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = ProgressDialogFragment.newInstance(message);
        dialog.show(fragmentManager, PROGRESS_DIALOG_TAG);
    }

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        hideProgressDialog();

        PlayingField playingField = (PlayingField) result;
        returnPlayingField(playingField);
    }

    private void hideProgressDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG);
        if(fragment != null){
            ((ProgressDialogFragment) fragment).dismiss();
        }
    }

    private void returnPlayingField(PlayingField playingField){
        Activity activity = getActivity();
        Intent data = new Intent();
        data.putExtra(PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_RESULT, playingField);
        activity.setResult(Activity.RESULT_OK, data);
        activity.finish();
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        hideProgressDialog();

        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(editingMode){
            loadData(0, null);
        }else if (savedInstanceState != null) {
            playingFieldData = savedInstanceState.getParcelable(STATE_PLAYING_FIELD_DATA);
        }else {
            playingFieldData = new PlayingFieldData();
        }

        if(playingFieldData != null){
            bindData();
        }
    }

    @Override
    protected BaseLoader<PlayingFieldData> onCreateLoader(Context context, int id, Bundle args) {
        return new PlayingFieldDataLoader(context, playingFieldId);
    }

    @Override
    protected void onLoaderReset(BaseLoader<PlayingFieldData> loader) {}

    @Override
    protected void onLoadFinished(BaseLoader<PlayingFieldData> loader, PlayingFieldData data) {
        if(data != null){
            playingFieldData = data;
            PlayingFieldDataLoader playingFieldDataLoader = (PlayingFieldDataLoader) loader;
            imageUrl = playingFieldDataLoader.getImageUrl();
            bindData();
        }
    }

    private void bindData(){
        String imageUri = !TextUtils.isEmpty(tempPhotoFilePath) ? tempPhotoFilePath : imageUrl;
        bindImage(imageUri);

        String name = playingFieldData.getName();
        nameEditText.setText(name);

        String address = playingFieldData.getAddress();
        addressEditText.setText(address);

        bindCoordinates();
    }

    private void bindImage(String imageUri){
        Drawable placeholder;
        if(TextUtils.isEmpty(imageUri)){
            placeholder = getDrawable(R.drawable.ic_field_placeholder);
        }else {
            placeholder = getAvatarPlaceholder();
        }
        Glide.with(this).load(imageUri)
                .placeholder(placeholder)
                .error(placeholder)
                .into(imageView);
    }

    private Drawable getAvatarPlaceholder(){
        Drawable drawable = imageView.getDrawable();
        if(drawable != null){
            Bitmap bitmap = findBitmap(drawable);
            if(bitmap != null){
                Bitmap.Config config = bitmap.getConfig();
                boolean mutable = bitmap.isMutable();
                bitmap = bitmap.copy(config, mutable);
                if(bitmap != null){
                    Resources resources = getResources();
                    return new BitmapDrawable(resources, bitmap);
                }
            }
        }

        return getDrawable(R.drawable.ic_field_placeholder);
    }

    private Bitmap findBitmap(Drawable drawable){
        Bitmap bitmap = null;
        if(drawable instanceof SquaringDrawable){
            Drawable current = drawable.getCurrent();
            if(current == drawable){
                bitmap = null;
            }else {
                bitmap = findBitmap(current);
            }
        }else if(drawable instanceof BitmapDrawable){
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        }else if(drawable instanceof GlideBitmapDrawable){
            bitmap = ((GlideBitmapDrawable) drawable).getBitmap();
        }
        return bitmap;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private Drawable getDrawable(@DrawableRes int resId){
        Resources resources = getResources();
        if(checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            return resources.getDrawable(resId, null);
        }else {
            return resources.getDrawable(resId);
        }
    }

    private void bindCoordinates(){
        double latitude = playingFieldData.getLatitude();
        double longitude = playingFieldData.getLongitude();
        if(latitude != 0 && longitude != 0){
            String coordinates = getString(R.string.coordinates, latitude, longitude);
            coordinatesTextView.setText(coordinates);
            coordinatesTextView.setVisibility(View.VISIBLE);
        }else {
            coordinatesTextView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_PLAYING_FIELD_DATA, playingFieldData);
        outState.putString(STATE_TEMP_PHOTO_FILE_PATH, tempPhotoFilePath);
        outState.putBoolean(STATE_CLEAR_IMAGE, clearImage);
    }
}
