package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.AuthorizationViaPhoneNumberActivity;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 07.08.2017.
 */

public class AuthorizationViaPhoneNumberFragment extends AbstractFragment
        implements View.OnClickListener, TextView.OnEditorActionListener {

    private static final String STATE_PERFORMING_VERIFICATION = "com.city_utd.city_utd.AuthorizationViaPhoneNumberFragment.STATE_PERFORMING_VERIFICATION";
    private static final String STATE_VERIFICATION_ID = "com.city_utd.city_utd.AuthorizationViaPhoneNumberFragment.VERIFICATION_ID";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.view_switcher)
    ViewSwitcher viewSwitcher;

    @Bind(R.id.phone_edit_text)
    EditText phoneEditText;

    @Bind(R.id.code_edit_text)
    EditText codeEditText;

    private boolean performingVerification;
    private String verificationId;

    public static AuthorizationViaPhoneNumberFragment newInstance() {
        return new AuthorizationViaPhoneNumberFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(savedInstanceState != null){
            performingVerification = savedInstanceState.getBoolean(STATE_PERFORMING_VERIFICATION);
            verificationId = savedInstanceState.getString(STATE_VERIFICATION_ID);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_authorization_via_phone_number;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        AbstractActivity activity = (AbstractActivity) getActivity();

        // setup toolbar
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);

        // setup view switcher
        int displayedChild = !TextUtils.isEmpty(verificationId) ? 1 : 0;
        viewSwitcher.setDisplayedChild(displayedChild);

        // setup input fields
        phoneEditText.setOnEditorActionListener(this);
        codeEditText.setOnEditorActionListener(this);
    }

    @Override
    public void onClick(View v) {
        getActivity().finish();
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if(actionId == EditorInfo.IME_ACTION_DONE){
            submit();
            return TextUtils.isEmpty(verificationId);
        }
        return false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.authorization_via_phone_number_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(!handled){
            int itemId = item.getItemId();
            if(itemId == R.id.done){
                submit();
                handled = true;
            }
        }
        return handled;
    }

    private void submit(){
        if(checkNetworkAvailability()){
            if(TextUtils.isEmpty(verificationId)){
                verifyPhoneNumber();
            }else {
                checkVerificationCode();
            }
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void verifyPhoneNumber(){
        String phoneNumber = phoneEditText.getText().toString();
        if(!TextUtils.isEmpty(phoneNumber)){
            verifyPhoneNumber(phoneNumber);
        }else {
            showToast(R.string.enter_phone_number, false);
        }
    }

    private void verifyPhoneNumber(String phoneNumber){
        PhoneAuthProvider provider = PhoneAuthProvider.getInstance();
        Callbacks callbacks = new Callbacks();
        provider.verifyPhoneNumber(phoneNumber, 60, TimeUnit.SECONDS, getActivity(), callbacks);
        performingVerification = true;
    }

    private class Callbacks extends PhoneAuthProvider.OnVerificationStateChangedCallbacks{

        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
            returnResult();
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            if(e instanceof FirebaseAuthInvalidCredentialsException){
                showToast(R.string.wrong_phone_number_format, false);
            }
        }

        @Override
        public void onCodeSent(String verificationId, PhoneAuthProvider.ForceResendingToken token) {
            AuthorizationViaPhoneNumberFragment.this.verificationId = verificationId;
            viewSwitcher.setDisplayedChild(1);
        }
    }

    private void returnResult(){
        Activity activity = getActivity();
        Intent data = new Intent();
        String phoneNumber = phoneEditText.getText().toString();
        data.putExtra(AuthorizationViaPhoneNumberActivity.EXTRA_PHONE_NUMBER, phoneNumber);
        activity.setResult(Activity.RESULT_OK, data);
        activity.finish();
    }

    private void checkVerificationCode(){
        String code = codeEditText.getText().toString();
        if(!TextUtils.isEmpty(code)){
            checkVerificationCode(code);
        }else {
            showToast(R.string.enter_verification_code, false);
        }
    }

    private void checkVerificationCode(String code){
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        FirebaseAuth auth = FirebaseAuth.getInstance();
        auth.signInWithCredential(credential).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    returnResult();
                }else {
                    try {
                        throw task.getException();
                    } catch (Exception e) {
                        if(e instanceof FirebaseAuthInvalidCredentialsException){
                            showToast(R.string.wrong_verification_code, false);
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_PERFORMING_VERIFICATION, performingVerification);
        outState.putString(STATE_VERIFICATION_ID, verificationId);
    }

    @Override
    public void onStart() {
        super.onStart();
        if(performingVerification){
            verifyPhoneNumber();
        }
    }
}
