package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.afollestad.materialdialogs.Theme;
import com.city_utd.city_utd.R;

/**
 * Created by Admin on 02.08.2017.
 */

public class SuccessfulInvitationDialogFragment extends DialogFragment
        implements MaterialDialog.SingleButtonCallback{

    private static final String EXTRA_FIELD_NAME = "com.city_utd.city_utd.SuccessfulInvitationDialogFragment.EXTRA_FIELD_NAME";

    private String message;

    public static SuccessfulInvitationDialogFragment newInstance(String fieldName) {
        Bundle args = new Bundle();
        args.putString(EXTRA_FIELD_NAME, fieldName);
        SuccessfulInvitationDialogFragment fragment = new SuccessfulInvitationDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        String fieldName = args.getString(EXTRA_FIELD_NAME);
        if(!TextUtils.isEmpty(fieldName)){
            message = getString(R.string.successful_invitation_message_with_name, fieldName);
        }else {
            message = getString(R.string.successful_invitation_message);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        return new MaterialDialog.Builder(context)
                .theme(Theme.LIGHT)
                .content(message)
                .positiveText(R.string.go_to_playing_field)
                .negativeText(android.R.string.ok)
                .onAny(this)
                .build();
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = which == DialogAction.POSITIVE ? Activity.RESULT_OK : Activity.RESULT_CANCELED;
            targetFragment.onActivityResult(requestCode, resultCode, null);
        }
    }
}

