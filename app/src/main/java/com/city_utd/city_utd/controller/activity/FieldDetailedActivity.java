package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.FieldDetailedFragment;

/**
 * Created by vadim on 7/16/17.
 */

public class FieldDetailedActivity extends AbstractActivity {

    public static final String EXTRA_FIELD_ID = "com.city_utd.city_utd.FieldDetailedActivity.EXTRA_FIELD_ID";
    public static final String EXTRA_DISTANCE = "com.city_utd.city_utd.FieldDetailedActivity.EXTRA_DISTANCE";

    @Override
    protected Fragment createFragment(Intent intent) {
        int fieldId = intent.getIntExtra(EXTRA_FIELD_ID, 0);
        int distance = intent.getIntExtra(EXTRA_DISTANCE, -1);
        return FieldDetailedFragment.newInstance(fieldId, distance);
    }
}
