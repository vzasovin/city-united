package com.city_utd.city_utd.controller;

import android.support.design.widget.FloatingActionButton;

/**
 * Created by Admin on 11.07.2017.
 */

public interface MainNavigationPart {

    MainNavigationHost getNavigationHost();

    void reloadContent();

    boolean setupFloatingActionButton(FloatingActionButton floatingActionButton);
}
