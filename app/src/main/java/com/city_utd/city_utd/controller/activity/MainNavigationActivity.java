package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.MainNavigationHost;
import com.city_utd.city_utd.controller.MainNavigationPart;
import com.city_utd.city_utd.controller.fragment.AwardFragment;
import com.city_utd.city_utd.controller.fragment.MapsFragment;
import com.city_utd.city_utd.controller.fragment.MyProfileFragment;
import com.city_utd.city_utd.controller.fragment.MyTeamFragment;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.view.behavior.SlideOnScrollBehavior;

import butterknife.Bind;

/**
 * Created by Admin on 06.07.2017.
 */

public class MainNavigationActivity extends AbstractActivity implements
        BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener,
        MainNavigationHost {

    public static final String EXTRA_DESTINATION = "com.city_utd.city_utd.MainNavigationActivity.EXTRA_DESTINATION";

    public static final int DESTINATION_MAPS = R.id.play;
    public static final int DESTINATION_PROFILE = R.id.my_profile;
    public static final int DESTINATION_MY_TEAM = R.id.my_team;
    public static final int DESTINATION_AWARD = R.id.month_award;

    private static final float BOTTOM_MENU_ELEVATION_DPS = 8;

    @Bind(R.id.floating_action_button)
    FloatingActionButton floatingActionButton;

    private int destination;
    private boolean setupMainNavigationPart = true;

    @Override
    protected boolean isNavigable() {
        return true;
    }

    @Override
    protected int getMode() {
        return MODE_FLOATING;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main_navigation;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(setupMainNavigationPart){
            MainNavigationPart part = getCurrentNavigationPart();
            prepareMainNavigationPart(part);
            setupMainNavigationPart = false;
        }
    }

    private void prepareMainNavigationPart(MainNavigationPart part){
        if(part.setupFloatingActionButton(floatingActionButton)){
            floatingActionButton.setVisibility(View.VISIBLE);
        }else {
            floatingActionButton.setVisibility(View.GONE);
        }
    }

    @Override
    protected Fragment createFragment(Intent intent) {
        destination = intent.getIntExtra(EXTRA_DESTINATION, DESTINATION_MAPS);
        return createDestinationFragment(destination);
    }

    private Fragment createDestinationFragment(int destination){
        switch (destination){
            case DESTINATION_MAPS:
                return MapsFragment.newInstance(MapsFragment.MODE_VIEW);
            case DESTINATION_PROFILE:
                return MyProfileFragment.newInstance();
            case DESTINATION_MY_TEAM:
                return MyTeamFragment.newInstance();
            case DESTINATION_AWARD:
                return AwardFragment.newInstance();
            default:
                throw new IllegalStateException("Unsupported destination: " + destination);
        }
    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPrepareBottomNavigationView(@NonNull BottomNavigationView navigationView) {
        Resources resources = getResources();

        navigationView.inflateMenu(R.menu.bottom_navigation_menu);

        LocalDataManager manager = LocalDataManager.getInstance(this);
        boolean authorized = manager.isAuthorized();

        Menu menu = navigationView.getMenu();
        MenuItem profileItem = menu.findItem(R.id.my_profile);
        profileItem.setEnabled(authorized);
        MenuItem myTeamItem = menu.findItem(R.id.my_team);
        myTeamItem.setEnabled(authorized);

        float elevation = convertDpsInPixels(BOTTOM_MENU_ELEVATION_DPS);
        ViewCompat.setElevation(navigationView, elevation);
        int colorPrimary = resources.getColor(R.color.colorPrimary);
        navigationView.setBackgroundColor(colorPrimary);

        int[] disabledStateSet = new int[]{-android.R.attr.state_enabled};
        int[] checkedStateSet = new int[]{android.R.attr.state_checked};
        int[] emptyStateSet = new int[]{};
        int[][] stateSets = new int[][]{disabledStateSet, checkedStateSet, emptyStateSet};

        int colorGrayLight = resources.getColor(R.color.colorGray);
        int colorAccent = resources.getColor(R.color.colorAccent);
        int colorWhite = Color.WHITE;
        int[] colors = new int[]{colorGrayLight, colorAccent, colorWhite};

        ColorStateList tintList = new ColorStateList(stateSets, colors);
        navigationView.setItemIconTintList(tintList);
        navigationView.setItemTextColor(tintList);

        navigationView.setSelectedItemId(destination);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.setOnNavigationItemReselectedListener(this);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int destination = item.getItemId();
        Fragment fragment = createDestinationFragment(destination);
        replaceContentFragment(fragment);

        MainNavigationPart part = (MainNavigationPart) fragment;
        if(part.setupFloatingActionButton(floatingActionButton)){
            floatingActionButton.setVisibility(View.VISIBLE);
        }else {
            floatingActionButton.setVisibility(View.GONE);
        }

        return true;
    }

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
        int destination = item.getItemId();
        if(shouldReloadOnReselected(destination)){
            reloadDestinationFragmentContent();
        }
    }

    private boolean shouldReloadOnReselected(int destination){
        return destination == DESTINATION_PROFILE || destination == DESTINATION_MY_TEAM;
    }

    private void reloadDestinationFragmentContent(){
        getCurrentNavigationPart().reloadContent();
    }

    private MainNavigationPart getCurrentNavigationPart(){
        return (MainNavigationPart) getContentFragment();
    }

    @Override
    public void hideBottomNavigationView() {
        toggleBottomNavigationView(false);
    }

    @Override
    public void showBottomNavigationView() {
        toggleBottomNavigationView(true);
    }

    private void toggleBottomNavigationView(boolean show){
        View navigationView = getNavigationView();
        if(navigationView != null){
            toggleView(navigationView, show);
        }
    }

    private void toggleView(View view, boolean show){
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) view.getLayoutParams();
        SlideOnScrollBehavior behavior = (SlideOnScrollBehavior) layoutParams.getBehavior();
        if(behavior != null){
            if(show){
                behavior.show(view);
            }else {
                View target = getContentFragment().getView();
                behavior.hide(view, target);
            }
        }
    }

    @Override
    public void hideFloatingActionButton() {
        toggleFloatingActionButton(false);
    }

    private void toggleFloatingActionButton(boolean show){
        toggleView(floatingActionButton, show);
    }

    @Override
    public void showFloatingActionButton() {
        toggleFloatingActionButton(true);
    }
}
