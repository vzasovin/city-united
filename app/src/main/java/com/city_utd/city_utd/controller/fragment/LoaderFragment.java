package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;

import com.imogene.android.carcase.worker.loader.BaseLoader;

/**
 * Created by vadim on 7/11/17.
 */

public abstract class LoaderFragment<D> extends AbstractFragment
        implements LoaderManager.LoaderCallbacks<D>{

    private Bundle loaderArgs;

    @Override
    public final Loader<D> onCreateLoader(int id, Bundle args) {
        Context context = getActivity();
        return onCreateLoader(context, id, args);
    }

    protected abstract BaseLoader<D> onCreateLoader(Context context, int id, Bundle args);

    @Override
    public final void onLoaderReset(Loader<D> loader) {
        onLoaderReset((BaseLoader<D>) loader);
    }

    protected abstract void onLoaderReset(BaseLoader<D> loader);

    @Override
    public final void onLoadFinished(Loader<D> loader, D data) {
        onLoadFinished((BaseLoader<D>) loader, data);
    }

    protected abstract void onLoadFinished(BaseLoader<D> loader, D data);

    protected final void loadData(int id, Bundle args, LoaderManager.LoaderCallbacks callbacks){
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(id, args, callbacks);
    }

    protected final void loadData(int id, Bundle args){
        loadData(id, args, this);
    }

    protected final void reloadData(int id, Bundle args, LoaderManager.LoaderCallbacks callbacks){
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.restartLoader(id, args, callbacks);
    }

    protected final void reloadData(int id, Bundle args){
        reloadData(id, args, this);
    }

    protected final Bundle updateLoaderArgs(int source){
        ensureLoaderArgs();
        loaderArgs.putInt(BaseLoader.EXTRA_SOURCE, source);
        return loaderArgs;
    }

    private void ensureLoaderArgs(){
        if(loaderArgs == null){
            loaderArgs = new Bundle();
        }
    }
}
