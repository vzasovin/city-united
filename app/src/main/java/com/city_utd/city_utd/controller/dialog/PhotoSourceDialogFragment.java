package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;

import com.city_utd.city_utd.R;

import butterknife.ButterKnife;

/**
 * Created by Admin on 29.07.2017.
 */

public class PhotoSourceDialogFragment extends AppCompatDialogFragment implements View.OnClickListener {

    public static final String EXTRA_PHOTO_SOURCE = "com.city_utd.city_utd.PhotoSourceDialogFragment.EXTRA_PHOTO_SOURCE";

    public static final int CAMERA = 1;
    public static final int FILE = 2;

    public static PhotoSourceDialogFragment newInstance() {
        return new PhotoSourceDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.setContentView(R.layout.dialog_photo_source);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();
        ButterKnife.findById(dialog, R.id.take_photo).setOnClickListener(this);
        ButterKnife.findById(dialog, R.id.choose_photo).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.take_photo:
                sendSelectedSource(CAMERA);
                break;
            case R.id.choose_photo:
                sendSelectedSource(FILE);
                break;
        }
    }

    private void sendSelectedSource(int source){
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = Activity.RESULT_OK;
            Intent data = new Intent();
            data.putExtra(EXTRA_PHOTO_SOURCE, source);
            targetFragment.onActivityResult(requestCode, resultCode, data);
        }
        dismiss();
    }
}
