package com.city_utd.city_utd.controller.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.widget.TextView;

import com.city_utd.city_utd.R;

import butterknife.ButterKnife;

/**
 * Created by Admin on 01.08.2017.
 */

public class ProgressDialogFragment extends AppCompatDialogFragment {

    private static final String EXTRA_MESSAGE = "com.city_utd.city_utd.ProgressDialogFragment.EXTRA_MESSAGE";

    private String message;

    public static ProgressDialogFragment newInstance(String message) {
        Bundle args = new Bundle();
        args.putString(EXTRA_MESSAGE, message);
        ProgressDialogFragment fragment = new ProgressDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        message = args.getString(EXTRA_MESSAGE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.setContentView(R.layout.dialog_progress);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();
        TextView textView = ButterKnife.findById(dialog, R.id.message_text_view);
        textView.setText(message);
    }
}
