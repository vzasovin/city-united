package com.city_utd.city_utd.controller.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.city_utd.city_utd.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 22.07.2017.
 */

public class UsersRatingFragment extends AbstractFragment implements View.OnClickListener {

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    public static UsersRatingFragment newInstance() {
        return new UsersRatingFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_users_rating;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);

        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.users_rating_type_fragment_container);
        if(fragment == null){
            fragment = UsersRatingTypeFragment.newInstance(UsersRatingTypeFragment.IN_THE_WORLD);
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(R.id.users_rating_type_fragment_container, fragment);
            transaction.commit();
        }
    }

    @Override
    public void onClick(View v) {
        getActivity().finish();
    }
}
