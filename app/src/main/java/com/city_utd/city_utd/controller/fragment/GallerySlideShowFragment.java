package com.city_utd.city_utd.controller.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.activity.GallerySlideShowActivity;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 31.07.2017.
 */

public class GallerySlideShowFragment extends AbstractFragment {

    private static final String STATE_CURRENT_POSITION = "com.city_utd.city_utd.GallerySlideShowFragment.STATE_CURRENT_POSITION";

    @Bind(R.id.view_pager)
    ViewPager viewPager;

    private ArrayList<String> gallery;
    private String selectedImageUrl;
    private int currentPosition;

    public static GallerySlideShowFragment newInstance(ArrayList<String> gallery, String selectedImageUrl) {
        Bundle args = new Bundle();
        args.putStringArrayList(GallerySlideShowActivity.EXTRA_GALLERY, gallery);
        args.putString(GallerySlideShowActivity.EXTRA_SELECTED_IMAGE_URL, selectedImageUrl);
        GallerySlideShowFragment fragment = new GallerySlideShowFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        gallery = args.getStringArrayList(GallerySlideShowActivity.EXTRA_GALLERY);
        selectedImageUrl = args.getString(GallerySlideShowActivity.EXTRA_SELECTED_IMAGE_URL);

        if(savedInstanceState != null){
            currentPosition = savedInstanceState.getInt(STATE_CURRENT_POSITION);
        }else {
            currentPosition = determineCurrentPosition();
        }
    }

    private int determineCurrentPosition(){
        for(int i = 0; i < gallery.size(); i++){
            String imageUrl = gallery.get(i);
            if(imageUrl.equals(selectedImageUrl)){
                return i;
            }
        }
        return 0;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_gallery_slide_show;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        FragmentManager fragmentManager = getChildFragmentManager();
        GalleryAdapter adapter = new GalleryAdapter(fragmentManager);
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(currentPosition, false);
    }

    private class GalleryAdapter extends FragmentStatePagerAdapter{

        private final ImageUrlBuilder imageUrlBuilder;

        private GalleryAdapter(FragmentManager fm) {
            super(fm);
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }

        @Override
        public int getCount() {
            return gallery.size();
        }

        @Override
        public Fragment getItem(int position) {
            String imageUrl = gallery.get(position);
            imageUrl = imageUrlBuilder.build(imageUrl);
            return ImageFragment.newInstance(imageUrl);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CURRENT_POSITION, currentPosition);
    }
}
