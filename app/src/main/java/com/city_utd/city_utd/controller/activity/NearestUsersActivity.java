package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.NearestUsersFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Admin on 17.07.2017.
 */

public class NearestUsersActivity extends AbstractActivity {

    public static final String EXTRA_LOCATION = "com.city_utd.city_utd.NearestUsersActivity.EXTRA_LOCATION";
    public static final String EXTRA_LATITUDE = "com.city_utd.city_utd.NearestUsersActivity.EXTRA_LATITUDE";
    public static final String EXTRA_LONGITUDE = "com.city_utd.city_utd.NearestUsersActivity.EXTRA_LONGITUDE";

    @Override
    protected Fragment createFragment(Intent intent) {
        LatLng location = intent.getParcelableExtra(EXTRA_LOCATION);
        if(location == null){
            double latitude = intent.getDoubleExtra(EXTRA_LATITUDE, 0);
            double longitude = intent.getDoubleExtra(EXTRA_LONGITUDE, 0);
            location = new LatLng(latitude, longitude);
        }
        return NearestUsersFragment.newInstance(location);
    }
}
