package com.city_utd.city_utd.controller.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.city_utd.city_utd.R;
import com.imogene.android.carcase.controller.BottomNavigationActivity;

import butterknife.ButterKnife;

/**
 * Created by Admin on 10.07.2017.
 */

public abstract class AbstractActivity extends BottomNavigationActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
    }

    @Override
    protected boolean isNavigable() {
        return false;
    }

    public final boolean isLandscapeOrientation(){
        Resources resources = getResources();
        return resources.getBoolean(R.bool.land);
    }
}
