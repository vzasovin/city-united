package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.city_utd.city_utd.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vadim on 7/15/17.
 */

public class ConnectivityStateFragment extends AbstractFragment implements View.OnClickListener {

    public static final int APPEARANCE_DARK = 1;
    public static final int APPEARANCE_LIGHT = 2;

    private static final String EXTRA_APPEARANCE = "com.city_utd.city_utd.ConnectivityStateFragment.EXTRA_APPEARANCE";

    @Bind(R.id.offline_mode_image_view)
    ImageView offlineModeImageView;

    @Bind(R.id.state_text_view)
    TextView stateTextView;

    @Bind(R.id.refresh_button)
    ImageView refreshButton;

    private int appearance;
    private OnReconnectListener onReconnectListener;
    private long progressAnimationStartTime;
    private boolean reconnecting;
    private float ballFinalRotation;

    public static ConnectivityStateFragment newInstance(int appearance) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_APPEARANCE, appearance);
        ConnectivityStateFragment fragment = new ConnectivityStateFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        appearance = args.getInt(EXTRA_APPEARANCE, APPEARANCE_LIGHT);
    }

    @Override
    protected int getLayoutRes() {
        return appearance == APPEARANCE_DARK ?
                R.layout.fragment_connectivity_state_dark :
                R.layout.fragment_connectivity_state_light;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        refreshButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.refresh_button:
                if(!reconnecting){
                    dispatchReconnection();
                }
                break;
        }
    }

    private void dispatchReconnection(){
        if(onReconnectListener != null){
            if(onReconnectListener.onReconnect()){
                notifyReconnecting();
                reconnecting = true;
            }
        }
    }

    public void notifyReconnecting(){
        changeConnectivityStateLabel(true);
        startProgressAnimation();
    }

    private void changeConnectivityStateLabel(final boolean reconnection){
        Context context = getActivity();

        if(reconnection){
            fadeOutOfflineModeImageView();
        }

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.fade_out);
        animation.setDuration(300);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setRepeatCount(1);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {}

            @Override
            public void onAnimationEnd(Animation animation) {}

            @Override
            public void onAnimationRepeat(Animation animation) {
                @StringRes int stateRes;
                if(reconnection){
                    stateRes = R.string.connecting;
                }else {
                    stateRes = R.string.offline_mode;
                    fadeInOfflineModeImageView();
                }
                stateTextView.setText(stateRes);
            }
        });
        stateTextView.startAnimation(animation);
    }

    private void fadeOutOfflineModeImageView(){
        offlineModeImageView.animate().alpha(0).withEndAction(new Runnable() {
            @Override
            public void run() {
                offlineModeImageView.setVisibility(View.GONE);
            }
        }).start();
    }

    private void fadeInOfflineModeImageView(){
        offlineModeImageView.setVisibility(View.VISIBLE);
        offlineModeImageView.animate().alpha(1);
    }

    private void startProgressAnimation(){
        Context context = getActivity();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.ball_rotation);
        animation.setRepeatMode(Animation.RESTART);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        refreshButton.startAnimation(animation);
        progressAnimationStartTime = System.currentTimeMillis();
    }

    public void setOnReconnectListener(OnReconnectListener listener){
        onReconnectListener = listener;
    }

    public interface OnReconnectListener{

        boolean onReconnect();
    }

    public void notifyReconnectionFailed(){
        changeConnectivityStateLabel(false);
        stopProgressAnimation();
        reconnecting = false;
    }

    private void stopProgressAnimation(){
        long currentTime = System.currentTimeMillis();
        long duration = currentTime - progressAnimationStartTime;
        ballFinalRotation += duration * 360 / 2000;

        refreshButton.clearAnimation();
        refreshButton.setRotation(ballFinalRotation);
    }
}
