package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.MapsFragment;

/**
 * Created by Admin on 19.07.2017.
 */

public class MapsActivity extends AbstractActivity {

    public static final String EXTRA_MODE = "com.city_utd.city_utd.MapsActivity.EXTRA_MODE";

    public static final String EXTRA_PICKED_PLAYING_FIELD_ID = "com.city_utd.city_utd.MapsActivity.EXTRA_PICKED_PLAYING_FIELD_ID";
    public static final String EXTRA_PICKED_PLAYING_FIELD_NAME = "com.city_utd.city_utd.MapsActivity.EXTRA_PICKED_PLAYING_FIELD_NAME";
    public static final String EXTRA_PICKED_DATE = "com.city_utd.city_utd.MapsActivity.EXTRA_PICKED_DATE";
    public static final String EXTRA_LOCATION = "com.city_utd.city_utd.MapsActivity.EXTRA_LOCATION";

    @Override
    protected Fragment createFragment(Intent intent) {
        int mode = intent.getIntExtra(EXTRA_MODE, MapsFragment.MODE_VIEW);
        return MapsFragment.newInstance(mode);
    }
}
