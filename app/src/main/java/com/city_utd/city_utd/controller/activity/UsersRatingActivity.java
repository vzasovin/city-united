package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.UsersRatingFragment;

/**
 * Created by Admin on 22.07.2017.
 */

public class UsersRatingActivity extends AbstractActivity {

    @Override
    protected Fragment createFragment(Intent intent) {
        return UsersRatingFragment.newInstance();
    }
}
