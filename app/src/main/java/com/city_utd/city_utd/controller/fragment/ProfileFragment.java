package com.city_utd.city_utd.controller.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.annotation.StyleRes;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Space;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.target.SquaringDrawable;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.controller.MainNavigationHost;
import com.city_utd.city_utd.controller.MainNavigationPart;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.DetailedGalleryActivity;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.GallerySlideShowActivity;
import com.city_utd.city_utd.controller.activity.ImageActivity;
import com.city_utd.city_utd.controller.activity.MapsActivity;
import com.city_utd.city_utd.controller.adapter.BaseAdapter;
import com.city_utd.city_utd.controller.dialog.AuthorizationNecessityDialogFragment;
import com.city_utd.city_utd.controller.dialog.RateUserDialogFragment;
import com.city_utd.city_utd.controller.dialog.SuccessfulInvitationDialogFragment;
import com.city_utd.city_utd.loader.UserLoader;
import com.city_utd.city_utd.model.PlayerLines;
import com.city_utd.city_utd.model.StatedRates;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.task.DeleteRateAsyncTask;
import com.city_utd.city_utd.task.InviteUserAsyncTask;
import com.city_utd.city_utd.task.RateUserAsyncTask;
import com.city_utd.city_utd.task.RemoveFromGalleryAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 19.07.2017.
 */

public abstract class ProfileFragment extends SimpleCachingFragment<UserDetailed>
        implements View.OnClickListener, MainNavigationPart, AppBarLayout.OnOffsetChangedListener,
        BaseAsyncTask.Callbacks{

    private static final String STATE_INVITATION_FIELD_ID = "com.city_utd.city_utd.ProfileFragment.STATE_INVITATION_FIELD_ID";

    private static final float APPBAR_VIEWS_APPEARING_THRESHOLD = .1f;

    private static final int RC_RATE_USER = 93475649;
    private static final int RC_PICK_FIELD_AND_DATE = 45681;
    private static final int RC_SUCCESSFUL_INVITATION = 985034;

    private static final int TASK_ID_RATE_USER = 24365276;
    private static final int TASK_ID_DELETE_RATE = 8767868;
    private static final int TASK_ID_REMOVE_FROM_GALLERY = 783443534;
    private static final int TASK_ID_INVITE_USER = 62329834;

    @Bind(R.id.app_bar_layout)
    AppBarLayout appBarLayout;

    @Bind(R.id.collapsing_toolbar_layout)
    CollapsingToolbarLayout toolbarLayout;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.line_text_view)
    TextView lineTextView;

    @Bind(R.id.rating_bar)
    RatingBar ratingBar;

    @Bind(R.id.action_button)
    Button actionButton;

    @Bind(R.id.gallery_recycler_view)
    RecyclerView galleryRecyclerView;

    @Bind(R.id.gallery_options_container)
    ViewGroup galleryOptionsContainer;

    @Bind(R.id.cancel_selection_button)
    View cancelSelectionButton;

    @Bind(R.id.delete_items_button)
    View deleteItemsButton;

    @Bind(R.id.height_text_view)
    TextView heightTextView;

    @Bind(R.id.weight_text_view)
    TextView weightTextView;

    @Bind(R.id.rating_text_view)
    TextView ratingTextView;

    @Bind(R.id.rates_text_view)
    TextView ratesTextView;

    @Bind(R.id.char_title_1)
    TextView charTextView1;

    @Bind(R.id.rating_bar_1)
    RatingBar charRatingBar1;

    @Bind(R.id.char_title_2)
    TextView charTextView2;

    @Bind(R.id.rating_bar_2)
    RatingBar charRatingBar2;

    @Bind(R.id.char_title_3)
    TextView charTextView3;

    @Bind(R.id.rating_bar_3)
    RatingBar charRatingBar3;

    @Bind(R.id.char_title_4)
    TextView charTextView4;

    @Bind(R.id.rating_bar_4)
    RatingBar charRatingBar4;

    @Bind(R.id.char_title_5)
    TextView charTextView5;

    @Bind(R.id.rating_bar_5)
    RatingBar charRatingBar5;

    @Bind(R.id.char_title_6)
    TextView charTextView6;

    @Bind(R.id.rating_bar_6)
    RatingBar charRatingBar6;

    @Bind(R.id.char_title_7)
    TextView charTextView7;

    @Bind(R.id.rating_bar_7)
    RatingBar charRatingBar7;

    @Bind(R.id.rate_button)
    TextView rateButton;

    @Bind(R.id.phone_text_view)
    TextView phoneTextView;

    @Bind(R.id.email_text_view)
    TextView emailTextView;

    @Bind(R.id.spacer)
    Space spacer;

    @Bind(R.id.invite_button)
    TextView inviteButton;

    private boolean myProfile;
    private GalleryAdapter galleryAdapter;
    private ImageUrlBuilder imageUrlBuilder;
    private boolean showAppBarViews;
    protected UserDetailed user;

    private int invitationFieldId;
    private String invitationFieldName;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        myProfile = isMyProfile();

        if(savedInstanceState != null){
            invitationFieldId = savedInstanceState.getInt(STATE_INVITATION_FIELD_ID);
        }
    }

    protected abstract boolean isMyProfile();

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        AbstractActivity activity = (AbstractActivity) getActivity();

        // setup app bar
        appBarLayout.addOnOffsetChangedListener(this);
        toolbar.setTitle("");
        lineTextView.setVisibility(View.GONE);
        ratingBar.setVisibility(View.GONE);

        imageView.setOnClickListener(this);

        // setup action button
        actionButton.setText(getActionButtonText());
        actionButton.setOnClickListener(this);
        actionButton.setVisibility(View.GONE);

        // setup gallery related views
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        galleryRecyclerView.setLayoutManager(layoutManager);
        galleryAdapter = new GalleryAdapter(null);
        galleryRecyclerView.setAdapter(galleryAdapter);
        OffsetsItemDecoration decoration = new OffsetsItemDecoration();
        galleryRecyclerView.addItemDecoration(decoration);
        galleryRecyclerView.setNestedScrollingEnabled(false);

        cancelSelectionButton.setOnClickListener(this);
        deleteItemsButton.setOnClickListener(this);

        // setup rate button
        if(!myProfile){
            rateButton.setVisibility(View.VISIBLE);
            rateButton.setOnClickListener(this);
        }

        // setup invite button
        if(!myProfile){
            inviteButton.setVisibility(View.VISIBLE);
            inviteButton.setOnClickListener(this);
        }

        // setup spacer
        if(myProfile){
            spacer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
        int totalScrollRange = appBarLayout.getTotalScrollRange();
        float fraction = Math.abs(verticalOffset / (float) totalScrollRange);
        if(showAppBarViews && fraction >= APPBAR_VIEWS_APPEARING_THRESHOLD){
            hideAppBarViews();
        }else if(!showAppBarViews && fraction <= APPBAR_VIEWS_APPEARING_THRESHOLD){
            showAppBarViews();
        }
        actionButton.setAlpha(1 - fraction);
    }

    private void hideAppBarViews(){
        showAppBarViews = false;
        animateFadeOut(ratingBar);
        animateFadeOut(lineTextView);
    }

    private void animateFadeOut(View view){
        view.animate().alpha(0).setDuration(200);
    }

    private void showAppBarViews(){
        showAppBarViews = true;
        animateFadeIn(ratingBar);
        animateFadeIn(lineTextView);
    }

    private void animateFadeIn(View view){
        view.animate().alpha(1).setDuration(200);
    }

    protected abstract String getActionButtonText();

    class GalleryAdapter extends BaseAdapter<String, GalleryAdapter.BaseViewHolder>{

        private static final int VIEW_TYPE_IMAGE = 1;
        private static final int VIEW_TYPE_BUTTON = 2;

        private SparseBooleanArray selectedPositions;

        GalleryAdapter(List<String> data) {
            super(data);
        }

        @Override
        public int getItemCount() {
            int itemCount = super.getItemCount();
            if(myProfile){
                itemCount ++;
            }
            return itemCount;
        }

        @Override
        public int getItemViewType(int position) {
            if(myProfile){
                int itemCount = getItemCount();
                if(position == itemCount - 1){
                    return VIEW_TYPE_BUTTON;
                }
            }
            return VIEW_TYPE_IMAGE;
        }

        @Override
        public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            @LayoutRes int itemViewRes;
            View itemView;
            if(viewType == VIEW_TYPE_IMAGE){
                itemViewRes = R.layout.list_item_gallery_item;
                itemView = LayoutInflater.from(parent.getContext()).inflate(itemViewRes, parent, false);
                return new ImageViewHolder(itemView);
            }else {
                itemViewRes = R.layout.list_item_gallery_button;
                itemView = LayoutInflater.from(parent.getContext()).inflate(itemViewRes, parent, false);
                return new ButtonViewHolder(itemView);
            }
        }

        @Override
        public void onBindViewHolder(BaseViewHolder holder, int position) {
            int viewType = getItemViewType(position);
            if(viewType == VIEW_TYPE_IMAGE){
                ImageViewHolder viewHolder = (ImageViewHolder) holder;
                String item = getItem(position);
                bindImageViewHolder(viewHolder, item);
            }
        }

        private void bindImageViewHolder(ImageViewHolder holder, String item){
            ensureImageUrlBuilder();
            String imageUrl = imageUrlBuilder.build(item);
            @DrawableRes int placeholderRes = R.drawable.gallery_item_placeholder_84dp;
            Glide.with(ProfileFragment.this).load(imageUrl)
                    .placeholder(placeholderRes)
                    .error(placeholderRes)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.imageView);

            int position = holder.getAdapterPosition();
            if(isPositionSelected(position)){
                holder.selectionIndicatorView.setVisibility(View.VISIBLE);
            }else {
                holder.selectionIndicatorView.setVisibility(View.GONE);
            }
        }

        @Override
        protected void onBindViewHolder(BaseViewHolder holder, String item) {}

        abstract class BaseViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            BaseViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }
        }

        class ImageViewHolder extends BaseViewHolder implements View.OnLongClickListener{

            @Bind(R.id.image_view)
            ImageView imageView;

            @Bind(R.id.selection_indicator)
            View selectionIndicatorView;

            ImageViewHolder(View itemView) {
                super(itemView);
                if(myProfile) {
                    itemView.setOnLongClickListener(this);
                }
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                if(isSelectionMode()){
                    changePositionSelected(adapterPosition);
                }else {
                    String imageUrl = getItem(adapterPosition);
                    openGallery(imageView, imageUrl);
                }
            }

            @Override
            public boolean onLongClick(View v) {
                int adapterPosition = getAdapterPosition();
                changePositionSelected(adapterPosition);
                return true;
            }
        }

        private boolean isSelectionMode(){
            return selectedPositions != null;
        }

        private void changePositionSelected(int position){
            boolean selected = isPositionSelected(position);
            setPositionSelected(position, !selected);
        }

        private boolean isPositionSelected(int position) {
            return selectedPositions != null && selectedPositions.get(position);
        }

        private void setPositionSelected(int position, boolean selected){
            if(ensureSelectedPositions()){
                galleryOptionsContainer.setVisibility(View.VISIBLE);
            }
            if(selected){
                selectedPositions.put(position, true);
            }else{
                selectedPositions.delete(position);
            }
            if(selectedPositions.size() == 0){
                selectedPositions = null;
                galleryOptionsContainer.setVisibility(View.GONE);
            }
            notifyItemChanged(position);
        }

        private boolean ensureSelectedPositions(){
            if(selectedPositions == null){
                selectedPositions = new SparseBooleanArray();
                return true;
            }
            return false;
        }

        class ButtonViewHolder extends BaseViewHolder{

            ButtonViewHolder(View itemView) {
                super(itemView);
            }

            @Override
            public void onClick(View v) {
                onAddPhotoToGallery();
            }
        }
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    protected final void openGallery(ImageView imageView, String imageUrl){
        if(user == null){
            return;
        }

        List<String> gallery = user.getGallery();
        if(gallery == null || gallery.isEmpty()){
            return;
        }

        int size = gallery.size();
        if(size == 1){
            openImageActivity(imageUrl);
        }else if(size >= 2 && size <= 6){
            openDetailedGalleryActivity(gallery, imageView, imageUrl);
        }else {
            openGallerySlideShowActivity(gallery, imageUrl);
        }
    }

    private void openImageActivity(String imageUrl){
        ensureImageUrlBuilder();
        imageUrl = imageUrlBuilder.build(imageUrl);
        Context context = getActivity();
        Intent intent = new Intent(context, ImageActivity.class);
        intent.putExtra(ImageActivity.EXTRA_IMAGE_URL, imageUrl);
        startActivity(intent);
    }

    private void openDetailedGalleryActivity(List<String> gallery, ImageView imageView, String imageUrl){
        gallery = new ArrayList<>(gallery);
        Activity activity = getActivity();
        Intent intent = new Intent(activity, DetailedGalleryActivity.class);
        intent.putStringArrayListExtra(DetailedGalleryActivity.EXTRA_GALLERY, (ArrayList<String>) gallery);
        intent.putExtra(DetailedGalleryActivity.EXTRA_SELECTED_IMAGE_URL, imageUrl);

        ViewCompat.setTransitionName(imageView, imageUrl);
        ActivityOptionsCompat options = ActivityOptionsCompat
                .makeSceneTransitionAnimation(activity, imageView, imageUrl);
        startActivity(intent, options.toBundle());
    }

    private void openGallerySlideShowActivity(List<String> gallery, String imageUrl){
        gallery = new ArrayList<>(gallery);
        Context context = getActivity();
        Intent intent = new Intent(context, GallerySlideShowActivity.class);
        intent.putStringArrayListExtra(GallerySlideShowActivity.EXTRA_GALLERY, (ArrayList<String>) gallery);
        intent.putExtra(GallerySlideShowActivity.EXTRA_SELECTED_IMAGE_URL, imageUrl);
        startActivity(intent);
    }

    protected void onAddPhotoToGallery(){}

    private class OffsetsItemDecoration extends RecyclerView.ItemDecoration{

        private static final int OFFSET_DP = 8;

        private final int offset;

        private OffsetsItemDecoration(){
            offset = convertDpsInPixels(OFFSET_DP);
        }

        @Override
        public void getItemOffsets(Rect outRect, View view,
                                   RecyclerView parent, RecyclerView.State state) {
            outRect.left = offset;

            RecyclerView.Adapter adapter = parent.getAdapter();
            int itemCount = adapter.getItemCount();
            int adapterPosition = parent.getChildAdapterPosition(view);
            if(adapterPosition == itemCount - 1){
                outRect.right = offset;
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.image_view:
                showAvatarDetailedIfPossible();
                break;
            case R.id.action_button:
                onActionButtonClick();
                break;
            case R.id.cancel_selection_button:
                galleryAdapter.selectedPositions = null;
                galleryAdapter.notifyDataSetChanged();
                galleryOptionsContainer.setVisibility(View.GONE);
                break;
            case R.id.delete_items_button:
                deleteSelectedGalleryItems();
                break;
            case R.id.rate_button:
                openRateUserDialogIfAuthorized();
                break;
            case R.id.invite_button:
                openMapsActivity();
                break;
        }
    }

    private void showAvatarDetailedIfPossible(){
        if(user == null){
            return;
        }

        String imageUrl = user.getImageUrl();
        if(TextUtils.isEmpty(imageUrl)){
            return;
        }

        Activity activity = getActivity();
        Intent intent = new Intent(activity, ImageActivity.class);
        intent.putExtra(ImageActivity.EXTRA_IMAGE_URL, imageUrl);
        startActivity(intent);
    }

    protected abstract void onActionButtonClick();

    private void deleteSelectedGalleryItems(){
        if(checkNetworkAvailability()){
            List<String> filesNames = prepareFilesNames();
            RemoveFromGalleryAsyncTask asyncTask = new RemoveFromGalleryAsyncTask(
                    TASK_ID_REMOVE_FROM_GALLERY, this, filesNames);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private List<String> prepareFilesNames(){
        int count = galleryAdapter.selectedPositions.size();
        List<String> filesNames = new ArrayList<>(count);
        List<String> gallery = galleryAdapter.getData();

        for(int i = 0; i < count; i++){
            int position = galleryAdapter.selectedPositions.keyAt(i);
            String galleryItem = gallery.get(position);
            int slashLastIndex = galleryItem.lastIndexOf("/");
            String name = galleryItem.substring(slashLastIndex + 1);
            filesNames.add(name);
        }

        return filesNames;
    }

    private void openRateUserDialogIfAuthorized(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        if(manager.isAuthorized()){
            openRateUserDialog();
        }else {
            int reason = AuthorizationNecessityDialogFragment.REASON_USER_RATE;
            showAuthorizationNecessityDialog(reason);
        }
    }

    private void openRateUserDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        String line = user.getLine();
        StatedRates rates = user.getStatedRates();
        DialogFragment dialog = RateUserDialogFragment.newInstance(line, rates);
        dialog.setTargetFragment(this, RC_RATE_USER);
        dialog.show(fragmentManager, "RATE_USER_DIALOG");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_RATE_USER:
                if(resultCode == Activity.RESULT_OK && data != null){
                    boolean deleteRate = data.getBooleanExtra(RateUserDialogFragment.EXTRA_DELETE_RATE, false);
                    if(deleteRate){
                        deleteRate();
                    }else {
                        Rate rate = data.getParcelableExtra(RateUserDialogFragment.EXTRA_RATE);
                        rateUser(rate);
                    }
                }
                break;
            case RC_PICK_FIELD_AND_DATE:
                if(resultCode == Activity.RESULT_OK && data != null){
                    invitationFieldId = data.getIntExtra(MapsActivity.EXTRA_PICKED_PLAYING_FIELD_ID, 0);
                    invitationFieldName = data.getStringExtra(MapsActivity.EXTRA_PICKED_PLAYING_FIELD_NAME);
                    Date date = (Date) data.getSerializableExtra(MapsActivity.EXTRA_PICKED_DATE);
                    inviteUser(invitationFieldId, date);
                }
                break;
            case RC_SUCCESSFUL_INVITATION:
                if(resultCode == Activity.RESULT_OK){
                    openFieldDetailedActivity(invitationFieldId);
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void deleteRate(){
        int userId = user.getId();
        DeleteRateAsyncTask asyncTask = new DeleteRateAsyncTask(TASK_ID_DELETE_RATE, this, userId);
        asyncTask.execute();
    }

    private void rateUser(Rate rate){
        int userId = user.getId();
        RateUserAsyncTask asyncTask = new RateUserAsyncTask(TASK_ID_RATE_USER, this, userId, rate);
        asyncTask.execute();
    }

    private void inviteUser(int playingFieldId, Date date){
        if(checkNetworkAvailability()){
            int userId = user.getId();
            InviteUserAsyncTask asyncTask = new InviteUserAsyncTask(
                    TASK_ID_INVITE_USER, this, userId, playingFieldId, date);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void openFieldDetailedActivity(int fieldId){
        Context context = getActivity();
        Intent intent = new Intent(context, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, fieldId);
        startActivity(intent);
    }

    @Override
    public void onTaskStarted(int taskId) {}

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_RATE_USER:
            case TASK_ID_DELETE_RATE:
                if((boolean) result){
                    reloadContent();
                    setResultOk(null);
                }
                break;
            case TASK_ID_REMOVE_FROM_GALLERY:
                if((boolean) result){
                    reloadContent();
                    galleryAdapter.selectedPositions = null;
                    galleryOptionsContainer.setVisibility(View.GONE);
                }
                break;
            case TASK_ID_INVITE_USER:
                if((boolean) result){
                    showSuccessfulInvitationDialog();
                }
                break;
        }
    }

    private void setResultOk(Intent data){
        getActivity().setResult(Activity.RESULT_OK, data);
    }

    private void showSuccessfulInvitationDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = SuccessfulInvitationDialogFragment.newInstance(invitationFieldName);
        dialog.setTargetFragment(this, RC_SUCCESSFUL_INVITATION);
        dialog.show(fragmentManager, "SUCCESSFUL_INVITATION_DIALOG");
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    private void openMapsActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, MapsActivity.class);
        intent.putExtra(MapsActivity.EXTRA_MODE, MapsFragment.MODE_PICK);
        startActivityForResult(intent, RC_PICK_FIELD_AND_DATE);
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_SERVER_DATABASE);
        reloadData(0, args);
    }

    @Override
    protected final BaseLoader<UserDetailed> onCreateLoader(Context context, int id, Bundle args) {
        int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        return onCreateUserLoader(context, source);
    }

    protected abstract UserLoader onCreateUserLoader(Context context, int source);

    @Override
    protected void onLoaderReset(BaseLoader<UserDetailed> loader) {}

    @Override
    protected void onLoadFinished(BaseLoader<UserDetailed> loader, UserDetailed data) {
        super.onLoadFinished(loader, data);
        if(data != null){
            user = data;
            bindData();
        }
    }

    protected final void bindData(){
        Context context = getActivity();

        String name = user.getName();
        Date dateOfBirth = user.getDateOfBirth();
        String title = createTitle(name, dateOfBirth);
        toolbarLayout.setTitle(title);

        ensureImageUrlBuilder();
        String imageUrl = user.getImageUrl();
        imageUrl = imageUrlBuilder.build(imageUrl);
        Drawable placeholder;
        if(TextUtils.isEmpty(imageUrl)){
            placeholder = getDrawable(R.drawable.user_placeholder_profile);
        }else {
            placeholder = getAvatarPlaceholder();
        }
        Glide.with(this).load(imageUrl)
                .placeholder(placeholder)
                .error(placeholder)
                .dontAnimate()
                .into(imageView);

        String line = user.getLine();
        String lineString = Util.Commons.getLineString(context, line);
        lineTextView.setText(lineString);
        showViewIfNeeded(lineTextView);

        float rating = user.getActualRating();
        ratingBar.setRating(rating);
        showViewIfNeeded(ratingBar);

        List<String> gallery = user.getGallery();
        if(gallery != null && !gallery.isEmpty()){
            galleryRecyclerView.setVisibility(View.VISIBLE);
            galleryAdapter.setData(gallery);
        }else {
            galleryRecyclerView.setVisibility(View.GONE);
        }

        int height = user.getHeight();
        setStat(heightTextView, height);

        int weight = user.getWeight();
        setStat(weightTextView, weight);

        String ratingString = getRatingString(rating);
        ratingTextView.setText(ratingString);

        int rates = user.getNumberOfRates();
        String ratesString = String.valueOf(rates);
        ratesTextView.setText(ratesString);

        bindCharacteristics(user);

        String email = user.getEmail();
        if(!TextUtils.isEmpty(email)){
            emailTextView.setText(email);
        }else {
            emailTextView.setText(R.string.not_specified_male);
        }

        String phone = user.getPhone();
        if(!TextUtils.isEmpty(phone)){
            phoneTextView.setText(phone);
        }else {
            phoneTextView.setText(R.string.not_specified_male);
        }

        actionButton.setVisibility(View.VISIBLE);
    }

    private String createTitle(String name, Date dateOfBirth){
        if(TextUtils.isEmpty(name)){
            name = getString(R.string.footballer);
        }

        if(dateOfBirth != null){
            int age = calculateAge(dateOfBirth);
            name += ", " + age;
        }

        return name;
    }

    private int calculateAge(Date dateOfBirth){
        Calendar now = Calendar.getInstance();
        Calendar birth = Calendar.getInstance();
        birth.setTime(dateOfBirth);

        int yearNow = now.get(Calendar.YEAR);
        int yearBirth = birth.get(Calendar.YEAR);
        int age = yearNow - yearBirth;

        int dayNow = now.get(Calendar.DAY_OF_YEAR);
        int dayBirth = birth.get(Calendar.DAY_OF_YEAR);
        if(dayBirth > dayNow){
            age--;
        }

        return age;
    }

    private Drawable getAvatarPlaceholder(){
        Drawable drawable = imageView.getDrawable();
        if(drawable != null){
            Bitmap bitmap = findBitmap(drawable);
            if(bitmap != null){
                Bitmap.Config config = bitmap.getConfig();
                boolean mutable = bitmap.isMutable();
                bitmap = bitmap.copy(config, mutable);
                if(bitmap != null){
                    Resources resources = getResources();
                    return new BitmapDrawable(resources, bitmap);
                }
            }
        }

        return getDrawable(R.drawable.user_placeholder_profile);
    }

    private Bitmap findBitmap(Drawable drawable){
        Bitmap bitmap = null;
        if(drawable instanceof SquaringDrawable){
            Drawable current = drawable.getCurrent();
            if(current == drawable){
                bitmap = null;
            }else {
                bitmap = findBitmap(current);
            }
        }else if(drawable instanceof BitmapDrawable){
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        }else if(drawable instanceof GlideBitmapDrawable){
            bitmap = ((GlideBitmapDrawable) drawable).getBitmap();
        }
        return bitmap;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    protected final Drawable getDrawable(@DrawableRes int resId){
        Resources resources = getResources();
        if(checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            return resources.getDrawable(resId, null);
        }else {
            return resources.getDrawable(resId);
        }
    }

    private void showViewIfNeeded(View view){
        if(!isVisible(view)){
            view.setAlpha(0);
            view.setVisibility(View.VISIBLE);
            animateFadeIn(view);
        }
    }

    private boolean isVisible(View view){
        return view.getVisibility() == View.VISIBLE;
    }

    private void setStat(TextView textView, int stat){
        if(stat > 0){
            setTextAppearance(textView, R.style.TextAppearance_Medium);
            textView.setTypeface(Typeface.DEFAULT_BOLD);
            textView.setText(String.valueOf(stat));
        }else {
            setTextAppearance(textView, R.style.TextAppearance_Caption_White);
            textView.setTypeface(Typeface.DEFAULT);
            textView.setText(R.string.not_specified_male);
        }
    }

    @SuppressWarnings("deprecation")
    private void setTextAppearance(TextView textView, @StyleRes int styleRes){
        textView.setTextAppearance(getActivity(), styleRes);
    }

    private String getRatingString(float rating){
        if(rating > 0){
            return getString(R.string.rating_value, rating);
        }else {
            return String.valueOf(0);
        }
    }

    private void bindCharacteristics(UserDetailed user){
        String line = user.getLine();
        if(TextUtils.isEmpty(line) || !PlayerLines.GOALKEEPER.equals(line)){
            bindFieldPlayerCharacteristics(user);
        }else {
            bindGoalkeeperCharacteristics(user);
        }
    }

    private void bindFieldPlayerCharacteristics(UserDetailed user){
        charTextView1.setText(R.string.speed);
        charRatingBar1.setRating(user.getSpeed());

        charTextView2.setText(R.string.stamina);
        charRatingBar2.setRating(user.getStamina());

        charTextView3.setText(R.string.technique);
        charRatingBar3.setRating(user.getTechnique());

        charTextView4.setText(R.string.impact_force);
        charRatingBar4.setRating(user.getImpactForce());

        charTextView5.setText(R.string.pass);
        charRatingBar5.setRating(user.getPass());

        charTextView6.setText(R.string.defense);
        charRatingBar6.setRating(user.getDefense());

        charTextView7.setText(R.string.friendliness);
        charRatingBar7.setRating(user.getFriendliness());
    }

    private void bindGoalkeeperCharacteristics(UserDetailed user){
        charTextView1.setText(R.string.reaction);
        charRatingBar1.setRating(user.getReaction());

        charTextView2.setText(R.string.reliability);
        charRatingBar2.setRating(user.getReliability());

        charTextView3.setText(R.string.hands);
        charRatingBar3.setRating(user.getHands());

        charTextView4.setText(R.string.feet);
        charRatingBar4.setRating(user.getFeet());

        charTextView5.setText(R.string.jump);
        charRatingBar5.setRating(user.getJump());

        charTextView6.setText(R.string.courage);
        charRatingBar6.setRating(user.getCourage());

        charTextView7.setText(R.string.friendliness);
        charRatingBar7.setRating(user.getFriendliness());
    }

    @Override
    public boolean shouldDisplayConnectivityState() {
        return false;
    }

    @Override
    public MainNavigationHost getNavigationHost() {
        Activity activity = getActivity();
        if(activity instanceof MainNavigationHost){
            return (MainNavigationHost) activity;
        }
        return null;
    }

    @Override
    public void reloadContent() {
        onLoadFromServer();
    }

    @Override
    public boolean setupFloatingActionButton(FloatingActionButton floatingActionButton) {
        return false;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_INVITATION_FIELD_ID, invitationFieldId);
    }
}
