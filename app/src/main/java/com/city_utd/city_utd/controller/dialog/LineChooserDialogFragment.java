package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.View;
import android.view.ViewGroup;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.model.PlayerLines;

import butterknife.ButterKnife;

/**
 * Created by Admin on 01.08.2017.
 */

public class LineChooserDialogFragment extends AppCompatDialogFragment implements View.OnClickListener {

    public static final String EXTRA_LINE = "com.city_utd.city_utd.LineChooserDialogFragment.EXTRA_LINE";

    public static LineChooserDialogFragment newInstance() {
        return new LineChooserDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.setContentView(R.layout.dialog_line_chooser);
        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();
        ViewGroup container = ButterKnife.findById(dialog, R.id.container);
        for(int i = 0; i < container.getChildCount(); i++){
            View child = container.getChildAt(i);
            if(child.getId() != View.NO_ID){
                child.setOnClickListener(this);
            }
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.goalkeeper:
                sendLine(PlayerLines.GOALKEEPER);
                break;
            case R.id.defender:
                sendLine(PlayerLines.DEFENDER);
                break;
            case R.id.midfielder:
                sendLine(PlayerLines.MIDFIELDER);
                break;
            case R.id.forward:
                sendLine(PlayerLines.FORWARD);
                break;
        }
    }

    private void sendLine(String line){
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = Activity.RESULT_OK;
            Intent data = new Intent();
            data.putExtra(EXTRA_LINE, line);
            targetFragment.onActivityResult(requestCode, resultCode, data);
        }
        dismiss();
    }
}
