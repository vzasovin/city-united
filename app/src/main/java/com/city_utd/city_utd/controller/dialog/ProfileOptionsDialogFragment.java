package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.graphics.NumberDrawable;

import butterknife.ButterKnife;

import static com.imogene.android.carcase.AppUtils.Commons.checkApiLevel;
import static com.imogene.android.carcase.AppUtils.Graphics.convertDpsInPixels;

/**
 * Created by Admin on 28.07.2017.
 */

public class ProfileOptionsDialogFragment extends AppCompatDialogFragment
        implements View.OnClickListener {

    public static final String EXTRA_OPTION = "com.city_utd.city_utd.ProfileOptionsDialogFragment.EXTRA_OPTION";

    private static final String EXTRA_HAS_DELETE_AVATAR_OPTION = "com.city_utd.city_utd.ProfileOptionsDialogFragment.EXTRA_HAS_DELETE_AVATAR_OPTION";

    public static final int OPTION_CHANGE_AVATAR = 1;
    public static final int OPTION_DELETE_AVATAR = 2;
    public static final int OPTION_ADD_PHOTO = 3;
    public static final int OPTION_INCOMING_INVITATIONS = 4;
    public static final int OPTION_EXIT = 5;

    private boolean hasDeleteAvatarOption;

    public static ProfileOptionsDialogFragment newInstance(boolean hasDeleteAvatarOption) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_HAS_DELETE_AVATAR_OPTION, hasDeleteAvatarOption);
        ProfileOptionsDialogFragment fragment = new ProfileOptionsDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        hasDeleteAvatarOption = args.getBoolean(EXTRA_HAS_DELETE_AVATAR_OPTION);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        AppCompatDialog dialog = new AppCompatDialog(context);
        dialog.setContentView(R.layout.dialog_profile_options);

        Window window = dialog.getWindow();
        if(window != null){
            WindowManager.LayoutParams layoutParams = window.getAttributes();
            layoutParams.gravity = Gravity.TOP | Gravity.END;
            if(checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
                layoutParams.flags &= ~WindowManager.LayoutParams.FLAG_DIM_BEHIND;
            }
        }

        return dialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Context context = getActivity();
        Dialog dialog = getDialog();

        // setup option views
        ViewGroup container = ButterKnife.findById(dialog, R.id.container);
        for(int i = 0; i < container.getChildCount(); i++){
            View child = container.getChildAt(i);
            int id = child.getId();
            if(id != View.NO_ID){
                child.setOnClickListener(this);
            }

            if(id == R.id.delete_avatar && !hasDeleteAvatarOption){
                child.setVisibility(View.GONE);
            }
        }

        // setup incoming invitations indicator
        LocalDataManager manager = LocalDataManager.getInstance(context);
        int unreadCount = manager.getUnreadIncomingInvitationsCount();
        if(unreadCount > 0){
            float circleSize = convertDpsInPixels(context, 24f);
            float textSize = convertDpsInPixels(context, 14f);
            Drawable numberDrawable = new NumberDrawable.Builder(context)
                    .circleSize(circleSize)
                    .textSize(textSize)
                    .number(unreadCount)
                    .build();

            TextView incomingInvitationsTextView = ButterKnife.findById(dialog, R.id.incoming_invitations);
            incomingInvitationsTextView.setCompoundDrawablesWithIntrinsicBounds(null, null, numberDrawable, null);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.change_avatar:
                sendSelectedOption(OPTION_CHANGE_AVATAR);
                break;
            case R.id.delete_avatar:
                sendSelectedOption(OPTION_DELETE_AVATAR);
                break;
            case R.id.add_photo:
                sendSelectedOption(OPTION_ADD_PHOTO);
                break;
            case R.id.incoming_invitations:
                sendSelectedOption(OPTION_INCOMING_INVITATIONS);
                break;
            case R.id.exit:
                sendSelectedOption(OPTION_EXIT);
                break;
        }
    }

    private void sendSelectedOption(int option){
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = Activity.RESULT_OK;
            Intent data = new Intent();
            data.putExtra(EXTRA_OPTION, option);
            targetFragment.onActivityResult(requestCode, resultCode, data);
        }
        dismiss();
    }
}
