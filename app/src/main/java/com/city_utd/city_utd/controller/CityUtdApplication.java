package com.city_utd.city_utd.controller;

import android.content.Context;
import android.support.multidex.MultiDex;

import com.city_utd.city_utd.client.ApiManager;
import com.crashlytics.android.Crashlytics;
import com.imogene.android.carcase.controller.BaseApplication;
import com.twitter.sdk.android.core.Twitter;
import com.vk.sdk.VKSdk;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Admin on 05.07.2017.
 */

public class CityUtdApplication extends BaseApplication {

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Context appContext = getApplicationContext();

        Fabric.with(appContext, new Crashlytics());

        ApiManager apiManager = ApiManager.getInstance();
        apiManager.setupGlide(appContext);

        VKSdk.initialize(appContext);
        Twitter.initialize(appContext);
    }
}
