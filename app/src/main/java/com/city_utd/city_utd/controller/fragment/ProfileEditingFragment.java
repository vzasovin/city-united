package com.city_utd.city_utd.controller.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.GlideBitmapDrawable;
import com.bumptech.glide.request.target.SquaringDrawable;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ProfileData;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.dialog.LineChooserDialogFragment;
import com.city_utd.city_utd.controller.dialog.ProgressDialogFragment;
import com.city_utd.city_utd.loader.ProfileDataLoader;
import com.city_utd.city_utd.task.UpdateProfileAsyncTask;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.loader.FilesLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 01.08.2017.
 */

public class ProfileEditingFragment extends LoaderFragment<ProfileData>
        implements View.OnClickListener, DatePickerDialog.OnDateSetListener,
        BaseAsyncTask.Callbacks {

    private static final String STATE_TEMP_PHOTO_FILE_PATH = "com.city_utd.city_utd.ProfileEditingFragment.STATE_TEMP_PHOTO_FILE_PATH";
    private static final String STATE_CLEAR_AVATAR = "com.city_utd.city_utd.ProfileEditingFragment.STATE_CLEAR_AVATAR";

    private static final int RC_TAKE_PHOTO = 1;
    private static final int RC_CHOOSE_PHOTO = 2;
    private static final int RC_CHOOSE_LINE = 3;

    private static final int LOADER_ID_PHOTO_FILE = 1;

    private static final int TASK_ID_UPDATE_PROFILE = 1;

    private static final String PROGRESS_DIALOG_TAG = "PROGRESS_DIALOG";

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.take_photo)
    View takePhotoButton;

    @Bind(R.id.choose_photo)
    View choosePhotoButton;

    @Bind(R.id.delete_avatar)
    View deleteAvatarButton;

    @Bind(R.id.name_edit_text)
    EditText nameEditText;

    @Bind(R.id.line_button)
    View lineButton;

    @Bind(R.id.line_text_view)
    TextView lineTextView;

    @Bind(R.id.date_of_birth_button)
    View dateOfBirthButton;

    @Bind(R.id.date_of_birth_text_view)
    TextView dateOfBirthTextView;

    @Bind(R.id.height_edit_text)
    EditText heightEditText;

    @Bind(R.id.weight_edit_text)
    EditText weightEditText;

    @Bind(R.id.phone_edit_text)
    EditText phoneEditText;

    @Bind(R.id.email_edit_text)
    EditText emailEditText;

    private ProfileData profileData;
    private String imageUrl;
    private String tempPhotoFilePath;
    private boolean clearAvatar;
    private PhotoFileLoaderCallbacks photoFileLoaderCallbacks;
    private Bundle photoFileLoaderArgs;

    public static ProfileEditingFragment newInstance() {
        return new ProfileEditingFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if(savedInstanceState != null){
            tempPhotoFilePath = savedInstanceState.getString(STATE_TEMP_PHOTO_FILE_PATH);
            clearAvatar = savedInstanceState.getBoolean(STATE_CLEAR_AVATAR);
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_profile_editing;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        AbstractActivity activity = (AbstractActivity) getActivity();

        // setup toolbar
        activity.setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);

        // setup take photo button
        if(!checkHasCameraFeature()){
            takePhotoButton.setVisibility(View.GONE);
        }else {
            takePhotoButton.setOnClickListener(this);
        }

        // setup on click listeners
        choosePhotoButton.setOnClickListener(this);
        deleteAvatarButton.setOnClickListener(this);
        lineButton.setOnClickListener(this);
        dateOfBirthButton.setOnClickListener(this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.save_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(!handled){
            int itemId = item.getItemId();
            if(itemId == R.id.save){
                saveData();
                handled = true;
            }
        }
        return handled;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.take_photo:
                dispatchTakePhotoIntent();
                break;
            case R.id.choose_photo:
                dispatchChoosePhotoIntent();
                break;
            case R.id.delete_avatar:
                deleteAvatar();
                break;
            case R.id.line_button:
                showLineChooserDialog();
                break;
            case R.id.date_of_birth_button:
                showDatePickerDialog();
                break;
            default:
                Activity activity = getActivity();
                activity.setResult(Activity.RESULT_CANCELED);
                activity.finish();
                break;
        }
    }

    private void dispatchTakePhotoIntent(){
        Context context = getActivity();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = context.getPackageManager();
        if(intent.resolveActivity(packageManager) != null){
            String mimeType = "image/jpeg";
            File file;
            try {
                file = Util.Storage.createTempFile(context, mimeType);
            }catch (IOException e){
                showToast(R.string.could_not_create_file_to_store_photo, false);
                return;
            }

            String authority = "com.city_utd.city_utd.pictures";
            Uri uri = FileProvider.getUriForFile(context, authority, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, RC_TAKE_PHOTO);
            tempPhotoFilePath = file.getAbsolutePath();
        }else {
            showToast(R.string.could_not_find_suitable_app_to_make_photo, false);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_TAKE_PHOTO:
                if(resultCode == Activity.RESULT_OK && !TextUtils.isEmpty(tempPhotoFilePath)){
                    bindAvatar(tempPhotoFilePath);
                    clearAvatar = false;
                }
                break;
            case RC_CHOOSE_PHOTO:
                if(resultCode == Activity.RESULT_OK && data != null){
                    Uri uri = data.getData();
                    if(uri != null){
                        copyPhotoFile(uri);
                    }
                }
                break;
            case RC_CHOOSE_LINE:
                if(resultCode == Activity.RESULT_OK && data != null){
                    String line = data.getStringExtra(LineChooserDialogFragment.EXTRA_LINE);
                    profileData.setLine(line);
                    bindLine();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    private void dispatchChoosePhotoIntent(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            Intent chooser = Intent.createChooser(intent, getString(R.string.choose_app));
            startActivityForResult(chooser, RC_CHOOSE_PHOTO);
        }else {
            showToast(R.string.could_not_find_suitable_app_to_choose_photo, false);
        }
    }

    private void copyPhotoFile(Uri uri){
        File dest;
        try {
            Context context = getActivity();
            String mimeType = "image/jpeg";
            dest = Util.Storage.createTempFile(context, mimeType);
        }catch (IOException e){
            showToast(R.string.could_not_create_file_to_store_photo, false);
            return;
        }
        updatePhotoFileLoaderCallbacks(dest);
        updatePhotoFileLoaderArgs(uri);
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.restartLoader(LOADER_ID_PHOTO_FILE, photoFileLoaderArgs, photoFileLoaderCallbacks);
    }

    private void updatePhotoFileLoaderCallbacks(File dest){
        if(photoFileLoaderCallbacks == null){
            photoFileLoaderCallbacks = new PhotoFileLoaderCallbacks();
        }
        photoFileLoaderCallbacks.dest = dest;
    }

    private void updatePhotoFileLoaderArgs(Uri uri){
        if(photoFileLoaderArgs == null){
            photoFileLoaderArgs = new Bundle();
        }
        photoFileLoaderArgs.putParcelable(FilesLoader.EXTRA_URI, uri);
    }

    private class PhotoFileLoaderCallbacks implements LoaderManager.LoaderCallbacks<FilesLoader.Result>{

        private File dest;

        @Override
        public Loader<FilesLoader.Result> onCreateLoader(int id, Bundle args) {
            Context context = getActivity();
            Uri uri = args.getParcelable(FilesLoader.EXTRA_URI);
            return new FilesLoader.Builder(context, uri).file(dest).build();
        }

        @Override
        public void onLoaderReset(Loader<FilesLoader.Result> loader) {}

        @Override
        public void onLoadFinished(Loader<FilesLoader.Result> loader, FilesLoader.Result data) {
            if(data != null && data.getItemsCount() == 1){
                FilesLoader.Item item = data.getItemAt(0);
                if(item.isSuccessful()){
                    File file = item.getFile();
                    tempPhotoFilePath = file.getAbsolutePath();
                    bindAvatar(tempPhotoFilePath);
                    clearAvatar = false;
                }else {
                    int errorCode = item.getErrorCode();
                    handleFileCopyingError(errorCode);
                }
            }
        }
    }

    private void handleFileCopyingError(int errorCode){
        @StringRes int messageRes;
        switch (errorCode){
            case FilesLoader.ERROR_TOO_LARGE_FILE:
                messageRes = R.string.too_large_file;
                break;
            case FilesLoader.ERROR_SOURCE_NOT_FOUND:
                messageRes = R.string.file_is_not_found;
                break;
            default:
                messageRes = R.string.file_reading_error;
                break;
        }
        showToast(messageRes, false);
    }

    private void deleteAvatar(){
        tempPhotoFilePath = null;
        if(!TextUtils.isEmpty(imageUrl)){
            clearAvatar = true;
        }
        bindAvatar(null);
    }

    private void showLineChooserDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = LineChooserDialogFragment.newInstance();
        dialog.setTargetFragment(this, RC_CHOOSE_LINE);
        dialog.show(fragmentManager, "LINE_CHOOSER_DIALOG");
    }

    private void showDatePickerDialog(){
        Calendar calendar = getInitialDateForPicker();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = DatePickerDialog.newInstance(this, year, month, day);
        dialog.setMaxDate(Calendar.getInstance());
        dialog.show(getFragmentManager(), "DATE_PICKER_DIALOG");
    }

    private Calendar getInitialDateForPicker(){
        Calendar calendar = Calendar.getInstance();
        Date date = profileData.getDateOfBirth();
        if(date != null){
            calendar.setTime(date);
        }else {
            calendar.set(Calendar.YEAR, 1990);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
        }
        return calendar;
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        Date date = calendar.getTime();
        profileData.setDateOfBirth(date);
        bindDateOfBirth();
    }

    private void saveData(){
        if(checkNetworkAvailability()){
            prepareProfileData();

            UpdateProfileAsyncTask asyncTask = new UpdateProfileAsyncTask(
                    TASK_ID_UPDATE_PROFILE, this, profileData, tempPhotoFilePath, clearAvatar);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void prepareProfileData(){
        String name = nameEditText.getText().toString();
        profileData.setName(name);

        String heightString = heightEditText.getText().toString();
        int height = !TextUtils.isEmpty(heightString) ? Integer.parseInt(heightString) : 0;
        profileData.setHeight(height);

        String weightString = weightEditText.getText().toString();
        int weight = !TextUtils.isEmpty(weightString) ? Integer.parseInt(weightString) : 0;
        profileData.setWeight(weight);

        String phone = phoneEditText.getText().toString();
        profileData.setPhone(phone);

        String email = emailEditText.getText().toString();
        profileData.setEmail(email);
    }

    @Override
    public void onTaskStarted(int taskId) {
        switch (taskId){
            case TASK_ID_UPDATE_PROFILE:
                String message = getString(R.string.profile_updating);
                showProgressDialog(message);
                break;
        }
    }

    private void showProgressDialog(String message){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = ProgressDialogFragment.newInstance(message);
        dialog.show(fragmentManager, PROGRESS_DIALOG_TAG);
    }

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        hideProgressDialogIfNeeded();
        if(result != null){
            Activity activity = getActivity();
            activity.setResult(Activity.RESULT_OK);
            activity.finish();
        }
    }

    private void hideProgressDialogIfNeeded(){
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(PROGRESS_DIALOG_TAG);
        if(fragment != null){
            ((DialogFragment) fragment).dismiss();
        }
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        hideProgressDialogIfNeeded();
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    private boolean checkHasCameraFeature(){
        Context context = getActivity();
        PackageManager manager = context.getPackageManager();
        return manager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData(0, null);
    }

    @Override
    protected BaseLoader<ProfileData> onCreateLoader(Context context, int id, Bundle args) {
        return new ProfileDataLoader(context);
    }

    @Override
    protected void onLoaderReset(BaseLoader<ProfileData> loader) {}

    @Override
    protected void onLoadFinished(BaseLoader<ProfileData> loader, ProfileData data) {
        if(data != null){
            profileData = data;
            ProfileDataLoader dataLoader = (ProfileDataLoader) loader;
            imageUrl = dataLoader.getImageUrl();
            bindData();
        }
    }

    private void bindData(){
        String imageUri = !TextUtils.isEmpty(tempPhotoFilePath) ? tempPhotoFilePath : imageUrl;
        bindAvatar(imageUri);
        nameEditText.setText(profileData.getName());

        bindLine();
        bindDateOfBirth();

        int height = profileData.getHeight();
        if(height > 0){
            heightEditText.setText(String.valueOf(height));
        }

        int weight = profileData.getWeight();
        if(weight > 0){
            weightEditText.setText(String.valueOf(weight));
        }

        phoneEditText.setText(profileData.getPhone());
        emailEditText.setText(profileData.getEmail());
    }

    private void bindAvatar(String imageUri){
        Drawable placeholder;
        if(TextUtils.isEmpty(imageUri)){
            placeholder = getDrawable(R.drawable.user_placeholder_84dp);
        }else {
            placeholder = getAvatarPlaceholder();
        }
        Glide.with(this).load(imageUri)
                .placeholder(placeholder)
                .error(placeholder)
                .into(imageView);
    }

    private Drawable getAvatarPlaceholder(){
        Drawable drawable = imageView.getDrawable();
        if(drawable != null){
            Bitmap bitmap = findBitmap(drawable);
            if(bitmap != null){
                Bitmap.Config config = bitmap.getConfig();
                boolean mutable = bitmap.isMutable();
                bitmap = bitmap.copy(config, mutable);
                if(bitmap != null){
                    Resources resources = getResources();
                    return new BitmapDrawable(resources, bitmap);
                }
            }
        }

        return getDrawable(R.drawable.user_placeholder_84dp);
    }

    private Bitmap findBitmap(Drawable drawable){
        Bitmap bitmap = null;
        if(drawable instanceof SquaringDrawable){
            Drawable current = drawable.getCurrent();
            if(current == drawable){
                bitmap = null;
            }else {
                bitmap = findBitmap(current);
            }
        }else if(drawable instanceof BitmapDrawable){
            bitmap = ((BitmapDrawable) drawable).getBitmap();
        }else if(drawable instanceof GlideBitmapDrawable){
            bitmap = ((GlideBitmapDrawable) drawable).getBitmap();
        }
        return bitmap;
    }

    @SuppressLint("NewApi")
    @SuppressWarnings("deprecation")
    private Drawable getDrawable(@DrawableRes int resId){
        Resources resources = getResources();
        if(checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            return resources.getDrawable(resId, null);
        }else {
            return resources.getDrawable(resId);
        }
    }

    private void bindLine(){
        Context context = getActivity();
        String line = profileData.getLine();
        String defaultValue = getString(R.string.not_specified);
        String lineString = Util.Commons.getLineString(context, line, defaultValue);
        lineTextView.setText(lineString);
    }

    private void bindDateOfBirth(){
        Date dateOfBirth = profileData.getDateOfBirth();
        String dateString;
        if(dateOfBirth != null){
            dateString = Util.Dates.dateToString(dateOfBirth, Util.Dates.DATE_FORMAT_1);
        }else {
            dateString = getString(R.string.not_specified_female);
        }
        dateOfBirthTextView.setText(dateString);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(STATE_TEMP_PHOTO_FILE_PATH, tempPhotoFilePath);
        outState.putBoolean(STATE_CLEAR_AVATAR, clearAvatar);
    }
}
