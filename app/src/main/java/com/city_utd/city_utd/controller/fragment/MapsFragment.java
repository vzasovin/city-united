package com.city_utd.city_utd.controller.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.NearestQueryParamsHolder;
import com.city_utd.city_utd.controller.MainNavigationHost;
import com.city_utd.city_utd.controller.MainNavigationPart;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.MapsActivity;
import com.city_utd.city_utd.controller.activity.PlayingFieldEditingActivity;
import com.city_utd.city_utd.controller.dialog.PermissionExplanationDialogFragment;
import com.city_utd.city_utd.loader.NearestFieldsLoader;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.service.SyncLocationJobService;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.clustering.FieldClusterItem;
import com.city_utd.city_utd.util.clustering.FieldsClusterRenderer;
import com.city_utd.city_utd.util.clustering.MapManager;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.imogene.android.carcase.AppUtils;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 11.07.2017.
 */

public class MapsFragment extends CachingFragment<List<PlayingField>>
        implements OnMapReadyCallback, View.OnClickListener,
        GoogleMap.OnMapClickListener, GoogleMap.OnCameraMoveStartedListener, MainNavigationPart,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener,
        ClusterManager.OnClusterItemClickListener<FieldClusterItem>,
        ClusterManager.OnClusterClickListener<FieldClusterItem>, MapManager.OnTriggerReloadListener{

    public static final int MODE_VIEW = 1;
    public static final int MODE_PICK = 2;
    public static final int MODE_LOCATE = 3;
    
    private static final String EXTRA_MODE = "com.city_utd.city_utd.MapsFragment.EXTRA_MODE";

    private static final String STATE_SELECTED_DATE = "com.city_utd.city_utd.MapsFragment.STATE_SELECTED_DATE";
    private static final String STATE_SELECTED_FIELD_ID = "com.city_utd.city_utd.MapsFragment.STATE_SELECTED_FIELD_ID";
    private static final String STATE_SELECTED_FIELD_NAME = "com.city_utd.city_utd.MapsFragment.STATE_SELECTED_FIELD_NAME";

    private static final int RC_PERMISSION_EXPLANATION = 1;
    private static final int RC_RESOLVE_LOCATION_SETTINGS_FAILURE = 2;
    private static final int RC_CREATE_PLAYING_FIELD = 3;

    private static final int RC_REQUEST_FINE_LOCATION_PERMISSION = 1;

    private static final int LOADER_ID_ADDITIONAL_FIELDS = 1;

    private static final String DATE_PICKER_DIALOG_TAG = "DATE_PICKER_DIALOG";
    private static final String TIME_PICKER_DIALOG_TAG = "TIME_PICKER_DIALOG";

    private static final int RADIUS_METERS = 5000;
    private static final int MAX_COUNT = 3000;

    private static final float ZOOM_STREET = 17;

    @Bind(R.id.label_text_view)
    TextView labelTextView;

    @Bind(R.id.zoom_in_button)
    ImageButton zoomInButton;

    @Bind(R.id.zoom_out_button)
    ImageButton zoomOutButton;

    @Bind(R.id.my_location_button)
    ImageButton myLocationButton;

    @Bind(R.id.locator_view)
    View locatorView;

    @Bind(R.id.next_button)
    TextView nextButton;

    @Bind(R.id.bottom_sheet_view)
    View bottomSheetView;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.name_text_view)
    TextView nameTextView;

    @Bind(R.id.address_text_view)
    TextView addressTextView;

    @Bind(R.id.distance_text_view)
    TextView distanceTextView;

    @Bind(R.id.field_detailed_action_button)
    FloatingActionButton fieldDetailedActionButton;

    private int mode;
    private LatLng cachedLocation;
    private LatLng location;
    private NearestQueryParamsHolder paramsHolder;
    private FusedLocationProviderClient locationProviderClient;
    private LocationRequest slowLocationRequest;
    private LocationRequest fastLocationRequest;
    private LocationCallback locationCallback;
    private boolean requestingLocationUpdates;
    private GoogleMap googleMap;
    private boolean navigationHostViewsShown = true;
    private BottomSheetBehavior bottomSheetBehavior;
    private Handler handler;
    private ImageUrlBuilder imageUrlBuilder;
    private int selectedFieldId;
    private int selectedFieldDistance;
    private String selectedFieldName;
    private Calendar selectedDate;
    private MapManager<FieldClusterItem> mapManager;

    public static MapsFragment newInstance(int mode) {
        Bundle args = new Bundle();
        args.putInt(EXTRA_MODE, mode);
        MapsFragment fragment = new MapsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        mode = args.getInt(EXTRA_MODE);

        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        cachedLocation = manager.getLastLocation();

        locationProviderClient = LocationServices.getFusedLocationProviderClient(context);

        if(savedInstanceState != null){
            selectedDate = (Calendar) savedInstanceState.getSerializable(STATE_SELECTED_DATE);
            selectedFieldId = savedInstanceState.getInt(STATE_SELECTED_FIELD_ID);
            selectedFieldName = savedInstanceState.getString(STATE_SELECTED_FIELD_NAME);
        }

        ensureDateAndTimePickersListeners();
    }

    private void ensureDateAndTimePickersListeners(){
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(DATE_PICKER_DIALOG_TAG);
        if(fragment != null){
            DatePickerDialog dialog = (DatePickerDialog) fragment;
            dialog.setOnDateSetListener(this);
        }else {
            fragment = fragmentManager.findFragmentByTag(TIME_PICKER_DIALOG_TAG);
            if(fragment != null){
                TimePickerDialog dialog = (TimePickerDialog) fragment;
                dialog.setOnTimeSetListener(this);
            }
        }
    }

    @Override
    protected BaseLoader<List<PlayingField>> onCreateLoader(Context context, int id, Bundle args) {
        int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        boolean fromCache = source == BaseLoader.SOURCE_DATABASE;
        if(fromCache){
            return new NearestFieldsLoader(context);
        }else {
            return new NearestFieldsLoader(context, paramsHolder, source);
        }
    }

    private void updateParamsHolder(LatLng location){
        updateParamsHolder(location, RADIUS_METERS);
    }

    private void updateParamsHolder(LatLng location, int radius){
        ensureParamsHolder(location);
        paramsHolder.setCoordinates(location);
        paramsHolder.setRadius(radius);
    }

    private void ensureParamsHolder(LatLng location){
        if(paramsHolder == null){
            paramsHolder = new NearestQueryParamsHolder(MAX_COUNT, location);
            paramsHolder.setPage(1);
        }
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<PlayingField>> loader) {}

    @Override
    protected void onLoadFinished(BaseLoader<List<PlayingField>> loader, List<PlayingField> data) {
        if(googleMap != null){
            bindData(loader, data);
        }
        super.onLoadFinished(loader, data);
    }

    private void bindData(BaseLoader<List<PlayingField>> loader, List<PlayingField> data) {
        if (data != null) {
            if(loader.getId() != LOADER_ID_ADDITIONAL_FIELDS){
                mapManager.clearItems();
            }
            addFields(data);
        } else {
            String errorMessage = loader.getErrorMessage();
            if (!TextUtils.isEmpty(errorMessage) && checkNetworkAvailability()) {
                showToast(errorMessage, false);
            }
        }
    }

    private void addFields(List<PlayingField> fields){
        for (PlayingField field : fields) {
            if(!hasItem(field)){
                addItem(field);
            }
        }
        mapManager.cluster();
    }

    private boolean hasItem(PlayingField field){
        Collection<FieldClusterItem> items = mapManager.getItems();
        if(items == null || items.isEmpty()){
            return false;
        }
        for(FieldClusterItem item : items){
            PlayingField element = item.getPlayingField();
            if(element.getId() == field.getId()){
                return true;
            }
        }
        return false;
    }

    private void addItem(PlayingField field){
        FieldClusterItem item = new FieldClusterItem(field);
        mapManager.addItem(item);
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        if(mode == MODE_LOCATE){
            return;
        }

        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        if(mode == MODE_LOCATE){
            return;
        }

        LatLng location = getMyLocation();
        if(location != null){
            loadFromServer(location, BaseLoader.SOURCE_SERVER_DATABASE);
        }
    }

    private LatLng getMyLocation(){
        return location != null ? location : cachedLocation;
    }

    private void loadFromServer(LatLng location, int source){
        Bundle args = updateLoaderArgs(source);
        updateParamsHolder(location);
        reloadData(0, args);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_maps;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // setup map fragment
        FragmentManager fragmentManager = getChildFragmentManager();
        Fragment fragment = fragmentManager.findFragmentById(R.id.map);
        SupportMapFragment mapFragment = (SupportMapFragment) fragment;
        mapFragment.getMapAsync(this);

        // setup on click listeners
        zoomInButton.setOnClickListener(this);
        zoomOutButton.setOnClickListener(this);
        myLocationButton.setOnClickListener(this);
        imageView.setOnClickListener(this);
        fieldDetailedActionButton.setOnClickListener(this);

        // setup mode dependent views
        if(mode == MODE_PICK || mode == MODE_LOCATE){
            @StringRes int textRes = mode == MODE_PICK ?
                    R.string.choose_field :
                    R.string.please_specify_location;
            labelTextView.setText(textRes);

            if(mode == MODE_LOCATE){
                nextButton.setOnClickListener(this);
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        prepareModeDependentViews();
        setupGoogleMap(googleMap);
        enableMyLocationLayerWithPermissionCheck(true);
        onLoadFromCache(false);
    }

    private void prepareModeDependentViews(){
        if(mode == MODE_PICK){
            animateFadeIn(labelTextView, 400);
        }else if(mode == MODE_LOCATE){
            showModeLocateSpecificViews();
        }
    }

    private void showModeLocateSpecificViews(){
        long startDelay = 400;
        animateFadeIn(labelTextView, startDelay);
        animateFadeIn(locatorView, startDelay);
        animateFadeIn(nextButton, startDelay);
    }

    private void animateFadeIn(View view, long startDelay){
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1).setStartDelay(startDelay);
    }

    private void enableMyLocationLayerWithPermissionCheck(boolean withExplanation){
        int result = requestFineLocationPermission(withExplanation);
        if(result == AppUtils.Permissions.RESULT_HAS_PERMISSION){
            enableMyLocationLayer();
        }else if(result == AppUtils.Permissions.RESULT_SHOW_EXPLANATION){
            showFineLocationPermissionExplanationDialog();
        }
    }

    private int requestFineLocationPermission(boolean withExplanation){
        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        return requestPermission(permission, withExplanation, RC_REQUEST_FINE_LOCATION_PERMISSION);
    }

    @SuppressWarnings("MissingPermission")
    private void enableMyLocationLayer(){
        googleMap.setMyLocationEnabled(true);

        Task<Location> task = locationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if(location != null) {
                    boolean hasLocation = MapsFragment.this.location != null;
                    MapsFragment.this.location = createLatLng(location);
                    onLoadFromServer();

                    if(!hasLocation){
                        updateMyLocationIcon();
                        moveCameraToMyLocation();
                    }

                    initSlowLocationRequest();
                    checkLocationSettings(slowLocationRequest);
                }else {
                    initSlowLocationRequest();
                    initFastLocationRequest();
                    checkLocationSettings(slowLocationRequest, fastLocationRequest);
                }
            }
        });
    }

    private LatLng createLatLng(Location location){
        if(location != null){
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            return new LatLng(latitude, longitude);
        }
        return null;
    }

    private void updateMyLocationIcon(){
        @DrawableRes int iconRes;
        if(location != null){
            iconRes = R.drawable.ic_near_me_accent_24dp;
        }else {
            iconRes = R.drawable.ic_near_me_gray_24dp;
        }
        myLocationButton.setImageResource(iconRes);
    }

    private void showFineLocationPermissionExplanationDialog(){
        String explanation = getString(R.string.fine_location_permission_explanation1);
        showPermissionExplanationDialog(explanation);
    }

    private void showPermissionExplanationDialog(String explanation){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = PermissionExplanationDialogFragment.newInstance(explanation);
        dialog.setTargetFragment(this, RC_PERMISSION_EXPLANATION);
        dialog.show(fragmentManager, "PERMISSION_EXPLANATION_DIALOG");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_PERMISSION_EXPLANATION:
                if(resultCode == Activity.RESULT_OK){
                    enableMyLocationLayerWithPermissionCheck(false);
                }
                break;
            case RC_RESOLVE_LOCATION_SETTINGS_FAILURE:
                if(resultCode == Activity.RESULT_OK){
                    initiateLocationUpdates();
                }
                break;
            case RC_CREATE_PLAYING_FIELD:
                if(resultCode == Activity.RESULT_OK && data != null){
                    PlayingField field = data.getParcelableExtra(PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_RESULT);
                    if(field != null){
                        int distance = calculateDistanceBetweenMeAndField(field);
                        field.setDistance(distance);
                        addItem(field);
                        mapManager.cluster();
                        animateCameraToField(field);
                    }
                }
                break;
        }
    }

    private int calculateDistanceBetweenMeAndField(PlayingField field){
        LatLng myLocation = getMyLocation();
        if(myLocation == null){
            return -1;
        }

        LatLng coordinates = field.getCoordinates();
        if(coordinates == null){
            return -1;
        }

        double startLatitude = myLocation.latitude;
        double startLongitude = myLocation.longitude;
        double endLatitude = coordinates.latitude;
        double endLongitude = coordinates.longitude;

        float[] results = new float[1];
        Location.distanceBetween(startLatitude, startLongitude, endLatitude, endLongitude, results);
        return (int) results[0];
    }

    private void animateCameraToField(PlayingField field){
        LatLng coordinates = field.getCoordinates();
        animateCamera(coordinates, ZOOM_STREET);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case RC_REQUEST_FINE_LOCATION_PERMISSION:
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    enableMyLocationLayer();
                }else {
                    showToast(R.string.fine_location_permission_denied, false);
                }
                break;
        }
    }

    private void initSlowLocationRequest(){
        slowLocationRequest = LocationRequest.create()
                .setInterval(10 * 60 * 1000)
                .setFastestInterval(10 * 60 * 1000)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
    }

    private void initFastLocationRequest(){
        fastLocationRequest = LocationRequest.create()
                .setInterval(3 * 1000)
                .setFastestInterval(1000)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private void checkLocationSettings(LocationRequest... locationRequests){
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder();
        for(LocationRequest locationRequest : locationRequests){
            builder.addLocationRequest(locationRequest);
        }
        LocationSettingsRequest request = builder.build();

        Context context = getActivity();
        SettingsClient client = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> task = client.checkLocationSettings(request);

        task.addOnSuccessListener(new OnSuccessListener<LocationSettingsResponse>() {
            @Override
            public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                initiateLocationUpdates();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                int statusCode = ((ApiException) e).getStatusCode();
                switch (statusCode){
                    case CommonStatusCodes.RESOLUTION_REQUIRED:
                        ResolvableApiException resolvable = (ResolvableApiException) e;
                        PendingIntent resolution = resolvable.getResolution();
                        resolveLocationSettingsFailure(resolution);
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        break;
                }
            }
        });
    }

    private void initiateLocationUpdates(){
        if(fastLocationRequest != null){
            initiateLocationUpdates(fastLocationRequest);
        }else if(slowLocationRequest != null){
            initiateLocationUpdates(slowLocationRequest);
        }
    }

    @SuppressWarnings("MissingPermission")
    private void initiateLocationUpdates(LocationRequest request){
        ensureLocationCallback();
        locationProviderClient.requestLocationUpdates(request, locationCallback, null);
        requestingLocationUpdates = true;
    }

    private void ensureLocationCallback(){
        if(locationCallback == null){
            locationCallback = createLocationCallback();
        }
    }

    private LocationCallback createLocationCallback(){
        return new LocationCallback(){
            @Override
            public void onLocationResult(LocationResult locationResult) {
                boolean hasLocation = MapsFragment.this.location != null;
                Location location = locationResult.getLastLocation();
                MapsFragment.this.location = createLatLng(location);

                if(!hasLocation){
                    updateMyLocationIcon();
                    moveCameraToMyLocation();
                }

                if(fastLocationRequest != null){
                    initiateLocationUpdates(slowLocationRequest);
                    fastLocationRequest = null;

                    onLoadFromServer();
                }
            }
        };
    }

    private void stopLocationUpdates(){
        if(locationCallback != null){
            locationProviderClient.removeLocationUpdates(locationCallback);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(requestingLocationUpdates){
            initiateLocationUpdates();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        stopLocationUpdates();

        Activity activity = getActivity();
        if(activity.isFinishing() && location != null){
            LocalDataManager manager = LocalDataManager.getInstance(activity);
            manager.setLastLocation(location);

            if(manager.isAuthorized()){
                SyncLocationJobService.syncRightNow();
            }
        }
    }

    private void resolveLocationSettingsFailure(PendingIntent resolution){
        resolveFailure(resolution, RC_RESOLVE_LOCATION_SETTINGS_FAILURE);
    }

    private void resolveFailure(PendingIntent resolution, int requestCode){
        IntentSender intentSender = resolution.getIntentSender();
        try {
            startIntentSenderForResult(intentSender, requestCode, null, 0, 0, 0, null);
        } catch (IntentSender.SendIntentException ignored) {}
    }

    private void setupGoogleMap(GoogleMap map){
        Context context = getActivity();
        ClusterManager<FieldClusterItem> clusterManager;
        clusterManager = new ClusterManager<>(context, map);
        FieldsClusterRenderer renderer = new FieldsClusterRenderer(context, map, clusterManager);
        clusterManager.setRenderer(renderer);
        clusterManager.setOnClusterItemClickListener(this);
        clusterManager.setOnClusterClickListener(this);

        googleMap = map;
        mapManager = new MapManager<>(clusterManager, map);
        mapManager.setOnTriggerReloadListener(this);

        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setOnMapClickListener(this);
        map.setOnCameraMoveStartedListener(this);
        map.setOnCameraIdleListener(mapManager);
        map.setOnCameraMoveListener(mapManager);
        map.setOnMarkerClickListener(clusterManager);
    }

    @Override
    public void onTriggerLoad() {
        loadNearestFieldsForCurrentCameraPosition();
    }

    private void loadNearestFieldsForCurrentCameraPosition(){
        LatLng location = getCurrentCameraPosition();
        int radius = getSearchRadius();
        updateParamsHolder(location, radius);
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_SERVER);
        reloadData(LOADER_ID_ADDITIONAL_FIELDS, args);
    }

    private LatLng getCurrentCameraPosition(){
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        return cameraPosition.target;
    }

    private int getSearchRadius(){
        Projection projection = googleMap.getProjection();
        VisibleRegion region = projection.getVisibleRegion();
        LatLngBounds bounds = region.latLngBounds;
        LatLng southwest = bounds.southwest;
        LatLng northeast = bounds.northeast;
        return getDistanceBetween(southwest, northeast) / 2;
    }

    private int getDistanceBetween(LatLng first, LatLng second){
        double lat1 = first.latitude;
        double lng1 = first.longitude;
        double lat2 = second.latitude;
        double lng2 = second.longitude;

        float[] results = new float[1];
        Location.distanceBetween(lat1, lng1, lat2, lng2, results);
        return (int) results[0];
    }

    private void moveCameraToMyLocation(){
        LatLng location = getMyLocation();
        if(location != null){
            moveCamera(location, ZOOM_STREET);
        }
    }

    private void moveCamera(LatLng location, float zoom){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(location, zoom);
        googleMap.moveCamera(update);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.zoom_in_button:
                zoomIn();
                break;
            case R.id.zoom_out_button:
                zoomOut();
                break;
            case R.id.my_location_button:
                animateCameraToMyLocation();
                break;
            case R.id.next_button:
                returnLocation();
                break;
            case R.id.image_view:
                openFieldDetailedActivity();
                break;
            case R.id.field_detailed_action_button:
                if(mode == MODE_VIEW){
                    openFieldDetailedActivity();
                }else if(mode == MODE_PICK){
                    hideBottomSheet();
                    ensureHandler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            showDatePickerDialog();
                        }
                    }, 400);
                }
                break;
        }
    }

    private void zoomIn(){
        zoom(true);
    }

    private void zoomOut(){
        zoom(false);
    }

    private void zoom(boolean in){
        if(googleMap != null){
            CameraPosition position = googleMap.getCameraPosition();
            float zoom = position.zoom;
            LatLng location = position.target;
            if(in){
                zoom ++;
            }else {
                zoom --;
            }
            animateCamera(location, zoom);
        }
    }

    private void animateCamera(LatLng location, float zoom){
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(location, zoom);
        googleMap.animateCamera(update);
    }

    private void animateCameraToMyLocation(){
        LatLng location = getMyLocation();
        if(location != null){
            animateCamera(location, ZOOM_STREET);
        }
    }

    private void returnLocation(){
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        LatLng location = cameraPosition.target;

        Activity activity = getActivity();
        Intent data = new Intent();
        data.putExtra(MapsActivity.EXTRA_LOCATION, location);
        activity.setResult(Activity.RESULT_OK, data);
        activity.finish();
    }

    private void openFieldDetailedActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, selectedFieldId);
        intent.putExtra(FieldDetailedActivity.EXTRA_DISTANCE, selectedFieldDistance);
        startActivity(intent);
    }

    private void showDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = DatePickerDialog.newInstance(this, year, month, day);

        dialog.setMinDate(calendar);
        calendar.add(Calendar.DAY_OF_MONTH, 7);
        dialog.setMaxDate(calendar);

        FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, DATE_PICKER_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.YEAR, year);
        selectedDate.set(Calendar.MONTH, monthOfYear);
        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        showTimePickerDialog();
    }

    private void showTimePickerDialog(){
        TimePickerDialog dialog = TimePickerDialog.newInstance(this, true);
        FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, TIME_PICKER_DIALOG_TAG);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDate.set(Calendar.MINUTE, minute);
        selectedDate.set(Calendar.SECOND, 0);
        selectedDate.set(Calendar.MILLISECOND, 0);

        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        if(selectedDate.before(now)){
            showToast(R.string.you_can_not_invite_on_past, false);
        }else {
            Activity activity = getActivity();
            Intent data = new Intent();
            data.putExtra(MapsActivity.EXTRA_PICKED_PLAYING_FIELD_ID, selectedFieldId);
            data.putExtra(MapsActivity.EXTRA_PICKED_PLAYING_FIELD_NAME, selectedFieldName);
            data.putExtra(MapsActivity.EXTRA_PICKED_DATE, selectedDate.getTime());
            activity.setResult(Activity.RESULT_OK, data);
            activity.finish();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        if(isBottomSheetShown()){
            hideBottomSheet();
        }else {
            toggleNavigationHostViews();
        }
    }

    private boolean isBottomSheetShown(){
        return bottomSheetBehavior != null
                && bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED;
    }

    private void hideBottomSheet(){
        if(bottomSheetBehavior != null){
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    private void toggleNavigationHostViews(){
        MainNavigationHost host = getNavigationHost();
        if(host != null){
            if(navigationHostViewsShown){
                host.hideBottomNavigationView();
                host.hideFloatingActionButton();
            }else {
                host.showBottomNavigationView();
                host.showFloatingActionButton();
            }
            navigationHostViewsShown = !navigationHostViewsShown;
        }
    }

    @Override
    public void onCameraMoveStarted(int reason) {
        if(reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE){
            if(isBottomSheetShown()){
                hideBottomSheet();
            }else if(navigationHostViewsShown){
                toggleNavigationHostViews();
            }
        }
    }

    @Override
    public boolean onClusterClick(Cluster<FieldClusterItem> cluster) {
        CameraPosition cameraPosition = googleMap.getCameraPosition();
        float zoom = cameraPosition.zoom;
        LatLng position = cluster.getPosition();
        animateCamera(position, ++zoom);
        return true;
    }

    @Override
    public boolean onClusterItemClick(FieldClusterItem item) {
        PlayingField field = item.getPlayingField();
        setupBottomSheet(field);
        boolean hiding = hideNavigationHostViewsIfNeeded();
        showBottomSheet(hiding);
        return true;
    }

    private void setupBottomSheet(PlayingField field){
        String imageUrl = field.getImageUrl();
        ensureImageUrlBuilder();
        imageUrl = imageUrlBuilder.build(imageUrl);
        @DrawableRes int placeholderRes = R.drawable.ic_field_placeholder;
        Glide.with(this).load(imageUrl).placeholder(placeholderRes).error(placeholderRes).into(imageView);

        String name = field.getName();
        if(!TextUtils.isEmpty(name)){
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(name);
        }else {
            nameTextView.setVisibility(View.GONE);
        }

        String address = field.getAddress();
        if(!TextUtils.isEmpty(address)){
            addressTextView.setVisibility(View.VISIBLE);
            addressTextView.setText(address);
        }else {
            addressTextView.setVisibility(View.GONE);
        }

        int distance = getDistanceToField(field);
        if(distance >= 0){
            String distanceString = getString(R.string.distance_value, distance);
            distanceTextView.setText(distanceString);
            distanceTextView.setVisibility(View.VISIBLE);
        }else{
            distanceTextView.setVisibility(View.GONE);
        }

        @DrawableRes int actionButtonIconRes;
        if(mode == MODE_VIEW){
            actionButtonIconRes = R.drawable.ic_more_horiz_accent_24dp;
        }else {
            actionButtonIconRes = R.drawable.ic_done_accent_24dp;
        }
        fieldDetailedActionButton.setImageResource(actionButtonIconRes);

        selectedFieldId = field.getId();
        selectedFieldDistance = distance;
        selectedFieldName = name;
    }

    private int getDistanceToField(PlayingField field){
        LatLng myLocation = getMyLocation();
        if(myLocation == null){
            return field.getDistance();
        }
        LatLng coordinates = field.getCoordinates();
        return getDistanceBetween(myLocation, coordinates);
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    private boolean hideNavigationHostViewsIfNeeded(){
        MainNavigationHost host = getNavigationHost();
        if(host != null){
            if(navigationHostViewsShown){
                host.hideBottomNavigationView();
                host.hideFloatingActionButton();
                navigationHostViewsShown = false;
                return true;
            }
        }
        return false;
    }

    private void showBottomSheet(boolean delayed){
        if(delayed){
            showBottomSheetDelayed();
        }else {
            ensureBottomSheetBehavior();
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
    }

    private void showBottomSheetDelayed(){
        ensureHandler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                showBottomSheet(false);
            }
        }, 400);
    }

    private void ensureHandler(){
        if(handler == null){
            handler = new Handler();
        }
    }

    private void ensureBottomSheetBehavior(){
        if(bottomSheetBehavior == null){
            bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetView);
            bottomSheetBehavior.setBottomSheetCallback(new InternalBottomSheetCallback());
        }
    }

    private class InternalBottomSheetCallback extends BottomSheetBehavior.BottomSheetCallback{

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if(newState == BottomSheetBehavior.STATE_COLLAPSED){
                toggleNavigationHostViews();
            }
        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
            fieldDetailedActionButton.setAlpha(slideOffset);
            fieldDetailedActionButton.setScaleX(slideOffset);
            fieldDetailedActionButton.setScaleY(slideOffset);
        }
    }

    @Override
    public void reloadContent() {}

    @Override
    public MainNavigationHost getNavigationHost(){
        Activity activity = getActivity();
        if(activity instanceof MainNavigationHost){
            return (MainNavigationHost) activity;
        }
        return null;
    }

    @Override
    public boolean setupFloatingActionButton(FloatingActionButton floatingActionButton) {
        floatingActionButton.setImageResource(R.drawable.ic_add_white_24dp);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPlayingFieldEditingActivity();
            }
        });
        return true;
    }

    private void openPlayingFieldEditingActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, PlayingFieldEditingActivity.class);
        startActivityForResult(intent, RC_CREATE_PLAYING_FIELD);
    }

    @Override
    public boolean onInterceptBackPressed() {
        if(isBottomSheetShown()){
            hideBottomSheet();
            return true;
        }
        return false;
    }

    @Override
    public int getConnectivityStateFragmentAppearance() {
        return ConnectivityStateFragment.APPEARANCE_DARK;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_SELECTED_DATE, selectedDate);
        outState.putInt(STATE_SELECTED_FIELD_ID, selectedFieldId);
        outState.putString(STATE_SELECTED_FIELD_NAME, selectedFieldName);
    }
}
