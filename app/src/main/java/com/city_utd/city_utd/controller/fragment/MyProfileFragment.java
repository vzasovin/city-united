package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.FileProvider;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.IncomingInvitationsActivity;
import com.city_utd.city_utd.controller.activity.ProfileEditingActivity;
import com.city_utd.city_utd.controller.activity.SplashActivity;
import com.city_utd.city_utd.controller.activity.UsersRatingActivity;
import com.city_utd.city_utd.controller.dialog.PhotoSourceDialogFragment;
import com.city_utd.city_utd.controller.dialog.ProfileOptionsDialogFragment;
import com.city_utd.city_utd.controller.dialog.RateUserDialogFragment;
import com.city_utd.city_utd.loader.AbstractLoader;
import com.city_utd.city_utd.loader.ProfileLoader;
import com.city_utd.city_utd.loader.UserLoader;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.task.AddToGalleryAsyncTask;
import com.city_utd.city_utd.task.ChangeAvatarAsyncTask;
import com.city_utd.city_utd.task.DeleteAvatarAsyncTask;
import com.city_utd.city_utd.task.LogoutAsyncTask;
import com.city_utd.city_utd.task.RateUserAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.Util;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.imogene.android.carcase.graphics.NumberDrawable;
import com.imogene.android.carcase.worker.loader.FilesLoader;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Admin on 19.07.2017.
 */

public class MyProfileFragment extends ProfileFragment {

    private static final String STATE_CHOOSING_PHOTO_REASON = "com.city_utd.city_utd.MyProfileFragment.STATE_CHOOSING_PHOTO_REASON";
    private static final String STATE_TEMP_PHOTO_FILE_PATH = "com.city_utd.city_utd.MyProfileFragment.STATE_TEMP_PHOTO_FILE_PATH";

    private static final int CHOOSING_PHOTO_REASON_AVATAR = 1;
    private static final int CHOOSING_PHOTO_REASON_GALLERY = 2;

    private static final int RC_OPTIONS_MENU = 1;
    private static final int RC_INCOMING_INVITATIONS = 2;
    private static final int RC_CHOOSE_PHOTO_SOURCE = 3;
    private static final int RC_CHOOSE_PHOTO_FOR_AVATAR = 4;
    private static final int RC_CHOOSE_PHOTO_FOR_GALLERY = 5;
    private static final int RC_TAKE_PHOTO_FOR_AVATAR = 6;
    private static final int RC_TAKE_PHOTO_FOR_GALLERY = 7;
    private static final int RC_EDIT_PROFILE = 8;
    private static final int RC_ESTIMATE_SELF = 9;

    private static final int TASK_ID_LOGOUT = 1;
    private static final int TASK_ID_CHANGE_AVATAR = 2;
    private static final int TASK_ID_DELETE_AVATAR = 3;
    private static final int TASK_ID_ADD_TO_GALLERY = 4;
    private static final int TASK_ID_ESTIMATE_SELF = 5;

    private static final int LOADER_ID_PHOTO_FILE = 1;

    private static final String DIALOG_TAG_SELF_ESTIMATE = "SELF_ESTIMATE_DIALOG";

    private Menu menu;
    private int choosingPhotoReason;
    private String tempPhotoFilePath;
    private Bundle photoFileLoaderArgs;
    private PhotoFileLoaderCallbacks photoFileLoaderCallbacks;
    private boolean loadWithSyntheticDelay = true;

    public static MyProfileFragment newInstance() {
        return new MyProfileFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        if(savedInstanceState != null){
            choosingPhotoReason = savedInstanceState.getInt(STATE_CHOOSING_PHOTO_REASON);
            tempPhotoFilePath = savedInstanceState.getString(STATE_TEMP_PHOTO_FILE_PATH);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AbstractActivity activity = (AbstractActivity) getActivity();
        activity.setSupportActionBar(toolbar);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        showSelfEstimateDialogIfNeeded();
    }

    private void showSelfEstimateDialogIfNeeded(){
        if(shouldShowSelfEstimateDialog()){
            showSelfEstimateDialogIfNotShown();
        }
    }

    private boolean shouldShowSelfEstimateDialog(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        return manager.getShowSelfEstimationFlag();
    }

    private void showSelfEstimateDialogIfNotShown(){
        FragmentManager fragmentManager = getFragmentManager();
        String tag = DIALOG_TAG_SELF_ESTIMATE;
        if(fragmentManager.findFragmentByTag(tag) == null){
            DialogFragment dialog = RateUserDialogFragment.newInstance("");
            dialog.setTargetFragment(this, RC_ESTIMATE_SELF);
            dialog.show(fragmentManager, tag);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        this.menu = menu;
        updateOptionsMenuItem();
    }

    private void updateOptionsMenuItem(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        int unreadCount = manager.getUnreadIncomingInvitationsCount();

        MenuItem optionsMenuItem = menu.findItem(R.id.options);
        if(unreadCount == 0){
            optionsMenuItem.setIcon(R.drawable.ic_more_vert_white_24dp);
        }else {
            LayerDrawable drawable = ensureOptionsMenuItemIconDrawable(optionsMenuItem);
            NumberDrawable numberDrawable = (NumberDrawable) drawable.getDrawable(1);
            numberDrawable.setNumber(unreadCount);
        }
    }

    private LayerDrawable ensureOptionsMenuItemIconDrawable(MenuItem menuItem){
        Drawable icon = menuItem.getIcon();
        if(icon == null){
            LayerDrawable drawable = createOptionsMenuItemIconDrawable();
            menuItem.setIcon(drawable);
            return drawable;
        }
        return (LayerDrawable) icon;
    }

    private LayerDrawable createOptionsMenuItemIconDrawable(){
        Drawable first = getDrawable(R.drawable.ic_more_vert_white_24dp);
        Drawable second = createNumberDrawable();
        Drawable[] layers = new Drawable[]{first, second};
        return new LayerDrawable(layers);
    }

    private NumberDrawable createNumberDrawable(){
        Context context = getActivity();
        float circleSize = convertDpsInPixels(12.8f);
        float textSize = convertDpsInPixels(8.f);
        return new NumberDrawable.Builder(context)
                .circleSize(circleSize)
                .textSize(textSize)
                .build();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(!handled){
            int itemId = item.getItemId();
            switch (itemId){
                case R.id.edit:
                    openProfileEditingActivity();
                    break;
                case R.id.options:
                    showOptionsDialog();
                    break;
            }
        }
        return handled;
    }

    private void openProfileEditingActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, ProfileEditingActivity.class);
        startActivityForResult(intent, RC_EDIT_PROFILE);
    }

    private void showOptionsDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        boolean hasAvatar = user != null && !TextUtils.isEmpty(user.getImageUrl());
        DialogFragment dialog = ProfileOptionsDialogFragment.newInstance(hasAvatar);
        dialog.setTargetFragment(this, RC_OPTIONS_MENU);
        dialog.show(fragmentManager, "OPTIONS_DIALOG");
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_OPTIONS_MENU:
                if(resultCode == Activity.RESULT_OK && data != null){
                    int option = data.getIntExtra(ProfileOptionsDialogFragment.EXTRA_OPTION, 0);
                    handleOption(option);
                }
                break;
            case RC_INCOMING_INVITATIONS:
                if(resultCode == Activity.RESULT_OK){
                    updateOptionsMenuItem();
                }
                break;
            case RC_CHOOSE_PHOTO_SOURCE:
                if(resultCode == Activity.RESULT_OK && data != null){
                    int source = data.getIntExtra(PhotoSourceDialogFragment.EXTRA_PHOTO_SOURCE, 0);
                    handleSelectedPhotoSource(source);
                }
                break;
            case RC_CHOOSE_PHOTO_FOR_AVATAR:
                if(resultCode == Activity.RESULT_OK && data != null){
                    Uri uri = data.getData();
                    if(uri != null){
                        copyPhotoFile(uri);
                    }
                }
                break;
            case RC_CHOOSE_PHOTO_FOR_GALLERY:
                if(resultCode == Activity.RESULT_OK && data != null){
                    ClipData clipData = data.getClipData();
                    if(clipData != null){
                        copyPhotosFiles(clipData);
                    }else {
                        Uri uri = data.getData();
                        if(uri != null){
                            copyPhotoFile(uri);
                        }
                    }
                }
                break;
            case RC_TAKE_PHOTO_FOR_AVATAR:
                if(resultCode == Activity.RESULT_OK && !TextUtils.isEmpty(tempPhotoFilePath)){
                    changeAvatar(tempPhotoFilePath);
                }
                break;
            case RC_TAKE_PHOTO_FOR_GALLERY:
                if(resultCode == Activity.RESULT_OK && !TextUtils.isEmpty(tempPhotoFilePath)){
                    addPhoto(tempPhotoFilePath);
                }
                break;
            case RC_EDIT_PROFILE:
                if(resultCode == Activity.RESULT_OK){
                    onLoadFromCache(true);
                }
                break;
            case RC_ESTIMATE_SELF:
                if(resultCode == Activity.RESULT_OK && data != null){
                    boolean doNotShowAgain = data.getBooleanExtra(RateUserDialogFragment.EXTRA_DO_NOT_SHOW_SELF_ESTIMATE, false);
                    if(doNotShowAgain){
                        clearShowSelfEstimationFlag();
                    }else {
                        Rate rate = data.getParcelableExtra(RateUserDialogFragment.EXTRA_RATE);
                        estimateSelf(rate);
                    }
                }
                break;
        }
    }

    private void clearShowSelfEstimationFlag(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setShowSelfEstimationFlag(false);
    }

    private void estimateSelf(Rate rate){
        int userId = user.getId();
        RateUserAsyncTask asyncTask = new RateUserAsyncTask(TASK_ID_ESTIMATE_SELF, this, userId, rate);
        asyncTask.execute();
    }

    private void handleOption(int option){
        switch (option){
            case ProfileOptionsDialogFragment.OPTION_CHANGE_AVATAR:
                handleChangeAvatarOption();
                break;
            case ProfileOptionsDialogFragment.OPTION_DELETE_AVATAR:
                deleteAvatar();
                break;
            case ProfileOptionsDialogFragment.OPTION_ADD_PHOTO:
                handleAddPhotoOption();
                break;
            case ProfileOptionsDialogFragment.OPTION_INCOMING_INVITATIONS:
                openIncomingInvitationsActivity();
                break;
            case ProfileOptionsDialogFragment.OPTION_EXIT:
                logout();
                break;
        }
    }

    private void handleChangeAvatarOption(){
        if(checkHasCameraFeature()){
            choosingPhotoReason = CHOOSING_PHOTO_REASON_AVATAR;
            showPhotoSourceDialog();
        }else {
            dispatchChoosePhotoForAvatarIntent();
        }
    }

    private boolean checkHasCameraFeature(){
        Context context = getActivity();
        PackageManager manager = context.getPackageManager();
        return manager.hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }

    private void showPhotoSourceDialog(){
        FragmentManager fragmentManager = getFragmentManager();
        PhotoSourceDialogFragment dialog = PhotoSourceDialogFragment.newInstance();
        dialog.setTargetFragment(this, RC_CHOOSE_PHOTO_SOURCE);
        dialog.show(fragmentManager, "PHOTO_SOURCE_DIALOG");
    }

    private void dispatchChoosePhotoForAvatarIntent(){
        dispatchChoosePhotoIntent(RC_CHOOSE_PHOTO_FOR_AVATAR, false);
    }

    private void dispatchChoosePhotoIntent(int requestCode, boolean selectMultiple){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, selectMultiple);

        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            Intent chooser = Intent.createChooser(intent, getString(R.string.choose_app));
            startActivityForResult(chooser, requestCode);
        }else {
            showToast(R.string.could_not_find_suitable_app_to_choose_photo, false);
        }
    }

    private void handleSelectedPhotoSource(int source){
        if(source == PhotoSourceDialogFragment.CAMERA){
            handleSelectedPhotoSourceCamera();
        }else if(source == PhotoSourceDialogFragment.FILE){
            handleSelectedPhotoSourceFile();
        }
    }

    private void handleSelectedPhotoSourceCamera(){
        if(choosingPhotoReason == CHOOSING_PHOTO_REASON_AVATAR){
            dispatchTakePhotoForAvatarIntent();
        }else if(choosingPhotoReason == CHOOSING_PHOTO_REASON_GALLERY){
            dispatchTakePhotoForGalleryIntent();
        }
    }

    private void dispatchTakePhotoForAvatarIntent(){
        dispatchTakePhotoIntent(RC_TAKE_PHOTO_FOR_AVATAR);
    }

    private void dispatchTakePhotoForGalleryIntent(){
        dispatchTakePhotoIntent(RC_TAKE_PHOTO_FOR_GALLERY);
    }

    private void dispatchTakePhotoIntent(int requestCode){
        Context context = getActivity();
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        PackageManager packageManager = context.getPackageManager();
        if(intent.resolveActivity(packageManager) != null){
            String mimeType = "image/jpeg";
            File file;
            try {
                file = Util.Storage.createTempFile(context, mimeType);
            }catch (IOException e){
                showToast(R.string.could_not_create_file_to_store_photo, false);
                return;
            }

            String authority = "com.city_utd.city_utd.pictures";
            Uri uri = FileProvider.getUriForFile(context, authority, file);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
            startActivityForResult(intent, requestCode);
            tempPhotoFilePath = file.getAbsolutePath();
        }else {
            showToast(R.string.could_not_find_suitable_app_to_make_photo, false);
        }
    }

    private void handleSelectedPhotoSourceFile(){
        if(choosingPhotoReason == CHOOSING_PHOTO_REASON_AVATAR){
            dispatchChoosePhotoForAvatarIntent();
        }else if(choosingPhotoReason == CHOOSING_PHOTO_REASON_GALLERY){
            dispatchChoosePhotoForGalleryIntent();
        }
    }

    private void dispatchChoosePhotoForGalleryIntent(){
        dispatchChoosePhotoIntent(RC_CHOOSE_PHOTO_FOR_GALLERY, true);
    }

    private void copyPhotoFile(Uri uri){
        File dest;
        try {
            Context context = getActivity();
            String mimeType = "image/jpeg";
            dest = Util.Storage.createTempFile(context, mimeType);
        }catch (IOException e){
            showToast(R.string.could_not_create_file_to_store_photo, false);
            return;
        }
        updatePhotoFileLoaderCallbacks(dest);
        updatePhotoFileLoaderArgs(uri);
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.restartLoader(LOADER_ID_PHOTO_FILE, photoFileLoaderArgs, photoFileLoaderCallbacks);
    }

    private void copyPhotosFiles(ClipData clipData){
        File dir;
        try {
            Context context = getActivity();
            dir = Util.Storage.getTempFilesDir(context);
        }catch (IOException e){
            showToast(R.string.could_not_create_file_to_store_photo, false);
            return;
        }
        updatePhotoFileLoaderCallbacks(dir);
        updatePhotoFileLoaderArgs(clipData);
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.restartLoader(LOADER_ID_PHOTO_FILE, photoFileLoaderArgs, photoFileLoaderCallbacks);
    }

    private void updatePhotoFileLoaderArgs(Uri uri){
        ensurePhotoFileLoaderArgs();
        photoFileLoaderArgs.remove(FilesLoader.EXTRA_CLIP_DATA);
        photoFileLoaderArgs.putParcelable(FilesLoader.EXTRA_URI, uri);
    }

    private void updatePhotoFileLoaderArgs(ClipData clipData){
        ensurePhotoFileLoaderArgs();
        photoFileLoaderArgs.remove(FilesLoader.EXTRA_URI);
        photoFileLoaderArgs.putParcelable(FilesLoader.EXTRA_CLIP_DATA, clipData);
    }

    private void ensurePhotoFileLoaderArgs(){
        if(photoFileLoaderArgs == null){
            photoFileLoaderArgs = new Bundle();
        }
    }

    private void updatePhotoFileLoaderCallbacks(File dest){
        ensurePhotoFileLoaderCallbacks();
        photoFileLoaderCallbacks.dest = dest;
    }

    private void ensurePhotoFileLoaderCallbacks(){
        if(photoFileLoaderCallbacks == null){
            photoFileLoaderCallbacks = new PhotoFileLoaderCallbacks();
        }
    }

    private class PhotoFileLoaderCallbacks implements LoaderManager.LoaderCallbacks<FilesLoader.Result>{

        private File dest;

        @Override
        public Loader<FilesLoader.Result> onCreateLoader(int id, Bundle args) {
            Context context = getActivity();
            Uri uri = args.getParcelable(FilesLoader.EXTRA_URI);
            ClipData clipData = args.getParcelable(FilesLoader.EXTRA_CLIP_DATA);

            if(uri != null){
                return new FilesLoader.Builder(context, uri).file(dest).build();
            }else if(clipData != null){
                return new FilesLoader.Builder(context, clipData).dir(dest).build();
            }
            return null;
        }

        @Override
        public void onLoadFinished(Loader<FilesLoader.Result> loader, FilesLoader.Result data) {
            if(data != null && data.getItemsCount() > 0){
                handleCopiedFileResult(data);
            }
        }

        @Override
        public void onLoaderReset(Loader<FilesLoader.Result> loader) {}
    }

    private void handleCopiedFileResult(FilesLoader.Result result){
        if(choosingPhotoReason == CHOOSING_PHOTO_REASON_AVATAR){
            FilesLoader.Item item = result.getItemAt(0);
            if(item.isSuccessful()){
                File file = item.getFile();
                changeAvatar(file);
            }else {
                int errorCode = item.getErrorCode();
                handleFileCopyingError(errorCode);
            }
        }else if(choosingPhotoReason == CHOOSING_PHOTO_REASON_GALLERY){
            List<File> files = new ArrayList<>(result.getItemsCount());
            for(FilesLoader.Item item : result){
                if (item.isSuccessful()){
                    files.add(item.getFile());
                }
            }
            addPhotos(files);
        }
    }

    private void handleFileCopyingError(int errorCode){
        @StringRes int messageRes;
        switch (errorCode){
            case FilesLoader.ERROR_TOO_LARGE_FILE:
                messageRes = R.string.too_large_file;
                break;
            case FilesLoader.ERROR_SOURCE_NOT_FOUND:
                messageRes = R.string.file_is_not_found;
                break;
            default:
                messageRes = R.string.file_reading_error;
                break;
        }
        showToast(messageRes, false);
    }

    private void changeAvatar(File file){
        if(checkNetworkAvailability()){
            ChangeAvatarAsyncTask asyncTask = new ChangeAvatarAsyncTask(TASK_ID_CHANGE_AVATAR, this, file);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void changeAvatar(String filePath){
        changeAvatar(new File(filePath));
    }

    private void deleteAvatar(){
        if(checkNetworkAvailability()){
            DeleteAvatarAsyncTask asyncTask = new DeleteAvatarAsyncTask(TASK_ID_DELETE_AVATAR, this);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void handleAddPhotoOption(){
        if(checkHasCameraFeature()){
            choosingPhotoReason = CHOOSING_PHOTO_REASON_GALLERY;
            showPhotoSourceDialog();
        }else {
            dispatchChoosePhotoForGalleryIntent();
        }
    }

    private void addPhoto(String filePath){
        addPhoto(new File(filePath));
    }

    private void addPhoto(File file){
        addPhotos(Collections.singletonList(file));
    }

    private void addPhotos(List<File> files){
        if(checkNetworkAvailability()){
            AddToGalleryAsyncTask asyncTask = new AddToGalleryAsyncTask(TASK_ID_ADD_TO_GALLERY, this, files);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    @Override
    protected void onAddPhotoToGallery() {
        handleAddPhotoOption();
    }

    private void openIncomingInvitationsActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, IncomingInvitationsActivity.class);
        startActivityForResult(intent, RC_INCOMING_INVITATIONS);
    }

    private void logout(){
        if(checkNetworkAvailability()){
            LogoutAsyncTask asyncTask = new LogoutAsyncTask(TASK_ID_LOGOUT, this);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_LOGOUT:
                clearUserSpecificInfo();
                cancelScheduledJobs();
                openSplashActivityInFreshTask();
                break;
            case TASK_ID_CHANGE_AVATAR:
            case TASK_ID_DELETE_AVATAR:
                this.user = (UserDetailed) result;
                bindData();
                break;
            case TASK_ID_ADD_TO_GALLERY:
            case TASK_ID_ESTIMATE_SELF:
                if((boolean) result){
                    loadWithSyntheticDelay = false;
                    reloadContent();
                }
                if(taskId == TASK_ID_ESTIMATE_SELF){
                    clearShowSelfEstimationFlag();
                }
                break;
            default:
                super.onTaskFinished(taskId, result);
        }
    }

    private void clearUserSpecificInfo(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setJwtToken(null).setClientToken(null).setUserId(0).setFirstLaunchFlag(true);
    }

    private void cancelScheduledJobs(){
        Context context = getActivity();
        Driver driver = new GooglePlayDriver(context);
        FirebaseJobDispatcher dispatcher = new FirebaseJobDispatcher(driver);
        dispatcher.cancelAll();
    }

    private void openSplashActivityInFreshTask(){
        Context context = getActivity();
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    @Override
    protected boolean isMyProfile() {
        return true;
    }

    @Override
    protected String getActionButtonText() {
        return getString(R.string.players_rating);
    }

    @Override
    protected void onActionButtonClick() {
        openUsersRatingActivity();
    }

    private void openUsersRatingActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, UsersRatingActivity.class);
        startActivity(intent);
    }

    @Override
    protected UserLoader onCreateUserLoader(Context context, int source) {
        long minDuration = loadWithSyntheticDelay ? AbstractLoader.SUGGESTED_MIN_DURATION : 0;
        loadWithSyntheticDelay = true;
        return new ProfileLoader(context, source, minDuration);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(STATE_CHOOSING_PHOTO_REASON, choosingPhotoReason);
        outState.putString(STATE_TEMP_PHOTO_FILE_PATH, tempPhotoFilePath);
    }
}
