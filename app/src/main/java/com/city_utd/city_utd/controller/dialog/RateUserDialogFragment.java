package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewPropertyAnimator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.client.RateInfo;
import com.city_utd.city_utd.loader.RateInfoLoader;
import com.city_utd.city_utd.model.PlayerLines;
import com.city_utd.city_utd.model.StatedRates;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.AppUtils;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.Date;

import butterknife.ButterKnife;

/**
 * Created by Admin on 20.07.2017.
 */

public class RateUserDialogFragment extends DialogFragment
        implements RatingBar.OnRatingBarChangeListener,
        MaterialDialog.SingleButtonCallback, View.OnClickListener,
        LoaderManager.LoaderCallbacks<RateInfo> {

    public static final String EXTRA_DELETE_RATE = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_DELETE_RATE";
    public static final String EXTRA_RATE = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_RATE";
    public static final String EXTRA_DO_NOT_SHOW_SELF_ESTIMATE = "com.city_utd.city_utd.RateUserDialogFragment.DO_NOT_SHOW_SELF_ESTIMATE";

    private static final String EXTRA_LINE = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_LINE";
    private static final String EXTRA_STATED_RATES = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_STATED_RATES";
    private static final String EXTRA_ESTIMATE_SELF = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_ESTIMATE_SELF";
    private static final String EXTRA_USER_ID = "com.city_utd.city_utd.RateUserDialogFragment.EXTRA_USER_ID";

    private static final String STATE_RATE = "com.city_utd.city_utd.RateUserDialogFragment.STATE_RATE";

    private ViewGroup container;
    private View progressView;

    private TextView charTitle1;
    private TextView charTitle2;
    private TextView charTitle3;
    private TextView charTitle4;
    private TextView charTitle5;
    private TextView charTitle6;
    private TextView charTitle7;
    private RatingBar ratingBar1;
    private RatingBar ratingBar2;
    private RatingBar ratingBar3;
    private RatingBar ratingBar4;
    private RatingBar ratingBar5;
    private RatingBar ratingBar6;
    private RatingBar ratingBar7;

    private TextView lastRateDateTextView;

    private Rate rate;
    private boolean hasInitialRate;
    private Date lastRateDate;
    private boolean fieldPlayer;
    private boolean estimateSelf;
    private int userId;
    private boolean needsLoadRateInfo;

    public static RateUserDialogFragment newInstance(String line, StatedRates rates) {
        Bundle args = createArgs(line, rates);
        RateUserDialogFragment fragment = new RateUserDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static RateUserDialogFragment newInstance(String line){
        Bundle args = createArgs(line, null);
        args.putBoolean(EXTRA_ESTIMATE_SELF, true);
        RateUserDialogFragment fragment = new RateUserDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static RateUserDialogFragment newInstance(String line, int userId){
        Bundle args = createArgs(line, null);
        args.putInt(EXTRA_USER_ID, userId);
        RateUserDialogFragment fragment = new RateUserDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    private static Bundle createArgs(String line, StatedRates rates){
        Bundle args = new Bundle();
        args.putString(EXTRA_LINE, line);
        args.putParcelable(EXTRA_STATED_RATES, rates);
        return args;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        String line = args.getString(EXTRA_LINE);
        StatedRates rates = args.getParcelable(EXTRA_STATED_RATES);
        hasInitialRate = rates != null;
        if(hasInitialRate){
            lastRateDate = rates.getLastRateDate();
        }
        fieldPlayer = TextUtils.isEmpty(line) || !PlayerLines.GOALKEEPER.equals(line);
        estimateSelf = args.getBoolean(EXTRA_ESTIMATE_SELF);
        userId = args.getInt(EXTRA_USER_ID, -1);
        needsLoadRateInfo = userId != -1;

        if(savedInstanceState != null){
            rate = savedInstanceState.getParcelable(STATE_RATE);
            if(needsLoadRateInfo && rate != null){
                needsLoadRateInfo = false;
            }
        }else if(hasInitialRate){
            rate = rates;
        }else if(!needsLoadRateInfo) {
            rate = new Rate();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        @StringRes int titleRes = estimateSelf ? R.string.estimate_self : R.string.rate_player;
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context)
                .backgroundColorRes(R.color.colorPrimary)
                .title(titleRes)
                .customView(R.layout.dialog_rate_user, true)
                .positiveText(R.string.rate)
                .negativeText(R.string.cancel)
                .onNegative(this);

        if(estimateSelf){
            builder.neutralText(R.string.do_not_show_anymore);
        }else if(hasInitialRate){
            builder.neutralText(R.string.delete_rate);
        }

        MaterialDialog dialog = builder.build();

        View positiveButton = dialog.getActionButton(DialogAction.POSITIVE);
        positiveButton.setOnClickListener(this);

        View neutralButton = dialog.getActionButton(DialogAction.NEUTRAL);
        if(neutralButton != null){
            neutralButton.setOnClickListener(new NeutralButtonOnClickListener());
        }

        return dialog;
    }

    private class NeutralButtonOnClickListener implements View.OnClickListener{

        @Override
        public void onClick(View v) {
            if(estimateSelf){
                Intent data = new Intent();
                data.putExtra(EXTRA_DO_NOT_SHOW_SELF_ESTIMATE, true);
                sendPositiveResult(data);
                dismiss();
            }else {
                Context context = getActivity();
                if (AppUtils.Commons.checkNetworkAvailability(context)) {
                    Intent data = new Intent();
                    data.putExtra(EXTRA_DELETE_RATE, true);
                    sendPositiveResult(data);
                    dismiss();
                }else {
                    @StringRes int messageRes = R.string.there_is_no_internet_connection;
                    Toast.makeText(context, messageRes, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    private void sendPositiveResult(Intent data){
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = Activity.RESULT_OK;
            targetFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = Activity.RESULT_CANCELED;
            targetFragment.onActivityResult(requestCode, resultCode, null);
        }
    }

    @Override
    public void onClick(View v) {
        Context context = getActivity();

        if(!AppUtils.Commons.checkNetworkAvailability(context)){
            Toast.makeText(context, R.string.there_is_no_internet_connection, Toast.LENGTH_SHORT).show();
            return;
        }

        if(!checkDate()){
            Toast.makeText(context, R.string.too_frequent_rate, Toast.LENGTH_SHORT).show();
            return;
        }

        if(checkRate()){
            Intent data = new Intent();
            data.putExtra(EXTRA_RATE, rate);
            sendPositiveResult(data);
            dismiss();
        }else {
            Toast.makeText(context, R.string.rate_all_characteristics, Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkDate(){
        if(hasInitialRate){
            Date now = new Date();
            long diffInMillis = now.getTime() - lastRateDate.getTime();
            long diffDays =  diffInMillis / (24 * 60 * 60 * 1000);
            return diffDays > 30;
        }
        return true;
    }

    private boolean checkRate(){
        if(fieldPlayer){
            return checkFieldPlayerRate();
        }else {
            return checkGoalkeeperRate();
        }
    }

    private boolean checkFieldPlayerRate(){
        return rate.getSpeed() > 0 && rate.getStamina() > 0 && rate.getTechnique() > 0
                && rate.getImpactForce() > 0 && rate.getPass() > 0 && rate.getDefense() > 0
                && rate.getFriendliness() > 0;
    }

    private boolean checkGoalkeeperRate(){
        return rate.getReaction() > 0 && rate.getReliability() > 0 && rate.getHands() > 0
                && rate.getFeet() > 0 && rate.getJump() > 0 && rate.getCourage() > 0
                && rate.getFriendliness() > 0;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Dialog dialog = getDialog();

        container = ButterKnife.findById(dialog, R.id.container);
        progressView = ButterKnife.findById(dialog, R.id.progress_view);

        charTitle1 = ButterKnife.findById(dialog, R.id.char_title_1);
        charTitle2 = ButterKnife.findById(dialog, R.id.char_title_2);
        charTitle3 = ButterKnife.findById(dialog, R.id.char_title_3);
        charTitle4 = ButterKnife.findById(dialog, R.id.char_title_4);
        charTitle5 = ButterKnife.findById(dialog, R.id.char_title_5);
        charTitle6 = ButterKnife.findById(dialog, R.id.char_title_6);
        charTitle7 = ButterKnife.findById(dialog, R.id.char_title_7);

        ratingBar1 = ButterKnife.findById(dialog, R.id.rating_bar_1);
        ratingBar2 = ButterKnife.findById(dialog, R.id.rating_bar_2);
        ratingBar3 = ButterKnife.findById(dialog, R.id.rating_bar_3);
        ratingBar4 = ButterKnife.findById(dialog, R.id.rating_bar_4);
        ratingBar5 = ButterKnife.findById(dialog, R.id.rating_bar_5);
        ratingBar6 = ButterKnife.findById(dialog, R.id.rating_bar_6);
        ratingBar7 = ButterKnife.findById(dialog, R.id.rating_bar_7);

        ratingBar1.setOnRatingBarChangeListener(this);
        ratingBar2.setOnRatingBarChangeListener(this);
        ratingBar3.setOnRatingBarChangeListener(this);
        ratingBar4.setOnRatingBarChangeListener(this);
        ratingBar5.setOnRatingBarChangeListener(this);
        ratingBar6.setOnRatingBarChangeListener(this);
        ratingBar7.setOnRatingBarChangeListener(this);

        lastRateDateTextView = ButterKnife.findById(dialog, R.id.last_rate_date_text_view);

        if(!needsLoadRateInfo){
            bindRate();
            if(hasInitialRate){
                bindLastRateDate();
            }
        }else {
            container.setVisibility(View.GONE);
            progressView.setVisibility(View.VISIBLE);
            loadRateInfo();
        }
    }

    private void bindRate() {
        if (fieldPlayer) {
            bindFieldPlayerRate();
        } else {
            bindGoalkeeperRate();
        }
    }

    private void bindFieldPlayerRate(){
        charTitle1.setText(R.string.speed);
        ratingBar1.setRating(rate.getSpeed());

        charTitle2.setText(R.string.stamina);
        ratingBar2.setRating(rate.getStamina());

        charTitle3.setText(R.string.technique);
        ratingBar3.setRating(rate.getTechnique());

        charTitle4.setText(R.string.impact_force);
        ratingBar4.setRating(rate.getImpactForce());

        charTitle5.setText(R.string.pass);
        ratingBar5.setRating(rate.getPass());

        charTitle6.setText(R.string.defense);
        ratingBar6.setRating(rate.getDefense());

        charTitle7.setText(R.string.friendliness);
        ratingBar7.setRating(rate.getFriendliness());
    }

    private void bindGoalkeeperRate(){
        charTitle1.setText(R.string.reaction);
        ratingBar1.setRating(rate.getReaction());

        charTitle2.setText(R.string.reliability);
        ratingBar2.setRating(rate.getReliability());

        charTitle3.setText(R.string.hands);
        ratingBar3.setRating(rate.getHands());

        charTitle4.setText(R.string.feet);
        ratingBar4.setRating(rate.getFeet());

        charTitle5.setText(R.string.jump);
        ratingBar5.setRating(rate.getJump());

        charTitle6.setText(R.string.courage);
        ratingBar6.setRating(rate.getCourage());

        charTitle7.setText(R.string.friendliness);
        ratingBar7.setRating(rate.getFriendliness());
    }

    private void bindLastRateDate(){
        String dateString = Util.Dates.dateToString(lastRateDate, Util.Dates.DATE_FORMAT_1);
        String label = getString(R.string.last_rate, dateString);
        lastRateDateTextView.setVisibility(View.VISIBLE);
        lastRateDateTextView.setText(label);
    }

    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        if(!fromUser){
            return;
        }

        int value = (int) rating;

        int id = ratingBar.getId();
        switch (id){
            case R.id.rating_bar_1:
                if(fieldPlayer){
                    rate.setSpeed(value);
                }else{
                    rate.setReaction(value);
                }
                break;
            case R.id.rating_bar_2:
                if(fieldPlayer){
                    rate.setStamina(value);
                }else {
                    rate.setReliability(value);
                }
                break;
            case R.id.rating_bar_3:
                if(fieldPlayer){
                    rate.setTechnique(value);
                }else {
                    rate.setHands(value);
                }
                break;
            case R.id.rating_bar_4:
                if(fieldPlayer){
                    rate.setImpactForce(value);
                }else {
                    rate.setFeet(value);
                }
                break;
            case R.id.rating_bar_5:
                if(fieldPlayer){
                    rate.setPass(value);
                }else {
                    rate.setJump(value);
                }
                break;
            case R.id.rating_bar_6:
                if(fieldPlayer){
                    rate.setDefense(value);
                }else {
                    rate.setCourage(value);
                }
                break;
            case R.id.rating_bar_7:
                rate.setFriendliness(value);
                break;
        }
    }

    private void loadRateInfo(){
        LoaderManager loaderManager = getLoaderManager();
        loaderManager.initLoader(0, null, this);
    }

    @Override
    public Loader<RateInfo> onCreateLoader(int id, Bundle args) {
        startProgressAnimation();

        Context context = getActivity();
        return new RateInfoLoader(context, userId);
    }

    private void startProgressAnimation(){
        Context context = getActivity();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.ball_rotation);
        animation.setRepeatMode(Animation.RESTART);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        progressView.startAnimation(animation);
    }

    @Override
    public void onLoadFinished(Loader<RateInfo> loader, RateInfo data) {
        final BaseLoader<RateInfo> baseLoader = (BaseLoader<RateInfo>) loader;
        final String errorMessage = baseLoader.getErrorMessage();
        final boolean hasError = !TextUtils.isEmpty(errorMessage);

        setupViewsAfterLoading(!hasError);

        if(hasError){
            Context context = getActivity();
            Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
            return;
        }

        if(data != null){
            rate = data;
            lastRateDate = data.getLastRateDate();
            bindLastRateDate();
        }else {
            rate = new Rate();
        }
        bindRate();
    }

    private void setupViewsAfterLoading(final boolean success){
        animateFadeOut(progressView, 0, null, new Runnable() {
            @Override
            public void run() {
                progressView.clearAnimation();
                progressView.setVisibility(View.GONE);
                if(success){
                    showContent();
                }
            }
        });
    }

    private void animateFadeOut(View view, long startDelay, Runnable startAction, Runnable endAction){
        view.setAlpha(1);
        ViewPropertyAnimator animator = view.animate();
        animator.alpha(0).setDuration(200).setStartDelay(startDelay);
        if(startAction != null){
            animator.withStartAction(startAction);
        }
        if(endAction != null){
            animator.withEndAction(endAction);
        }
    }

    private void showContent(){
        animateFadeIn(container);
    }

    private void animateFadeIn(View view){
        view.setAlpha(0);
        view.setVisibility(View.VISIBLE);
        view.animate().alpha(1).setDuration(200);
    }

    @Override
    public void onLoaderReset(Loader<RateInfo> loader) {
        rate = null;
        lastRateDate = null;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(STATE_RATE, rate);
    }
}
