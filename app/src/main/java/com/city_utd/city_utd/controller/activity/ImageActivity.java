package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.fragment.ImageFragment;

/**
 * Created by Admin on 31.07.2017.
 */

public class ImageActivity extends AbstractActivity {

    public static final String EXTRA_IMAGE_URL = "com.city_utd.city_utd.ImageActivity.EXTRA_IMAGE_URL";

    @Override
    protected Fragment createFragment(Intent intent) {
        String imageUrl = intent.getStringExtra(EXTRA_IMAGE_URL);
        ImageUrlBuilder builder = ApiManager.newImageUrlBuilder();
        imageUrl = builder.build(imageUrl);
        return ImageFragment.newInstance(imageUrl);
    }
}
