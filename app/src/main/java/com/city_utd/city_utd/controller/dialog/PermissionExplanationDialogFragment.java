package com.city_utd.city_utd.controller.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.city_utd.city_utd.R;

/**
 * Created by Admin on 11.07.2017.
 */

public class PermissionExplanationDialogFragment extends DialogFragment
        implements MaterialDialog.SingleButtonCallback {

    private static final String EXTRA_EXPLANATION = "com.city_utd.city_utd.PermissionExplanationDialogFragment.EXTRA_EXPLANATION";

    private String explanation;

    public static PermissionExplanationDialogFragment newInstance(String explanation) {
        Bundle args = new Bundle();
        args.putString(EXTRA_EXPLANATION, explanation);
        PermissionExplanationDialogFragment fragment = new PermissionExplanationDialogFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        explanation = args.getString(EXTRA_EXPLANATION);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Context context = getActivity();
        return new MaterialDialog.Builder(context)
                .backgroundColorRes(R.color.colorPrimary)
                .content(explanation)
                .positiveText(R.string.try_again)
                .negativeText(R.string.cancel)
                .onAny(this)
                .build();
    }

    @Override
    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
        Fragment targetFragment = getTargetFragment();
        if(targetFragment != null){
            int requestCode = getTargetRequestCode();
            int resultCode = which == DialogAction.POSITIVE ? Activity.RESULT_OK : Activity.RESULT_CANCELED;
            targetFragment.onActivityResult(requestCode, resultCode, null);
        }
        dismiss();
    }
}
