package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.ImageActivity;
import com.city_utd.city_utd.controller.activity.NearestUsersActivity;
import com.city_utd.city_utd.controller.activity.PlayingFieldEditingActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.BaseAdapter;
import com.city_utd.city_utd.controller.dialog.AuthorizationNecessityDialogFragment;
import com.city_utd.city_utd.loader.PlayingFieldLoader;
import com.city_utd.city_utd.loader.ScheduleLoader;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.task.AddRecordAsyncTask;
import com.city_utd.city_utd.task.ChangeRecordAsyncTask;
import com.city_utd.city_utd.task.DeleteRecordAsyncTask;
import com.city_utd.city_utd.task.SubscribeOnFieldAsyncTask;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.Util;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.worker.loader.BaseLoader;
import com.imogene.android.carcase.worker.task.BaseAsyncTask;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TODAY;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TOMORROW;
import static com.city_utd.city_utd.util.Util.Dates.getDayLabel;

/**
 * Created by vadim on 7/16/17.
 */

public class FieldDetailedFragment extends SimpleCachingFragment<PlayingFieldDetailed>
        implements View.OnClickListener, BaseAsyncTask.Callbacks,
        DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    private static final String STATE_SELECTED_DATE = "com.city_utd.city_utd.FieldDetailedFragment.STATE_SELECTED_DATE";
    private static final String STATE_DATE_PICK_REASON = "com.city_utd.city_utd.FieldDetailedFragment.STATE_DATE_PICK_REASON";
    private static final String STATE_TEMP_DATE = "com.city_utd.city_utd.FieldDetailedFragment.STATE_TEMP_DATE";

    private static final int RC_EDIT_FIELD = 1;

    private static final int LOADER_ID_PLAYING_FIELD = 1;
    private static final int LOADER_ID_SCHEDULE = 2;

    private static final int TASK_ID_SUBSCRIBE_ON_FIELD = 1;
    private static final int TASK_ID_ADD_RECORD = 2;
    private static final int TASK_ID_CHANGE_RECORD = 3;
    private static final int TASK_ID_DELETE_RECORD = 4;

    private static final String DATE_PICKER_DIALOG_TAG = "DATE_PICKER_DIALOG";
    private static final String TIME_PICKER_DIALOG_TAG = "TIME_PICKER_DIALOG";

    private static final int DATE_PICK_REASON_ADD_RECORD = 1;
    private static final int DATE_PICK_REASON_CHANGE_RECORD = 2;

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.field_info_card)
    CardView fieldInfoCardView;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.name_text_view)
    TextView nameTextView;

    @Bind(R.id.address_text_view)
    TextView addressTextView;

    @Bind(R.id.distance_text_view)
    TextView distanceTextView;

    @Bind(R.id.subscribe_button)
    TextView subscribeButton;

    @Bind(R.id.search_nearest_users_button)
    View searchNearestUsersButton;

    @Bind(R.id.refresh_schedule_button)
    ImageView refreshScheduleButton;

    @Bind(R.id.schedule_recycler_view)
    RecyclerView scheduleRecyclerView;

    @Bind(R.id.add_record_button)
    TextView addRecordButton;

    private int fieldId;
    private int distance;

    private PlayingFieldDetailed playingField;
    private ImageUrlBuilder imageUrlBuilder;
    private ScheduleItemAdapter adapter;
    private LoaderManager.LoaderCallbacks<List<ScheduleItem>> scheduleLoaderCallbacks;
    private long progressAnimationStartTime;
    private float ballFinalRotation;
    private int fieldInfoCardInitialMargin;
    private Calendar selectedDate;
    private int datePickReason;
    private Date tempDate;

    public static FieldDetailedFragment newInstance(int fieldId, int distance) {
        Bundle args = new Bundle();
        args.putInt(FieldDetailedActivity.EXTRA_FIELD_ID, fieldId);
        args.putInt(FieldDetailedActivity.EXTRA_DISTANCE, distance);
        FieldDetailedFragment fragment = new FieldDetailedFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        Bundle args = getArguments();
        fieldId = args.getInt(FieldDetailedActivity.EXTRA_FIELD_ID);
        distance = args.getInt(FieldDetailedActivity.EXTRA_DISTANCE);

        if(savedInstanceState != null){
            selectedDate = (Calendar) savedInstanceState.getSerializable(STATE_SELECTED_DATE);
            datePickReason = savedInstanceState.getInt(STATE_DATE_PICK_REASON);
            tempDate = (Date) savedInstanceState.getSerializable(STATE_TEMP_DATE);
        }

        ensureDateAndTimePickersListeners();
    }

    private void ensureDateAndTimePickersListeners(){
        FragmentManager fragmentManager = getFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(DATE_PICKER_DIALOG_TAG);
        if(fragment != null){
            ((DatePickerDialog) fragment).setOnDateSetListener(this);
        }else {
            fragment = fragmentManager.findFragmentByTag(TIME_PICKER_DIALOG_TAG);
            if(fragment != null){
                ((TimePickerDialog) fragment).setOnTimeSetListener(this);
            }
        }
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_field_detailed;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        AbstractActivity activity = (AbstractActivity) getActivity();

        // setup toolbar
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);
        activity.setSupportActionBar(toolbar);

        // setup on click listeners
        imageView.setOnClickListener(this);
        subscribeButton.setOnClickListener(this);
        searchNearestUsersButton.setOnClickListener(this);
        refreshScheduleButton.setOnClickListener(this);
        addRecordButton.setOnClickListener(this);

        // setup recycler view
        LinearLayoutManager layoutManager = new LinearLayoutManager(activity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        scheduleRecyclerView.setLayoutManager(layoutManager);
        adapter = new ScheduleItemAdapter(null);
        scheduleRecyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        scheduleRecyclerView.addItemDecoration(decoration);
        scheduleRecyclerView.setNestedScrollingEnabled(false);
    }

    class ScheduleItemAdapter extends BaseAdapter<ScheduleItem, ScheduleItemAdapter.ItemViewHolder>{

        private final int userId;

        ScheduleItemAdapter(List<ScheduleItem> data) {
            super(data);
            ensureImageUrlBuilder();

            Context context = getActivity();
            LocalDataManager manager = LocalDataManager.getInstance(context);
            userId = manager.getUserId();
        }

        @Override
        public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_schedule_item, parent, false);
            return new ItemViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(ItemViewHolder holder, ScheduleItem item) {
            String imageUrl = item.getImageUrl();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(FieldDetailedFragment.this).load(imageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(holder.imageView);

            String name = item.getUserName();
            if(!TextUtils.isEmpty(name)){
                holder.nameTextView.setVisibility(View.VISIBLE);
                holder.nameTextView.setText(name);
            }else {
                holder.nameTextView.setVisibility(View.GONE);
            }

            String line = item.getLine();
            String lineString = Util.Commons.getLineString(getActivity(), line);
            holder.lineTextView.setText(lineString);

            float rating = item.getActualRating();
            holder.ratingBar.setRating(rating);

            Date date = item.getDate();
            String dateString = getDateString(date);
            holder.timeTextView.setText(dateString);

            int id = item.getUserId();
            if(userId == id){
                holder.changeTimeButton.setVisibility(View.VISIBLE);
                holder.deleteItemButton.setVisibility(View.VISIBLE);
            }else {
                holder.changeTimeButton.setVisibility(View.GONE);
                holder.deleteItemButton.setVisibility(View.GONE);
            }
        }

        private String getDateString(Date date){
            int dayLabel = getDayLabel(date);
            String dateString;
            switch (dayLabel){
                case DAY_LABEL_TODAY:
                    dateString = Util.Dates.dateToTimeString(date);
                    return getString(R.string.will_come, dateString);
                case DAY_LABEL_TOMORROW:
                    dateString = Util.Dates.dateToTimeString(date);
                    return getString(R.string.will_come_tomorrow, dateString);
                default:
                    dateString = Util.Dates.dateToString(date, Util.Dates.DATE_TIME_FORMAT_1);
                    return getString(R.string.will_come, dateString);
            }
        }

        class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.image_view)
            ImageView imageView;

            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Bind(R.id.delete_item_button)
            View deleteItemButton;

            @Bind(R.id.line_text_view)
            TextView lineTextView;

            @Bind(R.id.rating_bar)
            RatingBar ratingBar;

            @Bind(R.id.time_text_view)
            TextView timeTextView;

            @Bind(R.id.change_time_button)
            View changeTimeButton;

            ItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
                changeTimeButton.setOnClickListener(this);
                deleteItemButton.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int id = v.getId();

                int adapterPosition = getAdapterPosition();
                ScheduleItem item = getItem(adapterPosition);

                switch (id){
                    case R.id.time_text_view:
                    case R.id.change_time_button:
                        if(item.getUserId() == userId){
                            datePickReason = DATE_PICK_REASON_CHANGE_RECORD;
                            tempDate = item.getDate();
                            showDatePickerDialog();
                        }
                        break;
                    case R.id.delete_item_button:
                        Date date = item.getDate();
                        deleteRecord(date);
                        break;
                    default:
                        int userId = item.getUserId();
                        openUserProfileActivity(userId);
                        break;
                }
            }
        }
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.image_view:
                openImageActivityIfPossible();
                break;
            case R.id.subscribe_button:
                subscribeOrUnsubscribe();
                break;
            case R.id.search_nearest_users_button:
                openNearestUsersActivity();
                break;
            case R.id.refresh_schedule_button:
                refreshScheduleIfThereIsInternetConnection();
                break;
            case R.id.add_record_button:
                handleAddRecordClick();
                break;
            default:
                getActivity().finish();
                break;
        }
    }

    private void openImageActivityIfPossible(){
        if(playingField != null){
            String imageUrl = playingField.getImageUrl();
            if(!TextUtils.isEmpty(imageUrl)){
                openImageActivity(imageUrl);
            }
        }
    }

    private void openImageActivity(String imageUrl){
        Context context = getActivity();
        Intent intent = new Intent(context, ImageActivity.class);
        intent.putExtra(ImageActivity.EXTRA_IMAGE_URL, imageUrl);
        startActivity(intent);
    }

    private void subscribeOrUnsubscribe(){
        if(checkNetworkAvailability()){
            if(isAuthorized()){
                subscribeOrUnsubscribeInternal();
            }else {
                int reason = AuthorizationNecessityDialogFragment.REASON_FIELD_SUBSCRIPTION;
                showAuthorizationNecessityDialog(reason);
            }
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void subscribeOrUnsubscribeInternal(){
        boolean subscribe = !playingField.isSubscription();
        SubscribeOnFieldAsyncTask asyncTask = new SubscribeOnFieldAsyncTask(TASK_ID_SUBSCRIBE_ON_FIELD, this, fieldId, subscribe);
        asyncTask.execute();
    }

    @Override
    public void onTaskStarted(int taskId) {}

    @Override
    public void onTaskProgress(int taskId, Object... progress) {}

    @Override
    public void onTaskFinished(int taskId, Object result) {
        switch (taskId){
            case TASK_ID_SUBSCRIBE_ON_FIELD:
                PlayingFieldDetailed data = (PlayingFieldDetailed) result;
                playingField.setSubscription(data.isSubscription());
                updateSubscribeButtonText();
                break;
            case TASK_ID_ADD_RECORD:
            case TASK_ID_CHANGE_RECORD:
            case TASK_ID_DELETE_RECORD:
                if((boolean) result){
                    refreshSchedule();
                }
                break;
        }
    }

    private void updateSubscribeButtonText(){
        boolean subscription = playingField.isSubscription();
        @StringRes int subscriptionButtonTextRes = subscription ?
                R.string.unsubscribe : R.string.subscribe;
        subscribeButton.setText(subscriptionButtonTextRes);
    }

    @Override
    public void onTaskError(int taskId, String errorMessage) {
        if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    private void openNearestUsersActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, NearestUsersActivity.class);
        intent.putExtra(NearestUsersActivity.EXTRA_LOCATION, playingField.getCoordinates());
        startActivity(intent);
    }

    private void refreshScheduleIfThereIsInternetConnection(){
        if(checkNetworkAvailability()){
            refreshSchedule();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private void refreshSchedule(){
        ensureScheduleLoaderCallbacks();
        reloadData(LOADER_ID_SCHEDULE, null, scheduleLoaderCallbacks);
    }

    private void ensureScheduleLoaderCallbacks(){
        if(scheduleLoaderCallbacks == null){
            scheduleLoaderCallbacks = new LoaderManager.LoaderCallbacks<List<ScheduleItem>>() {
                @Override
                public Loader<List<ScheduleItem>> onCreateLoader(int id, Bundle args) {
                    startBallProgressAnimation();

                    Context context = getActivity();
                    Date maxDate = createScheduleMaxDate();
                    return new ScheduleLoader(context, playingField, null, maxDate, false);
                }

                @Override
                public void onLoadFinished(Loader<List<ScheduleItem>> loader, List<ScheduleItem> data) {
                    stopBallProgressAnimation();

                    boolean hasData = data != null;
                    if(hasData){
                        playingField.setSchedule(data);
                        adapter.setData(data);
                    }else {
                        BaseLoader<List<ScheduleItem>> baseLoader = (BaseLoader<List<ScheduleItem>>) loader;
                        String errorMessage = baseLoader.getErrorMessage();
                        if(!TextUtils.isEmpty(errorMessage)){
                            showToast(errorMessage, false);
                        }
                    }
                }

                @Override
                public void onLoaderReset(Loader<List<ScheduleItem>> loader) {
                    adapter.setData(null);
                }
            };
        }
    }

    private void startBallProgressAnimation(){
        Context context = getActivity();
        Animation animation = AnimationUtils.loadAnimation(context, R.anim.ball_rotation);
        animation.setRepeatMode(Animation.RESTART);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setInterpolator(new LinearInterpolator());
        refreshScheduleButton.startAnimation(animation);
        progressAnimationStartTime = System.currentTimeMillis();
    }

    private void stopBallProgressAnimation(){
        long currentTime = System.currentTimeMillis();
        long duration = currentTime - progressAnimationStartTime;
        ballFinalRotation += duration * 360 / 2000;

        refreshScheduleButton.clearAnimation();
        refreshScheduleButton.setRotation(ballFinalRotation);
    }

    private void handleAddRecordClick(){
        if(checkNetworkAvailability()){
            if(isAuthorized()){
                datePickReason = DATE_PICK_REASON_ADD_RECORD;
                showDatePickerDialog();
            }else {
                int reason = AuthorizationNecessityDialogFragment.REASON_ADDING_RECORD;
                showAuthorizationNecessityDialog(reason);
            }
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    private boolean isAuthorized(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        return manager.isAuthorized();
    }

    private void showDatePickerDialog(){
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = DatePickerDialog.newInstance(this, year, month, day);

        dialog.setMinDate(calendar);
        calendar.add(Calendar.DAY_OF_MONTH, 7);
        dialog.setMaxDate(calendar);

        FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, DATE_PICKER_DIALOG_TAG);
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        selectedDate = Calendar.getInstance();
        selectedDate.set(Calendar.YEAR, year);
        selectedDate.set(Calendar.MONTH, monthOfYear);
        selectedDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        showTimePickerDialog();
    }

    private void showTimePickerDialog(){
        TimePickerDialog dialog = TimePickerDialog.newInstance(this, true);
        FragmentManager fragmentManager = getFragmentManager();
        dialog.show(fragmentManager, TIME_PICKER_DIALOG_TAG);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        selectedDate.set(Calendar.HOUR_OF_DAY, hourOfDay);
        selectedDate.set(Calendar.MINUTE, minute);
        selectedDate.set(Calendar.SECOND, 0);
        selectedDate.set(Calendar.MILLISECOND, 0);

        Calendar now = Calendar.getInstance();
        now.set(Calendar.SECOND, 0);
        now.set(Calendar.MILLISECOND, 0);
        if(selectedDate.before(now)){
            showToast(R.string.you_can_not_add_record_on_past, false);
        }else if(datePickReason == DATE_PICK_REASON_ADD_RECORD){
            addRecord();
        }else if(datePickReason == DATE_PICK_REASON_CHANGE_RECORD){
            changeRecord();
        }
    }

    private void addRecord(){
        int playingFieldId = playingField.getId();
        Date date = selectedDate.getTime();
        AddRecordAsyncTask asyncTask = new AddRecordAsyncTask(TASK_ID_ADD_RECORD, this, playingFieldId, date);
        asyncTask.execute();
    }

    private void changeRecord(){
        int playingFieldId = playingField.getId();
        Date newDate = selectedDate.getTime();
        ChangeRecordAsyncTask asyncTask = new ChangeRecordAsyncTask(
                TASK_ID_CHANGE_RECORD, this, playingFieldId, tempDate, newDate);
        asyncTask.execute();
    }

    private void deleteRecord(Date date){
        if(checkNetworkAvailability()){
            int playingFieldId = playingField.getId();
            DeleteRecordAsyncTask asyncTask = new DeleteRecordAsyncTask(
                    TASK_ID_DELETE_RECORD, this, playingFieldId, date);
            asyncTask.execute();
        }else {
            showToast(R.string.there_is_no_internet_connection, false);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.playing_field_menu, menu);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.edit);
        boolean visible = playingField != null && playingField.getCreatorId() == getUserId();
        item.setVisible(visible);
    }

    private int getUserId(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        return manager.getUserId();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean handled = super.onOptionsItemSelected(item);
        if(!handled){
            int itemId = item.getItemId();
            if(itemId == R.id.edit){
                openPlayingFieldEditingActivity();
                handled = true;
            }
        }
        return handled;
    }

    private void openPlayingFieldEditingActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, PlayingFieldEditingActivity.class);
        intent.putExtra(PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_ID, playingField.getId());
        startActivityForResult(intent, RC_EDIT_FIELD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_EDIT_FIELD:
                if(resultCode == Activity.RESULT_OK){
                    onLoadFromServer();
                }
                break;
            default:
                super.onActivityResult(requestCode, resultCode, data);
                break;
        }
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = prepareLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(LOADER_ID_PLAYING_FIELD, args);
        }else {
            loadData(LOADER_ID_PLAYING_FIELD, args);
        }
    }

    private Bundle prepareLoaderArgs(int source){
        Bundle args = updateLoaderArgs(source);
        Date maxDate = createScheduleMaxDate();
        args.putSerializable(PlayingFieldLoader.EXTRA_MAX_DATE, maxDate);
        return args;
    }

    private Date createScheduleMaxDate(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 7);
        return calendar.getTime();
    }

    @Override
    protected void onLoadFromServer() {
        Bundle args = prepareLoaderArgs(BaseLoader.SOURCE_SERVER_DATABASE);
        reloadData(LOADER_ID_PLAYING_FIELD, args);
    }

    @Override
    protected BaseLoader<PlayingFieldDetailed> onCreateLoader(Context context, int id, Bundle args) {
        int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        Date maxDate = (Date) args.getSerializable(PlayingFieldLoader.EXTRA_MAX_DATE);
        return new PlayingFieldLoader(context, source, fieldId, maxDate);
    }

    @Override
    protected void onLoaderReset(BaseLoader<PlayingFieldDetailed> loader) {
        adapter.setData(null);
        playingField = null;
    }

    @Override
    protected void onLoadFinished(BaseLoader<PlayingFieldDetailed> loader, PlayingFieldDetailed data) {
        super.onLoadFinished(loader, data);
        if(data != null){
            playingField = data;
            bindData(data);
            getActivity().supportInvalidateOptionsMenu();
        }
    }

    private void bindData(@NonNull PlayingFieldDetailed data){
        String imageUrl = data.getImageUrl();
        ensureImageUrlBuilder();
        imageUrl = imageUrlBuilder.build(imageUrl);
        @DrawableRes int placeholderRes = R.drawable.ic_field_placeholder;
        Glide.with(this).load(imageUrl).placeholder(placeholderRes).error(placeholderRes).into(imageView);

        String name = data.getName();
        if(!TextUtils.isEmpty(name)){
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(name);
        }else {
            nameTextView.setVisibility(View.GONE);
        }

        String address = data.getAddress();
        if(!TextUtils.isEmpty(address)){
            addressTextView.setVisibility(View.VISIBLE);
            addressTextView.setText(address);
        }else {
            addressTextView.setVisibility(View.GONE);
        }

        if(distance > -1){
            String distanceString = getString(R.string.distance_value, distance);
            distanceTextView.setText(distanceString);
            distanceTextView.setVisibility(View.VISIBLE);
        }else {
            distanceTextView.setVisibility(View.GONE);
        }

        updateSubscribeButtonText();

        adapter.setData(data.getSchedule());
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    @Override
    public boolean showConnectivityState(int appearance) {
        boolean result = super.showConnectivityState(appearance);
        if(result){
            adjustFieldInfoCardMarginForConnectivityStateView();
        }
        return result;
    }

    private void adjustFieldInfoCardMarginForConnectivityStateView(){
        int connectivityStateViewHeight = convertDpsInPixels(36);
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) fieldInfoCardView.getLayoutParams();
        fieldInfoCardInitialMargin = layoutParams.topMargin;
        int margin = fieldInfoCardInitialMargin + connectivityStateViewHeight;
        updateFieldInfoCardMargin(margin);
    }

    private void updateFieldInfoCardMargin(int margin){
        LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) fieldInfoCardView.getLayoutParams();
        layoutParams.topMargin = margin;
        fieldInfoCardView.setLayoutParams(layoutParams);
    }

    @Override
    public boolean hideConnectivityState() {
        boolean result = super.hideConnectivityState();
        if(result){
            updateFieldInfoCardMargin(fieldInfoCardInitialMargin);
        }
        return result;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(STATE_SELECTED_DATE, selectedDate);
        outState.putInt(STATE_DATE_PICK_REASON, datePickReason);
        outState.putSerializable(STATE_TEMP_DATE, tempDate);
    }
}
