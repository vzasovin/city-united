package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.DetailedGalleryActivity;
import com.city_utd.city_utd.controller.activity.GallerySlideShowActivity;
import com.city_utd.city_utd.controller.adapter.BaseAdapter;
import com.city_utd.city_utd.view.SquareImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 31.07.2017.
 */

public class DetailedGalleryFragment extends AbstractFragment
        implements View.OnClickListener, RequestListener<String, GlideDrawable>{

    private static final String STATE_POSTPONED_TRANSITION_INITIATED = "com.city_utd.city_utd.DetailedGalleryFragment.STATE_POSTPONED_TRANSITION_INITIATED";

    @Nullable
    @Bind(R.id.first_image_view)
    ImageView firstImageView;

    @Nullable
    @Bind(R.id.second_image_view)
    ImageView secondImageView;

    @Nullable
    @Bind(R.id.gallery_recycler_view)
    RecyclerView recyclerView;

    private ArrayList<String> gallery;
    private String selectedImageUrl;
    private boolean showAsList;
    private ImageUrlBuilder imageUrlBuilder;
    private boolean postponedTransitionInitiated;

    public static DetailedGalleryFragment newInstance(ArrayList<String> gallery, String selectedImageUrl) {
        Bundle args = new Bundle();
        args.putStringArrayList(DetailedGalleryActivity.EXTRA_GALLERY, gallery);
        args.putString(DetailedGalleryActivity.EXTRA_SELECTED_IMAGE_URL, selectedImageUrl);
        DetailedGalleryFragment fragment = new DetailedGalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        gallery = args.getStringArrayList(DetailedGalleryActivity.EXTRA_GALLERY);
        showAsList = gallery != null && gallery.size() == 2;
        selectedImageUrl = args.getString(DetailedGalleryActivity.EXTRA_SELECTED_IMAGE_URL);

        if(savedInstanceState != null){
            postponedTransitionInitiated = savedInstanceState.getBoolean(STATE_POSTPONED_TRANSITION_INITIATED);
        }
    }

    @Override
    protected int getLayoutRes() {
        return showAsList ? R.layout.fragment_detailed_gallery_list : R.layout.fragment_detailed_gallery_table;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        AbstractActivity activity = (AbstractActivity) getActivity();
        activity.supportPostponeEnterTransition();

        if(showAsList){
            setupListMode();
        }else {
            setupTableMode();
        }
    }

    private void setupListMode(){
        if(firstImageView == null || secondImageView == null){
            return;
        }

        String imageUrl;
        imageUrl = gallery.get(0);
        bindImage(firstImageView, imageUrl);
        imageUrl = gallery.get(1);
        bindImage(secondImageView, imageUrl);

        firstImageView.setOnClickListener(this);
        secondImageView.setOnClickListener(this);
    }

    private void bindImage(ImageView imageView, String imageUrl){
        boolean withPostponedTransition = !postponedTransitionInitiated;
        if(!TextUtils.isEmpty(imageUrl) && imageUrl.equals(selectedImageUrl)){
            withPostponedTransition &= true;
            ViewCompat.setTransitionName(imageView, selectedImageUrl);
        }
        postponedTransitionInitiated = withPostponedTransition;

        ensureImageUrlBuilder();
        imageUrl = imageUrlBuilder.build(imageUrl);
        DrawableTypeRequest<String> request = Glide.with(this).load(imageUrl);

        if(withPostponedTransition){
            request.dontAnimate();
            request.listener(this);
        }

        request.diskCacheStrategy(DiskCacheStrategy.SOURCE).into(imageView);
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    @Override
    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
        getActivity().supportStartPostponedEnterTransition();
        return false;
    }

    @Override
    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
        getActivity().supportStartPostponedEnterTransition();
        return false;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id){
            case R.id.first_image_view:
                openGallerySlideShowActivity(gallery.get(0));
                break;
            case R.id.second_image_view:
                openGallerySlideShowActivity(gallery.get(1));
                break;
        }
    }

    private void setupTableMode(){
        if(recyclerView == null){
            return;
        }

        Context context = getActivity();
        GridLayoutManager layoutManager = new GridLayoutManager(context, 2);
        int orientation = isLandscapeOrientation() ? GridLayoutManager.HORIZONTAL : GridLayoutManager.VERTICAL;
        layoutManager.setOrientation(orientation);
        recyclerView.setLayoutManager(layoutManager);
        GalleryAdapter adapter = new GalleryAdapter(gallery);
        recyclerView.setAdapter(adapter);
    }

    class GalleryAdapter extends BaseAdapter<String, GalleryAdapter.GalleryItemViewHolder>{

        GalleryAdapter(List<String> data) {
            super(data);
        }

        @Override
        public GalleryItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_gallery_detailed_item, parent, false);
            return new GalleryItemViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(GalleryItemViewHolder holder, String item) {
            ImageView imageView = holder.imageView;
            bindImage(imageView, item);
        }

        class GalleryItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.image_view)
            SquareImageView imageView;

            GalleryItemViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
                setupBaseDimension();
            }

            private void setupBaseDimension(){
                int baseDimension = isLandscapeOrientation() ?
                        SquareImageView.BASE_DIMENSION_HEIGHT :
                        SquareImageView.BASE_DIMENSION_WIDTH;
                imageView.setBaseDimension(baseDimension);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                String imageUrl = getItem(adapterPosition);
                openGallerySlideShowActivity(imageUrl);
            }
        }
    }

    private void openGallerySlideShowActivity(String imageUrl){
        Activity activity = getActivity();
        Intent intent = new Intent(activity, GallerySlideShowActivity.class);
        intent.putExtra(GallerySlideShowActivity.EXTRA_GALLERY, gallery);
        intent.putExtra(GallerySlideShowActivity.EXTRA_SELECTED_IMAGE_URL, imageUrl);
        startActivity(intent);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_POSTPONED_TRANSITION_INITIATED, postponedTransitionInitiated);
    }
}
