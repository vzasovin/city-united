package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.PlayingFieldEditingFragment;

/**
 * Created by Admin on 03.08.2017.
 */

public class PlayingFieldEditingActivity extends AbstractActivity{

    public static final String EXTRA_PLAYING_FIELD_ID = "com.city_utd.city_utd.PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_ID";
    public static final String EXTRA_PLAYING_FIELD_RESULT = "com.city_utd.city_utd.PlayingFieldEditingActivity.EXTRA_PLAYING_FIELD_RESULT";

    @Override
    protected Fragment createFragment(Intent intent) {
        int playingFieldId = intent.getIntExtra(EXTRA_PLAYING_FIELD_ID, -1);
        return PlayingFieldEditingFragment.newInstance(playingFieldId);
    }
}
