package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.NearestQueryParamsHolder;
import com.city_utd.city_utd.controller.activity.NearestUsersActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.controller.adapter.PagingAdapter;
import com.city_utd.city_utd.loader.NearestUsersLoader;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.util.Util;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.google.android.gms.maps.model.LatLng;
import com.imogene.android.carcase.controller.paging.OnThresholdReachedListener;
import com.imogene.android.carcase.controller.paging.RecyclerViewLoadMoreListener;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 17.07.2017.
 */

public class NearestUsersFragment extends SimpleCachingFragment<List<User>>
        implements View.OnClickListener, OnThresholdReachedListener{

    @Bind(R.id.toolbar)
    Toolbar toolbar;

    @Bind(R.id.container)
    RecyclerView recyclerView;

    private LatLng location;
    private NearestQueryParamsHolder paramsHolder;
    private UsersAdapter adapter;
    private RecyclerViewLoadMoreListener loadMoreListener;

    public static NearestUsersFragment newInstance(LatLng location) {
        Bundle args = new Bundle();
        args.putParcelable(NearestUsersActivity.EXTRA_LOCATION, location);
        NearestUsersFragment fragment = new NearestUsersFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        location = args.getParcelable(NearestUsersActivity.EXTRA_LOCATION);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_nearest_users;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        // setup toolbar
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp);
        toolbar.setNavigationOnClickListener(this);

        // setup recycler view
        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new UsersAdapter(null);
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        recyclerView.addItemDecoration(decoration);

        loadMoreListener = new RecyclerViewLoadMoreListener(.7f, this);
        recyclerView.addOnScrollListener(loadMoreListener);
    }

    @Override
    public void onClick(View v) {
        getActivity().finish();
    }

    @Override
    public void onThresholdReached(int position) {
        ensureParamsHolder();
        int page = paramsHolder.getPage();
        paramsHolder.setPage(page + 1);
        onLoadFromServer();
    }

    class UsersAdapter extends PagingAdapter<User, UsersAdapter.UserViewHolder> {

        private ImageUrlBuilder imageUrlBuilder;

        UsersAdapter(List<User> data) {
            super(data);
        }

        @Override
        public UserViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_user, parent, false);
            return new UserViewHolder(itemView);
        }

        @Override
        protected void onBindViewHolder(UserViewHolder holder, User item) {
            ensureImageUrlBuilder();
            String imageUrl = item.getImageUrl();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
            Glide.with(NearestUsersFragment.this).load(imageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(holder.imageView);

            String name = item.getName();
            if(!TextUtils.isEmpty(name)){
                holder.nameTextView.setVisibility(View.VISIBLE);
                holder.nameTextView.setText(name);
            }else {
                holder.nameTextView.setVisibility(View.INVISIBLE);
            }

            String line = item.getLine();
            String lineString = Util.Commons.getLineString(getActivity(), line);
            holder.lineTextView.setText(lineString);

            int distance = item.getDistance();
            String distanceString = getString(R.string.distance_value, distance);
            holder.distanceTextView.setText(distanceString);

            float rating = item.getActualRating();
            holder.ratingBar.setRating(rating);
        }

        private void ensureImageUrlBuilder(){
            if(imageUrlBuilder == null){
                imageUrlBuilder = ApiManager.newImageUrlBuilder();
            }
        }

        class UserViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Bind(R.id.image_view)
            ImageView imageView;

            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Bind(R.id.distance_text_view)
            TextView distanceTextView;

            @Bind(R.id.line_text_view)
            TextView lineTextView;

            @Bind(R.id.rating_bar)
            RatingBar ratingBar;

            UserViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                User user = getItem(adapterPosition);
                int userId = user.getId();
                openUserProfileActivity(userId);
            }
        }
    }

    private void openUserProfileActivity(int userId){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    protected boolean isCachingEnabled() {
        return false;
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {}

    @Override
    protected void onLoadFromServer() {
        reloadData(0, null);
    }

    @Override
    protected BaseLoader<List<User>> onCreateLoader(Context context, int id, Bundle args) {
        ensureParamsHolder();
        return new NearestUsersLoader(context, paramsHolder);
    }

    private void ensureParamsHolder(){
        if(paramsHolder == null){
            paramsHolder = new NearestQueryParamsHolder(100, location);
            paramsHolder.setPage(1);
        }
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<User>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<User>> loader, List<User> data) {
        super.onLoadFinished(loader, data);

        final boolean hasData = data != null;
        final String errorMessage = loader.getErrorMessage();

        if(hasData){
            adapter.addData(data);
            boolean emptyData = data.isEmpty();
            if(!emptyData){
                loadMoreListener.notifyContinueListen();
            }
        }else if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if(checkNetworkAvailability()){
            showProgressViewIfNeeded();
        }
    }
}
