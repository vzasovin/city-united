package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.controller.MainNavigationHost;
import com.city_utd.city_utd.controller.MainNavigationPart;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.loader.BestPlayerLoader;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 04.08.2017.
 */

public class AwardFragment extends SimpleCachingFragment<User>
        implements MainNavigationPart, View.OnClickListener {

    @Bind(R.id.user_info_container)
    View userInfoContainer;

    @Bind(R.id.message_text_view)
    TextView messageTextView;

    @Bind(R.id.image_view)
    ImageView imageView;

    @Bind(R.id.name_text_view)
    TextView nameTextView;

    @Bind(R.id.distance_text_view)
    TextView distanceTextView;

    @Bind(R.id.line_text_view)
    TextView lineTextView;

    @Bind(R.id.rating_bar)
    RatingBar ratingBar;

    private User bestPlayer;
    private ImageUrlBuilder imageUrlBuilder;

    public static AwardFragment newInstance() {
        return new AwardFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_award;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        distanceTextView.setVisibility(View.GONE);
        userInfoContainer.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(bestPlayer != null){
            openUserProfileActivity();
        }
    }

    private void openUserProfileActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, UserProfileActivity.class);
        int userId = bestPlayer.getId();
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, userId);
        startActivity(intent);
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_SERVER_DATABASE);
        reloadData(0, args);
    }

    @Override
    protected BaseLoader<User> onCreateLoader(Context context, int id, Bundle args) {
        int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        return new BestPlayerLoader(context, source);
    }

    @Override
    protected void onLoaderReset(BaseLoader<User> loader) {}

    @Override
    protected void onLoadFinished(BaseLoader<User> loader, User data) {
        final boolean hasData = data != null;
        final int source = loader.getSource();
        final boolean fromServer = source != BaseLoader.SOURCE_DATABASE;

        if(!hasData && fromServer){
            userInfoContainer.setVisibility(View.GONE);
            messageTextView.setVisibility(View.VISIBLE);
        }

        super.onLoadFinished(loader, data);

        if(hasData){
            bestPlayer = data;
            bindData(data);
        }
    }

    private void bindData(User user){
        ensureImageUrlBuilder();
        String imageUrl = user.getImageUrl();
        imageUrl = imageUrlBuilder.build(imageUrl);
        @DrawableRes int placeholderRes = R.drawable.user_placeholder_84dp;
        Glide.with(this).load(imageUrl)
                .placeholder(placeholderRes)
                .error(placeholderRes)
                .into(imageView);

        String name = user.getName();
        if(!TextUtils.isEmpty(name)){
            nameTextView.setVisibility(View.VISIBLE);
            nameTextView.setText(name);
        }else {
            nameTextView.setVisibility(View.INVISIBLE);
        }

        String line = user.getLine();
        String lineString = Util.Commons.getLineString(getActivity(), line);
        lineTextView.setText(lineString);

        float rating = user.getActualRating();
        ratingBar.setRating(rating);
    }

    private void ensureImageUrlBuilder(){
        if(imageUrlBuilder == null){
            imageUrlBuilder = ApiManager.newImageUrlBuilder();
        }
    }

    @Override
    public boolean shouldDisplayConnectivityState() {
        return false;
    }

    @Override
    public MainNavigationHost getNavigationHost() {
        return (MainNavigationHost) getActivity();
    }

    @Override
    public void reloadContent() {
        onLoadFromServer();
    }

    @Override
    public boolean setupFloatingActionButton(FloatingActionButton floatingActionButton) {
        return false;
    }
}
