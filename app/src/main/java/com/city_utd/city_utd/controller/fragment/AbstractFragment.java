package com.city_utd.city_utd.controller.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.activity.SplashActivity;
import com.city_utd.city_utd.controller.dialog.AuthorizationNecessityDialogFragment;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.controller.BaseFragment;

/**
 * Created by Admin on 19.07.2017.
 */

public abstract class AbstractFragment extends BaseFragment {

    private static final int RC_AUTHORIZATION_NECESSITY = 4757585;

    private static final String AUTHORIZATION_NECESSITY_DIALOG_TAG = "AUTHORIZATION_NECESSITY_DIALOG";

    protected final void showAuthorizationNecessityDialog(int reason){
        FragmentManager fragmentManager = getFragmentManager();
        DialogFragment dialog = AuthorizationNecessityDialogFragment.newInstance(reason);
        dialog.setTargetFragment(this, RC_AUTHORIZATION_NECESSITY);
        dialog.show(fragmentManager, AUTHORIZATION_NECESSITY_DIALOG_TAG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode){
            case RC_AUTHORIZATION_NECESSITY:
                if(resultCode == Activity.RESULT_OK){
                    setFirstLaunchFlag();
                    openSplashActivity();
                }
                break;
        }
    }

    private void setFirstLaunchFlag(){
        Context context = getActivity();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setFirstLaunchFlag(true);
    }

    private void openSplashActivity(){
        Context context = getActivity();
        Intent intent = new Intent(context, SplashActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public final boolean isLandscapeOrientation(){
        Resources resources = getResources();
        return resources.getBoolean(R.bool.land);
    }
}
