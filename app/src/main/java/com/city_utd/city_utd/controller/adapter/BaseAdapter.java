package com.city_utd.city_utd.controller.adapter;

import android.support.v7.widget.RecyclerView;

import java.util.List;

/**
 * Created by Admin on 17.07.2017.
 */

public abstract class BaseAdapter<T, VH extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<VH> {

    protected List<T> data;

    public BaseAdapter(List<T> data){
        this.data = data;
    }

    @Override
    public void onBindViewHolder(VH holder, int position) {
        T item = getItem(position);
        onBindViewHolder(holder, item);
    }

    public T getItem(int position){
        return data.get(position);
    }

    protected abstract void onBindViewHolder(VH holder, T item);

    @Override
    public int getItemCount() {
        return data != null ? data.size() : 0;
    }

    public final void setData(List<T> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public final List<T> getData(){
        return data;
    }
}
