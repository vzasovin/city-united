package com.city_utd.city_utd.controller.fragment;

/**
 * Created by vadim on 7/17/17.
 */

interface MyTeamPart {

    void reload();

    void addFakeItem();

    void removeFakeItem();
}
