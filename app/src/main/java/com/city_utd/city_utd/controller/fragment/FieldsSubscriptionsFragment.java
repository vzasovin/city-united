package com.city_utd.city_utd.controller.fragment;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.PagingQueryParamsHolder;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.adapter.FakeItemAdapter;
import com.city_utd.city_utd.loader.FieldsSubscriptionsLoader;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.view.decoration.DividerItemDecoration;
import com.imogene.android.carcase.controller.paging.OnThresholdReachedListener;
import com.imogene.android.carcase.controller.paging.RecyclerViewLoadMoreListener;
import com.imogene.android.carcase.worker.loader.BaseLoader;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by vadim on 7/17/17.
 */

public class FieldsSubscriptionsFragment extends SimpleCachingFragment<List<PlayingField>>
        implements MyTeamPart, OnThresholdReachedListener {

    @Bind(R.id.container)
    RecyclerView recyclerView;

    private PagingQueryParamsHolder paramsHolder;
    private FieldsAdapter adapter;
    private RecyclerViewLoadMoreListener loadMoreListener;

    public static FieldsSubscriptionsFragment newInstance() {
        return new FieldsSubscriptionsFragment();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.layout_recycler_view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        Context context = getActivity();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FieldsAdapter(null);
        recyclerView.setAdapter(adapter);

        Resources resources = getResources();
        @SuppressWarnings("deprecation")
        int color = resources.getColor(R.color.colorWhiteTranslucent2);
        DividerItemDecoration decoration = new DividerItemDecoration(color, .4f, false);
        recyclerView.addItemDecoration(decoration);

        loadMoreListener = new RecyclerViewLoadMoreListener(.7f, this);
        recyclerView.addOnScrollListener(loadMoreListener);
    }

    class FieldsAdapter extends FakeItemAdapter<PlayingField, FieldsAdapter.FieldViewHolder> {

        private ImageUrlBuilder imageUrlBuilder;

        FieldsAdapter(List<PlayingField> data) {
            super(data);
        }

        @Override
        public FieldViewHolder onCreateNormalViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_item_field, parent, false);
            return new FieldViewHolder(itemView);
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        protected void onBindViewHolder(RecyclerView.ViewHolder holder, PlayingField item) {
            FieldViewHolder viewHolder = (FieldViewHolder) holder;

            String imageUrl = item.getImageUrl();
            ensureImageUrlBuilder();
            imageUrl = imageUrlBuilder.build(imageUrl);
            @DrawableRes int placeholderRes = R.drawable.ic_field_placeholder;
            Glide.with(FieldsSubscriptionsFragment.this).load(imageUrl)
                    .placeholder(placeholderRes).error(placeholderRes).into(viewHolder.imageView);

            String name = item.getName();
            if(!TextUtils.isEmpty(name)){
                viewHolder.nameTextView.setVisibility(View.VISIBLE);
                viewHolder.nameTextView.setText(name);
            }else {
                viewHolder.nameTextView.setVisibility(View.GONE);
            }

            String address = item.getAddress();
            if(!TextUtils.isEmpty(address)){
                viewHolder.addressTextView.setVisibility(View.VISIBLE);
                viewHolder.addressTextView.setText(address);
            }else {
                viewHolder.addressTextView.setVisibility(View.GONE);
            }
        }

        private void ensureImageUrlBuilder(){
            if(imageUrlBuilder == null){
                imageUrlBuilder = ApiManager.newImageUrlBuilder();
            }
        }

        class FieldViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

            @Nullable
            @Bind(R.id.image_view)
            ImageView imageView;

            @Nullable
            @Bind(R.id.name_text_view)
            TextView nameTextView;

            @Nullable
            @Bind(R.id.address_text_view)
            TextView addressTextView;

            FieldViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View v) {
                int adapterPosition = getAdapterPosition();
                PlayingField field = getItem(adapterPosition);
                int fieldId = field.getId();
                openFieldDetailedActivity(fieldId);
            }
        }
    }

    private void openFieldDetailedActivity(int fieldId){
        Context context = getActivity();
        Intent intent = new Intent(context, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, fieldId);
        startActivity(intent);
    }

    @Override
    public void onThresholdReached(int position) {
        ensureParamsHolder();
        int page = paramsHolder.getPage();
        paramsHolder.setPage(page + 1);
        onLoadFromServer();
    }

    @Override
    protected void onLoadFromCache(boolean freshLoad) {
        Bundle args = updateLoaderArgs(BaseLoader.SOURCE_DATABASE);
        if(freshLoad){
            reloadData(0, args);
        }else {
            loadData(0, args);
        }
    }

    @Override
    protected void onLoadFromServer() {
        int source;
        if(paramsHolder != null){
            int page = paramsHolder.getPage();
            source = page > 1 ? BaseLoader.SOURCE_SERVER : BaseLoader.SOURCE_SERVER_DATABASE;
        }else {
            source = BaseLoader.SOURCE_SERVER_DATABASE;
        }

        Bundle args = updateLoaderArgs(source);
        reloadData(0, args);
    }

    @Override
    protected BaseLoader<List<PlayingField>> onCreateLoader(Context context, int id, Bundle args) {
        final int source = args.getInt(BaseLoader.EXTRA_SOURCE);
        final boolean fromCache = source == BaseLoader.SOURCE_DATABASE;
        if(fromCache){
            return new FieldsSubscriptionsLoader(context);
        }else {
            ensureParamsHolder();
            return new FieldsSubscriptionsLoader(context, source, paramsHolder);
        }
    }

    private void ensureParamsHolder(){
        if(paramsHolder == null){
            paramsHolder = new PagingQueryParamsHolder();
            paramsHolder.setPage(1);
        }
    }

    @Override
    protected void onLoaderReset(BaseLoader<List<PlayingField>> loader) {
        adapter.setData(null);
    }

    @Override
    protected void onLoadFinished(BaseLoader<List<PlayingField>> loader, List<PlayingField> data) {
        super.onLoadFinished(loader, data);

        final boolean hasData = data != null;
        final String errorMessage = loader.getErrorMessage();

        if(hasData){
            FieldsSubscriptionsLoader subscriptionsLoader = (FieldsSubscriptionsLoader) loader;
            final int page = subscriptionsLoader.getPage();

            if(page > 1){
                adapter.addData(data);
            }else {
                adapter.setData(data);
            }

            boolean emptyData = data.isEmpty();
            if(!emptyData){
                loadMoreListener.notifyContinueListen();
            }
        }else if(!TextUtils.isEmpty(errorMessage)){
            showToast(errorMessage, false);
        }
    }

    @Override
    public void reload() {
        ensureParamsHolder();
        paramsHolder.setPage(1);
        onLoadFromServer();
    }

    @Override
    protected ConnectivityStateHost getConnectivityStateHost() {
        return (ConnectivityStateHost) getParentFragment();
    }

    @Override
    public void addFakeItem() {
        adapter.addFakeItem();
    }

    @Override
    public void removeFakeItem() {
        adapter.removeFakeItem();
    }
}
