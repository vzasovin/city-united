package com.city_utd.city_utd.controller.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.city_utd.city_utd.R;
import com.city_utd.city_utd.controller.activity.AbstractActivity;
import com.city_utd.city_utd.controller.activity.ImageActivity;
import com.github.chrisbanes.photoview.PhotoView;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Admin on 31.07.2017.
 */

public class ImageFragment extends AbstractFragment{

    @Bind(R.id.image_view)
    PhotoView photoView;

    private String imageUrl;

    public static ImageFragment newInstance(String imageUrl) {
        Bundle args = new Bundle();
        args.putString(ImageActivity.EXTRA_IMAGE_URL, imageUrl);
        ImageFragment fragment = new ImageFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        imageUrl = args.getString(ImageActivity.EXTRA_IMAGE_URL);
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_image;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        AbstractActivity activity = (AbstractActivity) getActivity();
        activity.supportPostponeEnterTransition();

        Glide.with(this).load(imageUrl)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(photoView);
    }
}
