package com.city_utd.city_utd.controller.fragment;

/**
 * Created by Admin on 18.07.2017.
 */

public interface ConnectivityStateHost extends ConnectivityStateFragment.OnReconnectListener{

    boolean shouldDisplayConnectivityState();

    int getConnectivityStateFragmentAppearance();

    boolean showConnectivityState(int appearance);

    boolean hideConnectivityState();

    boolean notifyReconnecting();

    boolean notifyReconnectionFailed();
}
