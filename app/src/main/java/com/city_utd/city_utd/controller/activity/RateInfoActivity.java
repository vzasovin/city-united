package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.client.RateInfo;
import com.city_utd.city_utd.controller.fragment.RateInfoFragment;

import java.util.Date;

/**
 * Created by Admin on 27.07.2017.
 */

public class RateInfoActivity extends AbstractActivity {

    public static final String EXTRA_RATE_INFO = "com.city_utd.city_utd.RateInfoActivity.EXTRA_RATE_INFO";
    public static final String EXTRA_SOURCE_ID = "com.city_utd.city_utd.RateInfoActivity.EXTRA_SOURCE_ID";
    public static final String EXTRA_SOURCE_IMAGE_URL = "com.city_utd.city_utd.RateInfoActivity.EXTRA_SOURCE_IMAGE_URL";
    public static final String EXTRA_SOURCE_NAME = "com.city_utd.city_utd.RateInfoActivity.EXTRA_SOURCE_NAME";
    public static final String EXTRA_SOURCE_LINE = "com.city_utd.city_utd.RateInfoActivity.EXTRA_SOURCE_LINE";
    public static final String EXTRA_DATE = "com.city_utd.city_utd.RateInfoActivity.EXTRA_DATE";
    public static final String EXTRA_USER_LINE = "com.city_utd.city_utd.RateInfoActivity.EXTRA_USER_LINE";

    @Override
    protected Fragment createFragment(Intent intent) {
        Rate rate = intent.getParcelableExtra(EXTRA_RATE_INFO);
        if(rate instanceof RateInfo){
            RateInfo rateInfo = (RateInfo) rate;
            String userLine = intent.getStringExtra(EXTRA_USER_LINE);
            return RateInfoFragment.newInstance(rateInfo, userLine);
        }

        int sourceId = intent.getIntExtra(EXTRA_SOURCE_ID, 0);
        String sourceName = intent.getStringExtra(EXTRA_SOURCE_NAME);
        String imageUrl = intent.getStringExtra(EXTRA_SOURCE_IMAGE_URL);
        String sourceLine = intent.getStringExtra(EXTRA_SOURCE_LINE);
        Date date = (Date) intent.getSerializableExtra(EXTRA_DATE);
        return RateInfoFragment.newInstance(rate, sourceId, sourceName, imageUrl, sourceLine, date);
    }
}
