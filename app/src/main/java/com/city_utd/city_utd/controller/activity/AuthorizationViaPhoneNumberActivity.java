package com.city_utd.city_utd.controller.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.city_utd.city_utd.controller.fragment.AuthorizationViaPhoneNumberFragment;

/**
 * Created by Admin on 07.08.2017.
 */

public class AuthorizationViaPhoneNumberActivity extends AbstractActivity {

    public static final String EXTRA_PHONE_NUMBER = "com.city_utd.city_utd.AuthorizationViaPhoneNumberActivity.EXTRA_PHONE_NUMBER";

    @Override
    protected Fragment createFragment(Intent intent) {
        return AuthorizationViaPhoneNumberFragment.newInstance();
    }
}
