package com.city_utd.city_utd.service;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.Nullable;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.IncomingInvitationsActivity;
import com.city_utd.city_utd.controller.activity.MainNavigationActivity;
import com.city_utd.city_utd.controller.activity.UserProfileActivity;
import com.city_utd.city_utd.util.Util;
import com.imogene.android.carcase.AppUtils;
import com.imogene.android.carcase.exception.RequestException;

import java.util.Date;

/**
 * Created by Admin on 26.07.2017.
 */

public class RespondToInvitationService extends IntentService {

    public static final int NOTIFICATION_ID_INVITATION_RESULT = 3;

    public static final String EXTRA_ACCEPT = "com.city_utd.city_utd.RespondToInvitationService.EXTRA_ACCEPT";
    public static final String EXTRA_SOURCE_ID = "com.city_utd.city_utd.RespondToInvitationService.EXTRA_SOURCE_ID";
    public static final String EXTRA_FIELD_ID = "com.city_utd.city_utd.RespondToInvitationService.EXTRA_FIELD_ID";
    public static final String EXTRA_DATE = "com.city_utd.city_utd.RespondToInvitationService.EXTRA_DATE";

    private static final String SERVICE_NAME = RespondToInvitationService.class.getSimpleName();

    private boolean accept;
    private int sourceId;
    private int playingFieldId;
    private Date date;

    public RespondToInvitationService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null){
            handleIntent(intent);
        }
    }

    private void handleIntent(Intent intent){
        accept = intent.getBooleanExtra(EXTRA_ACCEPT, true);
        sourceId = intent.getIntExtra(EXTRA_SOURCE_ID, 0);
        playingFieldId = intent.getIntExtra(EXTRA_FIELD_ID, 0);
        date = (Date) intent.getSerializableExtra(EXTRA_DATE);

        cancelInvitationNotification();

        ApiManager apiManager = ApiManager.getInstance();
        boolean success;
        try {
            if(accept){
                success = apiManager.acceptInvitation(playingFieldId, date, sourceId);
            }else {
                success = apiManager.rejectInvitation(sourceId, playingFieldId, date);
            }
        }catch (RequestException e){
            e.printStackTrace();
            success = false;
        }

        if(success){
            showSuccessNotification();
        }else if(accept) {
            showFailureNotification();
        }
    }

    private void cancelInvitationNotification(){
        String dateString = Util.Dates.dateToServerString(date);
        String tag = sourceId + dateString + playingFieldId;

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        manager.cancel(tag, FCMMessagingService.NOTIFICATION_ID_INVITATION);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showSuccessNotification(){
        String title = getString(R.string.invitation_on_game);
        String category = Notification.CATEGORY_SERVICE;

        String content;
        PendingIntent contentIntent;

        if(accept){
            content = getString(R.string.you_have_accepted_invitation_on_game);
            contentIntent = createFieldDetailedActivityPendingIntent();
        }else {
            content = getString(R.string.you_have_rejected_invitation_on_game);
            contentIntent = createUserProfileActivityPendingIntent();
        }

        showNotification(title, content, contentIntent, category);
    }

    private PendingIntent createFieldDetailedActivityPendingIntent(){
        Intent intent = new Intent(this, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, playingFieldId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(FieldDetailedActivity.class);
        stackBuilder.addNextIntent(intent);
        Intent firstIntent = stackBuilder.editIntentAt(0);
        int destination = MainNavigationActivity.DESTINATION_MAPS;
        firstIntent.putExtra(MainNavigationActivity.EXTRA_DESTINATION, destination);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent createUserProfileActivityPendingIntent(){
        Intent firstIntent = new Intent(this, MainNavigationActivity.class);
        int destination = MainNavigationActivity.DESTINATION_MY_TEAM;
        firstIntent.putExtra(MainNavigationActivity.EXTRA_DESTINATION, destination);
        Intent intent = new Intent(this, UserProfileActivity.class);
        intent.putExtra(UserProfileActivity.EXTRA_USER_ID, sourceId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addNextIntent(firstIntent).addNextIntent(intent);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showFailureNotification(){
        String title = getString(R.string.invitation_on_game);
        String content = getString(R.string.invitation_on_game_error);
        String category = Notification.CATEGORY_ERROR;
        PendingIntent contentIntent = createIncomingInvitationsActivityPendingIntent();
        showNotification(title, content, contentIntent, category);
    }

    private PendingIntent createIncomingInvitationsActivityPendingIntent(){
        Intent intent = new Intent(this, IncomingInvitationsActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(IncomingInvitationsActivity.class);
        stackBuilder.addNextIntent(intent);
        Intent firstIntent = stackBuilder.editIntentAt(0);
        int destination = MainNavigationActivity.DESTINATION_PROFILE;
        firstIntent.putExtra(MainNavigationActivity.EXTRA_DESTINATION, destination);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressLint("NewApi")
    private void showNotification(String title, String content, PendingIntent contentIntent, String category){
        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_add_accent_36dp);
        builder.setContentTitle(title);
        builder.setContentText(content);
        if(contentIntent != null){
            builder.setContentIntent(contentIntent);
        }
        builder.setAutoCancel(true);
        builder.setPriority(Notification.PRIORITY_DEFAULT);
        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            builder.setCategory(category);
        }

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String tag = sourceId + date.toString() + playingFieldId;
        manager.notify(tag, NOTIFICATION_ID_INVITATION_RESULT, builder.build());
    }
}
