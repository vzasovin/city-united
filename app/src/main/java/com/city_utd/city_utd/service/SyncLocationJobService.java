package com.city_utd.city_utd.service;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.util.LocalDataManager;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.JobTrigger;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.SimpleJobService;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.maps.model.LatLng;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 12.07.2017.
 */

public class SyncLocationJobService extends SimpleJobService {

    private static final String JOB_TAG = "com.city_utd.city_utd.SyncLocationJobService.JOB_TAG";

    @Override
    public int onRunJob(JobParameters job) {
        String tag = job.getTag();
        if(!JOB_TAG.equals(tag)){
            return RESULT_FAIL_NORETRY;
        }

        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(!manager.isAuthorized()){
            return RESULT_FAIL_NORETRY;
        }

        LatLng location = manager.getLastLocation();
        if(location != null){
            try {
                sendLocation(location);
            }catch (RequestException e){
                e.printStackTrace();
                return RESULT_FAIL_RETRY;
            }
            return RESULT_SUCCESS;
        }
        return RESULT_FAIL_NORETRY;
    }

    private void sendLocation(LatLng location) throws RequestException{
        ApiManager apiManager = ApiManager.getInstance();
        apiManager.updatePosition(location);
    }

    public static void sync(int intervalSeconds){
        FirebaseJobDispatcher dispatcher = createDispatcher();
        Job job = createJob(dispatcher, Trigger.executionWindow(0, intervalSeconds));
        dispatcher.schedule(job);
    }

    private static FirebaseJobDispatcher createDispatcher(){
        Context context = CityUtdApplication.getAppContext();
        Driver driver = new GooglePlayDriver(context);
        return new FirebaseJobDispatcher(driver);
    }

    private static Job createJob(FirebaseJobDispatcher dispatcher, JobTrigger trigger){
        return dispatcher.newJobBuilder()
                .setService(SyncLocationJobService.class)
                .setTag(JOB_TAG)
                .setRecurring(false)
                .setLifetime(Lifetime.UNTIL_NEXT_BOOT)
                .setTrigger(trigger)
                .setReplaceCurrent(false)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
    }

    public static void syncRightNow(){
        FirebaseJobDispatcher dispatcher = createDispatcher();
        Job job = createJob(dispatcher, Trigger.NOW);
        dispatcher.schedule(job);
    }

    public static void cancel(){
        createDispatcher().cancel(JOB_TAG);
    }
}
