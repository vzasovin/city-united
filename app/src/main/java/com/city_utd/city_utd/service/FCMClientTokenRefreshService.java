package com.city_utd.city_utd.service;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.util.LocalDataManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 22.07.2017.
 */

public class FCMClientTokenRefreshService extends FirebaseInstanceIdService {

    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
        FirebaseInstanceId instanceId = FirebaseInstanceId.getInstance();
        String newToken = instanceId.getToken();
        boolean updateLocally = true;
        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(manager.isAuthorized()){
            String oldToken = manager.getClientToken();
            ApiManager apiManager = ApiManager.getInstance();
            try {
                updateLocally = apiManager.updateClientToken(oldToken, newToken);
            }catch (RequestException e){
                e.printStackTrace();
                updateLocally = false;
            }
        }

        if(updateLocally){
            manager.setClientToken(newToken);
        }
    }
}
