package com.city_utd.city_utd.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDetailedDao;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Admin on 17.07.2017.
 */

public class SubscribeOnFieldService extends IntentService {

    public static final String EXTRA_FIELD_ID = "com.city_utd.city_utd.SubscribeOnFieldService.EXTRA_FIELD_ID";
    public static final String EXTRA_SUBSCRIBE = "com.city_utd.city_utd.SubscribeOnFieldService.EXTRA_SUBSCRIBE";

    private static final String SERVICE_NAME = "SubscribeOnFieldService";

    public SubscribeOnFieldService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        if(intent != null){
            int fieldId = intent.getIntExtra(EXTRA_FIELD_ID, 0);
            boolean subscribe = intent.getBooleanExtra(EXTRA_SUBSCRIBE, true);

            ApiManager apiManager = ApiManager.getInstance();
            PlayingFieldDetailed result;
            try {
                if(subscribe){
                    result = apiManager.subscribeOnField(fieldId);
                }else {
                    result = apiManager.unsubscribeFromField(fieldId);
                }
            }catch (IOException e){
                e.printStackTrace();
                return;
            }

            if(result != null){
                try {
                    DatabaseHelper helper = DatabaseHelper.getInstance(this);
                    PlayingFieldDetailedDao dao = helper.getPlayingFieldDetailedDao();
                    dao.createOrUpdate(result);
                }catch (SQLException e){
                    throw new RuntimeSQLException(e);
                }
            }
        }
    }
}
