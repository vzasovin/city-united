package com.city_utd.city_utd.service;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.annotation.PluralsRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.controller.activity.FieldDetailedActivity;
import com.city_utd.city_utd.controller.activity.MainNavigationActivity;
import com.city_utd.city_utd.controller.activity.RateInfoActivity;
import com.city_utd.city_utd.model.PlayerLines;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.city_utd.city_utd.util.LocalDataManager;
import com.city_utd.city_utd.util.Util;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;
import com.imogene.android.carcase.AppUtils;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TODAY;
import static com.city_utd.city_utd.util.Util.Dates.DAY_LABEL_TOMORROW;
import static com.city_utd.city_utd.util.Util.Dates.getDayLabel;
import static com.city_utd.city_utd.util.Util.Dates.serverStringToDate;

/**
 * Created by Admin on 23.07.2017.
 */

public class FCMMessagingService extends FirebaseMessagingService {

    public static final int NOTIFICATION_ID_SCHEDULE = 1;
    public static final int NOTIFICATION_ID_INVITATION = 2;
    public static final int NOTIFICATION_ID_NEW_RATE = 4;
    public static final int NOTIFICATION_ID_RATE = 5;

    private static final String MESSAGE_TYPE_SCHEDULE = "schedule";
    private static final String MESSAGE_TYPE_INVITATION = "invitation";
    private static final String MESSAGE_TYPE_NEW_RATE = "new_rate";
    private static final String MESSAGE_TYPE_RATE = "rate";

    private static final String KEY_MESSAGE_TYPE = "type";
    private static final String KEY_PLAYING_FIELD_NAME = "playingFieldName";
    private static final String KEY_PLAYING_FIELD_ID = "playingFieldId";
    private static final String KEY_GROUPS = "groups";
    private static final String KEY_SOURCE_ID = "sourceId";
    private static final String KEY_SOURCE_NAME = "sourceName";
    private static final String KEY_SOURCE_IMAGE_URL = "sourceImageUrl";
    private static final String KEY_DATE = "date";
    private static final String KEY_LINE = "line";

    private static final String KEY_SPEED = "speed";
    private static final String KEY_STAMINA = "stamina";
    private static final String KEY_TECHNIQUE = "technique";
    private static final String KEY_IMPACT_FORCE = "impactForce";
    private static final String KEY_PASS = "pass";
    private static final String KEY_DEFENSE = "defense";
    private static final String KEY_FRIENDLINESS = "friendliness";
    private static final String KEY_REACTION = "reaction";
    private static final String KEY_RELIABILITY = "reliability";
    private static final String KEY_HANDS = "hands";
    private static final String KEY_FEET = "feet";
    private static final String KEY_JUMP = "jump";
    private static final String KEY_COURAGE = "courage";

    private static final int RC_ACCEPT_INVITATION = 1;
    private static final int RC_REJECT_INVITATION = 2;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        if(isUserAuthorized()){
            JSONObject data = new JSONObject(remoteMessage.getData());
            String messageType = data.optString(KEY_MESSAGE_TYPE);
            if(!TextUtils.isEmpty(messageType)){
                handleMessage(messageType, data);
            }
        }
    }

    private boolean isUserAuthorized(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        return manager.isAuthorized();
    }

    private void handleMessage(String type, JSONObject data){
        switch (type){
            case MESSAGE_TYPE_SCHEDULE:
                handleScheduleMessage(data);
                break;
            case MESSAGE_TYPE_INVITATION:
                handleInvitationMessage(data);
                break;
            case MESSAGE_TYPE_NEW_RATE:
                handleNewRateMessage(data);
                break;
        }
    }

    @SuppressLint("NewApi")
    private void handleScheduleMessage(JSONObject data){
        Resources resources = getResources();

        String name = data.optString(KEY_PLAYING_FIELD_NAME);
        boolean hasName = !TextUtils.isEmpty(name);
        int id = data.optInt(KEY_PLAYING_FIELD_ID);
        String groupsJson = data.optString(KEY_GROUPS);
        List<Group> groups = parseGroups(groupsJson);

        String title;
        String content;
        String bigText;
        PendingIntent contentIntent;

        if(groups.size() == 1){
            title = getString(R.string.game_is_planned);

            StringBuilder sb = new StringBuilder();
            if(hasName){
                sb.append(name).append('.');
            }

            Group group = groups.get(0);
            Date date = group.date;
            int groupSize = group.size;
            if(date != null){
                boolean today = getDayLabel(date) == DAY_LABEL_TODAY;
                @StringRes int timeRes = today ? R.string.today_in : R.string.tomorrow_in;
                String timeString = Util.Dates.dateToTimeString(date);
                sb.append(' ').append(getString(timeRes, timeString));

                if(today){
                    if(hasName){
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_today_with_name;
                        bigText = resources.getQuantityString(res, groupSize, timeString, name, groupSize);
                    }else {
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_today;
                        bigText = resources.getQuantityString(res, groupSize, timeString, groupSize);
                    }
                }else {
                    if(hasName){
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_tomorrow_with_name;
                        bigText = resources.getQuantityString(res, groupSize, timeString, name, groupSize);
                    }else {
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_tomorrow;
                        bigText = resources.getQuantityString(res, groupSize, timeString, groupSize);
                    }
                }
            }else {
                Date minDate = group.minDate;
                Date maxDate = group.maxDate;

                boolean today = getDayLabel(minDate) == DAY_LABEL_TODAY;
                @StringRes int timeRes = today ? R.string.today_from_to : R.string.tomorrow_from_to;
                String minTimeString = Util.Dates.dateToTimeString(minDate);
                String maxTimeString = Util.Dates.dateToTimeString(maxDate);
                sb.append(' ').append(getString(timeRes, minTimeString, maxTimeString));

                if(today){
                    if(hasName){
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_today_range_with_name;
                        bigText = resources.getQuantityString(res, groupSize, minTimeString, maxTimeString, name, groupSize);
                    }else {
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_today_range;
                        bigText = resources.getQuantityString(res, groupSize, minTimeString, maxTimeString, groupSize);
                    }
                }else {
                    if(hasName){
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_tomorrow_range_with_name;
                        bigText = resources.getQuantityString(res, groupSize, minTimeString, maxTimeString, name, groupSize);
                    }else {
                        @PluralsRes int res = R.plurals.schedule_notification_big_text_tomorrow_range;
                        bigText = resources.getQuantityString(res, groupSize, minTimeString, maxTimeString, groupSize);
                    }
                }
            }

            content = sb.toString();
        }else {
            title = getString(R.string.saturated_football_day);

            Group group = groups.get(0);
            Date date = group.date;
            if(date == null){
                date = group.minDate;
            }

            boolean today = getDayLabel(date) == DAY_LABEL_TODAY;
            if(today){
                if(hasName){
                    content = getString(R.string.schedule_notification_big_text_multiple_groups_today_with_name, name);
                }else {
                    content = getString(R.string.schedule_notification_big_text_multiple_groups_today);
                }
            }else {
                if(hasName){
                    content = getString(R.string.schedule_notification_big_text_multiple_groups_tomorrow_with_name, name);
                }else {
                    content = getString(R.string.schedule_notification_big_text_multiple_groups_tomorrow);
                }
            }

            bigText = content;
        }

        contentIntent = createFieldContentIntent(id);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_ball_white_24dp);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setContentIntent(contentIntent);
        builder.setStyle(new Notification.BigTextStyle().bigText(bigText));
        builder.setPriority(Notification.PRIORITY_LOW);

        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            builder.setCategory(Notification.CATEGORY_EVENT);

            @SuppressWarnings("deprecation")
            int colorPrimary = resources.getColor(R.color.colorPrimary);
            builder.setColor(colorPrimary);
        }

        builder.setAutoCancel(true);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String tag = String.valueOf(id);
        manager.notify(tag, NOTIFICATION_ID_SCHEDULE, builder.build());
    }

    private List<Group> parseGroups(String json){
        Gson gson = ApiManager.getGsonInstance();
        Type type = new TypeToken<List<Group>>(){}.getType();
        return gson.fromJson(json, type);
    }

    private static final class Group{

        @SerializedName("date")
        Date date;

        @SerializedName("minDate")
        Date minDate;

        @SerializedName("maxDate")
        Date maxDate;

        @SerializedName("groupSize")
        int size;
    }

    private PendingIntent createFieldContentIntent(int playingFieldId){
        Intent intent = new Intent(this, FieldDetailedActivity.class);
        intent.putExtra(FieldDetailedActivity.EXTRA_FIELD_ID, playingFieldId);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(FieldDetailedActivity.class);
        stackBuilder.addNextIntent(intent);
        Intent firstIntent = stackBuilder.editIntentAt(0);
        firstIntent.putExtra(MainNavigationActivity.EXTRA_DESTINATION, MainNavigationActivity.DESTINATION_MAPS);
        return stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressLint("NewApi")
    private void handleInvitationMessage(JSONObject data){
        int sourceId = data.optInt(KEY_SOURCE_ID);
        String sourceName = data.optString(KEY_SOURCE_NAME);
        boolean hasSourceName = !TextUtils.isEmpty(sourceName);
        int playingFieldId = data.optInt(KEY_PLAYING_FIELD_ID);
        String playingFieldName = data.optString(KEY_PLAYING_FIELD_NAME);
        boolean hasPlayingFieldName = !TextUtils.isEmpty(playingFieldName);
        String dateString = data.optString(KEY_DATE);
        Date date = serverStringToDate(dateString);
        int dayLabel = getDayLabel(date);

        String title;
        String content;
        PendingIntent contentIntent;

        @StringRes int res;
        String timeString;

        if(hasSourceName){
            title = sourceName;

            if(hasPlayingFieldName){
                switch (dayLabel){
                    case DAY_LABEL_TODAY:
                        res = R.string.invitation_notification_content_title_with_name_today;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, playingFieldName, timeString);
                        break;
                    case DAY_LABEL_TOMORROW:
                        res = R.string.invitation_notification_content_title_with_name_tomorrow;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, playingFieldName, timeString);
                        break;
                    default:
                        res = R.string.invitation_notification_content_title_with_name_later;
                        timeString = Util.Dates.dateToString(date, Util.Dates.DATE_TIME_FORMAT_2);
                        content = getString(res, playingFieldName, timeString);
                        break;
                }
            }else {
                switch (dayLabel){
                    case DAY_LABEL_TODAY:
                        res = R.string.invitation_notification_content_title_today;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, timeString);
                        break;
                    case DAY_LABEL_TOMORROW:
                        res = R.string.invitation_notification_content_title_tomorrow;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, timeString);
                        break;
                    default:
                        res = R.string.invitation_notification_content_title_later;
                        timeString = Util.Dates.dateToString(date, Util.Dates.DATE_TIME_FORMAT_2);
                        content = getString(res, timeString);
                        break;
                }
            }
        }else {
            title = getString(R.string.invitation_on_game);

            if(hasPlayingFieldName){
                switch (dayLabel){
                    case DAY_LABEL_TODAY:
                        res = R.string.invitation_notification_content_with_name_today;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, playingFieldName, timeString);
                        break;
                    case DAY_LABEL_TOMORROW:
                        res = R.string.invitation_notification_content_with_name_tomorrow;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, playingFieldName, timeString);
                        break;
                    default:
                        res = R.string.invitation_notification_content_with_name_later;
                        timeString = Util.Dates.dateToString(date, Util.Dates.DATE_TIME_FORMAT_2);
                        content = getString(res, playingFieldName, timeString);
                        break;
                }
            }else {
                switch (dayLabel){
                    case DAY_LABEL_TODAY:
                        res = R.string.invitation_notification_content_today;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, timeString);
                        break;
                    case DAY_LABEL_TOMORROW:
                        res = R.string.invitation_notification_content_tomorrow;
                        timeString = Util.Dates.dateToTimeString(date);
                        content = getString(res, timeString);
                        break;
                    default:
                        res = R.string.invitation_notification_content_later;
                        timeString = Util.Dates.dateToString(date, Util.Dates.DATE_TIME_FORMAT_2);
                        content = getString(res, timeString);
                        break;
                }
            }
        }

        contentIntent = createFieldContentIntent(playingFieldId);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_ball_white_24dp);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setContentIntent(contentIntent);
        builder.setStyle(new Notification.BigTextStyle().bigText(content));
        builder.setPriority(Notification.PRIORITY_LOW);

        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            builder.setCategory(Notification.CATEGORY_EVENT);

            Resources resources = getResources();
            @SuppressWarnings("deprecation")
            int colorPrimary = resources.getColor(R.color.colorPrimary);
            builder.setColor(colorPrimary);
        }

        builder.setAutoCancel(true);

        builder.addAction(createAcceptAction(sourceId, playingFieldId, date));
        builder.addAction(createRejectAction(sourceId, playingFieldId, date));

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String tag = sourceId + dateString + playingFieldId;
        manager.notify(tag, NOTIFICATION_ID_INVITATION, builder.build());

        addUnreadIncomingInvitation();
    }

    private Notification.Action createAcceptAction(int sourceId, int playingFieldId, Date date){
        @DrawableRes int iconRes;
        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            iconRes = R.drawable.ic_done_gray_24dp;
        }else {
            iconRes = R.drawable.ic_done_white_24dp;
        }
        @StringRes int textRes = R.string.accept;
        PendingIntent pendingIntent = createRespondToInvitationServicePendingIntent(true, sourceId, playingFieldId, date);
        return createAction(iconRes, textRes, pendingIntent);
    }

    private Notification.Action createRejectAction(int sourceId, int playingFieldId, Date date){
        @DrawableRes int iconRes;
        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            iconRes = R.drawable.ic_clear_gray_24dp;
        }else {
            iconRes = R.drawable.ic_clear_white_24dp;
        }
        @StringRes int textRes = R.string.reject;
        PendingIntent pendingIntent = createRespondToInvitationServicePendingIntent(false, sourceId, playingFieldId, date);
        return createAction(iconRes, textRes, pendingIntent);
    }

    private PendingIntent createRespondToInvitationServicePendingIntent(boolean accept, int sourceId, int playingFieldId, Date date){
        Intent intent = new Intent(this, RespondToInvitationService.class);
        intent.putExtra(RespondToInvitationService.EXTRA_ACCEPT, accept);
        intent.putExtra(RespondToInvitationService.EXTRA_SOURCE_ID, sourceId);
        intent.putExtra(RespondToInvitationService.EXTRA_FIELD_ID, playingFieldId);
        intent.putExtra(RespondToInvitationService.EXTRA_DATE, date);
        int requestCode = accept ? RC_ACCEPT_INVITATION : RC_REJECT_INVITATION;
        return PendingIntent.getService(this, requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @SuppressWarnings("deprecation")
    private Notification.Action createAction(@DrawableRes int iconRes, @StringRes int textRes, PendingIntent intent){
        String text = getString(textRes);
        return new Notification.Action(iconRes, text, intent);
    }

    private void addUnreadIncomingInvitation(){
        LocalDataManager manager = LocalDataManager.getInstance(this);
        manager.addUnreadIncomingInvitation();
    }

    @SuppressLint("NewApi")
    private void handleNewRateMessage(JSONObject data){
        LocalDataManager dataManager = LocalDataManager.getInstance(this);
        if(!dataManager.isAuthorized()){
            return;
        }

        int sourceId = data.optInt(KEY_SOURCE_ID);
        String sourceName = data.optString(KEY_SOURCE_NAME);
        boolean hasSourceName = !TextUtils.isEmpty(sourceName);
        String line = data.optString(KEY_LINE);
        String imageUrl = data.optString(KEY_SOURCE_IMAGE_URL);
        String dateString = data.optString(KEY_DATE);
        Date date = serverStringToDate(dateString);

        int speed = data.optInt(KEY_SPEED);
        int stamina = data.optInt(KEY_STAMINA);
        int technique = data.optInt(KEY_TECHNIQUE);
        int impactForce = data.optInt(KEY_IMPACT_FORCE);
        int pass = data.optInt(KEY_PASS);
        int defense = data.optInt(KEY_DEFENSE);
        int friendliness = data.optInt(KEY_FRIENDLINESS);
        int reaction = data.optInt(KEY_REACTION);
        int reliability = data.optInt(KEY_RELIABILITY);
        int hands = data.optInt(KEY_HANDS);
        int feet = data.optInt(KEY_FEET);
        int jump = data.optInt(KEY_JUMP);
        int courage = data.optInt(KEY_COURAGE);

        int userId = dataManager.getUserId();
        DatabaseHelper helper = DatabaseHelper.getInstance(this);
        UserDetailed user;
        try {
            UserDetailedDao dao = helper.getUserDetailedDao();
            user = dao.queryForId(userId);
        }catch (SQLException e){
            e.printStackTrace();
            return;
        }

        if(user == null){
            return;
        }

        String myLine = user.getLine();
        float rating;

        if(PlayerLines.GOALKEEPER.equals(myLine)){
            rating = (reaction + reliability + hands + feet + jump + courage + friendliness) / 7.f;
        }else {
            rating = (speed + stamina + technique + impactForce + pass + defense + friendliness) / 7.f;
        }

        String title;
        String content;
        PendingIntent contentIntent;

        title = getString(R.string.new_rate);

        if(hasSourceName){
            content = getString(R.string.player_has_rate_you_with_name, sourceName, rating);
        }else {
            content = getString(R.string.player_has_rate_you, rating);
        }

        Rate rate = new Rate();
        rate.setSpeed(speed);
        rate.setStamina(stamina);
        rate.setTechnique(technique);
        rate.setImpactForce(impactForce);
        rate.setPass(pass);
        rate.setDefense(defense);
        rate.setFriendliness(friendliness);
        rate.setReaction(reaction);
        rate.setReliability(reliability);
        rate.setHands(hands);
        rate.setFeet(feet);
        rate.setJump(jump);
        rate.setCourage(courage);

        Intent intent = new Intent(this, RateInfoActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(RateInfoActivity.EXTRA_RATE_INFO, rate);
        intent.putExtra(RateInfoActivity.EXTRA_SOURCE_ID, sourceId);
        intent.putExtra(RateInfoActivity.EXTRA_SOURCE_NAME, sourceName);
        intent.putExtra(RateInfoActivity.EXTRA_SOURCE_IMAGE_URL, imageUrl);
        intent.putExtra(RateInfoActivity.EXTRA_SOURCE_LINE, line);
        intent.putExtra(RateInfoActivity.EXTRA_DATE, date);

        contentIntent = PendingIntent.getActivity(this, sourceId, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder builder = new Notification.Builder(this);
        builder.setSmallIcon(R.drawable.ic_ball_white_24dp);
        builder.setContentTitle(title);
        builder.setContentText(content);
        builder.setContentIntent(contentIntent);
        builder.setPriority(Notification.PRIORITY_LOW);

        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.LOLLIPOP)){
            builder.setCategory(Notification.CATEGORY_SOCIAL);

            Resources resources = getResources();
            @SuppressWarnings("deprecation")
            int colorPrimary = resources.getColor(R.color.colorPrimary);
            builder.setColor(colorPrimary);
        }

        builder.setAutoCancel(true);

        NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String tag = String.valueOf(sourceId);
        manager.notify(tag, NOTIFICATION_ID_NEW_RATE, builder.build());
    }
}
