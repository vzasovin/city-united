package com.city_utd.city_utd.service;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.util.LocalDataManager;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.SimpleJobService;
import com.firebase.jobdispatcher.Trigger;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 23.07.2017.
 */

public class EnableClientJobService extends SimpleJobService {

    private static final String JOB_TAG = "com.city_utd.city_utd.EnableClientJobService.JOB_TAG";

    @Override
    public int onRunJob(JobParameters job) {
        String tag = job.getTag();
        if(!JOB_TAG.equals(tag)){
            return RESULT_FAIL_NORETRY;
        }

        LocalDataManager manager = LocalDataManager.getInstance(this);
        if(!manager.isAuthorized()){
            return RESULT_FAIL_NORETRY;
        }

        String token = manager.getClientToken();
        if(TextUtils.isEmpty(token)){
            Intent intent = new Intent(this, ForceRefreshClientTokenService.class);
            startService(intent);
            return RESULT_FAIL_NORETRY;
        }

        try {
            ApiManager apiManager = ApiManager.getInstance();
            boolean result = apiManager.enableClient(token);
            return result ? RESULT_SUCCESS : RESULT_FAIL_RETRY;
        }catch (RequestException e){
            e.printStackTrace();
            return RESULT_FAIL_RETRY;
        }
    }

    public static void enableClient(){
        FirebaseJobDispatcher dispatcher = createDispatcher();

        Job job = dispatcher.newJobBuilder()
                .setService(EnableClientJobService.class)
                .setTag(JOB_TAG)
                .setRecurring(false)
                .setLifetime(Lifetime.FOREVER)
                .setTrigger(Trigger.NOW)
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();

        dispatcher.schedule(job);
    }

    private static FirebaseJobDispatcher createDispatcher(){
        Context context = CityUtdApplication.getAppContext();
        Driver driver = new GooglePlayDriver(context);
        return new FirebaseJobDispatcher(driver);
    }
}
