package com.city_utd.city_utd.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;

import com.city_utd.city_utd.util.LocalDataManager;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

/**
 * Created by Admin on 23.07.2017.
 */

public class ForceRefreshClientTokenService extends IntentService {

    private static final String SERVICE_NAME = ForceRefreshClientTokenService.class.getSimpleName();

    public ForceRefreshClientTokenService() {
        super(SERVICE_NAME);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        FirebaseInstanceId instanceId = FirebaseInstanceId.getInstance();
        try {
            instanceId.deleteInstanceId();
            LocalDataManager manager = LocalDataManager.getInstance(this);
            manager.setClientToken(null);
            instanceId.getToken();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
