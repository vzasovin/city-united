package com.city_utd.city_utd.service;

import android.Manifest;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.receiver.LocationUpdatesReceiver;
import com.city_utd.city_utd.util.LocalDataManager;
import com.firebase.jobdispatcher.Constraint;
import com.firebase.jobdispatcher.Driver;
import com.firebase.jobdispatcher.FirebaseJobDispatcher;
import com.firebase.jobdispatcher.GooglePlayDriver;
import com.firebase.jobdispatcher.Job;
import com.firebase.jobdispatcher.JobParameters;
import com.firebase.jobdispatcher.Lifetime;
import com.firebase.jobdispatcher.RetryStrategy;
import com.firebase.jobdispatcher.SimpleJobService;
import com.firebase.jobdispatcher.Trigger;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.imogene.android.carcase.AppUtils;

/**
 * Created by Admin on 12.07.2017.
 */

public class RequestLocationUpdatesJobService extends SimpleJobService{

    private static final String JOB_TAG = "com.city_utd.city_utd.RequestLocationUpdatesJobService.JOB_TAG";

    @SuppressWarnings("MissingPermission")
    @Override
    public int onRunJob(JobParameters job) {
        String tag = job.getTag();
        if(!JOB_TAG.equals(tag)){
            return RESULT_FAIL_NORETRY;
        }

        String permission = Manifest.permission.ACCESS_FINE_LOCATION;
        if(!AppUtils.Permissions.checkPermission(this, permission)){
            LocalDataManager manager = LocalDataManager.getInstance(this);
            manager.setShowFineLocationPermissionExplanationOnLaunchFlag(true);
            return RESULT_FAIL_NORETRY;
        }

        FusedLocationProviderClient client = LocationServices.getFusedLocationProviderClient(this);

        LocationRequest locationRequest = LocationRequest.create()
                .setExpirationDuration(60 * 60 * 1000)
                .setInterval(5 * 60 * 1000)
                .setFastestInterval(60 * 1000)
                .setNumUpdates(1)
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setSmallestDisplacement(300);

        Context context = getApplicationContext();
        Intent receiver = new Intent(context, LocationUpdatesReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, receiver, PendingIntent.FLAG_UPDATE_CURRENT);

        client.requestLocationUpdates(locationRequest, pendingIntent);
        return RESULT_SUCCESS;
    }

    public static void requestLocationUpdates(){
        FirebaseJobDispatcher dispatcher = createDispatcher();
        Job job = dispatcher.newJobBuilder()
                .setService(RequestLocationUpdatesJobService.class)
                .setTag(JOB_TAG)
                .setRecurring(true)
                .setLifetime(Lifetime.FOREVER)
                .setTrigger(Trigger.executionWindow(0, 3600))
                .setReplaceCurrent(true)
                .setRetryStrategy(RetryStrategy.DEFAULT_EXPONENTIAL)
                .setConstraints(Constraint.ON_ANY_NETWORK)
                .build();
        dispatcher.schedule(job);
    }

    private static FirebaseJobDispatcher createDispatcher(){
        Context context = CityUtdApplication.getAppContext();
        Driver driver = new GooglePlayDriver(context);
        return new FirebaseJobDispatcher(driver);
    }

    public static void cancel(){
        createDispatcher().cancel(JOB_TAG);
    }
}
