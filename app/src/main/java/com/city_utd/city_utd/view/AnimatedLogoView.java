package com.city_utd.city_utd.view;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.city_utd.city_utd.R;

/**
 * Created by Admin on 13.07.2017.
 */

public class AnimatedLogoView extends View {

    private final Bitmap bitmap;
    private final float diameter;
    private final float radius;
    private final float circumference;
    private final Paint paint;
    private final Matrix matrix;

    public AnimatedLogoView(Context context) {
        this(context, null);
    }

    public AnimatedLogoView(Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public AnimatedLogoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        final Resources resources = getResources();
        bitmap = BitmapFactory.decodeResource(resources, R.drawable.logo_ball);
        diameter = bitmap.getHeight();
        radius = diameter / 2;
        circumference = 2 * (float) Math.PI * radius;
        paint = new Paint();
        paint.setFilterBitmap(true);
        matrix = new Matrix();
        matrix.postTranslate(-diameter, 0);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = bitmap.getHeight();
        setMeasuredDimension(width, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawBitmap(bitmap, matrix, paint);
    }

    public void animateLogo(Animator.AnimatorListener listener){
        ValueAnimator animator = ValueAnimator.ofFloat(-diameter, getWidth() / 2 - radius);
        animator.setDuration(800);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float movedDistance = (float) valueAnimator.getAnimatedValue();
                float rotation = movedDistance / circumference;
                float rotationDegrees = rotation * 360;

                matrix.reset();
                matrix.postRotate(rotationDegrees, radius, radius);
                matrix.postTranslate(movedDistance, 0);

                postInvalidateOnAnimation();
            }
        });

        if(listener != null){
            animator.addListener(listener);
        }

        animator.start();
    }
}
