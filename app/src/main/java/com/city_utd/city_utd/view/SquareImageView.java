package com.city_utd.city_utd.view;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;

/**
 * Created by Admin on 31.07.2017.
 */

public class SquareImageView extends AppCompatImageView {

    public static final int BASE_DIMENSION_WIDTH = 1;
    public static final int BASE_DIMENSION_HEIGHT = 2;

    private int baseDimension = BASE_DIMENSION_WIDTH;

    public SquareImageView(Context context) {
        this(context, null);
    }

    public SquareImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SquareImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public final void setBaseDimension(int baseDimension){
        if(baseDimension != BASE_DIMENSION_WIDTH && baseDimension != BASE_DIMENSION_HEIGHT){
            throw new IllegalArgumentException("Invalid base dimension: " + baseDimension);
        }
        this.baseDimension = baseDimension;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        final int size;
        if(baseDimension == BASE_DIMENSION_WIDTH){
            size = MeasureSpec.getSize(widthMeasureSpec);
        }else {
            size = MeasureSpec.getSize(heightMeasureSpec);
        }
        setMeasuredDimension(size, size);
    }
}
