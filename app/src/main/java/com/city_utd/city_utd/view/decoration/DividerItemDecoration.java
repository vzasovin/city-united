package com.city_utd.city_utd.view.decoration;

import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Admin on 17.07.2017.
 */

public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    private int mLeftOffset;
    private int mRightOffset;
    private int mItemOffset;
    private boolean mDrawOver;
    private boolean mDrawLastDivider;
    private boolean mConsiderParentPadding;
    private boolean mConsiderChildPadding;

    private Paint mPaint;

    public DividerItemDecoration(int color, float dividerWidth, boolean drawOver) {
        mDrawOver = drawOver;
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(color);
        mPaint.setStrokeWidth(dividerWidth);
    }

    public void setDividerOffsets(int left, int right) {
        mLeftOffset = left;
        mRightOffset = right;
    }

    public void setItemOffset(int offset) {
        mItemOffset = offset;
    }

    public void setDrawLastDivider(boolean drawLastDivider) {
        mDrawLastDivider = drawLastDivider;
    }

    public void setConsiderParentPadding(boolean considerParentPadding) {
        mConsiderParentPadding = considerParentPadding;
    }

    public void setConsiderChildPadding(boolean considerChildPadding) {
        mConsiderChildPadding = considerChildPadding;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent,
                               RecyclerView.State state) {
        int offset = mItemOffset / 2;
        outRect.set(0, offset, 0, offset);
    }

    @Override
    public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (!mDrawOver) {
            draw(c, parent, state);
        }
    }

    @Override
    public void onDrawOver(Canvas c, RecyclerView parent, RecyclerView.State state) {
        if (mDrawOver) {
            draw(c, parent, state);
        }
    }

    private void draw(Canvas canvas, RecyclerView parent, RecyclerView.State state) {
        float startX = mLeftOffset;
        if (mConsiderParentPadding) {
            startX += parent.getPaddingLeft();
        }

        float stopX = startX + parent.getWidth() - mRightOffset;
        if (mConsiderParentPadding) {
            stopX -= parent.getPaddingRight();
        }

        int verticalOffset = mItemOffset / 2;

        int childCount = parent.getChildCount();

        for (int i = 0; i < childCount; i++) {
            if (i == childCount - 1 && !mDrawLastDivider) {
                break;
            }

            View child = parent.getChildAt(i);
            float finalStartX = startX;
            float finalStopX = stopX;

            if (mConsiderChildPadding) {
                finalStartX += child.getPaddingLeft();
                finalStopX -= child.getPaddingRight();
            }

            float Y = child.getBottom() + verticalOffset;

            canvas.drawLine(finalStartX, Y, finalStopX, Y, mPaint);
        }
    }
}
