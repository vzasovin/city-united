package com.city_utd.city_utd.view.behavior;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewPropertyAnimator;

import com.imogene.android.carcase.view.behavior.SlideOnScrollBehavior;

/**
 * Created by Admin on 31.07.2017.
 */

public class BottomAnchoredViewSlideOnScrollBehavior extends SlideOnScrollBehavior {

    public BottomAnchoredViewSlideOnScrollBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    @Override
    protected void hide(View child, View target, ViewPropertyAnimator animator) {
        int childHeight = child.getHeight();
        animator.translationYBy(childHeight);
    }

    @Override
    protected void show(View child, ViewPropertyAnimator animator) {
        float translationY = child.getTranslationY();
        animator.translationYBy(-translationY);
    }
}
