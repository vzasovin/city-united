package com.city_utd.city_utd.exception;

import java.text.ParseException;

/**
 * Created by Admin on 06.07.2017.
 */

public class RuntimeParseException extends RuntimeException {

    public RuntimeParseException(ParseException cause){
        super(cause);
    }
}
