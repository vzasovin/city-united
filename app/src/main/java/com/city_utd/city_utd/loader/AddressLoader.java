package com.city_utd.city_utd.loader;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;

import com.google.android.gms.maps.model.LatLng;
import com.imogene.android.carcase.exception.RequestException;

import java.io.IOException;
import java.util.List;

/**
 * Created by Admin on 04.08.2017.
 */

public class AddressLoader extends AbstractLoader<Address> {

    public static final String EXTRA_LOCATION = "com.city_utd.city_utd.AddressLoader.EXTRA_LOCATION";

    private final LatLng location;

    public AddressLoader(Context context, LatLng location) {
        super(context, SOURCE_SERVER, 0);
        this.location = location;
    }

    @Override
    protected Address loadFromServer() throws RequestException {
        if(!Geocoder.isPresent()){
            return null;
        }

        try {
            Context context = getContext();
            Geocoder geocoder = new Geocoder(context);

            double latitude = location.latitude;
            double longitude = location.longitude;

            List<Address> results = geocoder.getFromLocation(latitude, longitude, 1);
            if(results == null || results.isEmpty()){
                return null;
            }

            return results.get(0);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static boolean canBeUsed(){
        return Geocoder.isPresent();
    }
}
