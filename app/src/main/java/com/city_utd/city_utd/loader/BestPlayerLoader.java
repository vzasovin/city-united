package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;

/**
 * Created by Admin on 08.08.2017.
 */

public class BestPlayerLoader extends AbstractLoader<User> {

    public BestPlayerLoader(Context context, int source) {
        super(context, source);
    }

    @Override
    protected User loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getBestPlayer();
    }

    @Override
    protected void saveData(User data) throws SQLException {
        getUserDao().replaceBestPlayer(data);
    }

    private UserDao getUserDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getUserDao();
    }

    @Override
    protected User loadFromDatabase() throws SQLException {
        return getUserDao().getBestPlayer();
    }
}
