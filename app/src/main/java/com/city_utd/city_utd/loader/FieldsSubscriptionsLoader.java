package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.PagingQueryParamsHolder;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class FieldsSubscriptionsLoader extends AbstractLoader<List<PlayingField>> {

    private PagingQueryParamsHolder paramsHolder;
    private final int page;

    public FieldsSubscriptionsLoader(Context context, int source, PagingQueryParamsHolder paramsHolder) {
        super(context, source);
        this.paramsHolder = paramsHolder;

        if(paramsHolder != null){
            page = paramsHolder.getPage();
        }else {
            page = 1;
        }
    }

    public FieldsSubscriptionsLoader(Context context){
        super(context, SOURCE_DATABASE);
        page = 1;
    }

    @Override
    protected List<PlayingField> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getPlayingFieldsSubscriptions(paramsHolder);
    }

    @Override
    protected void saveData(List<PlayingField> data) throws SQLException {
        getPlayingFieldDao().replaceSubscriptions(data);
    }

    private PlayingFieldDao getPlayingFieldDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getPlayingFieldDao();
    }

    @Override
    protected List<PlayingField> loadFromDatabase() throws SQLException {
        return getPlayingFieldDao().getSubscriptions();
    }

    public final int getPage() {
        return page;
    }
}
