package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.PlayingFieldData;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDao;
import com.google.android.gms.maps.model.LatLng;

import java.sql.SQLException;

/**
 * Created by Admin on 03.08.2017.
 */

public class PlayingFieldDataLoader extends AbstractLoader<PlayingFieldData> {

    private final int playingFieldId;
    private String imageUrl;

    public PlayingFieldDataLoader(Context context, int playingFieldId) {
        super(context, SOURCE_DATABASE, 0);
        this.playingFieldId = playingFieldId;
    }

    @Override
    protected PlayingFieldData loadFromDatabase() throws SQLException {
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        PlayingFieldDao dao = helper.getPlayingFieldDao();
        PlayingField field = dao.queryForId(playingFieldId);
        if(field != null){
            PlayingFieldData data = new PlayingFieldData();
            data.setName(field.getName());
            data.setAddress(field.getAddress());
            LatLng coordinates = field.getCoordinates();
            if(coordinates != null){
                data.setLatitude(coordinates.latitude);
                data.setLongitude(coordinates.longitude);
            }

            imageUrl = field.getImageUrl();
            ImageUrlBuilder builder = ApiManager.newImageUrlBuilder();
            imageUrl = builder.build(imageUrl);
            return data;
        }
        return null;
    }

    public final String getImageUrl(){
        return imageUrl;
    }
}
