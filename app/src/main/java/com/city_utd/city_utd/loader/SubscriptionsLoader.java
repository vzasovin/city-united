package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.PagingQueryParamsHolder;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class SubscriptionsLoader extends AbstractLoader<List<User>> {

    private PagingQueryParamsHolder paramsHolder;
    private final int page;

    public SubscriptionsLoader(Context context, int source, PagingQueryParamsHolder paramsHolder) {
        super(context, source);
        this.paramsHolder = paramsHolder;

        if(paramsHolder != null){
            page = paramsHolder.getPage();
        }else {
            page = 1;
        }
    }

    public SubscriptionsLoader(Context context){
        super(context, SOURCE_DATABASE);
        page = 1;
    }

    @Override
    protected List<User> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getSubscriptions(paramsHolder);
    }

    @Override
    protected void saveData(List<User> data) throws SQLException {
        getUserDao().replaceSubscriptions(data);
    }

    private UserDao getUserDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getUserDao();
    }

    @Override
    protected List<User> loadFromDatabase() throws SQLException {
        return getUserDao().getSubscriptions();
    }

    public final int getPage(){
        return page;
    }
}
