package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDetailedDao;
import com.city_utd.city_utd.orm.dao.ScheduleItemDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class PlayingFieldLoader extends AbstractLoader<PlayingFieldDetailed> {

    public static final String EXTRA_MAX_DATE = "com.city_utd.city_utd.PlayingFieldLoader.EXTRA_MAX_DATE";

    private final int id;
    private final Date maxDate;

    public PlayingFieldLoader(Context context, int source, int id, Date maxDate) {
        super(context, source);
        this.id = id;
        this.maxDate = maxDate;
    }

    @Override
    protected PlayingFieldDetailed loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getPlayingField(id, maxDate);
    }

    @Override
    protected void saveData(PlayingFieldDetailed data) throws SQLException {
        getPlayingFieldDetailedDao().createOrUpdate(data);
    }

    private PlayingFieldDetailedDao getPlayingFieldDetailedDao() throws SQLException{
        return getHelper().getPlayingFieldDetailedDao();
    }

    private DatabaseHelper getHelper() throws SQLException{
        Context context = getContext();
        return DatabaseHelper.getInstance(context);
    }

    @Override
    protected PlayingFieldDetailed loadFromDatabase() throws SQLException {
        PlayingFieldDetailed playingField = getPlayingFieldDetailedDao().queryForId(id);
        if(playingField != null){
            loadScheduleFromDatabase(playingField);
        }
        return playingField;
    }

    private void loadScheduleFromDatabase(PlayingFieldDetailed parent) throws SQLException{
        List<ScheduleItem> schedule = getScheduleItemDao().getSchedule(parent, true, maxDate);
        parent.setSchedule(schedule);
    }

    private ScheduleItemDao getScheduleItemDao() throws SQLException{
        return getHelper().getScheduleItemDao();
    }
}
