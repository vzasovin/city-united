package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ImageUrlBuilder;
import com.city_utd.city_utd.client.ProfileData;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.city_utd.city_utd.util.LocalDataManager;

import java.sql.SQLException;

/**
 * Created by Admin on 01.08.2017.
 */

public class ProfileDataLoader extends AbstractLoader<ProfileData> {

    private String imageUrl;

    public ProfileDataLoader(Context context) {
        super(context, SOURCE_DATABASE, 0);
    }

    @Override
    protected ProfileData loadFromDatabase() throws SQLException {
        Context context = getContext();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        int userId = manager.getUserId();
        UserDetailedDao dao = getUserDetailedDao();
        UserDetailed user = dao.queryForId(userId);

        if(user != null){
            ImageUrlBuilder builder = ApiManager.newImageUrlBuilder();
            imageUrl = builder.build(user.getImageUrl());
            return createProfileData(user);
        }
        return null;
    }

    private UserDetailedDao getUserDetailedDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getUserDetailedDao();
    }

    private ProfileData createProfileData(UserDetailed user){
        ProfileData data = new ProfileData();
        data.setName(user.getName());
        data.setLine(user.getLine());
        data.setDateOfBirth(user.getDateOfBirth());
        data.setHeight(user.getHeight());
        data.setWeight(user.getWeight());
        data.setPhone(user.getPhone());
        data.setEmail(user.getEmail());
        return data;
    }

    public final String getImageUrl(){
        return imageUrl;
    }
}
