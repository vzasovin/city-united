package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.Invitation;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.InvitationDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class IncomingInvitationsLoader extends AbstractLoader<List<Invitation>> {

    public IncomingInvitationsLoader(Context context, int source) {
        super(context, source);
    }

    @Override
    protected List<Invitation> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getIncomingInvitations();
    }

    @Override
    protected void saveData(List<Invitation> data) throws SQLException {
        getInvitationDao().replaceIncomingInvitations(data);
    }

    private InvitationDao getInvitationDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getInvitationDao();
    }

    @Override
    protected List<Invitation> loadFromDatabase() throws SQLException {
        return getInvitationDao().getIncomingInvitations();
    }
}
