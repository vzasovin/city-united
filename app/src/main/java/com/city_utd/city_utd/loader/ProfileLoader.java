package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 10.07.2017.
 */

public class ProfileLoader extends UserLoader {

    public ProfileLoader(Context context, int source, long minDuration) {
        super(context, source, minDuration, LocalDataManager.getInstance(context).getUserId());
    }

    @Override
    protected UserDetailed loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getMyProfile();
    }
}
