package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.NearestQueryParamsHolder;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class NearestFieldsLoader extends AbstractLoader<List<PlayingField>> {

    private NearestQueryParamsHolder paramsHolder;

    public NearestFieldsLoader(Context context, NearestQueryParamsHolder paramsHolder, int source) {
        super(context, source, 0);
        if(source != SOURCE_SERVER && source != SOURCE_SERVER_DATABASE){
            throw new IllegalArgumentException("source must be SOURCE_SERVER or SOURCE_SERVER_DATABASE in this case");
        }
        this.paramsHolder = paramsHolder;
    }

    public NearestFieldsLoader(Context context){
        super(context, SOURCE_DATABASE, 0);
    }

    @Override
    protected List<PlayingField> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getNearestFields(paramsHolder);
    }

    @Override
    protected void saveData(List<PlayingField> data) throws SQLException {
        try {
            getPlayingFieldDao().replaceNearest(data);
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private PlayingFieldDao getPlayingFieldDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getPlayingFieldDao();
    }

    @Override
    protected List<PlayingField> loadFromDatabase() throws SQLException {
        return getPlayingFieldDao().getNearest();
    }
}
