package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.ScheduleItemDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class ScheduleLoader extends AbstractLoader<List<ScheduleItem>> {

    private final PlayingFieldDetailed parent;
    private final int playingFieldId;
    private final Date minDate;
    private final Date maxDate;
    private final boolean excludeSelf;

    public ScheduleLoader(Context context, PlayingFieldDetailed parent, Date minDate, Date maxDate, boolean excludeSelf) {
        super(context, SOURCE_SERVER_DATABASE);
        this.parent = parent;
        this.playingFieldId = -1;
        this.minDate = minDate;
        this.maxDate = maxDate;
        this.excludeSelf = excludeSelf;
    }

    public ScheduleLoader(Context context, int playingFieldId, Date minDate, Date maxDate,
                          boolean excludeSelf, boolean minDurationEnabled) {
        super(context, SOURCE_SERVER);
        this.parent = null;
        this.playingFieldId = playingFieldId;
        this.minDate = minDate;
        this.maxDate = maxDate;
        this.excludeSelf = excludeSelf;
        setMinDurationEnabled(minDurationEnabled);
    }

    @Override
    protected List<ScheduleItem> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        int id = parent != null ? parent.getId() : playingFieldId;
        return apiManager.getSchedule(id, minDate, maxDate, excludeSelf);
    }

    @Override
    protected void saveData(List<ScheduleItem> data) throws SQLException {
        getScheduleItemDao().replaceSchedule(parent, data);
    }

    private ScheduleItemDao getScheduleItemDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getScheduleItemDao();
    }

    @Override
    protected List<ScheduleItem> loadFromDatabase() throws SQLException {
        return getScheduleItemDao().getSchedule(parent, true, maxDate);
    }
}
