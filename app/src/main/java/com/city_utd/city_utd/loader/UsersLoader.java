package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.UsersQueryParamsHolder;
import com.city_utd.city_utd.model.User;
import com.imogene.android.carcase.exception.RequestException;

import java.util.List;

/**
 * Created by Admin on 10.07.2017.
 */

public class UsersLoader extends AbstractLoader<List<User>> {

    private final UsersQueryParamsHolder paramsHolder;

    public UsersLoader(Context context, UsersQueryParamsHolder paramsHolder) {
        super(context, SOURCE_SERVER);
        this.paramsHolder = paramsHolder;
    }

    @Override
    protected List<User> loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getAllUsers(paramsHolder);
    }
}
