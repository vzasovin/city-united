package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RequestException;

import java.sql.SQLException;

/**
 * Created by Admin on 10.07.2017.
 */

public class UserLoader extends AbstractLoader<UserDetailed> {

    private final int id;

    public UserLoader(Context context, int source, long minDuration, int id) {
        super(context, source, minDuration);
        this.id = id;
    }

    @Override
    protected UserDetailed loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.getUser(id);
    }

    @Override
    protected void saveData(UserDetailed data) throws SQLException {
        getUserDetailedDao().createOrUpdate(data);
    }

    private UserDetailedDao getUserDetailedDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getUserDetailedDao();
    }

    @Override
    protected UserDetailed loadFromDatabase() throws SQLException {
        return getUserDetailedDao().queryForId(id);
    }
}
