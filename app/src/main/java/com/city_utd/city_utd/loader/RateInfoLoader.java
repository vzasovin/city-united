package com.city_utd.city_utd.loader;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.RateInfo;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 14.08.2017.
 */

public class RateInfoLoader extends AbstractLoader<RateInfo> {

    private final int userId;
    private boolean dataLoaded;

    public RateInfoLoader(Context context, int userId) {
        super(context, SOURCE_SERVER);
        this.userId = userId;
    }

    @Override
    protected void onStartLoading() {
        if(!dataLoaded || takeContentChanged()){
            forceLoad();
        }else {
            deliverResult(data);
        }
    }

    @Override
    protected RateInfo loadFromServer() throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        RateInfo rateInfo = apiManager.getStatedRate(userId);
        dataLoaded = true;
        return rateInfo;
    }
}
