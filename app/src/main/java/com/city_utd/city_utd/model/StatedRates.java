package com.city_utd.city_utd.model;

import android.os.Parcel;

import com.city_utd.city_utd.client.Rate;
import com.city_utd.city_utd.orm.dao.StatedRatesDao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Admin on 20.07.2017.
 */

@DatabaseTable(tableName = StatedRates.TABLE_NAME, daoClass = StatedRatesDao.class)
public class StatedRates extends Rate {

    public static final String TABLE_NAME = "stated_rates";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_LAST_RATE_DATE = "last_rate_date";

    @DatabaseField(columnName = FIELD_ID, generatedId = true)
    private transient int id;

    @DatabaseField(columnName = FIELD_LAST_RATE_DATE, dataType = DataType.DATE_LONG)
    Date lastRateDate;

    StatedRates(){}

    private StatedRates(Parcel in){
        super(in);
        id = in.readInt();
        lastRateDate = (Date) in.readSerializable();
    }

    public Date getLastRateDate() {
        return lastRateDate;
    }

    public static final Creator<StatedRates> CREATOR = new Creator<StatedRates>() {
        @Override
        public StatedRates createFromParcel(Parcel in) {
            return new StatedRates(in);
        }

        @Override
        public StatedRates[] newArray(int size) {
            return new StatedRates[size];
        }
    };

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeInt(id);
        dest.writeSerializable(lastRateDate);
    }
}
