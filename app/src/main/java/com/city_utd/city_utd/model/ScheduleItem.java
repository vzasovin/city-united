package com.city_utd.city_utd.model;

import com.city_utd.city_utd.orm.dao.ScheduleItemDao;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Admin on 07.07.2017.
 */

@DatabaseTable(tableName = ScheduleItem.TABLE_NAME, daoClass = ScheduleItemDao.class)
public class ScheduleItem {

    public static final String TABLE_NAME = "schedule_items";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_USER_NAME = "user_name";
    public static final String FIELD_LINE = "line";
    public static final String FIELD_IMAGE_URL = "image_url";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_NUMBER_OF_RATES = "number_of_rates";
    public static final String FIELD_AVERAGE_RATING = "average_rating";
    public static final String FIELD_ACTUAL_RATING = "actual_rating";
    public static final String FIELD_PLAYING_FIELD = "playing_field";

    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, generatedId = true)
    private transient int id;

    @SerializedName("id")
    @DatabaseField(columnName = FIELD_USER_ID, dataType = DataType.INTEGER)
    int userId;

    @SerializedName("name")
    @DatabaseField(columnName = FIELD_USER_NAME, dataType = DataType.STRING)
    String userName;

    @SerializedName("line")
    @DatabaseField(columnName = FIELD_LINE, dataType = DataType.STRING)
    String line;

    @SerializedName("imageUrl")
    @DatabaseField(columnName = FIELD_IMAGE_URL, dataType = DataType.STRING)
    String imageUrl;

    @SerializedName("date")
    @DatabaseField(columnName = FIELD_DATE, dataType = DataType.DATE_LONG)
    Date date;

    @SerializedName("numberOfRates")
    @DatabaseField(columnName = FIELD_NUMBER_OF_RATES, dataType = DataType.INTEGER)
    int numberOfRates;

    @SerializedName("rating")
    @DatabaseField(columnName = FIELD_AVERAGE_RATING, dataType = DataType.FLOAT)
    float averageRating;

    @SerializedName("actualRating")
    @DatabaseField(columnName = FIELD_ACTUAL_RATING, dataType = DataType.FLOAT)
    float actualRating;

    @DatabaseField(columnName = FIELD_PLAYING_FIELD, foreign = true)
    private transient PlayingFieldDetailed playingField;

    ScheduleItem(){}

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getLine() {
        return line;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Date getDate() {
        return date;
    }

    public int getNumberOfRates() {
        return numberOfRates;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public float getActualRating() {
        return actualRating;
    }

    public void setPlayingField(PlayingFieldDetailed playingField) {
        this.playingField = playingField;
    }
}
