package com.city_utd.city_utd.model;

import com.city_utd.city_utd.orm.dao.PlayingFieldDetailedDao;
import com.city_utd.city_utd.util.Util;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 07.07.2017.
 */

@DatabaseTable(tableName = PlayingFieldDetailed.TABLE_NAME, daoClass = PlayingFieldDetailedDao.class)
public class PlayingFieldDetailed {

    public static final String TABLE_NAME = "playing_fields_detailed";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_BASE = "base";
    public static final String FIELD_ITEMS = "items";

    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, id = true)
    private int id;

    @DatabaseField(columnName = FIELD_BASE, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private PlayingField base;

    @ForeignCollectionField(columnName = FIELD_ITEMS)
    private ForeignCollection<ScheduleItem> persistedItems;

    private List<ScheduleItem> items;

    private PlayingFieldDetailed(){}

    public int getId() {
        return id;
    }

    public PlayingField getBase() {
        return base;
    }

    public String getName() {
        return base.getName();
    }

    public String getAddress() {
        return base.getAddress();
    }

    public String getImageUrl() {
        return base.getImageUrl();
    }

    public LatLng getCoordinates(){
        return base.getCoordinates();
    }

    public int getCreatorId() {
        return base.getCreatorId();
    }

    public Date getCreationDate() {
        return base.getCreationDate();
    }

    public Date getUpdateDate() {
        return base.getUpdateDate();
    }

    public boolean isSubscription() {
        return base.isSubscription();
    }

    public void setSubscription(boolean subscription){
        base.setSubscription(subscription);
    }

    public List<ScheduleItem> getSchedule(){
        return items;
    }

    public void setSchedule(List<ScheduleItem> items){
        this.items = items;
    }

    public ForeignCollection<ScheduleItem> getPersistedSchedule(){
        return persistedItems;
    }

    public static class Adapter extends TypeAdapter<PlayingFieldDetailed>{

        @Override
        public void write(JsonWriter out, PlayingFieldDetailed value) throws IOException {
            throw new UnsupportedOperationException("PlayingFieldDetailed objects is never sent to the server");
        }

        @Override
        public PlayingFieldDetailed read(JsonReader in) throws IOException {
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            PlayingFieldDetailed value = new PlayingFieldDetailed();
            PlayingField base = new PlayingField();
            value.base = base;
            in.beginObject();

            while (in.hasNext()){
                if(in.peek() != JsonToken.NAME){
                    in.skipValue();
                    continue;
                }

                String name = in.nextName();
                switch (name){
                    case "id":
                        int id = in.nextInt();
                        base.id = id;
                        value.id = id;
                        break;
                    case PlayingField.FIELD_NAME:
                        base.name = Util.TypeAdapter.nextString(in);
                        break;
                    case PlayingField.FIELD_ADDRESS:
                        base.address = Util.TypeAdapter.nextString(in);
                        break;
                    case "imageUrl":
                        base.imageUrl = Util.TypeAdapter.nextString(in);
                        break;
                    case PlayingField.FIELD_LATITUDE:
                        base.latitude = Util.TypeAdapter.nextDouble(in);
                        break;
                    case PlayingField.FIELD_LONGITUDE:
                        base.longitude = Util.TypeAdapter.nextDouble(in);
                        break;
                    case "creatorId":
                        base.creatorId = Util.TypeAdapter.nextInt(in);
                        break;
                    case "createdAt":
                        base.creationDate = Util.TypeAdapter.nextDate(in);
                        break;
                    case "updatedAt":
                        base.updateDate = Util.TypeAdapter.nextDate(in);
                        break;
                    case PlayingField.FIELD_IS_SUBSCRIPTION:
                        base.subscription = in.nextBoolean();
                        break;
                    case "schedule":
                        value.items = getSchedule(in);
                        break;
                }
            }

            in.endObject();
            return value;
        }

        private List<ScheduleItem> getSchedule(JsonReader in) throws IOException{
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            List<ScheduleItem> list = new ArrayList<>();
            in.beginArray();

            while (in.hasNext()){
                ScheduleItem item = new ScheduleItem();
                in.beginObject();

                while (in.hasNext()){
                    if(in.peek() != JsonToken.NAME){
                        in.skipValue();
                        continue;
                    }

                    String name = in.nextName();
                    switch (name){
                        case "id":
                            item.userId = in.nextInt();
                            break;
                        case "name":
                            item.userName = Util.TypeAdapter.nextString(in);
                            break;
                        case ScheduleItem.FIELD_LINE:
                            item.line = Util.TypeAdapter.nextString(in);
                            break;
                        case "imageUrl":
                            item.imageUrl = Util.TypeAdapter.nextString(in);
                            break;
                        case ScheduleItem.FIELD_DATE:
                            item.date = Util.TypeAdapter.nextDate(in);
                            break;
                        case "numberOfRates":
                            item.numberOfRates = in.nextInt();
                            break;
                        case "rating":
                            item.averageRating = Util.TypeAdapter.nextFloat(in);
                            break;
                        case "actualRating":
                            item.actualRating = Util.TypeAdapter.nextFloat(in);
                            break;
                    }
                }

                in.endObject();
                list.add(item);
            }

            in.endArray();
            return list;
        }
    }
}
