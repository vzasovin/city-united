package com.city_utd.city_utd.model;

/**
 * Created by Admin on 05.07.2017.
 */

public final class PlayerLines {

    public static final String GOALKEEPER = "goalkeeper";
    public static final String DEFENDER = "defender";
    public static final String MIDFIELDER = "midfielder";
    public static final String FORWARD = "forward";

    private PlayerLines(){}

    public static boolean validate(String line){
        return GOALKEEPER.equals(line)
                || DEFENDER.equals(line)
                || MIDFIELDER.equals(line)
                || FORWARD.equals(line);
    }
}
