package com.city_utd.city_utd.model;

import com.city_utd.city_utd.orm.dao.UserDao;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Admin on 05.07.2017.
 */

@DatabaseTable(tableName = User.TABLE_NAME, daoClass = UserDao.class)
public class User {

    public static final String TABLE_NAME = "users";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_LINE = "line";
    public static final String FIELD_IMAGE_URL = "image_url";
    public static final String FIELD_DATE_OF_BIRTH = "date_of_birth";
    public static final String FIELD_HEIGHT = "height";
    public static final String FIELD_WEIGHT = "weight";
    public static final String FIELD_PHONE = "phone";
    public static final String FIELD_EMAIL = "email";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_NUMBER_OF_RATES = "number_of_rates";
    public static final String FIELD_AVERAGE_RATING = "average_rating";
    public static final String FIELD_ACTUAL_RATING = "actual_rating";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_UPDATED_AT = "updated_at";
    public static final String FIELD_IS_BASE = "base";
    public static final String FIELD_IS_SUBSCRIPTION = "subscription";
    public static final String FIELD_IS_BEST_PLAYER = "best_player";

    @SerializedName("id")
    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, id = true)
    int id;

    @SerializedName("name")
    @DatabaseField(columnName = FIELD_NAME, dataType = DataType.STRING)
    String name;

    @SerializedName("line")
    @DatabaseField(columnName = FIELD_LINE, dataType = DataType.STRING)
    String line;

    @SerializedName("imageUrl")
    @DatabaseField(columnName = FIELD_IMAGE_URL, dataType = DataType.STRING)
    String imageUrl;

    @SerializedName("dateOfBirth")
    @DatabaseField(columnName = FIELD_DATE_OF_BIRTH, dataType = DataType.DATE_LONG)
    Date dateOfBirth;

    @SerializedName("height")
    @DatabaseField(columnName = FIELD_HEIGHT, dataType = DataType.INTEGER)
    int height;

    @SerializedName("weight")
    @DatabaseField(columnName = FIELD_WEIGHT, dataType = DataType.INTEGER)
    int weight;

    @SerializedName("phone")
    @DatabaseField(columnName = FIELD_PHONE, dataType = DataType.STRING)
    String phone;

    @SerializedName("email")
    @DatabaseField(columnName = FIELD_EMAIL, dataType = DataType.STRING)
    String email;

    @SerializedName("latitude")
    @DatabaseField(columnName = FIELD_LATITUDE, dataType = DataType.DOUBLE)
    double latitude;

    @SerializedName("longitude")
    @DatabaseField(columnName = FIELD_LONGITUDE, dataType = DataType.DOUBLE)
    double longitude;

    @SerializedName("numberOfRates")
    @DatabaseField(columnName = FIELD_NUMBER_OF_RATES, dataType = DataType.INTEGER)
    int numberOfRates;

    @SerializedName("rating")
    @DatabaseField(columnName = FIELD_AVERAGE_RATING, dataType = DataType.FLOAT)
    float averageRating;

    @SerializedName("actualRating")
    @DatabaseField(columnName = FIELD_ACTUAL_RATING, dataType = DataType.FLOAT)
    float actualRating;

    @SerializedName("createdAt")
    @DatabaseField(columnName = FIELD_CREATED_AT, dataType = DataType.DATE_LONG)
    Date createdAt;

    @SerializedName("updatedAt")
    @DatabaseField(columnName = FIELD_UPDATED_AT, dataType = DataType.DATE_LONG)
    Date updatedAt;

    @DatabaseField(columnName = FIELD_IS_BASE, dataType = DataType.BOOLEAN)
    private transient boolean base;

    @DatabaseField(columnName = FIELD_IS_SUBSCRIPTION, dataType = DataType.BOOLEAN)
    transient boolean subscription;

    @DatabaseField(columnName = FIELD_IS_BEST_PLAYER, dataType = DataType.BOOLEAN)
    private transient boolean bestPlayer;

    @SerializedName("distance")
    private int distance;

    User(){}

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getLine() {
        return line;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public int getHeight() {
        return height;
    }

    public int getWeight() {
        return weight;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public LatLng getCoordinates(){
        if(latitude != 0 && longitude != 0){
            return new LatLng(latitude, longitude);
        }
        return null;
    }

    public int getNumberOfRates() {
        return numberOfRates;
    }

    public float getAverageRating() {
        return averageRating;
    }

    public float getActualRating() {
        return actualRating;
    }

    public Date getCreationDate(){
        return createdAt;
    }

    public Date getUpdateDate(){
        return updatedAt;
    }

    public void setBase(boolean base) {
        this.base = base;
    }

    public boolean isBase() {
        return base;
    }

    public void setSubscription(boolean subscription) {
        this.subscription = subscription;
    }

    public boolean isSubscription() {
        return subscription;
    }

    public void setBestPlayer(boolean bestPlayer) {
        this.bestPlayer = bestPlayer;
    }

    public boolean isBestPlayer() {
        return bestPlayer;
    }

    public int getDistance() {
        return distance;
    }
}
