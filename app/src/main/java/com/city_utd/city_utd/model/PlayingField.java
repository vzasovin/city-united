package com.city_utd.city_utd.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.city_utd.city_utd.orm.dao.PlayingFieldDao;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Admin on 06.07.2017.
 */

@DatabaseTable(tableName = PlayingField.TABLE_NAME, daoClass = PlayingFieldDao.class)
public class PlayingField implements Parcelable{

    public static final String TABLE_NAME = "playing_fields";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_NAME = "name";
    public static final String FIELD_ADDRESS = "address";
    public static final String FIELD_IMAGE_URL = "image_url";
    public static final String FIELD_LATITUDE = "latitude";
    public static final String FIELD_LONGITUDE = "longitude";
    public static final String FIELD_CREATOR_ID = "creator_id";
    public static final String FIELD_CREATED_AT = "created_at";
    public static final String FIELD_UPDATED_AT = "updated_at";
    public static final String FIELD_IS_BASE = "base";
    public static final String FIELD_IS_SUBSCRIPTION = "subscription";
    public static final String FIELD_IS_NEAREST = "nearest";
    public static final String FIELD_DISTANCE = "distance";

    @SerializedName("id")
    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, id = true)
    int id;

    @SerializedName("name")
    @DatabaseField(columnName = FIELD_NAME, dataType = DataType.STRING)
    String name;

    @SerializedName("address")
    @DatabaseField(columnName = FIELD_ADDRESS, dataType = DataType.STRING)
    String address;

    @SerializedName("imageUrl")
    @DatabaseField(columnName = FIELD_IMAGE_URL, dataType = DataType.STRING)
    String imageUrl;

    @SerializedName("latitude")
    @DatabaseField(columnName = FIELD_LATITUDE, dataType = DataType.DOUBLE)
    double latitude;

    @SerializedName("longitude")
    @DatabaseField(columnName = FIELD_LONGITUDE, dataType = DataType.DOUBLE)
    double longitude;

    @SerializedName("creatorId")
    @DatabaseField(columnName = FIELD_CREATOR_ID, dataType = DataType.INTEGER)
    int creatorId;

    @SerializedName("createdAt")
    @DatabaseField(columnName = FIELD_CREATED_AT, dataType = DataType.DATE_LONG)
    Date creationDate;

    @SerializedName("updatedAt")
    @DatabaseField(columnName = FIELD_UPDATED_AT, dataType = DataType.DATE_LONG)
    Date updateDate;

    @DatabaseField(columnName = FIELD_IS_BASE, dataType = DataType.BOOLEAN)
    private transient boolean base;

    @DatabaseField(columnName = FIELD_IS_SUBSCRIPTION, dataType = DataType.BOOLEAN)
    transient boolean subscription;

    @DatabaseField(columnName = FIELD_IS_NEAREST, dataType = DataType.BOOLEAN)
    private transient boolean nearest;

    @SerializedName("distance")
    @DatabaseField(columnName = FIELD_DISTANCE, dataType = DataType.INTEGER)
    private int distance;

    PlayingField(){}

    private PlayingField(Parcel in) {
        id = in.readInt();
        name = in.readString();
        address = in.readString();
        imageUrl = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        creatorId = in.readInt();
        creationDate = (Date) in.readSerializable();
        base = in.readByte() == 1;
        subscription = in.readByte() == 1;
        nearest = in.readByte() == 1;
        distance = in.readInt();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public LatLng getCoordinates(){
        if(latitude != 0 && longitude != 0){
            return new LatLng(latitude, longitude);
        }
        return null;
    }

    public int getCreatorId() {
        return creatorId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setBase(boolean base) {
        this.base = base;
    }

    public boolean isBase() {
        return base;
    }

    public void setSubscription(boolean subscription) {
        this.subscription = subscription;
    }

    public boolean isSubscription() {
        return subscription;
    }

    public void setNearest(boolean nearest) {
        this.nearest = nearest;
    }

    public boolean isNearest() {
        return nearest;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public int getDistance() {
        return distance;
    }

    public static final Creator<PlayingField> CREATOR = new Creator<PlayingField>() {
        @Override
        public PlayingField createFromParcel(Parcel in) {
            return new PlayingField(in);
        }

        @Override
        public PlayingField[] newArray(int size) {
            return new PlayingField[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(imageUrl);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeInt(creatorId);
        dest.writeSerializable(creationDate);
        dest.writeByte((byte) (base ? 1 : 0));
        dest.writeByte((byte) (subscription ? 1 : 0));
        dest.writeByte((byte) (nearest ? 1 : 0));
        dest.writeInt(distance);
    }
}
