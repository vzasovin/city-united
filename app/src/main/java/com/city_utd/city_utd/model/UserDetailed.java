package com.city_utd.city_utd.model;

import android.text.TextUtils;

import com.city_utd.city_utd.client.NotificationSetting;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.city_utd.city_utd.util.Util;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 06.07.2017.
 */

@DatabaseTable(tableName = UserDetailed.TABLE_NAME, daoClass = UserDetailedDao.class)
public class UserDetailed {

    public static final String TABLE_NAME = "users_detailed";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_BASE = "base";
    public static final String FIELD_SPEED = "speed";
    public static final String FIELD_STAMINA = "stamina";
    public static final String FIELD_TECHNIQUE = "technique";
    public static final String FIELD_IMPACT_FORCE = "impact_force";
    public static final String FIELD_PASS = "pass";
    public static final String FIELD_DEFENSE = "defense";
    public static final String FIELD_FRIENDLINESS = "friendliness";
    public static final String FIELD_REACTION = "reaction";
    public static final String FIELD_RELIABILITY = "reliability";
    public static final String FIELD_HANDS = "hands";
    public static final String FIELD_FEET = "feet";
    public static final String FIELD_JUMP = "jump";
    public static final String FIELD_COURAGE = "courage";
    public static final String FIELD_GALLERY = "gallery";
    public static final String FIELD_STATED_RATES = "stated_rates";

    private static final String GALLERY_DELIMITER = "~#gallery#~";

    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, id = true)
    private int id;

    @DatabaseField(columnName = FIELD_BASE, foreign = true, foreignAutoRefresh = true, canBeNull = false)
    private User base;

    @DatabaseField(columnName = FIELD_SPEED, dataType = DataType.FLOAT)
    private float speed;

    @DatabaseField(columnName = FIELD_STAMINA, dataType = DataType.FLOAT)
    private float stamina;

    @DatabaseField(columnName = FIELD_TECHNIQUE, dataType = DataType.FLOAT)
    private float technique;

    @DatabaseField(columnName = FIELD_IMPACT_FORCE, dataType = DataType.FLOAT)
    private float impactForce;

    @DatabaseField(columnName = FIELD_PASS, dataType = DataType.FLOAT)
    private float pass;

    @DatabaseField(columnName = FIELD_DEFENSE, dataType = DataType.FLOAT)
    private float defense;

    @DatabaseField(columnName = FIELD_FRIENDLINESS, dataType = DataType.FLOAT)
    private float friendliness;

    @DatabaseField(columnName = FIELD_REACTION, dataType = DataType.FLOAT)
    private float reaction;

    @DatabaseField(columnName = FIELD_RELIABILITY, dataType = DataType.FLOAT)
    private float reliability;

    @DatabaseField(columnName = FIELD_HANDS, dataType = DataType.FLOAT)
    private float hands;

    @DatabaseField(columnName = FIELD_FEET, dataType = DataType.FLOAT)
    private float feet;

    @DatabaseField(columnName = FIELD_JUMP, dataType = DataType.FLOAT)
    private float jump;

    @DatabaseField(columnName = FIELD_COURAGE, dataType = DataType.FLOAT)
    private float courage;

    @DatabaseField(columnName = FIELD_GALLERY, dataType = DataType.STRING)
    private String gallery;

    private List<NotificationSetting> notificationsSettings;

    @DatabaseField(columnName = FIELD_STATED_RATES, foreign = true, foreignAutoRefresh = true)
    private StatedRates statedRates;

    private UserDetailed(){}

    public int getId() {
        return id;
    }

    public User getBase() {
        return base;
    }

    public String getName() {
        return base.name;
    }

    public String getLine() {
        return base.getLine();
    }

    public String getImageUrl() {
        return base.getImageUrl();
    }

    public Date getDateOfBirth() {
        return base.getDateOfBirth();
    }

    public int getHeight() {
        return base.getHeight();
    }

    public int getWeight() {
        return base.getWeight();
    }

    public String getPhone() {
        return base.getPhone();
    }

    public String getEmail() {
        return base.getEmail();
    }

    public LatLng getCoordinates(){
        return base.getCoordinates();
    }

    public int getNumberOfRates() {
        return base.getNumberOfRates();
    }

    public float getAverageRating() {
        return base.getAverageRating();
    }

    public float getActualRating(){
        return base.getActualRating();
    }

    public Date getCreationDate(){
        return base.getCreationDate();
    }

    public Date getUpdateDate(){
        return base.getUpdateDate();
    }

    public float getSpeed() {
        return speed;
    }

    public float getStamina() {
        return stamina;
    }

    public float getTechnique() {
        return technique;
    }

    public float getImpactForce() {
        return impactForce;
    }

    public float getPass() {
        return pass;
    }

    public float getDefense() {
        return defense;
    }

    public float getFriendliness() {
        return friendliness;
    }

    public float getReaction() {
        return reaction;
    }

    public float getReliability() {
        return reliability;
    }

    public float getHands() {
        return hands;
    }

    public float getFeet() {
        return feet;
    }

    public float getJump() {
        return jump;
    }

    public float getCourage() {
        return courage;
    }

    public boolean isSubscription() {
        return base.isSubscription();
    }

    public void setSubscription(boolean subscription){
        base.setSubscription(subscription);
    }

    public List<String> getGallery() {
        if(!TextUtils.isEmpty(gallery)){
            return Arrays.asList(gallery.split(GALLERY_DELIMITER));
        }
        return null;
    }

    public List<NotificationSetting> getNotificationsSettings() {
        return notificationsSettings;
    }

    public StatedRates getStatedRates(){
        return statedRates;
    }

    public static class Adapter extends TypeAdapter<UserDetailed>{

        @Override
        public void write(JsonWriter out, UserDetailed value) throws IOException {
            throw new UnsupportedOperationException("UserDetailed objects is never sent to the server");
        }

        @Override
        public UserDetailed read(JsonReader in) throws IOException {
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            final UserDetailed value = new UserDetailed();
            final User base = new User();
            value.base = base;
            in.beginObject();

            while (in.hasNext()){
                if(in.peek() != JsonToken.NAME){
                    continue;
                }

                String name = in.nextName();
                switch (name){
                    case "id":
                        int id = in.nextInt();
                        base.id = id;
                        value.id = id;
                        break;
                    case User.FIELD_NAME:
                        base.name = Util.TypeAdapter.nextString(in);
                        break;
                    case User.FIELD_LINE:
                        base.line = Util.TypeAdapter.nextString(in);
                        break;
                    case "imageUrl":
                        base.imageUrl = Util.TypeAdapter.nextString(in);
                        break;
                    case "dateOfBirth":
                        base.dateOfBirth = Util.TypeAdapter.nextDate(in);
                        break;
                    case User.FIELD_HEIGHT:
                        base.height = Util.TypeAdapter.nextInt(in);
                        break;
                    case User.FIELD_WEIGHT:
                        base.weight = Util.TypeAdapter.nextInt(in);
                        break;
                    case User.FIELD_PHONE:
                        base.phone = Util.TypeAdapter.nextString(in);
                        break;
                    case User.FIELD_EMAIL:
                        base.email = Util.TypeAdapter.nextString(in);
                        break;
                    case User.FIELD_LATITUDE:
                        base.latitude = Util.TypeAdapter.nextDouble(in);
                        break;
                    case User.FIELD_LONGITUDE:
                        base.longitude = Util.TypeAdapter.nextDouble(in);
                        break;
                    case "createdAt":
                        base.createdAt = Util.TypeAdapter.nextDate(in);
                        break;
                    case "updatedAt":
                        base.updatedAt = Util.TypeAdapter.nextDate(in);
                        break;
                    case FIELD_SPEED:
                        value.speed = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_STAMINA:
                        value.stamina = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_TECHNIQUE:
                        value.technique = Util.TypeAdapter.nextFloat(in);
                        break;
                    case "impactForce":
                        value.impactForce = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_PASS:
                        value.pass = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_DEFENSE:
                        value.defense = Util.TypeAdapter.nextFloat(in);
                        break;
                    case "numberOfRates":
                        base.numberOfRates = in.nextInt();
                        break;
                    case "rating":
                        base.averageRating = Util.TypeAdapter.nextFloat(in);
                        break;
                    case "actualRating":
                        base.actualRating = Util.TypeAdapter.nextFloat(in);
                        break;
                    case User.FIELD_IS_SUBSCRIPTION:
                        base.subscription = in.nextBoolean();
                        break;
                    case FIELD_GALLERY:
                        value.gallery = getGallery(in);
                        break;
                    case "notificationsSettings":
                        value.notificationsSettings = getNotificationsSettings(in);
                        break;
                    case "statedRates":
                        value.statedRates = getStatedRates(in);
                        break;
                    case FIELD_FRIENDLINESS:
                        value.friendliness = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_REACTION:
                        value.reaction = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_RELIABILITY:
                        value.reliability = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_HANDS:
                        value.hands = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_FEET:
                        value.feet = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_JUMP:
                        value.jump = Util.TypeAdapter.nextFloat(in);
                        break;
                    case FIELD_COURAGE:
                        value.courage = Util.TypeAdapter.nextFloat(in);
                        break;
                    default:
                        in.skipValue();
                        break;
                }
            }

            in.endObject();
            return value;
        }

        private String getGallery(JsonReader in) throws IOException{
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            StringBuilder sb = new StringBuilder();
            in.beginArray();

            while (in.hasNext()){
                String item = in.nextString();
                sb.append(item);
                if(in.hasNext()){
                    sb.append(GALLERY_DELIMITER);
                }
            }

            in.endArray();
            return sb.toString();
        }

        private List<NotificationSetting> getNotificationsSettings(JsonReader in) throws IOException{
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            List<NotificationSetting> list = new ArrayList<>();
            in.beginArray();

            while (in.hasNext()){
                String type = null;
                boolean enabled = false;
                NotificationSetting setting;
                in.beginObject();

                while (in.hasNext()){
                    String name = in.nextName();
                    switch (name){
                        case "type":
                            type = in.nextString();
                            break;
                        case "enabled":
                            enabled = in.nextBoolean();
                            break;
                    }
                }

                in.endObject();
                setting = new NotificationSetting(type, enabled);
                list.add(setting);
            }

            in.endArray();
            return list;
        }

        private StatedRates getStatedRates(JsonReader in) throws IOException{
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return null;
            }

            in.beginObject();
            StatedRates rates = new StatedRates();

            while (in.hasNext()){
                String name = in.nextName();
                switch (name){
                    case FIELD_SPEED:
                        rates.setSpeed(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_STAMINA:
                        rates.setStamina(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_TECHNIQUE:
                        rates.setTechnique(Util.TypeAdapter.nextInt(in));
                        break;
                    case "impactForce":
                        rates.setImpactForce(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_PASS:
                        rates.setPass(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_DEFENSE:
                        rates.setDefense(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_FRIENDLINESS:
                        rates.setFriendliness(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_REACTION:
                        rates.setReaction(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_RELIABILITY:
                        rates.setReliability(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_HANDS:
                        rates.setHands(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_FEET:
                        rates.setFeet(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_JUMP:
                        rates.setJump(Util.TypeAdapter.nextInt(in));
                        break;
                    case FIELD_COURAGE:
                        rates.setCourage(Util.TypeAdapter.nextInt(in));
                        break;
                    case "lastRateDate":
                        rates.lastRateDate = Util.TypeAdapter.nextDate(in);
                        break;
                }
            }

            in.endObject();
            return rates;
        }
    }
}
