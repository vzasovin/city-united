package com.city_utd.city_utd.model;

import com.city_utd.city_utd.orm.dao.InvitationDao;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.Date;

/**
 * Created by Admin on 06.07.2017.
 */

@DatabaseTable(tableName = Invitation.TABLE_NAME, daoClass = InvitationDao.class)
public class Invitation {

    public static final String TABLE_NAME = "invitations";
    public static final String FIELD_ID = "_id";
    public static final String FIELD_USER_ID = "user_id";
    public static final String FIELD_USER_NAME = "user_name";
    public static final String FIELD_LINE = "line";
    public static final String FIELD_USER_IMAGE_URL = "user_image_url";
    public static final String FIELD_FIELD_ID = "field_id";
    public static final String FIELD_FIELD_ADDRESS = "field_address";
    public static final String FIELD_FIELD_NAME = "field_name";
    public static final String FIELD_FIELD_IMAGE_URL = "field_image_url";
    public static final String FIELD_MESSAGE = "message";
    public static final String FIELD_DATE = "date";
    public static final String FIELD_STATUS = "status";
    public static final String FIELD_INCOMING = "incoming";

    public static final String STATUS_ACTIVE = "active";
    public static final String STATUS_ACCEPTED = "accepted";
    public static final String STATUS_REJECTED = "rejected";

    @DatabaseField(columnName = FIELD_ID, dataType = DataType.INTEGER, generatedId = true)
    private transient int id;

    @SerializedName("userId")
    @DatabaseField(columnName = FIELD_USER_ID, dataType = DataType.INTEGER)
    private int userId;

    @SerializedName("userName")
    @DatabaseField(columnName = FIELD_USER_NAME, dataType = DataType.STRING)
    private String userName;

    @SerializedName("line")
    @DatabaseField(columnName = FIELD_LINE, dataType = DataType.STRING)
    private String line;

    @SerializedName("userImageUrl")
    @DatabaseField(columnName = FIELD_USER_IMAGE_URL, dataType = DataType.STRING)
    private String userImageUrl;

    @SerializedName("fieldId")
    @DatabaseField(columnName = FIELD_FIELD_ID, dataType = DataType.INTEGER)
    private int fieldId;

    @SerializedName("fieldAddress")
    @DatabaseField(columnName = FIELD_FIELD_ADDRESS, dataType = DataType.STRING)
    private String fieldAddress;

    @SerializedName("fieldName")
    @DatabaseField(columnName = FIELD_FIELD_NAME, dataType = DataType.STRING)
    private String fieldName;

    @SerializedName("fieldImageUrl")
    @DatabaseField(columnName = FIELD_FIELD_IMAGE_URL, dataType = DataType.STRING)
    private String fieldImageUrl;

    @SerializedName("message")
    @DatabaseField(columnName = FIELD_MESSAGE, dataType = DataType.STRING)
    private String message;

    @SerializedName("date")
    @DatabaseField(columnName = FIELD_DATE, dataType = DataType.DATE_LONG)
    private Date date;

    @SerializedName("status")
    @DatabaseField(columnName = FIELD_STATUS, dataType = DataType.STRING)
    private String status;

    @DatabaseField(columnName = FIELD_INCOMING, dataType = DataType.BOOLEAN)
    private transient boolean incoming;

    Invitation(){}

    public int getUserId() {
        return userId;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserLine() {
        return line;
    }

    public String getUserImageUrl() {
        return userImageUrl;
    }

    public int getFieldId() {
        return fieldId;
    }

    public String getFieldAddress() {
        return fieldAddress;
    }

    public String getFieldName() {
        return fieldName;
    }

    public String getFieldImageUrl() {
        return fieldImageUrl;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setIncoming(boolean incoming) {
        this.incoming = incoming;
    }

    public boolean isIncoming() {
        return incoming;
    }
}
