package com.city_utd.city_utd.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;

import com.city_utd.city_utd.service.SyncLocationJobService;
import com.city_utd.city_utd.util.LocalDataManager;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Admin on 12.07.2017.
 */

public class LocationUpdatesReceiver extends BroadcastReceiver {

    private static final int SYNC_INTERVAL_SECONDS = 30 * 60;

    @Override
    public void onReceive(Context context, Intent intent) {
        if(LocationResult.hasResult(intent)){
            LocationResult result = LocationResult.extractResult(intent);
            Location location = result.getLastLocation();
            if(location != null){
                double latitude = location.getLatitude();
                double longitude = location.getLongitude();
                LatLng latLng = new LatLng(latitude, longitude);

                LocalDataManager manager = LocalDataManager.getInstance(context);
                manager.setLastLocation(latLng);

                SyncLocationJobService.sync(SYNC_INTERVAL_SECONDS);
            }
        }
    }
}
