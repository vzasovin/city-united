package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.AuthorizationResponse;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.sql.SQLException;

/**
 * Created by Admin on 13.07.2017.
 */

public abstract class AuthorizationAsyncTask<Param> extends AbstractAsyncTask<Param, Void, UserDetailed> {

    private static final long MIN_DURATION = 2500;

    public AuthorizationAsyncTask(int taskId, GenericCallbacks<Void, UserDetailed> callbacks) {
        super(taskId, callbacks, MIN_DURATION);
    }

    @Override
    protected final UserDetailed onExecuteOnNetwork(Param[] params) throws RequestException {
        AuthorizationResponse response = authorize(params);
        UserDetailed user = response.getUser();
        String token = response.getToken();
        try {
            saveUser(user);
            saveTokenAndId(token, user.getId());
            saveAdditionalFlags(response);
        }catch (SQLException e){
            throw new RuntimeSQLException(e);
        }
        return user;
    }

    protected abstract AuthorizationResponse authorize(Param[] params) throws RequestException;

    private void saveUser(UserDetailed user) throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        UserDetailedDao dao = helper.getUserDetailedDao();
        dao.createOrUpdate(user);
    }

    private void saveTokenAndId(String token, int id){
        Context context = getContext();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setJwtToken(token).setUserId(id);
    }

    private void saveAdditionalFlags(AuthorizationResponse response){
        Context context = getContext();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        manager.setShowSelfEstimationFlag(response.isCreated());
    }
}
