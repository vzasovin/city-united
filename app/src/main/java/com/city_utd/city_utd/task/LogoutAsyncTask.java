package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.util.LocalDataManager;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 28.07.2017.
 */

public class LogoutAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    public LogoutAsyncTask(int taskId, Callbacks callbacks) {
        super(taskId, callbacks);
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] voids) throws RequestException {
        Context context = getContext();
        LocalDataManager manager = LocalDataManager.getInstance(context);
        String clientToken = manager.getClientToken();
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.logout(clientToken);
    }
}
