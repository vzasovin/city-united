package com.city_utd.city_utd.task;

import android.content.Context;
import android.text.TextUtils;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.KnownStatusCodes;
import com.city_utd.city_utd.client.PlayingFieldData;
import com.city_utd.city_utd.model.PlayingField;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 04.08.2017.
 */

public class CreatePlayingFieldAsyncTask extends AbstractAsyncTask<Void, Void, PlayingField> {

    private final PlayingFieldData playingFieldData;
    private final String imageFilePath;

    public CreatePlayingFieldAsyncTask(int taskId, Callbacks callbacks,
                                       PlayingFieldData playingFieldData, String imageFilePath) {
        super(taskId, callbacks, SUGGESTED_MIN_DURATION);
        this.playingFieldData = playingFieldData;
        this.imageFilePath = imageFilePath;
    }

    @Override
    protected PlayingField onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        if(!TextUtils.isEmpty(imageFilePath)){
            return apiManager.createPlayingField(playingFieldData, imageFilePath);
        }else {
            return apiManager.createPlayingField(playingFieldData);
        }
    }

    @Override
    protected String createErrorMessage(int errorCode) {
        switch (errorCode){
            case KnownStatusCodes.BAD_REQUEST:
                double latitude = playingFieldData.getLatitude();
                double longitude = playingFieldData.getLongitude();
                boolean hasCoordinates = latitude != 0 && longitude != 0;

                Context context = getContext();
                if(!hasCoordinates){
                    return context.getString(R.string.could_not_determine_coordinates_by_address);
                }else {
                    return context.getString(R.string.could_not_determine_address_by_coordinates);
                }
            default:
                return super.createErrorMessage(errorCode);
        }
    }
}
