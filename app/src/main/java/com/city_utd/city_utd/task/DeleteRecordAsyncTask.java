package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.util.Date;

/**
 * Created by Admin on 02.08.2017.
 */

public class DeleteRecordAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final int playingFieldId;
    private final Date date;

    public DeleteRecordAsyncTask(int taskId, Callbacks callbacks, int playingFieldId, Date date) {
        super(taskId, callbacks);
        this.playingFieldId = playingFieldId;
        this.date = date;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.deleteRecord(playingFieldId, date);
    }
}
