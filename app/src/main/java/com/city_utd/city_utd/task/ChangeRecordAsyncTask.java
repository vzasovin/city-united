package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.util.Date;

/**
 * Created by Admin on 02.08.2017.
 */

public class ChangeRecordAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private int playingFieldId;
    private Date oldDate;
    private Date newDate;

    public ChangeRecordAsyncTask(int taskId, Callbacks callbacks, int playingFieldId,
                                 Date oldDate, Date newDate) {
        super(taskId, callbacks);
        this.playingFieldId = playingFieldId;
        this.oldDate = oldDate;
        this.newDate = newDate;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.updateRecord(playingFieldId, oldDate, newDate);
    }
}
