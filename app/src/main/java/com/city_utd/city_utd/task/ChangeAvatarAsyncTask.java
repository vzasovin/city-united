package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.io.File;
import java.sql.SQLException;

/**
 * Created by Admin on 29.07.2017.
 */

public class ChangeAvatarAsyncTask extends AbstractAsyncTask<Void, Void, UserDetailed>{

    private final File file;

    public ChangeAvatarAsyncTask(int taskId, Callbacks callbacks, File file) {
        super(taskId, callbacks);
        this.file = file;
    }

    public ChangeAvatarAsyncTask(int taskId, Callbacks callbacks, String filePath) {
        this(taskId, callbacks, new File(filePath));
    }

    @Override
    protected UserDetailed onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        UserDetailed user = apiManager.updateAvatar(file);
        if(user != null){
            try {
                saveUserInfo(user);
            }catch (SQLException e){
                throw new RuntimeSQLException(e);
            }
        }
        return user;
    }

    private void saveUserInfo(UserDetailed user) throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        UserDetailedDao dao = helper.getUserDetailedDao();
        dao.update(user);
    }
}
