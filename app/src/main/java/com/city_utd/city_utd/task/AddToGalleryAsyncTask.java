package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.io.File;
import java.util.Collections;
import java.util.List;

/**
 * Created by Admin on 30.07.2017.
 */

public class AddToGalleryAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final List<File> files;

    public AddToGalleryAsyncTask(int taskId, Callbacks callbacks, List<File> files) {
        super(taskId, callbacks);
        this.files = files;
    }

    public AddToGalleryAsyncTask(int taskId, Callbacks callbacks, File file) {
        this(taskId, callbacks, Collections.singletonList(file));
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.addToGallery(files);
    }
}
