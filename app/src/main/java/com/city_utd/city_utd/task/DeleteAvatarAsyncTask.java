package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.sql.SQLException;

/**
 * Created by Admin on 29.07.2017.
 */

public class DeleteAvatarAsyncTask extends AbstractAsyncTask<Void, Void, UserDetailed> {

    public DeleteAvatarAsyncTask(int taskId, Callbacks callbacks) {
        super(taskId, callbacks);
    }

    @Override
    protected UserDetailed onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        UserDetailed user = apiManager.clearAvatar();
        if(user != null){
            try {
                saveUserInfo(user);
            }catch (SQLException e){
                throw new RuntimeSQLException(e);
            }
        }
        return user;
    }

    private void saveUserInfo(UserDetailed user) throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        UserDetailedDao dao = helper.getUserDetailedDao();
        dao.update(user);
    }
}
