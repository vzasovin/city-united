package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.util.List;

/**
 * Created by Admin on 01.08.2017.
 */

public class RemoveFromGalleryAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final List<String> filesNames;

    public RemoveFromGalleryAsyncTask(int taskId, Callbacks callbacks, List<String> filesNames) {
        super(taskId, callbacks);
        this.filesNames = filesNames;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.removeFromGallery(filesNames);
    }
}
