package com.city_utd.city_utd.task;

import android.content.Context;
import android.text.TextUtils;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.KnownStatusCodes;
import com.city_utd.city_utd.client.PlayingFieldData;
import com.city_utd.city_utd.model.PlayingField;
import com.imogene.android.carcase.exception.RequestException;

import java.io.File;

/**
 * Created by Admin on 04.08.2017.
 */

public class UpdatePlayingFieldAsyncTask extends AbstractAsyncTask<Void, Void, PlayingField> {

    private final int playingFieldId;
    private final PlayingFieldData playingFieldData;
    private final String imageFilePath;
    private final boolean clearImage;

    public UpdatePlayingFieldAsyncTask(int taskId, Callbacks callbacks,
                                       int playingFieldId, PlayingFieldData playingFieldData,
                                       String imageFilePath, boolean clearImage) {
        super(taskId, callbacks, SUGGESTED_MIN_DURATION);
        this.playingFieldId = playingFieldId;
        this.playingFieldData = playingFieldData;
        this.imageFilePath = imageFilePath;
        this.clearImage = clearImage;
    }

    @Override
    protected PlayingField onExecuteOnNetwork(Void[] params) throws RequestException {
        File imageFile = null;
        if(!TextUtils.isEmpty(imageFilePath)){
            imageFile = new File(imageFilePath);
        }
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.updatePlayingField(playingFieldId, playingFieldData, imageFile, clearImage);
    }

    @Override
    protected String createErrorMessage(int errorCode) {
        switch (errorCode){
            case KnownStatusCodes.BAD_REQUEST:
                double latitude = playingFieldData.getLatitude();
                double longitude = playingFieldData.getLongitude();
                boolean hasCoordinates = latitude != 0 && longitude != 0;

                Context context = getContext();
                if(!hasCoordinates){
                    return context.getString(R.string.could_not_determine_coordinates_by_address);
                }else {
                    return context.getString(R.string.could_not_determine_address_by_coordinates);
                }
            default:
                return super.createErrorMessage(errorCode);
        }
    }
}
