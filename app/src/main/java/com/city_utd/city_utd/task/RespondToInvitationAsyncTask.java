package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

import java.util.Date;

/**
 * Created by Admin on 26.07.2017.
 */

public class RespondToInvitationAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final boolean accept;
    private final int sourceId;
    private final int playingFieldId;
    private final Date date;

    public RespondToInvitationAsyncTask(int taskId, Callbacks callbacks, boolean accept,
                                        int sourceId, int playingFieldId, Date date) {
        super(taskId, callbacks);
        this.accept = accept;
        this.sourceId = sourceId;
        this.playingFieldId = playingFieldId;
        this.date = date;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        if(accept){
            return apiManager.acceptInvitation(playingFieldId, date, sourceId);
        }
        return apiManager.rejectInvitation(sourceId, playingFieldId, date);
    }
}
