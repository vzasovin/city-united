package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.KnownStatusCodes;
import com.imogene.android.carcase.exception.RequestException;

import java.util.Date;

/**
 * Created by Admin on 02.08.2017.
 */

public class InviteUserAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final int userId;
    private final int fieldId;
    private final Date date;

    public InviteUserAsyncTask(int taskId, Callbacks callbacks, int userId, int filedId, Date date) {
        super(taskId, callbacks);
        this.userId = userId;
        this.fieldId = filedId;
        this.date = date;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.invite(userId, fieldId, date, null);
    }

    @Override
    protected String createErrorMessage(int errorCode) {
        if(errorCode == KnownStatusCodes.ALREADY_IN_SCHEDULE){
            Context context = getContext();
            return context.getString(R.string.user_is_already_in_the_schedule);
        }else {
            return super.createErrorMessage(errorCode);
        }
    }
}
