package com.city_utd.city_utd.task;

import android.content.Context;
import android.text.TextUtils;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.ProfileData;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.sql.SQLException;

/**
 * Created by Admin on 01.08.2017.
 */

public class UpdateProfileAsyncTask extends AbstractAsyncTask<Void, Void, UserDetailed> {

    private final ProfileData profileData;
    private final String avatarFilePath;
    private final boolean clearAvatar;

    public UpdateProfileAsyncTask(int taskId, Callbacks callbacks, ProfileData profileData,
                                  String avatarFilePath, boolean clearAvatar) {
        super(taskId, callbacks, SUGGESTED_MIN_DURATION);
        this.profileData = profileData;
        this.avatarFilePath = avatarFilePath;
        this.clearAvatar = clearAvatar;
    }

    @Override
    protected UserDetailed onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        UserDetailed user;
        if(!TextUtils.isEmpty(avatarFilePath)){
            user = apiManager.updateProfile(profileData, avatarFilePath);
        }else {
            user = apiManager.updateProfile(profileData, clearAvatar);
        }

        if(user != null){
            try {
                UserDetailedDao dao = getUserDetailedDao();
                dao.createOrUpdate(user);
            }catch (SQLException e){
                throw new RuntimeSQLException(e);
            }
        }

        return user;
    }

    private UserDetailedDao getUserDetailedDao() throws SQLException{
        Context context = getContext();
        DatabaseHelper helper = DatabaseHelper.getInstance(context);
        return helper.getUserDetailedDao();
    }
}
