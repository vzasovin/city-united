package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Admin on 19.07.2017.
 */

public class SubscribeOnUserAsyncTask extends AbstractAsyncTask<Void, Void, UserDetailed> {

    private final int userId;
    private final boolean subscribe;

    public SubscribeOnUserAsyncTask(int taskId, Callbacks callbacks, int userId, boolean subscribe) {
        super(taskId, callbacks);
        this.userId = userId;
        this.subscribe = subscribe;
    }

    @Override
    protected UserDetailed onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        UserDetailed result;
        try {
            if(subscribe){
                result = apiManager.subscribeOnUser(userId);
            }else {
                result = apiManager.unsubscribeFromUser(userId);
            }
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }

        if(result != null){
            try {
                Context context = getContext();
                DatabaseHelper helper = DatabaseHelper.getInstance(context);
                UserDetailedDao dao = helper.getUserDetailedDao();
                dao.createOrUpdate(result);
            }catch (SQLException e){
                throw new RuntimeSQLException(e);
            }
        }

        return result;
    }
}
