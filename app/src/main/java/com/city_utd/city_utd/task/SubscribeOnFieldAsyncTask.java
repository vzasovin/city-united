package com.city_utd.city_utd.task;

import android.content.Context;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.city_utd.city_utd.orm.dao.PlayingFieldDetailedDao;
import com.imogene.android.carcase.exception.RequestException;
import com.imogene.android.carcase.exception.RuntimeSQLException;

import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Admin on 17.07.2017.
 */

public class SubscribeOnFieldAsyncTask extends AbstractAsyncTask<Void, Void, PlayingFieldDetailed> {

    private final int fieldId;
    private final boolean subscribe;

    public SubscribeOnFieldAsyncTask(int taskId, Callbacks callbacks, int fieldId, boolean subscribe) {
        super(taskId, callbacks, 0);
        this.fieldId = fieldId;
        this.subscribe = subscribe;
    }

    @Override
    protected PlayingFieldDetailed onExecuteOnNetwork(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        PlayingFieldDetailed result;
        try {
            if(subscribe){
                result = apiManager.subscribeOnField(fieldId);
            }else {
                result = apiManager.unsubscribeFromField(fieldId);
            }
        }catch (IOException e){
            e.printStackTrace();
            return null;
        }

        if(result != null){
            try {
                Context context = getContext();
                DatabaseHelper helper = DatabaseHelper.getInstance(context);
                PlayingFieldDetailedDao dao = helper.getPlayingFieldDetailedDao();
                dao.createOrUpdate(result);
            }catch (SQLException e){
                throw new RuntimeSQLException(e);
            }
        }

        return result;
    }
}
