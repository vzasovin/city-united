package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.Rate;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 21.07.2017.
 */

public class RateUserAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final int userId;
    private final Rate rate;

    public RateUserAsyncTask(int taskId, Callbacks callbacks, int userId, Rate rate) {
        super(taskId, callbacks);
        this.userId = userId;
        this.rate = rate;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] voids) throws RequestException {
        return ApiManager.getInstance().rateUser(userId, rate);
    }
}
