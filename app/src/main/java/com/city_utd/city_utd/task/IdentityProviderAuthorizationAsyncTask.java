package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.AuthorizationResponse;
import com.city_utd.city_utd.model.UserDetailed;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 13.07.2017.
 */

public class IdentityProviderAuthorizationAsyncTask extends AuthorizationAsyncTask<String> {

    public IdentityProviderAuthorizationAsyncTask(int taskId, GenericCallbacks<Void, UserDetailed> callbacks) {
        super(taskId, callbacks);
    }

    @Override
    protected AuthorizationResponse authorize(String[] params) throws RequestException {
        String identityProvider = params[0];
        String accessToken = params[1];
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.authorize(identityProvider, accessToken);
    }
}
