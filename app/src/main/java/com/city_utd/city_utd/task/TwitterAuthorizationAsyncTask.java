package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.AuthorizationResponse;
import com.city_utd.city_utd.model.UserDetailed;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 28.07.2017.
 */

public class TwitterAuthorizationAsyncTask extends AuthorizationAsyncTask<Void> {

    private final long twitterId;
    private final String userName;

    public TwitterAuthorizationAsyncTask(int taskId, GenericCallbacks<Void, UserDetailed> callbacks,
                                         long twitterId, String userName) {
        super(taskId, callbacks);
        this.twitterId = twitterId;
        this.userName = userName;
    }

    @Override
    protected AuthorizationResponse authorize(Void[] params) throws RequestException {
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.authorizeViaTwitter(twitterId, userName);
    }
}
