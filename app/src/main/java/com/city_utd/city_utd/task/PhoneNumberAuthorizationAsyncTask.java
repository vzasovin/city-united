package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.city_utd.city_utd.client.AuthorizationResponse;
import com.city_utd.city_utd.model.UserDetailed;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 07.08.2017.
 */

public class PhoneNumberAuthorizationAsyncTask extends AuthorizationAsyncTask<String> {

    public PhoneNumberAuthorizationAsyncTask(int taskId, GenericCallbacks<Void, UserDetailed> callbacks) {
        super(taskId, callbacks);
    }

    @Override
    protected AuthorizationResponse authorize(String[] params) throws RequestException {
        String phoneNumber = params[0];
        ApiManager apiManager = ApiManager.getInstance();
        return apiManager.authorizeViaPhoneNumber(phoneNumber);
    }
}
