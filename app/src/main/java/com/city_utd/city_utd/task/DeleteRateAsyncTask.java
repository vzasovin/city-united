package com.city_utd.city_utd.task;

import com.city_utd.city_utd.client.ApiManager;
import com.imogene.android.carcase.exception.RequestException;

/**
 * Created by Admin on 21.07.2017.
 */

public class DeleteRateAsyncTask extends AbstractAsyncTask<Void, Void, Boolean> {

    private final int userId;

    public DeleteRateAsyncTask(int taskId, Callbacks callbacks, int userId) {
        super(taskId, callbacks);
        this.userId = userId;
    }

    @Override
    protected Boolean onExecuteOnNetwork(Void[] voids) throws RequestException {
        return ApiManager.getInstance().deleteUserRate(userId);
    }
}
