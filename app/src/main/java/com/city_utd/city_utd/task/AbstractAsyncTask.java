package com.city_utd.city_utd.task;

import android.content.Context;
import android.support.annotation.StringRes;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.client.KnownStatusCodes;
import com.imogene.android.carcase.worker.task.NetworkAsyncTask;

import java.io.IOException;
import java.net.SocketTimeoutException;

/**
 * Created by Admin on 13.07.2017.
 */

public abstract class AbstractAsyncTask<Param, Progress, Result> extends NetworkAsyncTask<Param, Progress, Result> {

    protected static final long SUGGESTED_MIN_DURATION = 1000;

    public AbstractAsyncTask(int taskId, Callbacks callbacks, long minDuration) {
        super(taskId, callbacks, minDuration);
    }

    public AbstractAsyncTask(int taskId, Callbacks callbacks) {
        super(taskId, callbacks);
    }

    public AbstractAsyncTask(int taskId) {
        super(taskId);
    }

    public AbstractAsyncTask(int taskId, GenericCallbacks<Progress, Result> callbacks, long minDuration) {
        super(taskId, callbacks, minDuration);
    }

    public AbstractAsyncTask(int taskId, GenericCallbacks<Progress, Result> callbacks) {
        super(taskId, callbacks);
    }

    @Override
    protected String createErrorMessage(IOException cause) {
        cause.printStackTrace();

        try {
            throw cause;
        } catch (SocketTimeoutException e) {
            return getString(R.string.server_not_responses);
        } catch (IOException e){
            return getString(R.string.server_connection_error);
        }
    }

    protected final String getString(@StringRes int stringRes, Object... args){
        Context context = getContext();
        return context.getString(stringRes, args);
    }

    @Override
    protected String createErrorMessage(int errorCode) {
        switch (errorCode){
            case KnownStatusCodes.BAD_REQUEST:
                return getString(R.string.bad_request);
            case KnownStatusCodes.UNAUTHORIZED:
                return getString(R.string.unauthorized);
            case KnownStatusCodes.NOT_FOUND:
                return getString(R.string.resource_not_found);
            case KnownStatusCodes.INTERNAL_SERVER_ERROR:
                return getString(R.string.internal_server_error);
            default:
                return getString(R.string.unknown_error_code, errorCode);
        }
    }
}
