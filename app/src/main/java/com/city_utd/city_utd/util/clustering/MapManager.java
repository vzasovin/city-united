package com.city_utd.city_utd.util.clustering;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.VisibleRegion;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;

import java.util.Collection;

/**
 * Created by Admin on 10.08.2017.
 */

public class MapManager<T extends ClusterItem>
        implements GoogleMap.OnCameraIdleListener, GoogleMap.OnCameraMoveListener{

    private final ClusterManager<T> clusterManager;
    private final GoogleMap map;

    private double latitude;
    private double longitude;
    private boolean notifyListenerOnIdle;
    private OnTriggerReloadListener listener;

    public MapManager(ClusterManager<T> clusterManager, GoogleMap map){
        this.clusterManager = clusterManager;
        this.map = map;
    }

    public void addItem(T item){
        clusterManager.addItem(item);
    }

    public void cluster(){
        clusterManager.cluster();
    }

    public void clearItems(){
        clusterManager.clearItems();
    }

    public Collection<T> getItems(){
        return clusterManager.getAlgorithm().getItems();
    }

    @Override
    public void onCameraIdle() {
        clusterManager.onCameraIdle();

        if(notifyListenerOnIdle){
            notifyListener();
            notifyListenerOnIdle = false;
        }
    }

    private void notifyListener(){
        if(listener != null){
            listener.onTriggerLoad();
        }
    }

    @Override
    public void onCameraMove() {
        Projection projection = map.getProjection();
        VisibleRegion region = projection.getVisibleRegion();
        LatLng position = region.nearRight;
        double newLatitude = position.latitude;
        double newLongitude = position.longitude;

        if(latitude == 0 || longitude == 0){
            latitude = newLatitude;
            longitude = newLongitude;
            return;
        }

        double offset = Math.pow(latitude - newLatitude, 2) + Math.pow(longitude - newLongitude, 2);
        if(offset > 0.00003){
            latitude = newLatitude;
            longitude = newLongitude;
            notifyListenerOnIdle = true;
        }
    }

    public void setOnTriggerReloadListener(OnTriggerReloadListener listener){
        this.listener = listener;
    }

    public interface OnTriggerReloadListener{

        void onTriggerLoad();
    }
}
