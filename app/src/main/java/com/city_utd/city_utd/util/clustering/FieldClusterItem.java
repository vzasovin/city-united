package com.city_utd.city_utd.util.clustering;

import com.city_utd.city_utd.model.PlayingField;

/**
 * Created by Admin on 06.08.2017.
 */

public class FieldClusterItem extends SimpleClusterItem {

    private final PlayingField playingField;

    public FieldClusterItem(PlayingField field) {
        super(field.getCoordinates());
        playingField = field;
    }

    public final PlayingField getPlayingField(){
        return playingField;
    }
}
