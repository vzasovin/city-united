package com.city_utd.city_utd.util;

import android.view.animation.Animation;

/**
 * Created by Admin on 02.08.2017.
 */

public class AnimationListenerAdapter implements Animation.AnimationListener {

    @Override
    public void onAnimationStart(Animation animation) {}

    @Override
    public void onAnimationEnd(Animation animation) {}

    @Override
    public void onAnimationRepeat(Animation animation) {}
}
