package com.city_utd.city_utd.util.clustering;

import android.content.Context;
import android.support.annotation.DrawableRes;

import com.city_utd.city_utd.R;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by Admin on 06.08.2017.
 */

public class FieldsClusterRenderer extends DefaultClusterRenderer<FieldClusterItem> {

    public FieldsClusterRenderer(Context context, GoogleMap map, ClusterManager<FieldClusterItem> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(FieldClusterItem item, MarkerOptions markerOptions) {
        @DrawableRes int iconRes = R.drawable.ic_playing_field_inclined_36dp;
        markerOptions.icon(BitmapDescriptorFactory.fromResource(iconRes));
    }
}
