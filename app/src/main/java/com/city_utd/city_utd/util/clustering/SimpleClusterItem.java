package com.city_utd.city_utd.util.clustering;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by Admin on 06.08.2017.
 */

public class SimpleClusterItem implements ClusterItem {

    private final LatLng position;

    public SimpleClusterItem(LatLng position){
        this.position = position;
    }

    @Override
    public LatLng getPosition() {
        return position;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }
}
