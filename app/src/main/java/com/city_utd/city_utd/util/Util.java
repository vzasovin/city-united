package com.city_utd.city_utd.util;

import android.content.Context;
import android.text.TextUtils;
import android.webkit.MimeTypeMap;

import com.city_utd.city_utd.R;
import com.city_utd.city_utd.exception.RuntimeParseException;
import com.city_utd.city_utd.model.PlayerLines;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Admin on 05.07.2017.
 */

public final class Util {

    private Util(){}

    public static final class Dates{

        private Dates(){}

        public static final String SERVER_SIDE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZ";
        public static final String TIME_FORMAT = "HH:mm";
        public static final String DATE_TIME_FORMAT_1 = "EEE dd MMM HH:mm";
        public static final String DATE_TIME_FORMAT_2 = "dd MMM HH:mm";
        public static final String DATE_FORMAT_1 = "dd.MM.yyyy";

        public static final int DAY_LABEL_EARLIER = -2;
        public static final int DAY_LABEL_YESTERDAY = -1;
        public static final int DAY_LABEL_TODAY = 1;
        public static final int DAY_LABEL_TOMORROW = 2;
        public static final int DAY_LABEL_LATER = 3;

        private static final SimpleDateFormat DATE_FORMAT;

        static {
            DATE_FORMAT = (SimpleDateFormat) DateFormat.getDateTimeInstance();
            DATE_FORMAT.applyPattern(SERVER_SIDE_FORMAT);
        }

        public static String dateToString(Date date, String format){
            synchronized (DATE_FORMAT){
                DATE_FORMAT.applyPattern(format);
                return DATE_FORMAT.format(date);
            }
        }

        public static String dateToServerString(Date date){
            String result = dateToString(date, SERVER_SIDE_FORMAT);
            StringBuilder sb = new StringBuilder(result);
            sb.insert(sb.length() - 2, ':');
            return sb.toString();
        }

        public static String dateToTimeString(Date date){
            return dateToString(date, TIME_FORMAT);
        }

        public static Date stringToDate(String s, String format){
            s = s.replaceAll("Z$", "+00");
            synchronized (DATE_FORMAT){
                DATE_FORMAT.applyPattern(format);
                try {
                    return DATE_FORMAT.parse(s);
                } catch (ParseException e) {
                    throw new RuntimeParseException(e);
                }
            }
        }

        public static Date serverStringToDate(String s){
            return stringToDate(s, SERVER_SIDE_FORMAT);
        }

        public static int getDayLabel(Date date){
            Calendar instance = Calendar.getInstance();
            instance.setTime(date);

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            if(instance.before(calendar)){
                calendar.add(Calendar.DATE, -1);
                return instance.before(calendar) ? DAY_LABEL_EARLIER : DAY_LABEL_YESTERDAY;
            }

            calendar.add(Calendar.DATE, 1);
            if(instance.before(calendar)){
                return DAY_LABEL_TODAY;
            }else {
                calendar.add(Calendar.DATE, 1);
                return instance.before(calendar) ? DAY_LABEL_TOMORROW : DAY_LABEL_LATER;
            }
        }
    }

    public static final class Commons{

        private Commons(){}

        public static String extractMimeType(File file){
            String name = file.getName();
            String extension = name.substring(name.lastIndexOf('.') + 1);
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            return mimeTypeMap.getMimeTypeFromExtension(extension);
        }

        public static String getLineString(Context context, String line){
            String defaultValue = context.getString(R.string.line_is_not_specified);
            return getLineString(context, line, defaultValue);
        }

        public static String getLineString(Context context, String line, String defaultValue){
            if(!TextUtils.isEmpty(line)){
                switch (line){
                    case PlayerLines.GOALKEEPER:
                        return context.getString(R.string.goalkeeper);
                    case PlayerLines.DEFENDER:
                        return context.getString(R.string.defender);
                    case PlayerLines.MIDFIELDER:
                        return context.getString(R.string.midfielder);
                    case PlayerLines.FORWARD:
                        return context.getString(R.string.forward);
                    default:
                        return defaultValue;
                }
            }else {
                return defaultValue;
            }
        }
    }

    public static final class TypeAdapter{

        private TypeAdapter(){}

        public static int nextInt(JsonReader in) throws IOException {
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return  0;
            }else {
                return in.nextInt();
            }
        }

        public static double nextDouble(JsonReader in) throws IOException {
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return  0;
            }else {
                return in.nextDouble();
            }
        }

        public static String nextString(JsonReader in) throws IOException{
            if(in.peek() == JsonToken.NULL){
                in.nextNull();
                return  null;
            }else {
                return in.nextString();
            }
        }

        public static Date nextDate(JsonReader in) throws IOException{
            String s = nextString(in);

            if(TextUtils.isEmpty(s)){
                return null;
            }

            try {
                return Dates.serverStringToDate(s);
            }catch (RuntimeParseException e){
                e.printStackTrace();
                return null;
            }
        }

        public static float nextFloat(JsonReader in) throws IOException{
            return (float) nextDouble(in);
        }
    }

    public static final class Storage{

        public static final String DIRECTORY_TEMP = "com.city_utd.city_utd.temp_files";

        private Storage(){}

        public static File getTempFilesDir(Context context) throws IOException{
            File root = context.getCacheDir();
            File dir = new File(root, DIRECTORY_TEMP);
            if(dir.mkdirs() || dir.isDirectory()){
                return dir;
            }else {
                throw new IOException("Could not create a dir");
            }
        }

        public static File createTempFile(Context context, String mimeType) throws IOException{
            File dir = getTempFilesDir(context);
            Date date = new Date();
            String dateString = Dates.dateToString(date, "yyyyMMdd_HHmmss");
            String fileName = "temp_" + dateString + "_";
            MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
            String extension = mimeTypeMap.getExtensionFromMimeType(mimeType);
            String suffix = '.' + extension;
            return File.createTempFile(fileName, suffix, dir);
        }
    }
}
