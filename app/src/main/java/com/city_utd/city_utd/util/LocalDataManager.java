package com.city_utd.city_utd.util;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;

import com.city_utd.city_utd.controller.CityUtdApplication;
import com.google.android.gms.maps.model.LatLng;
import com.imogene.android.carcase.AppUtils;

/**
 * Created by Admin on 05.07.2017.
 */

public class LocalDataManager {

    private static final String PREFS_FILE_NAME = "city_utd_prefs";

    private static final String KEY_TOK = "jwt_token";
    private static final String KEY_UID = "user_id";
    private static final String KEY_LAT = "latitude";
    private static final String KEY_LON = "longitude";
    private static final String KEY_FLF = "first_launch_flag";
    private static final String KEY_SPE = "show_fine_location_permission_explanation_on_launch";
    private static final String KEY_CRT = "client_registration_token";
    private static final String KEY_UIC = "unread_incoming_invitations_count";
    private static final String KEY_SSE = "show_self_estimation_dialog";

    private static volatile LocalDataManager instance;
    private final SharedPreferences prefs;

    public static LocalDataManager getInstance(Context context){
        LocalDataManager localRef = instance;
        if(localRef == null){
            synchronized (LocalDataManager.class){
                localRef = instance;
                if(localRef == null){
                    localRef = instance = new LocalDataManager(context);
                }
            }
        }
        return localRef;
    }

    private LocalDataManager(Context context){
        prefs = context.getSharedPreferences(PREFS_FILE_NAME, Context.MODE_PRIVATE);
    }

    public String getJwtToken(){
        return prefs.getString(KEY_TOK, null);
    }

    public boolean isAuthorized(){
        return !TextUtils.isEmpty(getJwtToken());
    }

    public LocalDataManager setJwtToken(String token){
        prefs.edit().putString(KEY_TOK, token).apply();
        return this;
    }

    public int getUserId(){
        return prefs.getInt(KEY_UID, 0);
    }

    public LocalDataManager setUserId(int id){
        prefs.edit().putInt(KEY_UID, id).apply();
        return this;
    }

    public LatLng getLastLocation(){
        double latitude = prefs.getFloat(KEY_LAT, 0);
        double longitude = prefs.getFloat(KEY_LON, 0);
        if(latitude == 0 || longitude == 0){
            return null;
        }else {
            return new LatLng(latitude, longitude);
        }
    }

    public LocalDataManager setLastLocation(LatLng location){
        float latitude = (float) location.latitude;
        float longitude = (float) location.longitude;
        prefs.edit().putFloat(KEY_LAT, latitude).putFloat(KEY_LON, longitude).apply();
        return this;
    }

    public boolean getFistLaunchFlag(){
        return prefs.getBoolean(KEY_FLF, true);
    }

    public LocalDataManager setFirstLaunchFlag(boolean flag){
        prefs.edit().putBoolean(KEY_FLF, flag).apply();
        return this;
    }

    public boolean getShowFineLocationPermissionExplanationOnLaunchFlag(){
        return prefs.getBoolean(KEY_SPE, true);
    }

    public LocalDataManager setShowFineLocationPermissionExplanationOnLaunchFlag(boolean flag){
        prefs.edit().putBoolean(KEY_SPE, flag).apply();
        return this;
    }

    public boolean needsShowFineLocationPermissionExplanationOnLaunch(){
        if(AppUtils.Commons.checkApiLevel(Build.VERSION_CODES.M)){
            Context context = CityUtdApplication.getAppContext();
            String permission = Manifest.permission.ACCESS_FINE_LOCATION;
            if(!AppUtils.Permissions.checkPermission(context, permission)){
                return isAuthorized() && getShowFineLocationPermissionExplanationOnLaunchFlag();
            }
        }
        return false;
    }

    public LocalDataManager setClientToken(String token){
        prefs.edit().putString(KEY_CRT, token).apply();
        return this;
    }

    public String getClientToken(){
        return prefs.getString(KEY_CRT, null);
    }

    public LocalDataManager addUnreadIncomingInvitation(){
        int count = prefs.getInt(KEY_UIC, 0);
        prefs.edit().putInt(KEY_UIC, ++count).apply();
        return this;
    }

    public int getUnreadIncomingInvitationsCount(){
        return prefs.getInt(KEY_UIC, 0);
    }

    public LocalDataManager resetUnreadIncomingInvitationsCount(){
        prefs.edit().putInt(KEY_UIC, 0).apply();
        return this;
    }

    public boolean getShowSelfEstimationFlag(){
        return prefs.getBoolean(KEY_SSE, false);
    }

    public LocalDataManager setShowSelfEstimationFlag(boolean flag){
        prefs.edit().putBoolean(KEY_SSE, flag).apply();
        return this;
    }
}
