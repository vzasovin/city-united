package com.city_utd.city_utd.orm.dao;

import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 07.07.2017.
 */

public class ScheduleItemDao extends BaseDaoImpl<ScheduleItem, Integer> {

    public ScheduleItemDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, ScheduleItem.class);
    }

    public List<ScheduleItem> getSchedule(PlayingFieldDetailed parent, boolean excludeOutdated, Date maxDate) throws SQLException{
        Date min = null;
        if(excludeOutdated){
            min = new Date();
        }
        return getSchedule(parent, min, maxDate);
    }

    public List<ScheduleItem> getSchedule(PlayingFieldDetailed parent, Date min, Date max) throws SQLException{
        QueryBuilder<ScheduleItem, Integer> queryBuilder = queryBuilder();
        Where<ScheduleItem, Integer> where = queryBuilder.where();
        where.eq(ScheduleItem.FIELD_PLAYING_FIELD, parent);
        if(min != null && max != null){
            where.and().between(ScheduleItem.FIELD_DATE, min, max);
        }else if(min != null){
            where.and().ge(ScheduleItem.FIELD_DATE, min);
        }else if(max != null){
            where.and().le(ScheduleItem.FIELD_DATE, max);
        }
        return where.query();
    }

    public void clearSchedule(PlayingFieldDetailed parent) throws SQLException{
        DeleteBuilder<ScheduleItem, Integer> deleteBuilder = deleteBuilder();
        Where<ScheduleItem, Integer> where = deleteBuilder.where();
        where.eq(ScheduleItem.FIELD_PLAYING_FIELD, parent);
        deleteBuilder.delete();
    }

    public void replaceSchedule(PlayingFieldDetailed parent, List<ScheduleItem> items) throws SQLException{
        clearSchedule(parent);
        for(ScheduleItem item : items){
            item.setPlayingField(parent);
            create(item);
        }
    }
}
