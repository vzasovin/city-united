package com.city_utd.city_utd.orm.dao;

import android.content.Context;

import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.model.StatedRates;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collection;

/**
 * Created by Admin on 07.07.2017.
 */

public class UserDetailedDao extends BaseDaoImpl<UserDetailed, Integer> {

    public UserDetailedDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, UserDetailed.class);
    }

    @Override
    public int create(UserDetailed data) throws SQLException {
        createOrUpdateBase(data);
        createOrUpdateStatedRates(data);
        return super.create(data);
    }

    private void createOrUpdateBase(UserDetailed data) throws SQLException{
        User base = data.getBase();
        base.setBase(true);
        getUserDao().createOrUpdate(base);
    }

    private UserDao getUserDao() throws SQLException{
        return getHelper().getUserDao();
    }

    private DatabaseHelper getHelper() {
        Context context = CityUtdApplication.getAppContext();
        return DatabaseHelper.getInstance(context);
    }

    private void createOrUpdateStatedRates(UserDetailed data) throws SQLException{
        StatedRates rates = data.getStatedRates();
        getStatedRatesDao().createOrUpdate(rates);
    }

    private StatedRatesDao getStatedRatesDao() throws SQLException{
        return getHelper().getStatedRatesDao();
    }

    @Override
    public int create(Collection<UserDetailed> datas) throws SQLException {
        for(UserDetailed data : datas){
            createOrUpdateBase(data);
            createOrUpdateStatedRates(data);
        }
        return super.create(datas);
    }

    @Override
    public int update(UserDetailed data) throws SQLException {
        createOrUpdateBase(data);
        createOrUpdateStatedRates(data);
        return super.update(data);
    }

    @Override
    public CreateOrUpdateStatus createOrUpdate(UserDetailed data) throws SQLException {
        return super.createOrUpdate(data);
    }
}
