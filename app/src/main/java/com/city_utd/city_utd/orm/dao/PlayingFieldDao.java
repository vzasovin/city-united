package com.city_utd.city_utd.orm.dao;

import com.city_utd.city_utd.model.PlayingField;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 06.07.2017.
 */

public class PlayingFieldDao extends BaseDaoImpl<PlayingField, Integer> {

    private boolean forceUpdateNearest = false;

    public PlayingFieldDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, PlayingField.class);
    }

    public List<PlayingField> getSubscriptions() throws SQLException{
        QueryBuilder<PlayingField, Integer> queryBuilder = queryBuilder();
        Where<PlayingField, Integer> where = queryBuilder.where();
        where.eq(PlayingField.FIELD_IS_SUBSCRIPTION, true);
        return where.query();
    }

    @SuppressWarnings("unchecked")
    public void clearSubscriptions() throws SQLException{
        DeleteBuilder<PlayingField, Integer> deleteBuilder = deleteBuilder();
        Where<PlayingField, Integer> deleteWhere = deleteBuilder.where();
        deleteWhere.eq(PlayingField.FIELD_IS_SUBSCRIPTION, true);
        deleteWhere.and().not().eq(PlayingField.FIELD_IS_BASE, true);
        deleteWhere.and().not().eq(PlayingField.FIELD_IS_NEAREST, true);
        deleteBuilder.delete();

        UpdateBuilder<PlayingField, Integer> updateBuilder = updateBuilder();
        Where<PlayingField, Integer> updateWhere = updateBuilder.where();
        Where<PlayingField, Integer> first = updateWhere.eq(PlayingField.FIELD_IS_SUBSCRIPTION, true);
        Where<PlayingField, Integer> second = updateWhere.eq(PlayingField.FIELD_IS_BASE, true);
        second.or().eq(PlayingField.FIELD_IS_NEAREST, true);
        updateWhere.and(first, second);
        updateBuilder.updateColumnValue(PlayingField.FIELD_IS_SUBSCRIPTION, false);
        updateBuilder.update();
    }

    public void replaceSubscriptions(List<PlayingField> playingFields) throws SQLException{
        clearSubscriptions();
        for(PlayingField playingField : playingFields){
            playingField.setSubscription(true);
            createOrUpdate(playingField);
        }
    }

    @Override
    public int update(PlayingField data) throws SQLException {
        if(!forceUpdateNearest){
            int id = data.getId();
            PlayingField existing = queryForId(id);
            if(existing != null){
                data.setNearest(existing.isNearest());
                data.setDistance(existing.getDistance());
            }
        }
        return super.update(data);
    }

    public List<PlayingField> getNearest() throws SQLException{
        QueryBuilder<PlayingField, Integer> queryBuilder = queryBuilder();
        Where<PlayingField, Integer> where = queryBuilder.where();
        where.eq(PlayingField.FIELD_IS_NEAREST, true);
        where.and().gt(PlayingField.FIELD_DISTANCE, 0);
        return where.query();
    }

    @SuppressWarnings("unchecked")
    public void clearNearest() throws SQLException{
        DeleteBuilder<PlayingField, Integer> deleteBuilder = deleteBuilder();
        Where<PlayingField, Integer> deleteWhere = deleteBuilder.where();
        deleteWhere.eq(PlayingField.FIELD_IS_NEAREST, true);
        deleteWhere.and().not().eq(PlayingField.FIELD_IS_BASE, true);
        deleteWhere.and().not().eq(PlayingField.FIELD_IS_SUBSCRIPTION, true);
        deleteBuilder.delete();

        UpdateBuilder<PlayingField, Integer> updateBuilder = updateBuilder();
        Where<PlayingField, Integer> updateWhere = updateBuilder.where();
        Where<PlayingField, Integer> first = updateWhere.eq(PlayingField.FIELD_IS_NEAREST, true);
        Where<PlayingField, Integer> second = updateWhere.eq(PlayingField.FIELD_IS_BASE, true);
        second.or().eq(PlayingField.FIELD_IS_SUBSCRIPTION, true);
        updateWhere.and(first, second);
        updateBuilder.updateColumnValue(PlayingField.FIELD_IS_NEAREST, false);
        updateBuilder.updateColumnValue(PlayingField.FIELD_DISTANCE, 0);
        updateBuilder.update();
    }

    public void replaceNearest(List<PlayingField> playingFields) throws SQLException{
        clearNearest();
        forceUpdateNearest = true;
        for(PlayingField playingField : playingFields){
            playingField.setNearest(true);

            int id = playingField.getId();
            PlayingField existing = queryForId(id);
            if(existing != null){
                playingField.setSubscription(existing.isSubscription());
                update(playingField);
            }else {
                create(playingField);
            }
        }
        forceUpdateNearest = false;
    }
}
