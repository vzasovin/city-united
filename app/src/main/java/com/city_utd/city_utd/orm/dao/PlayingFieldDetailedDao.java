package com.city_utd.city_utd.orm.dao;

import android.content.Context;

import com.city_utd.city_utd.controller.CityUtdApplication;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.orm.DatabaseHelper;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Created by vadim on 7/9/17.
 */

public class PlayingFieldDetailedDao extends BaseDaoImpl<PlayingFieldDetailed, Integer> {

    public PlayingFieldDetailedDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, PlayingFieldDetailed.class);
    }

    @Override
    public int create(PlayingFieldDetailed data) throws SQLException {
        createOrUpdateBase(data);
        int result = super.create(data);
        createScheduleItems(data);
        return result;
    }

    private void createOrUpdateBase(PlayingFieldDetailed data) throws SQLException{
        PlayingField base = data.getBase();
        base.setBase(true);
        getPlayingFieldDao().createOrUpdate(base);
    }

    private PlayingFieldDao getPlayingFieldDao() throws SQLException{
        return getHelper().getPlayingFieldDao();
    }

    private DatabaseHelper getHelper(){
        Context context = CityUtdApplication.getAppContext();
        return DatabaseHelper.getInstance(context);
    }

    private void createScheduleItems(PlayingFieldDetailed data) throws SQLException{
        List<ScheduleItem> items = data.getSchedule();
        if(items != null && !items.isEmpty()){
            assignEmptyForeignCollection(data, PlayingFieldDetailed.FIELD_ITEMS);
            Collection<ScheduleItem> persistedItems = data.getPersistedSchedule();
            for(ScheduleItem item : items){
                item.setPlayingField(data);
                persistedItems.add(item);
            }
        }
    }

    @Override
    public int create(Collection<PlayingFieldDetailed> datas) throws SQLException {
        for(PlayingFieldDetailed data : datas){
            createOrUpdateBase(data);
        }
        int result = super.create(datas);
        for(PlayingFieldDetailed data : datas){
            createScheduleItems(data);
        }
        return result;
    }

    @Override
    public int update(PlayingFieldDetailed data) throws SQLException {
        createOrUpdateBase(data);
        int result = super.update(data);
        clearScheduleItems(data);
        createScheduleItems(data);
        return result;
    }

    private void clearScheduleItems(PlayingFieldDetailed data) throws SQLException{
        ScheduleItemDao dao = getScheduleItemDao();
        DeleteBuilder<ScheduleItem, Integer> deleteBuilder = dao.deleteBuilder();
        deleteBuilder.where().eq(ScheduleItem.FIELD_PLAYING_FIELD, data);
        deleteBuilder.delete();
    }

    private ScheduleItemDao getScheduleItemDao() throws SQLException{
        return getHelper().getScheduleItemDao();
    }

    @Override
    public CreateOrUpdateStatus createOrUpdate(PlayingFieldDetailed data) throws SQLException {
        return super.createOrUpdate(data);
    }
}
