package com.city_utd.city_utd.orm.dao;

import com.city_utd.city_utd.model.User;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by Admin on 06.07.2017.
 */

public class UserDao extends BaseDaoImpl<User, Integer> {

    private boolean forceUpdateBestPlayer = false;

    public UserDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, User.class);
    }

    public List<User> getSubscriptions() throws SQLException{
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        Where<User, Integer> where = queryBuilder.where();
        where.eq(User.FIELD_IS_SUBSCRIPTION, true);
        return where.query();
    }

    @SuppressWarnings("unchecked")
    public void clearSubscriptions() throws SQLException{
        DeleteBuilder<User, Integer> deleteBuilder = deleteBuilder();
        Where<User, Integer> deleteWhere = deleteBuilder.where();
        deleteWhere.eq(User.FIELD_IS_SUBSCRIPTION, true);
        deleteWhere.and().eq(User.FIELD_IS_BASE, false);
        deleteWhere.and().eq(User.FIELD_IS_BEST_PLAYER, false);
        deleteBuilder.delete();

        UpdateBuilder<User, Integer> updateBuilder = updateBuilder();
        Where<User, Integer> updateWhere = updateBuilder.where();
        Where<User, Integer> first = updateWhere.eq(User.FIELD_IS_SUBSCRIPTION, true);
        Where<User, Integer> second = updateWhere.eq(User.FIELD_IS_BASE, true);
        second.or().eq(User.FIELD_IS_BEST_PLAYER, true);
        updateWhere.and(first, second);
        updateBuilder.updateColumnValue(User.FIELD_IS_SUBSCRIPTION, false);
        updateBuilder.update();
    }

    public void replaceSubscriptions(List<User> users) throws SQLException{
        clearSubscriptions();
        for (User user : users){
            user.setSubscription(true);
            createOrUpdate(user);
        }
    }

    @Override
    public int update(User data) throws SQLException {
        if(!forceUpdateBestPlayer){
            int id = data.getId();
            User existing = queryForId(id);
            if(existing != null){
                data.setBestPlayer(existing.isBestPlayer());
            }
        }
        return super.update(data);

    }

    public User getBestPlayer() throws SQLException{
        QueryBuilder<User, Integer> queryBuilder = queryBuilder();
        Where<User, Integer> where = queryBuilder.where();
        where.eq(User.FIELD_IS_BEST_PLAYER, true);
        return where.queryForFirst();
    }

    public void replaceBestPlayer(User user) throws SQLException{
        forceUpdateBestPlayer = true;
        if(user == null){
            return;
        }
        User current = getBestPlayer();
        if(current != null){
            int id = current.getId();
            if(id != user.getId()){
                current.setBestPlayer(false);
                update(current);
            }else {
                user.setSubscription(current.isSubscription());
            }
        }
        user.setBestPlayer(true);
        createOrUpdate(user);
        forceUpdateBestPlayer = false;
    }
}
