package com.city_utd.city_utd.orm;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.city_utd.city_utd.model.Invitation;
import com.city_utd.city_utd.model.PlayingField;
import com.city_utd.city_utd.model.PlayingFieldDetailed;
import com.city_utd.city_utd.model.ScheduleItem;
import com.city_utd.city_utd.model.StatedRates;
import com.city_utd.city_utd.model.User;
import com.city_utd.city_utd.model.UserDetailed;
import com.city_utd.city_utd.orm.dao.InvitationDao;
import com.city_utd.city_utd.orm.dao.PlayingFieldDao;
import com.city_utd.city_utd.orm.dao.PlayingFieldDetailedDao;
import com.city_utd.city_utd.orm.dao.ScheduleItemDao;
import com.city_utd.city_utd.orm.dao.StatedRatesDao;
import com.city_utd.city_utd.orm.dao.UserDao;
import com.city_utd.city_utd.orm.dao.UserDetailedDao;
import com.imogene.android.carcase.exception.RuntimeSQLException;
import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by Admin on 06.07.2017.
 */

public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME = "city_utd_local_cache.db";
    private static final int CURRENT_VERSION = 1;

    private static DatabaseHelper instance;

    public static DatabaseHelper getInstance(Context context){
        DatabaseHelper localRef = instance;
        if(localRef == null){
            synchronized (DatabaseHelper.class){
                localRef = instance;
                if(localRef == null){
                    localRef = instance = OpenHelperManager
                            .getHelper(context, DatabaseHelper.class);
                }
            }
        }
        return localRef;
    }

    @SuppressWarnings("unused")
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, CURRENT_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, User.class);
            TableUtils.createTable(connectionSource, StatedRates.class);
            TableUtils.createTable(connectionSource, UserDetailed.class);
            TableUtils.createTable(connectionSource, PlayingField.class);
            TableUtils.createTable(connectionSource, ScheduleItem.class);
            TableUtils.createTable(connectionSource, PlayingFieldDetailed.class);
            TableUtils.createTable(connectionSource, Invitation.class);
        } catch (SQLException e) {
            throw new RuntimeSQLException(e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase database, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {}

    public UserDao getUserDao() throws SQLException{
        return getDao(User.class);
    }

    public UserDetailedDao getUserDetailedDao() throws SQLException{
        return getDao(UserDetailed.class);
    }

    public PlayingFieldDao getPlayingFieldDao() throws SQLException{
        return getDao(PlayingField.class);
    }

    public ScheduleItemDao getScheduleItemDao() throws SQLException{
        return getDao(ScheduleItem.class);
    }

    public PlayingFieldDetailedDao getPlayingFieldDetailedDao() throws SQLException{
        return getDao(PlayingFieldDetailed.class);
    }

    public InvitationDao getInvitationDao() throws SQLException{
        return getDao(Invitation.class);
    }

    public StatedRatesDao getStatedRatesDao() throws SQLException{
        return getDao(StatedRates.class);
    }
}
