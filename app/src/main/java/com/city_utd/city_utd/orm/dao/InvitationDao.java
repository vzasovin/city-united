package com.city_utd.city_utd.orm.dao;

import com.city_utd.city_utd.model.Invitation;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.Where;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by vadim on 7/9/17.
 */

public class InvitationDao extends BaseDaoImpl<Invitation, Integer> {

    public InvitationDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, Invitation.class);
    }

    public List<Invitation> getIncomingInvitations() throws SQLException{
        return getInvitations(true, true);
    }

    public List<Invitation> getAllIncomingInvitations() throws SQLException{
        return getInvitations(true, false);
    }

    public List<Invitation> getOutgoingInvitations() throws SQLException{
        return getInvitations(false, true);
    }

    public List<Invitation> getAllOutgoingInvitations() throws SQLException{
        return getInvitations(false, false);
    }

    private List<Invitation> getInvitations(boolean incoming, boolean excludeOutdated) throws SQLException{
        QueryBuilder<Invitation, Integer> queryBuilder = queryBuilder();
        Where<Invitation, Integer> where = queryBuilder.where();
        where.eq(Invitation.FIELD_INCOMING, incoming);
        if(excludeOutdated){
            where.and().ge(Invitation.FIELD_DATE, new Date());
        }
        queryBuilder.orderBy(Invitation.FIELD_DATE, true);
        return queryBuilder.query();
    }

    public void clearIncomingInvitations() throws SQLException{
        clearInvitations(true);
    }

    private void clearInvitations(boolean incoming) throws SQLException{
        DeleteBuilder<Invitation, Integer> deleteBuilder = deleteBuilder();
        Where<Invitation, Integer> where = deleteBuilder.where();
        where.eq(Invitation.FIELD_INCOMING, incoming);
        deleteBuilder.delete();
    }

    public void replaceIncomingInvitations(List<Invitation> invitations) throws SQLException{
        clearIncomingInvitations();
        for(Invitation invitation : invitations){
            invitation.setIncoming(true);
            create(invitation);
        }
    }

    public void clearOutgoingInvitations() throws SQLException{
        clearInvitations(false);
    }

    public void replaceOutgoingInvitations(List<Invitation> invitations) throws SQLException{
        clearOutgoingInvitations();
        for(Invitation invitation : invitations){
            invitation.setIncoming(false);
            create(invitation);
        }
    }
}
