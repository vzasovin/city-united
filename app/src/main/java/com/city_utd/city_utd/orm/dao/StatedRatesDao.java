package com.city_utd.city_utd.orm.dao;

import com.city_utd.city_utd.model.StatedRates;
import com.j256.ormlite.dao.BaseDaoImpl;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;

/**
 * Created by Admin on 20.07.2017.
 */

public class StatedRatesDao extends BaseDaoImpl<StatedRates, Integer> {

    public StatedRatesDao(ConnectionSource connectionSource) throws SQLException {
        super(connectionSource, StatedRates.class);
    }
}
